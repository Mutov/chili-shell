
/*
 *
 * More info at [www.dropzonejs.com](http://www.dropzonejs.com)
 *
 * Copyright (c) 2012, Matias Meno
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

(function() {
  var Dropzone, Emitter, camelize, contentLoaded, detectVerticalSquash, drawImageIOSFix, noop, without,
    __slice = [].slice,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  noop = function() {};

  Emitter = (function() {
    function Emitter() {}

    Emitter.prototype.addEventListener = Emitter.prototype.on;

    Emitter.prototype.on = function(event, fn) {
      this._callbacks = this._callbacks || {};
      if (!this._callbacks[event]) {
        this._callbacks[event] = [];
      }
      this._callbacks[event].push(fn);
      return this;
    };

    Emitter.prototype.emit = function() {
      var args, callback, callbacks, event, _i, _len;
      event = arguments[0], args = 2 <= arguments.length ? __slice.call(arguments, 1) : [];
      this._callbacks = this._callbacks || {};
      callbacks = this._callbacks[event];
      if (callbacks) {
        for (_i = 0, _len = callbacks.length; _i < _len; _i++) {
          callback = callbacks[_i];
          callback.apply(this, args);
        }
      }
      return this;
    };

    Emitter.prototype.removeListener = Emitter.prototype.off;

    Emitter.prototype.removeAllListeners = Emitter.prototype.off;

    Emitter.prototype.removeEventListener = Emitter.prototype.off;

    Emitter.prototype.off = function(event, fn) {
      var callback, callbacks, i, _i, _len;
      if (!this._callbacks || arguments.length === 0) {
        this._callbacks = {};
        return this;
      }
      callbacks = this._callbacks[event];
      if (!callbacks) {
        return this;
      }
      if (arguments.length === 1) {
        delete this._callbacks[event];
        return this;
      }
      for (i = _i = 0, _len = callbacks.length; _i < _len; i = ++_i) {
        callback = callbacks[i];
        if (callback === fn) {
          callbacks.splice(i, 1);
          break;
        }
      }
      return this;
    };

    return Emitter;

  })();

  Dropzone = (function(_super) {
    var extend, resolveOption;

    __extends(Dropzone, _super);

    Dropzone.prototype.Emitter = Emitter;


    /*
    This is a list of all available events you can register on a dropzone object.
    
    You can register an event handler like this:
    
        dropzone.on("dragEnter", function() { });
     */

    Dropzone.prototype.events = ["drop", "dragstart", "dragend", "dragenter", "dragover", "dragleave", "addedfile", "addedfiles", "removedfile", "thumbnail", "error", "errormultiple", "processing", "processingmultiple", "uploadprogress", "totaluploadprogress", "sending", "sendingmultiple", "success", "successmultiple", "canceled", "canceledmultiple", "complete", "completemultiple", "reset", "maxfilesexceeded", "maxfilesreached", "queuecomplete"];

    Dropzone.prototype.defaultOptions = {
      url: null,
      method: "post",
      withCredentials: false,
      parallelUploads: 2,
      uploadMultiple: false,
      maxFilesize: 256,
      paramName: "file",
      createImageThumbnails: true,
      maxThumbnailFilesize: 10,
      thumbnailWidth: 120,
      thumbnailHeight: 120,
      filesizeBase: 1000,
      maxFiles: null,
      params: {},
      clickable: true,
      ignoreHiddenFiles: true,
      acceptedFiles: null,
      acceptedMimeTypes: null,
      autoProcessQueue: true,
      autoQueue: true,
      addRemoveLinks: false,
      previewsContainer: null,
      hiddenInputContainer: "body",
      capture: null,
      dictDefaultMessage: "Drop files here to upload",
      dictFallbackMessage: "Your browser does not support drag'n'drop file uploads.",
      dictFallbackText: "Please use the fallback form below to upload your files like in the olden days.",
      dictFileTooBig: "File is too big ({{filesize}}MiB). Max filesize: {{maxFilesize}}MiB.",
      dictInvalidFileType: "You can't upload files of this type.",
      dictResponseError: "Server responded with {{statusCode}} code.",
      dictCancelUpload: "Cancel upload",
      dictCancelUploadConfirmation: "Are you sure you want to cancel this upload?",
      dictRemoveFile: "Remove file",
      dictRemoveFileConfirmation: null,
      dictMaxFilesExceeded: "You can not upload any more files.",
      accept: function(file, done) {
        return done();
      },
      init: function() {
        return noop;
      },
      forceFallback: false,
      fallback: function() {
        var child, messageElement, span, _i, _len, _ref;
        this.element.className = "" + this.element.className + " dz-browser-not-supported";
        _ref = this.element.getElementsByTagName("div");
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          child = _ref[_i];
          if (/(^| )dz-message($| )/.test(child.className)) {
            messageElement = child;
            child.className = "dz-message";
            continue;
          }
        }
        if (!messageElement) {
          messageElement = Dropzone.createElement("<div class=\"dz-message\"><span></span></div>");
          this.element.appendChild(messageElement);
        }
        span = messageElement.getElementsByTagName("span")[0];
        if (span) {
          if (span.textContent != null) {
            span.textContent = this.options.dictFallbackMessage;
          } else if (span.innerText != null) {
            span.innerText = this.options.dictFallbackMessage;
          }
        }
        return this.element.appendChild(this.getFallbackForm());
      },
      resize: function(file) {
        var info, srcRatio, trgRatio;
        info = {
          srcX: 0,
          srcY: 0,
          srcWidth: file.width,
          srcHeight: file.height
        };
        srcRatio = file.width / file.height;
        info.optWidth = this.options.thumbnailWidth;
        info.optHeight = this.options.thumbnailHeight;
        if ((info.optWidth == null) && (info.optHeight == null)) {
          info.optWidth = info.srcWidth;
          info.optHeight = info.srcHeight;
        } else if (info.optWidth == null) {
          info.optWidth = srcRatio * info.optHeight;
        } else if (info.optHeight == null) {
          info.optHeight = (1 / srcRatio) * info.optWidth;
        }
        trgRatio = info.optWidth / info.optHeight;
        if (file.height < info.optHeight || file.width < info.optWidth) {
          info.trgHeight = info.srcHeight;
          info.trgWidth = info.srcWidth;
        } else {
          if (srcRatio > trgRatio) {
            info.srcHeight = file.height;
            info.srcWidth = info.srcHeight * trgRatio;
          } else {
            info.srcWidth = file.width;
            info.srcHeight = info.srcWidth / trgRatio;
          }
        }
        info.srcX = (file.width - info.srcWidth) / 2;
        info.srcY = (file.height - info.srcHeight) / 2;
        return info;
      },

      /*
      Those functions register themselves to the events on init and handle all
      the user interface specific stuff. Overwriting them won't break the upload
      but can break the way it's displayed.
      You can overwrite them if you don't like the default behavior. If you just
      want to add an additional event handler, register it on the dropzone object
      and don't overwrite those options.
       */
      drop: function(e) {
        return this.element.classList.remove("dz-drag-hover");
      },
      dragstart: noop,
      dragend: function(e) {
        return this.element.classList.remove("dz-drag-hover");
      },
      dragenter: function(e) {
        return this.element.classList.add("dz-drag-hover");
      },
      dragover: function(e) {
        return this.element.classList.add("dz-drag-hover");
      },
      dragleave: function(e) {
        return this.element.classList.remove("dz-drag-hover");
      },
      paste: noop,
      reset: function() {
        return this.element.classList.remove("dz-started");
      },
      addedfile: function(file) {
        var node, removeFileEvent, removeLink, _i, _j, _k, _len, _len1, _len2, _ref, _ref1, _ref2, _results;
        if (this.element === this.previewsContainer) {
          this.element.classList.add("dz-started");
        }
        if (this.previewsContainer) {
          file.previewElement = Dropzone.createElement(this.options.previewTemplate.trim());
          file.previewTemplate = file.previewElement;
          this.previewsContainer.appendChild(file.previewElement);
          _ref = file.previewElement.querySelectorAll("[data-dz-name]");
          for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            node = _ref[_i];
            node.textContent = file.name;
          }
          _ref1 = file.previewElement.querySelectorAll("[data-dz-size]");
          for (_j = 0, _len1 = _ref1.length; _j < _len1; _j++) {
            node = _ref1[_j];
            node.innerHTML = this.filesize(file.size);
          }
          if (this.options.addRemoveLinks) {
            file._removeLink = Dropzone.createElement("<a class=\"dz-remove\" href=\"javascript:undefined;\" data-dz-remove>" + this.options.dictRemoveFile + "</a>");
            file.previewElement.appendChild(file._removeLink);
          }
          removeFileEvent = (function(_this) {
            return function(e) {
              e.preventDefault();
              e.stopPropagation();
              if (file.status === Dropzone.UPLOADING) {
                return Dropzone.confirm(_this.options.dictCancelUploadConfirmation, function() {
                  return _this.removeFile(file);
                });
              } else {
                if (_this.options.dictRemoveFileConfirmation) {
                  return Dropzone.confirm(_this.options.dictRemoveFileConfirmation, function() {
                    return _this.removeFile(file);
                  });
                } else {
                  return _this.removeFile(file);
                }
              }
            };
          })(this);
          _ref2 = file.previewElement.querySelectorAll("[data-dz-remove]");
          _results = [];
          for (_k = 0, _len2 = _ref2.length; _k < _len2; _k++) {
            removeLink = _ref2[_k];
            _results.push(removeLink.addEventListener("click", removeFileEvent));
          }
          return _results;
        }
      },
      removedfile: function(file) {
        var _ref;
        if (file.previewElement) {
          if ((_ref = file.previewElement) != null) {
            _ref.parentNode.removeChild(file.previewElement);
          }
        }
        return this._updateMaxFilesReachedClass();
      },
      thumbnail: function(file, dataUrl) {
        var thumbnailElement, _i, _len, _ref;
        if (file.previewElement) {
          file.previewElement.classList.remove("dz-file-preview");
          _ref = file.previewElement.querySelectorAll("[data-dz-thumbnail]");
          for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            thumbnailElement = _ref[_i];
            thumbnailElement.alt = file.name;
            thumbnailElement.src = dataUrl;
          }
          return setTimeout(((function(_this) {
            return function() {
              return file.previewElement.classList.add("dz-image-preview");
            };
          })(this)), 1);
        }
      },
      error: function(file, message) {
        var node, _i, _len, _ref, _results;
        if (file.previewElement) {
          file.previewElement.classList.add("dz-error");
          if (typeof message !== "String" && message.error) {
            message = message.error;
          }
          _ref = file.previewElement.querySelectorAll("[data-dz-errormessage]");
          _results = [];
          for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            node = _ref[_i];
            _results.push(node.textContent = message);
          }
          return _results;
        }
      },
      errormultiple: noop,
      processing: function(file) {
        if (file.previewElement) {
          file.previewElement.classList.add("dz-processing");
          if (file._removeLink) {
            return file._removeLink.textContent = this.options.dictCancelUpload;
          }
        }
      },
      processingmultiple: noop,
      uploadprogress: function(file, progress, bytesSent) {
        var node, _i, _len, _ref, _results;
        if (file.previewElement) {
          _ref = file.previewElement.querySelectorAll("[data-dz-uploadprogress]");
          _results = [];
          for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            node = _ref[_i];
            if (node.nodeName === 'PROGRESS') {
              _results.push(node.value = progress);
            } else {
              _results.push(node.style.width = "" + progress + "%");
            }
          }
          return _results;
        }
      },
      totaluploadprogress: noop,
      sending: noop,
      sendingmultiple: noop,
      success: function(file) {
        if (file.previewElement) {
          return file.previewElement.classList.add("dz-success");
        }
      },
      successmultiple: noop,
      canceled: function(file) {
        return this.emit("error", file, "Upload canceled.");
      },
      canceledmultiple: noop,
      complete: function(file) {
        if (file._removeLink) {
          file._removeLink.textContent = this.options.dictRemoveFile;
        }
        if (file.previewElement) {
          return file.previewElement.classList.add("dz-complete");
        }
      },
      completemultiple: noop,
      maxfilesexceeded: noop,
      maxfilesreached: noop,
      queuecomplete: noop,
      addedfiles: noop,
      previewTemplate: "<div class=\"dz-preview dz-file-preview\">\n  <div class=\"dz-image\"><img data-dz-thumbnail /></div>\n  <div class=\"dz-details\">\n    <div class=\"dz-size\"><span data-dz-size></span></div>\n    <div class=\"dz-filename\"><span data-dz-name></span></div>\n  </div>\n  <div class=\"dz-progress\"><span class=\"dz-upload\" data-dz-uploadprogress></span></div>\n  <div class=\"dz-error-message\"><span data-dz-errormessage></span></div>\n  <div class=\"dz-success-mark\">\n    <svg width=\"54px\" height=\"54px\" viewBox=\"0 0 54 54\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:sketch=\"http://www.bohemiancoding.com/sketch/ns\">\n      <title>Check</title>\n      <defs></defs>\n      <g id=\"Page-1\" stroke=\"none\" stroke-width=\"1\" fill=\"none\" fill-rule=\"evenodd\" sketch:type=\"MSPage\">\n        <path d=\"M23.5,31.8431458 L17.5852419,25.9283877 C16.0248253,24.3679711 13.4910294,24.366835 11.9289322,25.9289322 C10.3700136,27.4878508 10.3665912,30.0234455 11.9283877,31.5852419 L20.4147581,40.0716123 C20.5133999,40.1702541 20.6159315,40.2626649 20.7218615,40.3488435 C22.2835669,41.8725651 24.794234,41.8626202 26.3461564,40.3106978 L43.3106978,23.3461564 C44.8771021,21.7797521 44.8758057,19.2483887 43.3137085,17.6862915 C41.7547899,16.1273729 39.2176035,16.1255422 37.6538436,17.6893022 L23.5,31.8431458 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z\" id=\"Oval-2\" stroke-opacity=\"0.198794158\" stroke=\"#747474\" fill-opacity=\"0.816519475\" fill=\"#FFFFFF\" sketch:type=\"MSShapeGroup\"></path>\n      </g>\n    </svg>\n  </div>\n  <div class=\"dz-error-mark\">\n    <svg width=\"54px\" height=\"54px\" viewBox=\"0 0 54 54\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:sketch=\"http://www.bohemiancoding.com/sketch/ns\">\n      <title>Error</title>\n      <defs></defs>\n      <g id=\"Page-1\" stroke=\"none\" stroke-width=\"1\" fill=\"none\" fill-rule=\"evenodd\" sketch:type=\"MSPage\">\n        <g id=\"Check-+-Oval-2\" sketch:type=\"MSLayerGroup\" stroke=\"#747474\" stroke-opacity=\"0.198794158\" fill=\"#FFFFFF\" fill-opacity=\"0.816519475\">\n          <path d=\"M32.6568542,29 L38.3106978,23.3461564 C39.8771021,21.7797521 39.8758057,19.2483887 38.3137085,17.6862915 C36.7547899,16.1273729 34.2176035,16.1255422 32.6538436,17.6893022 L27,23.3431458 L21.3461564,17.6893022 C19.7823965,16.1255422 17.2452101,16.1273729 15.6862915,17.6862915 C14.1241943,19.2483887 14.1228979,21.7797521 15.6893022,23.3461564 L21.3431458,29 L15.6893022,34.6538436 C14.1228979,36.2202479 14.1241943,38.7516113 15.6862915,40.3137085 C17.2452101,41.8726271 19.7823965,41.8744578 21.3461564,40.3106978 L27,34.6568542 L32.6538436,40.3106978 C34.2176035,41.8744578 36.7547899,41.8726271 38.3137085,40.3137085 C39.8758057,38.7516113 39.8771021,36.2202479 38.3106978,34.6538436 L32.6568542,29 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z\" id=\"Oval-2\" sketch:type=\"MSShapeGroup\"></path>\n        </g>\n      </g>\n    </svg>\n  </div>\n</div>"
    };

    extend = function() {
      var key, object, objects, target, val, _i, _len;
      target = arguments[0], objects = 2 <= arguments.length ? __slice.call(arguments, 1) : [];
      for (_i = 0, _len = objects.length; _i < _len; _i++) {
        object = objects[_i];
        for (key in object) {
          val = object[key];
          target[key] = val;
        }
      }
      return target;
    };

    function Dropzone(element, options) {
      var elementOptions, fallback, _ref;
      this.element = element;
      this.version = Dropzone.version;
      this.defaultOptions.previewTemplate = this.defaultOptions.previewTemplate.replace(/\n*/g, "");
      this.clickableElements = [];
      this.listeners = [];
      this.files = [];
      if (typeof this.element === "string") {
        this.element = document.querySelector(this.element);
      }
      if (!(this.element && (this.element.nodeType != null))) {
        throw new Error("Invalid dropzone element.");
      }
      if (this.element.dropzone) {
        throw new Error("Dropzone already attached.");
      }
      Dropzone.instances.push(this);
      this.element.dropzone = this;
      elementOptions = (_ref = Dropzone.optionsForElement(this.element)) != null ? _ref : {};
      this.options = extend({}, this.defaultOptions, elementOptions, options != null ? options : {});
      if (this.options.forceFallback || !Dropzone.isBrowserSupported()) {
        return this.options.fallback.call(this);
      }
      if (this.options.url == null) {
        this.options.url = this.element.getAttribute("action");
      }
      if (!this.options.url) {
        throw new Error("No URL provided.");
      }
      if (this.options.acceptedFiles && this.options.acceptedMimeTypes) {
        throw new Error("You can't provide both 'acceptedFiles' and 'acceptedMimeTypes'. 'acceptedMimeTypes' is deprecated.");
      }
      if (this.options.acceptedMimeTypes) {
        this.options.acceptedFiles = this.options.acceptedMimeTypes;
        delete this.options.acceptedMimeTypes;
      }
      this.options.method = this.options.method.toUpperCase();
      if ((fallback = this.getExistingFallback()) && fallback.parentNode) {
        fallback.parentNode.removeChild(fallback);
      }
      if (this.options.previewsContainer !== false) {
        if (this.options.previewsContainer) {
          this.previewsContainer = Dropzone.getElement(this.options.previewsContainer, "previewsContainer");
        } else {
          this.previewsContainer = this.element;
        }
      }
      if (this.options.clickable) {
        if (this.options.clickable === true) {
          this.clickableElements = [this.element];
        } else {
          this.clickableElements = Dropzone.getElements(this.options.clickable, "clickable");
        }
      }
      this.init();
    }

    Dropzone.prototype.getAcceptedFiles = function() {
      var file, _i, _len, _ref, _results;
      _ref = this.files;
      _results = [];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        file = _ref[_i];
        if (file.accepted) {
          _results.push(file);
        }
      }
      return _results;
    };

    Dropzone.prototype.getRejectedFiles = function() {
      var file, _i, _len, _ref, _results;
      _ref = this.files;
      _results = [];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        file = _ref[_i];
        if (!file.accepted) {
          _results.push(file);
        }
      }
      return _results;
    };

    Dropzone.prototype.getFilesWithStatus = function(status) {
      var file, _i, _len, _ref, _results;
      _ref = this.files;
      _results = [];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        file = _ref[_i];
        if (file.status === status) {
          _results.push(file);
        }
      }
      return _results;
    };

    Dropzone.prototype.getQueuedFiles = function() {
      return this.getFilesWithStatus(Dropzone.QUEUED);
    };

    Dropzone.prototype.getUploadingFiles = function() {
      return this.getFilesWithStatus(Dropzone.UPLOADING);
    };

    Dropzone.prototype.getAddedFiles = function() {
      return this.getFilesWithStatus(Dropzone.ADDED);
    };

    Dropzone.prototype.getActiveFiles = function() {
      var file, _i, _len, _ref, _results;
      _ref = this.files;
      _results = [];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        file = _ref[_i];
        if (file.status === Dropzone.UPLOADING || file.status === Dropzone.QUEUED) {
          _results.push(file);
        }
      }
      return _results;
    };

    Dropzone.prototype.init = function() {
      var eventName, noPropagation, setupHiddenFileInput, _i, _len, _ref, _ref1;
      if (this.element.tagName === "form") {
        this.element.setAttribute("enctype", "multipart/form-data");
      }
      if (this.element.classList.contains("dropzone") && !this.element.querySelector(".dz-message")) {
        this.element.appendChild(Dropzone.createElement("<div class=\"dz-default dz-message\"><span>" + this.options.dictDefaultMessage + "</span></div>"));
      }
      if (this.clickableElements.length) {
        setupHiddenFileInput = (function(_this) {
          return function() {
            if (_this.hiddenFileInput) {
              _this.hiddenFileInput.parentNode.removeChild(_this.hiddenFileInput);
            }
            _this.hiddenFileInput = document.createElement("input");
            _this.hiddenFileInput.setAttribute("type", "file");
            if ((_this.options.maxFiles == null) || _this.options.maxFiles > 1) {
              _this.hiddenFileInput.setAttribute("multiple", "multiple");
            }
            _this.hiddenFileInput.className = "dz-hidden-input";
            if (_this.options.acceptedFiles != null) {
              _this.hiddenFileInput.setAttribute("accept", _this.options.acceptedFiles);
            }
            if (_this.options.capture != null) {
              _this.hiddenFileInput.setAttribute("capture", _this.options.capture);
            }
            _this.hiddenFileInput.style.visibility = "hidden";
            _this.hiddenFileInput.style.position = "absolute";
            _this.hiddenFileInput.style.top = "0";
            _this.hiddenFileInput.style.left = "0";
            _this.hiddenFileInput.style.height = "0";
            _this.hiddenFileInput.style.width = "0";
            document.querySelector(_this.options.hiddenInputContainer).appendChild(_this.hiddenFileInput);
            return _this.hiddenFileInput.addEventListener("change", function() {
              var file, files, _i, _len;
              files = _this.hiddenFileInput.files;
              if (files.length) {
                for (_i = 0, _len = files.length; _i < _len; _i++) {
                  file = files[_i];
                  _this.addFile(file);
                }
              }
              _this.emit("addedfiles", files);
              return setupHiddenFileInput();
            });
          };
        })(this);
        setupHiddenFileInput();
      }
      this.URL = (_ref = window.URL) != null ? _ref : window.webkitURL;
      _ref1 = this.events;
      for (_i = 0, _len = _ref1.length; _i < _len; _i++) {
        eventName = _ref1[_i];
        this.on(eventName, this.options[eventName]);
      }
      this.on("uploadprogress", (function(_this) {
        return function() {
          return _this.updateTotalUploadProgress();
        };
      })(this));
      this.on("removedfile", (function(_this) {
        return function() {
          return _this.updateTotalUploadProgress();
        };
      })(this));
      this.on("canceled", (function(_this) {
        return function(file) {
          return _this.emit("complete", file);
        };
      })(this));
      this.on("complete", (function(_this) {
        return function(file) {
          if (_this.getAddedFiles().length === 0 && _this.getUploadingFiles().length === 0 && _this.getQueuedFiles().length === 0) {
            return setTimeout((function() {
              return _this.emit("queuecomplete");
            }), 0);
          }
        };
      })(this));
      noPropagation = function(e) {
        e.stopPropagation();
        if (e.preventDefault) {
          return e.preventDefault();
        } else {
          return e.returnValue = false;
        }
      };
      this.listeners = [
        {
          element: this.element,
          events: {
            "dragstart": (function(_this) {
              return function(e) {
                return _this.emit("dragstart", e);
              };
            })(this),
            "dragenter": (function(_this) {
              return function(e) {
                noPropagation(e);
                return _this.emit("dragenter", e);
              };
            })(this),
            "dragover": (function(_this) {
              return function(e) {
                var efct;
                try {
                  efct = e.dataTransfer.effectAllowed;
                } catch (_error) {}
                e.dataTransfer.dropEffect = 'move' === efct || 'linkMove' === efct ? 'move' : 'copy';
                noPropagation(e);
                return _this.emit("dragover", e);
              };
            })(this),
            "dragleave": (function(_this) {
              return function(e) {
                return _this.emit("dragleave", e);
              };
            })(this),
            "drop": (function(_this) {
              return function(e) {
                noPropagation(e);
                return _this.drop(e);
              };
            })(this),
            "dragend": (function(_this) {
              return function(e) {
                return _this.emit("dragend", e);
              };
            })(this)
          }
        }
      ];
      this.clickableElements.forEach((function(_this) {
        return function(clickableElement) {
          return _this.listeners.push({
            element: clickableElement,
            events: {
              "click": function(evt) {
                if ((clickableElement !== _this.element) || (evt.target === _this.element || Dropzone.elementInside(evt.target, _this.element.querySelector(".dz-message")))) {
                  _this.hiddenFileInput.click();
                }
                return true;
              }
            }
          });
        };
      })(this));
      this.enable();
      return this.options.init.call(this);
    };

    Dropzone.prototype.destroy = function() {
      var _ref;
      this.disable();
      this.removeAllFiles(true);
      if ((_ref = this.hiddenFileInput) != null ? _ref.parentNode : void 0) {
        this.hiddenFileInput.parentNode.removeChild(this.hiddenFileInput);
        this.hiddenFileInput = null;
      }
      delete this.element.dropzone;
      return Dropzone.instances.splice(Dropzone.instances.indexOf(this), 1);
    };

    Dropzone.prototype.updateTotalUploadProgress = function() {
      var activeFiles, file, totalBytes, totalBytesSent, totalUploadProgress, _i, _len, _ref;
      totalBytesSent = 0;
      totalBytes = 0;
      activeFiles = this.getActiveFiles();
      if (activeFiles.length) {
        _ref = this.getActiveFiles();
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          file = _ref[_i];
          totalBytesSent += file.upload.bytesSent;
          totalBytes += file.upload.total;
        }
        totalUploadProgress = 100 * totalBytesSent / totalBytes;
      } else {
        totalUploadProgress = 100;
      }
      return this.emit("totaluploadprogress", totalUploadProgress, totalBytes, totalBytesSent);
    };

    Dropzone.prototype._getParamName = function(n) {
      if (typeof this.options.paramName === "function") {
        return this.options.paramName(n);
      } else {
        return "" + this.options.paramName + (this.options.uploadMultiple ? "[" + n + "]" : "");
      }
    };

    Dropzone.prototype.getFallbackForm = function() {
      var existingFallback, fields, fieldsString, form;
      if (existingFallback = this.getExistingFallback()) {
        return existingFallback;
      }
      fieldsString = "<div class=\"dz-fallback\">";
      if (this.options.dictFallbackText) {
        fieldsString += "<p>" + this.options.dictFallbackText + "</p>";
      }
      fieldsString += "<input type=\"file\" name=\"" + (this._getParamName(0)) + "\" " + (this.options.uploadMultiple ? 'multiple="multiple"' : void 0) + " /><input type=\"submit\" value=\"Upload!\"></div>";
      fields = Dropzone.createElement(fieldsString);
      if (this.element.tagName !== "FORM") {
        form = Dropzone.createElement("<form action=\"" + this.options.url + "\" enctype=\"multipart/form-data\" method=\"" + this.options.method + "\"></form>");
        form.appendChild(fields);
      } else {
        this.element.setAttribute("enctype", "multipart/form-data");
        this.element.setAttribute("method", this.options.method);
      }
      return form != null ? form : fields;
    };

    Dropzone.prototype.getExistingFallback = function() {
      var fallback, getFallback, tagName, _i, _len, _ref;
      getFallback = function(elements) {
        var el, _i, _len;
        for (_i = 0, _len = elements.length; _i < _len; _i++) {
          el = elements[_i];
          if (/(^| )fallback($| )/.test(el.className)) {
            return el;
          }
        }
      };
      _ref = ["div", "form"];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        tagName = _ref[_i];
        if (fallback = getFallback(this.element.getElementsByTagName(tagName))) {
          return fallback;
        }
      }
    };

    Dropzone.prototype.setupEventListeners = function() {
      var elementListeners, event, listener, _i, _len, _ref, _results;
      _ref = this.listeners;
      _results = [];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        elementListeners = _ref[_i];
        _results.push((function() {
          var _ref1, _results1;
          _ref1 = elementListeners.events;
          _results1 = [];
          for (event in _ref1) {
            listener = _ref1[event];
            _results1.push(elementListeners.element.addEventListener(event, listener, false));
          }
          return _results1;
        })());
      }
      return _results;
    };

    Dropzone.prototype.removeEventListeners = function() {
      var elementListeners, event, listener, _i, _len, _ref, _results;
      _ref = this.listeners;
      _results = [];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        elementListeners = _ref[_i];
        _results.push((function() {
          var _ref1, _results1;
          _ref1 = elementListeners.events;
          _results1 = [];
          for (event in _ref1) {
            listener = _ref1[event];
            _results1.push(elementListeners.element.removeEventListener(event, listener, false));
          }
          return _results1;
        })());
      }
      return _results;
    };

    Dropzone.prototype.disable = function() {
      var file, _i, _len, _ref, _results;
      this.clickableElements.forEach(function(element) {
        return element.classList.remove("dz-clickable");
      });
      this.removeEventListeners();
      _ref = this.files;
      _results = [];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        file = _ref[_i];
        _results.push(this.cancelUpload(file));
      }
      return _results;
    };

    Dropzone.prototype.enable = function() {
      this.clickableElements.forEach(function(element) {
        return element.classList.add("dz-clickable");
      });
      return this.setupEventListeners();
    };

    Dropzone.prototype.filesize = function(size) {
      var cutoff, i, selectedSize, selectedUnit, unit, units, _i, _len;
      selectedSize = 0;
      selectedUnit = "b";
      if (size > 0) {
        units = ['TB', 'GB', 'MB', 'KB', 'b'];
        for (i = _i = 0, _len = units.length; _i < _len; i = ++_i) {
          unit = units[i];
          cutoff = Math.pow(this.options.filesizeBase, 4 - i) / 10;
          if (size >= cutoff) {
            selectedSize = size / Math.pow(this.options.filesizeBase, 4 - i);
            selectedUnit = unit;
            break;
          }
        }
        selectedSize = Math.round(10 * selectedSize) / 10;
      }
      return "<strong>" + selectedSize + "</strong> " + selectedUnit;
    };

    Dropzone.prototype._updateMaxFilesReachedClass = function() {
      if ((this.options.maxFiles != null) && this.getAcceptedFiles().length >= this.options.maxFiles) {
        if (this.getAcceptedFiles().length === this.options.maxFiles) {
          this.emit('maxfilesreached', this.files);
        }
        return this.element.classList.add("dz-max-files-reached");
      } else {
        return this.element.classList.remove("dz-max-files-reached");
      }
    };

    Dropzone.prototype.drop = function(e) {
      var files, items;
      if (!e.dataTransfer) {
        return;
      }
      this.emit("drop", e);
      files = e.dataTransfer.files;
      this.emit("addedfiles", files);
      if (files.length) {
        items = e.dataTransfer.items;
        if (items && items.length && (items[0].webkitGetAsEntry != null)) {
          this._addFilesFromItems(items);
        } else {
          this.handleFiles(files);
        }
      }
    };

    Dropzone.prototype.paste = function(e) {
      var items, _ref;
      if ((e != null ? (_ref = e.clipboardData) != null ? _ref.items : void 0 : void 0) == null) {
        return;
      }
      this.emit("paste", e);
      items = e.clipboardData.items;
      if (items.length) {
        return this._addFilesFromItems(items);
      }
    };

    Dropzone.prototype.handleFiles = function(files) {
      var file, _i, _len, _results;
      _results = [];
      for (_i = 0, _len = files.length; _i < _len; _i++) {
        file = files[_i];
        _results.push(this.addFile(file));
      }
      return _results;
    };

    Dropzone.prototype._addFilesFromItems = function(items) {
      var entry, item, _i, _len, _results;
      _results = [];
      for (_i = 0, _len = items.length; _i < _len; _i++) {
        item = items[_i];
        if ((item.webkitGetAsEntry != null) && (entry = item.webkitGetAsEntry())) {
          if (entry.isFile) {
            _results.push(this.addFile(item.getAsFile()));
          } else if (entry.isDirectory) {
            _results.push(this._addFilesFromDirectory(entry, entry.name));
          } else {
            _results.push(void 0);
          }
        } else if (item.getAsFile != null) {
          if ((item.kind == null) || item.kind === "file") {
            _results.push(this.addFile(item.getAsFile()));
          } else {
            _results.push(void 0);
          }
        } else {
          _results.push(void 0);
        }
      }
      return _results;
    };

    Dropzone.prototype._addFilesFromDirectory = function(directory, path) {
      var dirReader, entriesReader;
      dirReader = directory.createReader();
      entriesReader = (function(_this) {
        return function(entries) {
          var entry, _i, _len;
          for (_i = 0, _len = entries.length; _i < _len; _i++) {
            entry = entries[_i];
            if (entry.isFile) {
              entry.file(function(file) {
                if (_this.options.ignoreHiddenFiles && file.name.substring(0, 1) === '.') {
                  return;
                }
                file.fullPath = "" + path + "/" + file.name;
                return _this.addFile(file);
              });
            } else if (entry.isDirectory) {
              _this._addFilesFromDirectory(entry, "" + path + "/" + entry.name);
            }
          }
        };
      })(this);
      return dirReader.readEntries(entriesReader, function(error) {
        return typeof console !== "undefined" && console !== null ? typeof console.log === "function" ? console.log(error) : void 0 : void 0;
      });
    };

    Dropzone.prototype.accept = function(file, done) {
      if (file.size > this.options.maxFilesize * 1024 * 1024) {
        return done(this.options.dictFileTooBig.replace("{{filesize}}", Math.round(file.size / 1024 / 10.24) / 100).replace("{{maxFilesize}}", this.options.maxFilesize));
      } else if (!Dropzone.isValidFile(file, this.options.acceptedFiles)) {
        return done(this.options.dictInvalidFileType);
      } else if ((this.options.maxFiles != null) && this.getAcceptedFiles().length >= this.options.maxFiles) {
        done(this.options.dictMaxFilesExceeded.replace("{{maxFiles}}", this.options.maxFiles));
        return this.emit("maxfilesexceeded", file);
      } else {
        return this.options.accept.call(this, file, done);
      }
    };

    Dropzone.prototype.addFile = function(file) {
      file.upload = {
        progress: 0,
        total: file.size,
        bytesSent: 0
      };
      this.files.push(file);
      file.status = Dropzone.ADDED;
      this.emit("addedfile", file);
      this._enqueueThumbnail(file);
      return this.accept(file, (function(_this) {
        return function(error) {
          if (error) {
            file.accepted = false;
            _this._errorProcessing([file], error);
          } else {
            file.accepted = true;
            if (_this.options.autoQueue) {
              _this.enqueueFile(file);
            }
          }
          return _this._updateMaxFilesReachedClass();
        };
      })(this));
    };

    Dropzone.prototype.enqueueFiles = function(files) {
      var file, _i, _len;
      for (_i = 0, _len = files.length; _i < _len; _i++) {
        file = files[_i];
        this.enqueueFile(file);
      }
      return null;
    };

    Dropzone.prototype.enqueueFile = function(file) {
      if (file.status === Dropzone.ADDED && file.accepted === true) {
        file.status = Dropzone.QUEUED;
        if (this.options.autoProcessQueue) {
          return setTimeout(((function(_this) {
            return function() {
              return _this.processQueue();
            };
          })(this)), 0);
        }
      } else {
        throw new Error("This file can't be queued because it has already been processed or was rejected.");
      }
    };

    Dropzone.prototype._thumbnailQueue = [];

    Dropzone.prototype._processingThumbnail = false;

    Dropzone.prototype._enqueueThumbnail = function(file) {
      if (this.options.createImageThumbnails && file.type.match(/image.*/) && file.size <= this.options.maxThumbnailFilesize * 1024 * 1024) {
        this._thumbnailQueue.push(file);
        return setTimeout(((function(_this) {
          return function() {
            return _this._processThumbnailQueue();
          };
        })(this)), 0);
      }
    };

    Dropzone.prototype._processThumbnailQueue = function() {
      if (this._processingThumbnail || this._thumbnailQueue.length === 0) {
        return;
      }
      this._processingThumbnail = true;
      return this.createThumbnail(this._thumbnailQueue.shift(), (function(_this) {
        return function() {
          _this._processingThumbnail = false;
          return _this._processThumbnailQueue();
        };
      })(this));
    };

    Dropzone.prototype.removeFile = function(file) {
      if (file.status === Dropzone.UPLOADING) {
        this.cancelUpload(file);
      }
      this.files = without(this.files, file);
      this.emit("removedfile", file);
      if (this.files.length === 0) {
        return this.emit("reset");
      }
    };

    Dropzone.prototype.removeAllFiles = function(cancelIfNecessary) {
      var file, _i, _len, _ref;
      if (cancelIfNecessary == null) {
        cancelIfNecessary = false;
      }
      _ref = this.files.slice();
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        file = _ref[_i];
        if (file.status !== Dropzone.UPLOADING || cancelIfNecessary) {
          this.removeFile(file);
        }
      }
      return null;
    };

    Dropzone.prototype.createThumbnail = function(file, callback) {
      var fileReader;
      fileReader = new FileReader;
      fileReader.onload = (function(_this) {
        return function() {
          if (file.type === "image/svg+xml") {
            _this.emit("thumbnail", file, fileReader.result);
            if (callback != null) {
              callback();
            }
            return;
          }
          return _this.createThumbnailFromUrl(file, fileReader.result, callback);
        };
      })(this);
      return fileReader.readAsDataURL(file);
    };

    Dropzone.prototype.createThumbnailFromUrl = function(file, imageUrl, callback, crossOrigin) {
      var img;
      img = document.createElement("img");
      if (crossOrigin) {
        img.crossOrigin = crossOrigin;
      }
      img.onload = (function(_this) {
        return function() {
          var canvas, ctx, resizeInfo, thumbnail, _ref, _ref1, _ref2, _ref3;
          file.width = img.width;
          file.height = img.height;
          resizeInfo = _this.options.resize.call(_this, file);
          if (resizeInfo.trgWidth == null) {
            resizeInfo.trgWidth = resizeInfo.optWidth;
          }
          if (resizeInfo.trgHeight == null) {
            resizeInfo.trgHeight = resizeInfo.optHeight;
          }
          canvas = document.createElement("canvas");
          ctx = canvas.getContext("2d");
          canvas.width = resizeInfo.trgWidth;
          canvas.height = resizeInfo.trgHeight;
          drawImageIOSFix(ctx, img, (_ref = resizeInfo.srcX) != null ? _ref : 0, (_ref1 = resizeInfo.srcY) != null ? _ref1 : 0, resizeInfo.srcWidth, resizeInfo.srcHeight, (_ref2 = resizeInfo.trgX) != null ? _ref2 : 0, (_ref3 = resizeInfo.trgY) != null ? _ref3 : 0, resizeInfo.trgWidth, resizeInfo.trgHeight);
          thumbnail = canvas.toDataURL("image/png");
          _this.emit("thumbnail", file, thumbnail);
          if (callback != null) {
            return callback();
          }
        };
      })(this);
      if (callback != null) {
        img.onerror = callback;
      }
      return img.src = imageUrl;
    };

    Dropzone.prototype.processQueue = function() {
      var i, parallelUploads, processingLength, queuedFiles;
      parallelUploads = this.options.parallelUploads;
      processingLength = this.getUploadingFiles().length;
      i = processingLength;
      if (processingLength >= parallelUploads) {
        return;
      }
      queuedFiles = this.getQueuedFiles();
      if (!(queuedFiles.length > 0)) {
        return;
      }
      if (this.options.uploadMultiple) {
        return this.processFiles(queuedFiles.slice(0, parallelUploads - processingLength));
      } else {
        while (i < parallelUploads) {
          if (!queuedFiles.length) {
            return;
          }
          this.processFile(queuedFiles.shift());
          i++;
        }
      }
    };

    Dropzone.prototype.processFile = function(file) {
      return this.processFiles([file]);
    };

    Dropzone.prototype.processFiles = function(files) {
      var file, _i, _len;
      for (_i = 0, _len = files.length; _i < _len; _i++) {
        file = files[_i];
        file.processing = true;
        file.status = Dropzone.UPLOADING;
        this.emit("processing", file);
      }
      if (this.options.uploadMultiple) {
        this.emit("processingmultiple", files);
      }
      return this.uploadFiles(files);
    };

    Dropzone.prototype._getFilesWithXhr = function(xhr) {
      var file, files;
      return files = (function() {
        var _i, _len, _ref, _results;
        _ref = this.files;
        _results = [];
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          file = _ref[_i];
          if (file.xhr === xhr) {
            _results.push(file);
          }
        }
        return _results;
      }).call(this);
    };

    Dropzone.prototype.cancelUpload = function(file) {
      var groupedFile, groupedFiles, _i, _j, _len, _len1, _ref;
      if (file.status === Dropzone.UPLOADING) {
        groupedFiles = this._getFilesWithXhr(file.xhr);
        for (_i = 0, _len = groupedFiles.length; _i < _len; _i++) {
          groupedFile = groupedFiles[_i];
          groupedFile.status = Dropzone.CANCELED;
        }
        file.xhr.abort();
        for (_j = 0, _len1 = groupedFiles.length; _j < _len1; _j++) {
          groupedFile = groupedFiles[_j];
          this.emit("canceled", groupedFile);
        }
        if (this.options.uploadMultiple) {
          this.emit("canceledmultiple", groupedFiles);
        }
      } else if ((_ref = file.status) === Dropzone.ADDED || _ref === Dropzone.QUEUED) {
        file.status = Dropzone.CANCELED;
        this.emit("canceled", file);
        if (this.options.uploadMultiple) {
          this.emit("canceledmultiple", [file]);
        }
      }
      if (this.options.autoProcessQueue) {
        return this.processQueue();
      }
    };

    resolveOption = function() {
      var args, option;
      option = arguments[0], args = 2 <= arguments.length ? __slice.call(arguments, 1) : [];
      if (typeof option === 'function') {
        return option.apply(this, args);
      }
      return option;
    };

    Dropzone.prototype.uploadFile = function(file) {
      return this.uploadFiles([file]);
    };

    Dropzone.prototype.uploadFiles = function(files) {
      var file, formData, handleError, headerName, headerValue, headers, i, input, inputName, inputType, key, method, option, progressObj, response, updateProgress, url, value, xhr, _i, _j, _k, _l, _len, _len1, _len2, _len3, _m, _ref, _ref1, _ref2, _ref3, _ref4, _ref5;
      xhr = new XMLHttpRequest();
      for (_i = 0, _len = files.length; _i < _len; _i++) {
        file = files[_i];
        file.xhr = xhr;
      }
      method = resolveOption(this.options.method, files);
      url = resolveOption(this.options.url, files);
      xhr.open(method, url, true);
      xhr.withCredentials = !!this.options.withCredentials;
      response = null;
      handleError = (function(_this) {
        return function() {
          var _j, _len1, _results;
          _results = [];
          for (_j = 0, _len1 = files.length; _j < _len1; _j++) {
            file = files[_j];
            _results.push(_this._errorProcessing(files, response || _this.options.dictResponseError.replace("{{statusCode}}", xhr.status), xhr));
          }
          return _results;
        };
      })(this);
      updateProgress = (function(_this) {
        return function(e) {
          var allFilesFinished, progress, _j, _k, _l, _len1, _len2, _len3, _results;
          if (e != null) {
            progress = 100 * e.loaded / e.total;
            for (_j = 0, _len1 = files.length; _j < _len1; _j++) {
              file = files[_j];
              file.upload = {
                progress: progress,
                total: e.total,
                bytesSent: e.loaded
              };
            }
          } else {
            allFilesFinished = true;
            progress = 100;
            for (_k = 0, _len2 = files.length; _k < _len2; _k++) {
              file = files[_k];
              if (!(file.upload.progress === 100 && file.upload.bytesSent === file.upload.total)) {
                allFilesFinished = false;
              }
              file.upload.progress = progress;
              file.upload.bytesSent = file.upload.total;
            }
            if (allFilesFinished) {
              return;
            }
          }
          _results = [];
          for (_l = 0, _len3 = files.length; _l < _len3; _l++) {
            file = files[_l];
            _results.push(_this.emit("uploadprogress", file, progress, file.upload.bytesSent));
          }
          return _results;
        };
      })(this);
      xhr.onload = (function(_this) {
        return function(e) {
          var _ref;
          if (files[0].status === Dropzone.CANCELED) {
            return;
          }
          if (xhr.readyState !== 4) {
            return;
          }
          response = xhr.responseText;
          if (xhr.getResponseHeader("content-type") && ~xhr.getResponseHeader("content-type").indexOf("application/json")) {
            try {
              response = JSON.parse(response);
            } catch (_error) {
              e = _error;
              response = "Invalid JSON response from server.";
            }
          }
          updateProgress();
          if (!((200 <= (_ref = xhr.status) && _ref < 300))) {
            return handleError();
          } else {
            return _this._finished(files, response, e);
          }
        };
      })(this);
      xhr.onerror = (function(_this) {
        return function() {
          if (files[0].status === Dropzone.CANCELED) {
            return;
          }
          return handleError();
        };
      })(this);
      progressObj = (_ref = xhr.upload) != null ? _ref : xhr;
      progressObj.onprogress = updateProgress;
      headers = {
        "Accept": "application/json",
        "Cache-Control": "no-cache",
        "X-Requested-With": "XMLHttpRequest"
      };
      if (this.options.headers) {
        extend(headers, this.options.headers);
      }
      for (headerName in headers) {
        headerValue = headers[headerName];
        if (headerValue) {
          xhr.setRequestHeader(headerName, headerValue);
        }
      }
      formData = new FormData();
      if (this.options.params) {
        _ref1 = this.options.params;
        for (key in _ref1) {
          value = _ref1[key];
          formData.append(key, value);
        }
      }
      for (_j = 0, _len1 = files.length; _j < _len1; _j++) {
        file = files[_j];
        this.emit("sending", file, xhr, formData);
      }
      if (this.options.uploadMultiple) {
        this.emit("sendingmultiple", files, xhr, formData);
      }
      if (this.element.tagName === "FORM") {
        _ref2 = this.element.querySelectorAll("input, textarea, select, button");
        for (_k = 0, _len2 = _ref2.length; _k < _len2; _k++) {
          input = _ref2[_k];
          inputName = input.getAttribute("name");
          inputType = input.getAttribute("type");
          if (input.tagName === "SELECT" && input.hasAttribute("multiple")) {
            _ref3 = input.options;
            for (_l = 0, _len3 = _ref3.length; _l < _len3; _l++) {
              option = _ref3[_l];
              if (option.selected) {
                formData.append(inputName, option.value);
              }
            }
          } else if (!inputType || ((_ref4 = inputType.toLowerCase()) !== "checkbox" && _ref4 !== "radio") || input.checked) {
            formData.append(inputName, input.value);
          }
        }
      }
      for (i = _m = 0, _ref5 = files.length - 1; 0 <= _ref5 ? _m <= _ref5 : _m >= _ref5; i = 0 <= _ref5 ? ++_m : --_m) {
        formData.append(this._getParamName(i), files[i], files[i].name);
      }
      return this.submitRequest(xhr, formData, files);
    };

    Dropzone.prototype.submitRequest = function(xhr, formData, files) {
      return xhr.send(formData);
    };

    Dropzone.prototype._finished = function(files, responseText, e) {
      var file, _i, _len;
      for (_i = 0, _len = files.length; _i < _len; _i++) {
        file = files[_i];
        file.status = Dropzone.SUCCESS;
        this.emit("success", file, responseText, e);
        this.emit("complete", file);
      }
      if (this.options.uploadMultiple) {
        this.emit("successmultiple", files, responseText, e);
        this.emit("completemultiple", files);
      }
      if (this.options.autoProcessQueue) {
        return this.processQueue();
      }
    };

    Dropzone.prototype._errorProcessing = function(files, message, xhr) {
      var file, _i, _len;
      for (_i = 0, _len = files.length; _i < _len; _i++) {
        file = files[_i];
        file.status = Dropzone.ERROR;
        this.emit("error", file, message, xhr);
        this.emit("complete", file);
      }
      if (this.options.uploadMultiple) {
        this.emit("errormultiple", files, message, xhr);
        this.emit("completemultiple", files);
      }
      if (this.options.autoProcessQueue) {
        return this.processQueue();
      }
    };

    return Dropzone;

  })(Emitter);

  Dropzone.version = "4.2.0";

  Dropzone.options = {};

  Dropzone.optionsForElement = function(element) {
    if (element.getAttribute("id")) {
      return Dropzone.options[camelize(element.getAttribute("id"))];
    } else {
      return void 0;
    }
  };

  Dropzone.instances = [];

  Dropzone.forElement = function(element) {
    if (typeof element === "string") {
      element = document.querySelector(element);
    }
    if ((element != null ? element.dropzone : void 0) == null) {
      throw new Error("No Dropzone found for given element. This is probably because you're trying to access it before Dropzone had the time to initialize. Use the `init` option to setup any additional observers on your Dropzone.");
    }
    return element.dropzone;
  };

  Dropzone.autoDiscover = true;

  Dropzone.discover = function() {
    var checkElements, dropzone, dropzones, _i, _len, _results;
    if (document.querySelectorAll) {
      dropzones = document.querySelectorAll(".dropzone");
    } else {
      dropzones = [];
      checkElements = function(elements) {
        var el, _i, _len, _results;
        _results = [];
        for (_i = 0, _len = elements.length; _i < _len; _i++) {
          el = elements[_i];
          if (/(^| )dropzone($| )/.test(el.className)) {
            _results.push(dropzones.push(el));
          } else {
            _results.push(void 0);
          }
        }
        return _results;
      };
      checkElements(document.getElementsByTagName("div"));
      checkElements(document.getElementsByTagName("form"));
    }
    _results = [];
    for (_i = 0, _len = dropzones.length; _i < _len; _i++) {
      dropzone = dropzones[_i];
      if (Dropzone.optionsForElement(dropzone) !== false) {
        _results.push(new Dropzone(dropzone));
      } else {
        _results.push(void 0);
      }
    }
    return _results;
  };

  Dropzone.blacklistedBrowsers = [/opera.*Macintosh.*version\/12/i];

  Dropzone.isBrowserSupported = function() {
    var capableBrowser, regex, _i, _len, _ref;
    capableBrowser = true;
    if (window.File && window.FileReader && window.FileList && window.Blob && window.FormData && document.querySelector) {
      if (!("classList" in document.createElement("a"))) {
        capableBrowser = false;
      } else {
        _ref = Dropzone.blacklistedBrowsers;
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          regex = _ref[_i];
          if (regex.test(navigator.userAgent)) {
            capableBrowser = false;
            continue;
          }
        }
      }
    } else {
      capableBrowser = false;
    }
    return capableBrowser;
  };

  without = function(list, rejectedItem) {
    var item, _i, _len, _results;
    _results = [];
    for (_i = 0, _len = list.length; _i < _len; _i++) {
      item = list[_i];
      if (item !== rejectedItem) {
        _results.push(item);
      }
    }
    return _results;
  };

  camelize = function(str) {
    return str.replace(/[\-_](\w)/g, function(match) {
      return match.charAt(1).toUpperCase();
    });
  };

  Dropzone.createElement = function(string) {
    var div;
    div = document.createElement("div");
    div.innerHTML = string;
    return div.childNodes[0];
  };

  Dropzone.elementInside = function(element, container) {
    if (element === container) {
      return true;
    }
    while (element = element.parentNode) {
      if (element === container) {
        return true;
      }
    }
    return false;
  };

  Dropzone.getElement = function(el, name) {
    var element;
    if (typeof el === "string") {
      element = document.querySelector(el);
    } else if (el.nodeType != null) {
      element = el;
    }
    if (element == null) {
      throw new Error("Invalid `" + name + "` option provided. Please provide a CSS selector or a plain HTML element.");
    }
    return element;
  };

  Dropzone.getElements = function(els, name) {
    var e, el, elements, _i, _j, _len, _len1, _ref;
    if (els instanceof Array) {
      elements = [];
      try {
        for (_i = 0, _len = els.length; _i < _len; _i++) {
          el = els[_i];
          elements.push(this.getElement(el, name));
        }
      } catch (_error) {
        e = _error;
        elements = null;
      }
    } else if (typeof els === "string") {
      elements = [];
      _ref = document.querySelectorAll(els);
      for (_j = 0, _len1 = _ref.length; _j < _len1; _j++) {
        el = _ref[_j];
        elements.push(el);
      }
    } else if (els.nodeType != null) {
      elements = [els];
    }
    if (!((elements != null) && elements.length)) {
      throw new Error("Invalid `" + name + "` option provided. Please provide a CSS selector, a plain HTML element or a list of those.");
    }
    return elements;
  };

  Dropzone.confirm = function(question, accepted, rejected) {
    if (window.confirm(question)) {
      return accepted();
    } else if (rejected != null) {
      return rejected();
    }
  };

  Dropzone.isValidFile = function(file, acceptedFiles) {
    var baseMimeType, mimeType, validType, _i, _len;
    if (!acceptedFiles) {
      return true;
    }
    acceptedFiles = acceptedFiles.split(",");
    mimeType = file.type;
    baseMimeType = mimeType.replace(/\/.*$/, "");
    for (_i = 0, _len = acceptedFiles.length; _i < _len; _i++) {
      validType = acceptedFiles[_i];
      validType = validType.trim();
      if (validType.charAt(0) === ".") {
        if (file.name.toLowerCase().indexOf(validType.toLowerCase(), file.name.length - validType.length) !== -1) {
          return true;
        }
      } else if (/\/\*$/.test(validType)) {
        if (baseMimeType === validType.replace(/\/.*$/, "")) {
          return true;
        }
      } else {
        if (mimeType === validType) {
          return true;
        }
      }
    }
    return false;
  };

  if (typeof jQuery !== "undefined" && jQuery !== null) {
    jQuery.fn.dropzone = function(options) {
      return this.each(function() {
        return new Dropzone(this, options);
      });
    };
  }

  if (typeof module !== "undefined" && module !== null) {
    module.exports = Dropzone;
  } else {
    window.Dropzone = Dropzone;
  }

  Dropzone.ADDED = "added";

  Dropzone.QUEUED = "queued";

  Dropzone.ACCEPTED = Dropzone.QUEUED;

  Dropzone.UPLOADING = "uploading";

  Dropzone.PROCESSING = Dropzone.UPLOADING;

  Dropzone.CANCELED = "canceled";

  Dropzone.ERROR = "error";

  Dropzone.SUCCESS = "success";


  /*
  
  Bugfix for iOS 6 and 7
  Source: http://stackoverflow.com/questions/11929099/html5-canvas-drawimage-ratio-bug-ios
  based on the work of https://github.com/stomita/ios-imagefile-megapixel
   */

  detectVerticalSquash = function(img) {
    var alpha, canvas, ctx, data, ey, ih, iw, py, ratio, sy;
    iw = img.naturalWidth;
    ih = img.naturalHeight;
    canvas = document.createElement("canvas");
    canvas.width = 1;
    canvas.height = ih;
    ctx = canvas.getContext("2d");
    ctx.drawImage(img, 0, 0);
    data = ctx.getImageData(0, 0, 1, ih).data;
    sy = 0;
    ey = ih;
    py = ih;
    while (py > sy) {
      alpha = data[(py - 1) * 4 + 3];
      if (alpha === 0) {
        ey = py;
      } else {
        sy = py;
      }
      py = (ey + sy) >> 1;
    }
    ratio = py / ih;
    if (ratio === 0) {
      return 1;
    } else {
      return ratio;
    }
  };

  drawImageIOSFix = function(ctx, img, sx, sy, sw, sh, dx, dy, dw, dh) {
    var vertSquashRatio;
    vertSquashRatio = detectVerticalSquash(img);
    return ctx.drawImage(img, sx, sy, sw, sh, dx, dy, dw, dh / vertSquashRatio);
  };


  /*
   * contentloaded.js
   *
   * Author: Diego Perini (diego.perini at gmail.com)
   * Summary: cross-browser wrapper for DOMContentLoaded
   * Updated: 20101020
   * License: MIT
   * Version: 1.2
   *
   * URL:
   * http://javascript.nwbox.com/ContentLoaded/
   * http://javascript.nwbox.com/ContentLoaded/MIT-LICENSE
   */

  contentLoaded = function(win, fn) {
    var add, doc, done, init, poll, pre, rem, root, top;
    done = false;
    top = true;
    doc = win.document;
    root = doc.documentElement;
    add = (doc.addEventListener ? "addEventListener" : "attachEvent");
    rem = (doc.addEventListener ? "removeEventListener" : "detachEvent");
    pre = (doc.addEventListener ? "" : "on");
    init = function(e) {
      if (e.type === "readystatechange" && doc.readyState !== "complete") {
        return;
      }
      (e.type === "load" ? win : doc)[rem](pre + e.type, init, false);
      if (!done && (done = true)) {
        return fn.call(win, e.type || e);
      }
    };
    poll = function() {
      var e;
      try {
        root.doScroll("left");
      } catch (_error) {
        e = _error;
        setTimeout(poll, 50);
        return;
      }
      return init("poll");
    };
    if (doc.readyState !== "complete") {
      if (doc.createEventObject && root.doScroll) {
        try {
          top = !win.frameElement;
        } catch (_error) {}
        if (top) {
          poll();
        }
      }
      doc[add](pre + "DOMContentLoaded", init, false);
      doc[add](pre + "readystatechange", init, false);
      return win[add](pre + "load", init, false);
    }
  };

  Dropzone._autoDiscoverFunction = function() {
    if (Dropzone.autoDiscover) {
      return Dropzone.discover();
    }
  };

  contentLoaded(window, Dropzone._autoDiscoverFunction);

}).call(this);
/*!
 * jQuery JavaScript Library v2.2.4
 * http://jquery.com/
 *
 * Includes Sizzle.js
 * http://sizzlejs.com/
 *
 * Copyright jQuery Foundation and other contributors
 * Released under the MIT license
 * http://jquery.org/license
 *
 * Date: 2016-05-20T17:23Z
 */

(function( global, factory ) {

  if ( typeof module === "object" && typeof module.exports === "object" ) {
    // For CommonJS and CommonJS-like environments where a proper `window`
    // is present, execute the factory and get jQuery.
    // For environments that do not have a `window` with a `document`
    // (such as Node.js), expose a factory as module.exports.
    // This accentuates the need for the creation of a real `window`.
    // e.g. var jQuery = require("jquery")(window);
    // See ticket #14549 for more info.
    module.exports = global.document ?
      factory( global, true ) :
      function( w ) {
        if ( !w.document ) {
          throw new Error( "jQuery requires a window with a document" );
        }
        return factory( w );
      };
  } else {
    factory( global );
  }

// Pass this if window is not defined yet
}(typeof window !== "undefined" ? window : this, function( window, noGlobal ) {

// Support: Firefox 18+
// Can't be in strict mode, several libs including ASP.NET trace
// the stack via arguments.caller.callee and Firefox dies if
// you try to trace through "use strict" call chains. (#13335)
//"use strict";
var arr = [];

var document = window.document;

var slice = arr.slice;

var concat = arr.concat;

var push = arr.push;

var indexOf = arr.indexOf;

var class2type = {};

var toString = class2type.toString;

var hasOwn = class2type.hasOwnProperty;

var support = {};



var
  version = "2.2.4",

  // Define a local copy of jQuery
  jQuery = function( selector, context ) {

    // The jQuery object is actually just the init constructor 'enhanced'
    // Need init if jQuery is called (just allow error to be thrown if not included)
    return new jQuery.fn.init( selector, context );
  },

  // Support: Android<4.1
  // Make sure we trim BOM and NBSP
  rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,

  // Matches dashed string for camelizing
  rmsPrefix = /^-ms-/,
  rdashAlpha = /-([\da-z])/gi,

  // Used by jQuery.camelCase as callback to replace()
  fcamelCase = function( all, letter ) {
    return letter.toUpperCase();
  };

jQuery.fn = jQuery.prototype = {

  // The current version of jQuery being used
  jquery: version,

  constructor: jQuery,

  // Start with an empty selector
  selector: "",

  // The default length of a jQuery object is 0
  length: 0,

  toArray: function() {
    return slice.call( this );
  },

  // Get the Nth element in the matched element set OR
  // Get the whole matched element set as a clean array
  get: function( num ) {
    return num != null ?

      // Return just the one element from the set
      ( num < 0 ? this[ num + this.length ] : this[ num ] ) :

      // Return all the elements in a clean array
      slice.call( this );
  },

  // Take an array of elements and push it onto the stack
  // (returning the new matched element set)
  pushStack: function( elems ) {

    // Build a new jQuery matched element set
    var ret = jQuery.merge( this.constructor(), elems );

    // Add the old object onto the stack (as a reference)
    ret.prevObject = this;
    ret.context = this.context;

    // Return the newly-formed element set
    return ret;
  },

  // Execute a callback for every element in the matched set.
  each: function( callback ) {
    return jQuery.each( this, callback );
  },

  map: function( callback ) {
    return this.pushStack( jQuery.map( this, function( elem, i ) {
      return callback.call( elem, i, elem );
    } ) );
  },

  slice: function() {
    return this.pushStack( slice.apply( this, arguments ) );
  },

  first: function() {
    return this.eq( 0 );
  },

  last: function() {
    return this.eq( -1 );
  },

  eq: function( i ) {
    var len = this.length,
      j = +i + ( i < 0 ? len : 0 );
    return this.pushStack( j >= 0 && j < len ? [ this[ j ] ] : [] );
  },

  end: function() {
    return this.prevObject || this.constructor();
  },

  // For internal use only.
  // Behaves like an Array's method, not like a jQuery method.
  push: push,
  sort: arr.sort,
  splice: arr.splice
};

jQuery.extend = jQuery.fn.extend = function() {
  var options, name, src, copy, copyIsArray, clone,
    target = arguments[ 0 ] || {},
    i = 1,
    length = arguments.length,
    deep = false;

  // Handle a deep copy situation
  if ( typeof target === "boolean" ) {
    deep = target;

    // Skip the boolean and the target
    target = arguments[ i ] || {};
    i++;
  }

  // Handle case when target is a string or something (possible in deep copy)
  if ( typeof target !== "object" && !jQuery.isFunction( target ) ) {
    target = {};
  }

  // Extend jQuery itself if only one argument is passed
  if ( i === length ) {
    target = this;
    i--;
  }

  for ( ; i < length; i++ ) {

    // Only deal with non-null/undefined values
    if ( ( options = arguments[ i ] ) != null ) {

      // Extend the base object
      for ( name in options ) {
        src = target[ name ];
        copy = options[ name ];

        // Prevent never-ending loop
        if ( target === copy ) {
          continue;
        }

        // Recurse if we're merging plain objects or arrays
        if ( deep && copy && ( jQuery.isPlainObject( copy ) ||
          ( copyIsArray = jQuery.isArray( copy ) ) ) ) {

          if ( copyIsArray ) {
            copyIsArray = false;
            clone = src && jQuery.isArray( src ) ? src : [];

          } else {
            clone = src && jQuery.isPlainObject( src ) ? src : {};
          }

          // Never move original objects, clone them
          target[ name ] = jQuery.extend( deep, clone, copy );

        // Don't bring in undefined values
        } else if ( copy !== undefined ) {
          target[ name ] = copy;
        }
      }
    }
  }

  // Return the modified object
  return target;
};

jQuery.extend( {

  // Unique for each copy of jQuery on the page
  expando: "jQuery" + ( version + Math.random() ).replace( /\D/g, "" ),

  // Assume jQuery is ready without the ready module
  isReady: true,

  error: function( msg ) {
    throw new Error( msg );
  },

  noop: function() {},

  isFunction: function( obj ) {
    return jQuery.type( obj ) === "function";
  },

  isArray: Array.isArray,

  isWindow: function( obj ) {
    return obj != null && obj === obj.window;
  },

  isNumeric: function( obj ) {

    // parseFloat NaNs numeric-cast false positives (null|true|false|"")
    // ...but misinterprets leading-number strings, particularly hex literals ("0x...")
    // subtraction forces infinities to NaN
    // adding 1 corrects loss of precision from parseFloat (#15100)
    var realStringObj = obj && obj.toString();
    return !jQuery.isArray( obj ) && ( realStringObj - parseFloat( realStringObj ) + 1 ) >= 0;
  },

  isPlainObject: function( obj ) {
    var key;

    // Not plain objects:
    // - Any object or value whose internal [[Class]] property is not "[object Object]"
    // - DOM nodes
    // - window
    if ( jQuery.type( obj ) !== "object" || obj.nodeType || jQuery.isWindow( obj ) ) {
      return false;
    }

    // Not own constructor property must be Object
    if ( obj.constructor &&
        !hasOwn.call( obj, "constructor" ) &&
        !hasOwn.call( obj.constructor.prototype || {}, "isPrototypeOf" ) ) {
      return false;
    }

    // Own properties are enumerated firstly, so to speed up,
    // if last one is own, then all properties are own
    for ( key in obj ) {}

    return key === undefined || hasOwn.call( obj, key );
  },

  isEmptyObject: function( obj ) {
    var name;
    for ( name in obj ) {
      return false;
    }
    return true;
  },

  type: function( obj ) {
    if ( obj == null ) {
      return obj + "";
    }

    // Support: Android<4.0, iOS<6 (functionish RegExp)
    return typeof obj === "object" || typeof obj === "function" ?
      class2type[ toString.call( obj ) ] || "object" :
      typeof obj;
  },

  // Evaluates a script in a global context
  globalEval: function( code ) {
    var script,
      indirect = eval;

    code = jQuery.trim( code );

    if ( code ) {

      // If the code includes a valid, prologue position
      // strict mode pragma, execute code by injecting a
      // script tag into the document.
      if ( code.indexOf( "use strict" ) === 1 ) {
        script = document.createElement( "script" );
        script.text = code;
        document.head.appendChild( script ).parentNode.removeChild( script );
      } else {

        // Otherwise, avoid the DOM node creation, insertion
        // and removal by using an indirect global eval

        indirect( code );
      }
    }
  },

  // Convert dashed to camelCase; used by the css and data modules
  // Support: IE9-11+
  // Microsoft forgot to hump their vendor prefix (#9572)
  camelCase: function( string ) {
    return string.replace( rmsPrefix, "ms-" ).replace( rdashAlpha, fcamelCase );
  },

  nodeName: function( elem, name ) {
    return elem.nodeName && elem.nodeName.toLowerCase() === name.toLowerCase();
  },

  each: function( obj, callback ) {
    var length, i = 0;

    if ( isArrayLike( obj ) ) {
      length = obj.length;
      for ( ; i < length; i++ ) {
        if ( callback.call( obj[ i ], i, obj[ i ] ) === false ) {
          break;
        }
      }
    } else {
      for ( i in obj ) {
        if ( callback.call( obj[ i ], i, obj[ i ] ) === false ) {
          break;
        }
      }
    }

    return obj;
  },

  // Support: Android<4.1
  trim: function( text ) {
    return text == null ?
      "" :
      ( text + "" ).replace( rtrim, "" );
  },

  // results is for internal usage only
  makeArray: function( arr, results ) {
    var ret = results || [];

    if ( arr != null ) {
      if ( isArrayLike( Object( arr ) ) ) {
        jQuery.merge( ret,
          typeof arr === "string" ?
          [ arr ] : arr
        );
      } else {
        push.call( ret, arr );
      }
    }

    return ret;
  },

  inArray: function( elem, arr, i ) {
    return arr == null ? -1 : indexOf.call( arr, elem, i );
  },

  merge: function( first, second ) {
    var len = +second.length,
      j = 0,
      i = first.length;

    for ( ; j < len; j++ ) {
      first[ i++ ] = second[ j ];
    }

    first.length = i;

    return first;
  },

  grep: function( elems, callback, invert ) {
    var callbackInverse,
      matches = [],
      i = 0,
      length = elems.length,
      callbackExpect = !invert;

    // Go through the array, only saving the items
    // that pass the validator function
    for ( ; i < length; i++ ) {
      callbackInverse = !callback( elems[ i ], i );
      if ( callbackInverse !== callbackExpect ) {
        matches.push( elems[ i ] );
      }
    }

    return matches;
  },

  // arg is for internal usage only
  map: function( elems, callback, arg ) {
    var length, value,
      i = 0,
      ret = [];

    // Go through the array, translating each of the items to their new values
    if ( isArrayLike( elems ) ) {
      length = elems.length;
      for ( ; i < length; i++ ) {
        value = callback( elems[ i ], i, arg );

        if ( value != null ) {
          ret.push( value );
        }
      }

    // Go through every key on the object,
    } else {
      for ( i in elems ) {
        value = callback( elems[ i ], i, arg );

        if ( value != null ) {
          ret.push( value );
        }
      }
    }

    // Flatten any nested arrays
    return concat.apply( [], ret );
  },

  // A global GUID counter for objects
  guid: 1,

  // Bind a function to a context, optionally partially applying any
  // arguments.
  proxy: function( fn, context ) {
    var tmp, args, proxy;

    if ( typeof context === "string" ) {
      tmp = fn[ context ];
      context = fn;
      fn = tmp;
    }

    // Quick check to determine if target is callable, in the spec
    // this throws a TypeError, but we will just return undefined.
    if ( !jQuery.isFunction( fn ) ) {
      return undefined;
    }

    // Simulated bind
    args = slice.call( arguments, 2 );
    proxy = function() {
      return fn.apply( context || this, args.concat( slice.call( arguments ) ) );
    };

    // Set the guid of unique handler to the same of original handler, so it can be removed
    proxy.guid = fn.guid = fn.guid || jQuery.guid++;

    return proxy;
  },

  now: Date.now,

  // jQuery.support is not used in Core but other projects attach their
  // properties to it so it needs to exist.
  support: support
} );

// JSHint would error on this code due to the Symbol not being defined in ES5.
// Defining this global in .jshintrc would create a danger of using the global
// unguarded in another place, it seems safer to just disable JSHint for these
// three lines.
/* jshint ignore: start */
if ( typeof Symbol === "function" ) {
  jQuery.fn[ Symbol.iterator ] = arr[ Symbol.iterator ];
}
/* jshint ignore: end */

// Populate the class2type map
jQuery.each( "Boolean Number String Function Array Date RegExp Object Error Symbol".split( " " ),
function( i, name ) {
  class2type[ "[object " + name + "]" ] = name.toLowerCase();
} );

function isArrayLike( obj ) {

  // Support: iOS 8.2 (not reproducible in simulator)
  // `in` check used to prevent JIT error (gh-2145)
  // hasOwn isn't used here due to false negatives
  // regarding Nodelist length in IE
  var length = !!obj && "length" in obj && obj.length,
    type = jQuery.type( obj );

  if ( type === "function" || jQuery.isWindow( obj ) ) {
    return false;
  }

  return type === "array" || length === 0 ||
    typeof length === "number" && length > 0 && ( length - 1 ) in obj;
}
var Sizzle =
/*!
 * Sizzle CSS Selector Engine v2.2.1
 * http://sizzlejs.com/
 *
 * Copyright jQuery Foundation and other contributors
 * Released under the MIT license
 * http://jquery.org/license
 *
 * Date: 2015-10-17
 */
(function( window ) {

var i,
  support,
  Expr,
  getText,
  isXML,
  tokenize,
  compile,
  select,
  outermostContext,
  sortInput,
  hasDuplicate,

  // Local document vars
  setDocument,
  document,
  docElem,
  documentIsHTML,
  rbuggyQSA,
  rbuggyMatches,
  matches,
  contains,

  // Instance-specific data
  expando = "sizzle" + 1 * new Date(),
  preferredDoc = window.document,
  dirruns = 0,
  done = 0,
  classCache = createCache(),
  tokenCache = createCache(),
  compilerCache = createCache(),
  sortOrder = function( a, b ) {
    if ( a === b ) {
      hasDuplicate = true;
    }
    return 0;
  },

  // General-purpose constants
  MAX_NEGATIVE = 1 << 31,

  // Instance methods
  hasOwn = ({}).hasOwnProperty,
  arr = [],
  pop = arr.pop,
  push_native = arr.push,
  push = arr.push,
  slice = arr.slice,
  // Use a stripped-down indexOf as it's faster than native
  // http://jsperf.com/thor-indexof-vs-for/5
  indexOf = function( list, elem ) {
    var i = 0,
      len = list.length;
    for ( ; i < len; i++ ) {
      if ( list[i] === elem ) {
        return i;
      }
    }
    return -1;
  },

  booleans = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",

  // Regular expressions

  // http://www.w3.org/TR/css3-selectors/#whitespace
  whitespace = "[\\x20\\t\\r\\n\\f]",

  // http://www.w3.org/TR/CSS21/syndata.html#value-def-identifier
  identifier = "(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+",

  // Attribute selectors: http://www.w3.org/TR/selectors/#attribute-selectors
  attributes = "\\[" + whitespace + "*(" + identifier + ")(?:" + whitespace +
    // Operator (capture 2)
    "*([*^$|!~]?=)" + whitespace +
    // "Attribute values must be CSS identifiers [capture 5] or strings [capture 3 or capture 4]"
    "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + identifier + "))|)" + whitespace +
    "*\\]",

  pseudos = ":(" + identifier + ")(?:\\((" +
    // To reduce the number of selectors needing tokenize in the preFilter, prefer arguments:
    // 1. quoted (capture 3; capture 4 or capture 5)
    "('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|" +
    // 2. simple (capture 6)
    "((?:\\\\.|[^\\\\()[\\]]|" + attributes + ")*)|" +
    // 3. anything else (capture 2)
    ".*" +
    ")\\)|)",

  // Leading and non-escaped trailing whitespace, capturing some non-whitespace characters preceding the latter
  rwhitespace = new RegExp( whitespace + "+", "g" ),
  rtrim = new RegExp( "^" + whitespace + "+|((?:^|[^\\\\])(?:\\\\.)*)" + whitespace + "+$", "g" ),

  rcomma = new RegExp( "^" + whitespace + "*," + whitespace + "*" ),
  rcombinators = new RegExp( "^" + whitespace + "*([>+~]|" + whitespace + ")" + whitespace + "*" ),

  rattributeQuotes = new RegExp( "=" + whitespace + "*([^\\]'\"]*?)" + whitespace + "*\\]", "g" ),

  rpseudo = new RegExp( pseudos ),
  ridentifier = new RegExp( "^" + identifier + "$" ),

  matchExpr = {
    "ID": new RegExp( "^#(" + identifier + ")" ),
    "CLASS": new RegExp( "^\\.(" + identifier + ")" ),
    "TAG": new RegExp( "^(" + identifier + "|[*])" ),
    "ATTR": new RegExp( "^" + attributes ),
    "PSEUDO": new RegExp( "^" + pseudos ),
    "CHILD": new RegExp( "^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + whitespace +
      "*(even|odd|(([+-]|)(\\d*)n|)" + whitespace + "*(?:([+-]|)" + whitespace +
      "*(\\d+)|))" + whitespace + "*\\)|)", "i" ),
    "bool": new RegExp( "^(?:" + booleans + ")$", "i" ),
    // For use in libraries implementing .is()
    // We use this for POS matching in `select`
    "needsContext": new RegExp( "^" + whitespace + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" +
      whitespace + "*((?:-\\d)?\\d*)" + whitespace + "*\\)|)(?=[^-]|$)", "i" )
  },

  rinputs = /^(?:input|select|textarea|button)$/i,
  rheader = /^h\d$/i,

  rnative = /^[^{]+\{\s*\[native \w/,

  // Easily-parseable/retrievable ID or TAG or CLASS selectors
  rquickExpr = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,

  rsibling = /[+~]/,
  rescape = /'|\\/g,

  // CSS escapes http://www.w3.org/TR/CSS21/syndata.html#escaped-characters
  runescape = new RegExp( "\\\\([\\da-f]{1,6}" + whitespace + "?|(" + whitespace + ")|.)", "ig" ),
  funescape = function( _, escaped, escapedWhitespace ) {
    var high = "0x" + escaped - 0x10000;
    // NaN means non-codepoint
    // Support: Firefox<24
    // Workaround erroneous numeric interpretation of +"0x"
    return high !== high || escapedWhitespace ?
      escaped :
      high < 0 ?
        // BMP codepoint
        String.fromCharCode( high + 0x10000 ) :
        // Supplemental Plane codepoint (surrogate pair)
        String.fromCharCode( high >> 10 | 0xD800, high & 0x3FF | 0xDC00 );
  },

  // Used for iframes
  // See setDocument()
  // Removing the function wrapper causes a "Permission Denied"
  // error in IE
  unloadHandler = function() {
    setDocument();
  };

// Optimize for push.apply( _, NodeList )
try {
  push.apply(
    (arr = slice.call( preferredDoc.childNodes )),
    preferredDoc.childNodes
  );
  // Support: Android<4.0
  // Detect silently failing push.apply
  arr[ preferredDoc.childNodes.length ].nodeType;
} catch ( e ) {
  push = { apply: arr.length ?

    // Leverage slice if possible
    function( target, els ) {
      push_native.apply( target, slice.call(els) );
    } :

    // Support: IE<9
    // Otherwise append directly
    function( target, els ) {
      var j = target.length,
        i = 0;
      // Can't trust NodeList.length
      while ( (target[j++] = els[i++]) ) {}
      target.length = j - 1;
    }
  };
}

function Sizzle( selector, context, results, seed ) {
  var m, i, elem, nid, nidselect, match, groups, newSelector,
    newContext = context && context.ownerDocument,

    // nodeType defaults to 9, since context defaults to document
    nodeType = context ? context.nodeType : 9;

  results = results || [];

  // Return early from calls with invalid selector or context
  if ( typeof selector !== "string" || !selector ||
    nodeType !== 1 && nodeType !== 9 && nodeType !== 11 ) {

    return results;
  }

  // Try to shortcut find operations (as opposed to filters) in HTML documents
  if ( !seed ) {

    if ( ( context ? context.ownerDocument || context : preferredDoc ) !== document ) {
      setDocument( context );
    }
    context = context || document;

    if ( documentIsHTML ) {

      // If the selector is sufficiently simple, try using a "get*By*" DOM method
      // (excepting DocumentFragment context, where the methods don't exist)
      if ( nodeType !== 11 && (match = rquickExpr.exec( selector )) ) {

        // ID selector
        if ( (m = match[1]) ) {

          // Document context
          if ( nodeType === 9 ) {
            if ( (elem = context.getElementById( m )) ) {

              // Support: IE, Opera, Webkit
              // TODO: identify versions
              // getElementById can match elements by name instead of ID
              if ( elem.id === m ) {
                results.push( elem );
                return results;
              }
            } else {
              return results;
            }

          // Element context
          } else {

            // Support: IE, Opera, Webkit
            // TODO: identify versions
            // getElementById can match elements by name instead of ID
            if ( newContext && (elem = newContext.getElementById( m )) &&
              contains( context, elem ) &&
              elem.id === m ) {

              results.push( elem );
              return results;
            }
          }

        // Type selector
        } else if ( match[2] ) {
          push.apply( results, context.getElementsByTagName( selector ) );
          return results;

        // Class selector
        } else if ( (m = match[3]) && support.getElementsByClassName &&
          context.getElementsByClassName ) {

          push.apply( results, context.getElementsByClassName( m ) );
          return results;
        }
      }

      // Take advantage of querySelectorAll
      if ( support.qsa &&
        !compilerCache[ selector + " " ] &&
        (!rbuggyQSA || !rbuggyQSA.test( selector )) ) {

        if ( nodeType !== 1 ) {
          newContext = context;
          newSelector = selector;

        // qSA looks outside Element context, which is not what we want
        // Thanks to Andrew Dupont for this workaround technique
        // Support: IE <=8
        // Exclude object elements
        } else if ( context.nodeName.toLowerCase() !== "object" ) {

          // Capture the context ID, setting it first if necessary
          if ( (nid = context.getAttribute( "id" )) ) {
            nid = nid.replace( rescape, "\\$&" );
          } else {
            context.setAttribute( "id", (nid = expando) );
          }

          // Prefix every selector in the list
          groups = tokenize( selector );
          i = groups.length;
          nidselect = ridentifier.test( nid ) ? "#" + nid : "[id='" + nid + "']";
          while ( i-- ) {
            groups[i] = nidselect + " " + toSelector( groups[i] );
          }
          newSelector = groups.join( "," );

          // Expand context for sibling selectors
          newContext = rsibling.test( selector ) && testContext( context.parentNode ) ||
            context;
        }

        if ( newSelector ) {
          try {
            push.apply( results,
              newContext.querySelectorAll( newSelector )
            );
            return results;
          } catch ( qsaError ) {
          } finally {
            if ( nid === expando ) {
              context.removeAttribute( "id" );
            }
          }
        }
      }
    }
  }

  // All others
  return select( selector.replace( rtrim, "$1" ), context, results, seed );
}

/**
 * Create key-value caches of limited size
 * @returns {function(string, object)} Returns the Object data after storing it on itself with
 *  property name the (space-suffixed) string and (if the cache is larger than Expr.cacheLength)
 *  deleting the oldest entry
 */
function createCache() {
  var keys = [];

  function cache( key, value ) {
    // Use (key + " ") to avoid collision with native prototype properties (see Issue #157)
    if ( keys.push( key + " " ) > Expr.cacheLength ) {
      // Only keep the most recent entries
      delete cache[ keys.shift() ];
    }
    return (cache[ key + " " ] = value);
  }
  return cache;
}

/**
 * Mark a function for special use by Sizzle
 * @param {Function} fn The function to mark
 */
function markFunction( fn ) {
  fn[ expando ] = true;
  return fn;
}

/**
 * Support testing using an element
 * @param {Function} fn Passed the created div and expects a boolean result
 */
function assert( fn ) {
  var div = document.createElement("div");

  try {
    return !!fn( div );
  } catch (e) {
    return false;
  } finally {
    // Remove from its parent by default
    if ( div.parentNode ) {
      div.parentNode.removeChild( div );
    }
    // release memory in IE
    div = null;
  }
}

/**
 * Adds the same handler for all of the specified attrs
 * @param {String} attrs Pipe-separated list of attributes
 * @param {Function} handler The method that will be applied
 */
function addHandle( attrs, handler ) {
  var arr = attrs.split("|"),
    i = arr.length;

  while ( i-- ) {
    Expr.attrHandle[ arr[i] ] = handler;
  }
}

/**
 * Checks document order of two siblings
 * @param {Element} a
 * @param {Element} b
 * @returns {Number} Returns less than 0 if a precedes b, greater than 0 if a follows b
 */
function siblingCheck( a, b ) {
  var cur = b && a,
    diff = cur && a.nodeType === 1 && b.nodeType === 1 &&
      ( ~b.sourceIndex || MAX_NEGATIVE ) -
      ( ~a.sourceIndex || MAX_NEGATIVE );

  // Use IE sourceIndex if available on both nodes
  if ( diff ) {
    return diff;
  }

  // Check if b follows a
  if ( cur ) {
    while ( (cur = cur.nextSibling) ) {
      if ( cur === b ) {
        return -1;
      }
    }
  }

  return a ? 1 : -1;
}

/**
 * Returns a function to use in pseudos for input types
 * @param {String} type
 */
function createInputPseudo( type ) {
  return function( elem ) {
    var name = elem.nodeName.toLowerCase();
    return name === "input" && elem.type === type;
  };
}

/**
 * Returns a function to use in pseudos for buttons
 * @param {String} type
 */
function createButtonPseudo( type ) {
  return function( elem ) {
    var name = elem.nodeName.toLowerCase();
    return (name === "input" || name === "button") && elem.type === type;
  };
}

/**
 * Returns a function to use in pseudos for positionals
 * @param {Function} fn
 */
function createPositionalPseudo( fn ) {
  return markFunction(function( argument ) {
    argument = +argument;
    return markFunction(function( seed, matches ) {
      var j,
        matchIndexes = fn( [], seed.length, argument ),
        i = matchIndexes.length;

      // Match elements found at the specified indexes
      while ( i-- ) {
        if ( seed[ (j = matchIndexes[i]) ] ) {
          seed[j] = !(matches[j] = seed[j]);
        }
      }
    });
  });
}

/**
 * Checks a node for validity as a Sizzle context
 * @param {Element|Object=} context
 * @returns {Element|Object|Boolean} The input node if acceptable, otherwise a falsy value
 */
function testContext( context ) {
  return context && typeof context.getElementsByTagName !== "undefined" && context;
}

// Expose support vars for convenience
support = Sizzle.support = {};

/**
 * Detects XML nodes
 * @param {Element|Object} elem An element or a document
 * @returns {Boolean} True iff elem is a non-HTML XML node
 */
isXML = Sizzle.isXML = function( elem ) {
  // documentElement is verified for cases where it doesn't yet exist
  // (such as loading iframes in IE - #4833)
  var documentElement = elem && (elem.ownerDocument || elem).documentElement;
  return documentElement ? documentElement.nodeName !== "HTML" : false;
};

/**
 * Sets document-related variables once based on the current document
 * @param {Element|Object} [doc] An element or document object to use to set the document
 * @returns {Object} Returns the current document
 */
setDocument = Sizzle.setDocument = function( node ) {
  var hasCompare, parent,
    doc = node ? node.ownerDocument || node : preferredDoc;

  // Return early if doc is invalid or already selected
  if ( doc === document || doc.nodeType !== 9 || !doc.documentElement ) {
    return document;
  }

  // Update global variables
  document = doc;
  docElem = document.documentElement;
  documentIsHTML = !isXML( document );

  // Support: IE 9-11, Edge
  // Accessing iframe documents after unload throws "permission denied" errors (jQuery #13936)
  if ( (parent = document.defaultView) && parent.top !== parent ) {
    // Support: IE 11
    if ( parent.addEventListener ) {
      parent.addEventListener( "unload", unloadHandler, false );

    // Support: IE 9 - 10 only
    } else if ( parent.attachEvent ) {
      parent.attachEvent( "onunload", unloadHandler );
    }
  }

  /* Attributes
  ---------------------------------------------------------------------- */

  // Support: IE<8
  // Verify that getAttribute really returns attributes and not properties
  // (excepting IE8 booleans)
  support.attributes = assert(function( div ) {
    div.className = "i";
    return !div.getAttribute("className");
  });

  /* getElement(s)By*
  ---------------------------------------------------------------------- */

  // Check if getElementsByTagName("*") returns only elements
  support.getElementsByTagName = assert(function( div ) {
    div.appendChild( document.createComment("") );
    return !div.getElementsByTagName("*").length;
  });

  // Support: IE<9
  support.getElementsByClassName = rnative.test( document.getElementsByClassName );

  // Support: IE<10
  // Check if getElementById returns elements by name
  // The broken getElementById methods don't pick up programatically-set names,
  // so use a roundabout getElementsByName test
  support.getById = assert(function( div ) {
    docElem.appendChild( div ).id = expando;
    return !document.getElementsByName || !document.getElementsByName( expando ).length;
  });

  // ID find and filter
  if ( support.getById ) {
    Expr.find["ID"] = function( id, context ) {
      if ( typeof context.getElementById !== "undefined" && documentIsHTML ) {
        var m = context.getElementById( id );
        return m ? [ m ] : [];
      }
    };
    Expr.filter["ID"] = function( id ) {
      var attrId = id.replace( runescape, funescape );
      return function( elem ) {
        return elem.getAttribute("id") === attrId;
      };
    };
  } else {
    // Support: IE6/7
    // getElementById is not reliable as a find shortcut
    delete Expr.find["ID"];

    Expr.filter["ID"] =  function( id ) {
      var attrId = id.replace( runescape, funescape );
      return function( elem ) {
        var node = typeof elem.getAttributeNode !== "undefined" &&
          elem.getAttributeNode("id");
        return node && node.value === attrId;
      };
    };
  }

  // Tag
  Expr.find["TAG"] = support.getElementsByTagName ?
    function( tag, context ) {
      if ( typeof context.getElementsByTagName !== "undefined" ) {
        return context.getElementsByTagName( tag );

      // DocumentFragment nodes don't have gEBTN
      } else if ( support.qsa ) {
        return context.querySelectorAll( tag );
      }
    } :

    function( tag, context ) {
      var elem,
        tmp = [],
        i = 0,
        // By happy coincidence, a (broken) gEBTN appears on DocumentFragment nodes too
        results = context.getElementsByTagName( tag );

      // Filter out possible comments
      if ( tag === "*" ) {
        while ( (elem = results[i++]) ) {
          if ( elem.nodeType === 1 ) {
            tmp.push( elem );
          }
        }

        return tmp;
      }
      return results;
    };

  // Class
  Expr.find["CLASS"] = support.getElementsByClassName && function( className, context ) {
    if ( typeof context.getElementsByClassName !== "undefined" && documentIsHTML ) {
      return context.getElementsByClassName( className );
    }
  };

  /* QSA/matchesSelector
  ---------------------------------------------------------------------- */

  // QSA and matchesSelector support

  // matchesSelector(:active) reports false when true (IE9/Opera 11.5)
  rbuggyMatches = [];

  // qSa(:focus) reports false when true (Chrome 21)
  // We allow this because of a bug in IE8/9 that throws an error
  // whenever `document.activeElement` is accessed on an iframe
  // So, we allow :focus to pass through QSA all the time to avoid the IE error
  // See http://bugs.jquery.com/ticket/13378
  rbuggyQSA = [];

  if ( (support.qsa = rnative.test( document.querySelectorAll )) ) {
    // Build QSA regex
    // Regex strategy adopted from Diego Perini
    assert(function( div ) {
      // Select is set to empty string on purpose
      // This is to test IE's treatment of not explicitly
      // setting a boolean content attribute,
      // since its presence should be enough
      // http://bugs.jquery.com/ticket/12359
      docElem.appendChild( div ).innerHTML = "<a id='" + expando + "'></a>" +
        "<select id='" + expando + "-\r\\' msallowcapture=''>" +
        "<option selected=''></option></select>";

      // Support: IE8, Opera 11-12.16
      // Nothing should be selected when empty strings follow ^= or $= or *=
      // The test attribute must be unknown in Opera but "safe" for WinRT
      // http://msdn.microsoft.com/en-us/library/ie/hh465388.aspx#attribute_section
      if ( div.querySelectorAll("[msallowcapture^='']").length ) {
        rbuggyQSA.push( "[*^$]=" + whitespace + "*(?:''|\"\")" );
      }

      // Support: IE8
      // Boolean attributes and "value" are not treated correctly
      if ( !div.querySelectorAll("[selected]").length ) {
        rbuggyQSA.push( "\\[" + whitespace + "*(?:value|" + booleans + ")" );
      }

      // Support: Chrome<29, Android<4.4, Safari<7.0+, iOS<7.0+, PhantomJS<1.9.8+
      if ( !div.querySelectorAll( "[id~=" + expando + "-]" ).length ) {
        rbuggyQSA.push("~=");
      }

      // Webkit/Opera - :checked should return selected option elements
      // http://www.w3.org/TR/2011/REC-css3-selectors-20110929/#checked
      // IE8 throws error here and will not see later tests
      if ( !div.querySelectorAll(":checked").length ) {
        rbuggyQSA.push(":checked");
      }

      // Support: Safari 8+, iOS 8+
      // https://bugs.webkit.org/show_bug.cgi?id=136851
      // In-page `selector#id sibing-combinator selector` fails
      if ( !div.querySelectorAll( "a#" + expando + "+*" ).length ) {
        rbuggyQSA.push(".#.+[+~]");
      }
    });

    assert(function( div ) {
      // Support: Windows 8 Native Apps
      // The type and name attributes are restricted during .innerHTML assignment
      var input = document.createElement("input");
      input.setAttribute( "type", "hidden" );
      div.appendChild( input ).setAttribute( "name", "D" );

      // Support: IE8
      // Enforce case-sensitivity of name attribute
      if ( div.querySelectorAll("[name=d]").length ) {
        rbuggyQSA.push( "name" + whitespace + "*[*^$|!~]?=" );
      }

      // FF 3.5 - :enabled/:disabled and hidden elements (hidden elements are still enabled)
      // IE8 throws error here and will not see later tests
      if ( !div.querySelectorAll(":enabled").length ) {
        rbuggyQSA.push( ":enabled", ":disabled" );
      }

      // Opera 10-11 does not throw on post-comma invalid pseudos
      div.querySelectorAll("*,:x");
      rbuggyQSA.push(",.*:");
    });
  }

  if ( (support.matchesSelector = rnative.test( (matches = docElem.matches ||
    docElem.webkitMatchesSelector ||
    docElem.mozMatchesSelector ||
    docElem.oMatchesSelector ||
    docElem.msMatchesSelector) )) ) {

    assert(function( div ) {
      // Check to see if it's possible to do matchesSelector
      // on a disconnected node (IE 9)
      support.disconnectedMatch = matches.call( div, "div" );

      // This should fail with an exception
      // Gecko does not error, returns false instead
      matches.call( div, "[s!='']:x" );
      rbuggyMatches.push( "!=", pseudos );
    });
  }

  rbuggyQSA = rbuggyQSA.length && new RegExp( rbuggyQSA.join("|") );
  rbuggyMatches = rbuggyMatches.length && new RegExp( rbuggyMatches.join("|") );

  /* Contains
  ---------------------------------------------------------------------- */
  hasCompare = rnative.test( docElem.compareDocumentPosition );

  // Element contains another
  // Purposefully self-exclusive
  // As in, an element does not contain itself
  contains = hasCompare || rnative.test( docElem.contains ) ?
    function( a, b ) {
      var adown = a.nodeType === 9 ? a.documentElement : a,
        bup = b && b.parentNode;
      return a === bup || !!( bup && bup.nodeType === 1 && (
        adown.contains ?
          adown.contains( bup ) :
          a.compareDocumentPosition && a.compareDocumentPosition( bup ) & 16
      ));
    } :
    function( a, b ) {
      if ( b ) {
        while ( (b = b.parentNode) ) {
          if ( b === a ) {
            return true;
          }
        }
      }
      return false;
    };

  /* Sorting
  ---------------------------------------------------------------------- */

  // Document order sorting
  sortOrder = hasCompare ?
  function( a, b ) {

    // Flag for duplicate removal
    if ( a === b ) {
      hasDuplicate = true;
      return 0;
    }

    // Sort on method existence if only one input has compareDocumentPosition
    var compare = !a.compareDocumentPosition - !b.compareDocumentPosition;
    if ( compare ) {
      return compare;
    }

    // Calculate position if both inputs belong to the same document
    compare = ( a.ownerDocument || a ) === ( b.ownerDocument || b ) ?
      a.compareDocumentPosition( b ) :

      // Otherwise we know they are disconnected
      1;

    // Disconnected nodes
    if ( compare & 1 ||
      (!support.sortDetached && b.compareDocumentPosition( a ) === compare) ) {

      // Choose the first element that is related to our preferred document
      if ( a === document || a.ownerDocument === preferredDoc && contains(preferredDoc, a) ) {
        return -1;
      }
      if ( b === document || b.ownerDocument === preferredDoc && contains(preferredDoc, b) ) {
        return 1;
      }

      // Maintain original order
      return sortInput ?
        ( indexOf( sortInput, a ) - indexOf( sortInput, b ) ) :
        0;
    }

    return compare & 4 ? -1 : 1;
  } :
  function( a, b ) {
    // Exit early if the nodes are identical
    if ( a === b ) {
      hasDuplicate = true;
      return 0;
    }

    var cur,
      i = 0,
      aup = a.parentNode,
      bup = b.parentNode,
      ap = [ a ],
      bp = [ b ];

    // Parentless nodes are either documents or disconnected
    if ( !aup || !bup ) {
      return a === document ? -1 :
        b === document ? 1 :
        aup ? -1 :
        bup ? 1 :
        sortInput ?
        ( indexOf( sortInput, a ) - indexOf( sortInput, b ) ) :
        0;

    // If the nodes are siblings, we can do a quick check
    } else if ( aup === bup ) {
      return siblingCheck( a, b );
    }

    // Otherwise we need full lists of their ancestors for comparison
    cur = a;
    while ( (cur = cur.parentNode) ) {
      ap.unshift( cur );
    }
    cur = b;
    while ( (cur = cur.parentNode) ) {
      bp.unshift( cur );
    }

    // Walk down the tree looking for a discrepancy
    while ( ap[i] === bp[i] ) {
      i++;
    }

    return i ?
      // Do a sibling check if the nodes have a common ancestor
      siblingCheck( ap[i], bp[i] ) :

      // Otherwise nodes in our document sort first
      ap[i] === preferredDoc ? -1 :
      bp[i] === preferredDoc ? 1 :
      0;
  };

  return document;
};

Sizzle.matches = function( expr, elements ) {
  return Sizzle( expr, null, null, elements );
};

Sizzle.matchesSelector = function( elem, expr ) {
  // Set document vars if needed
  if ( ( elem.ownerDocument || elem ) !== document ) {
    setDocument( elem );
  }

  // Make sure that attribute selectors are quoted
  expr = expr.replace( rattributeQuotes, "='$1']" );

  if ( support.matchesSelector && documentIsHTML &&
    !compilerCache[ expr + " " ] &&
    ( !rbuggyMatches || !rbuggyMatches.test( expr ) ) &&
    ( !rbuggyQSA     || !rbuggyQSA.test( expr ) ) ) {

    try {
      var ret = matches.call( elem, expr );

      // IE 9's matchesSelector returns false on disconnected nodes
      if ( ret || support.disconnectedMatch ||
          // As well, disconnected nodes are said to be in a document
          // fragment in IE 9
          elem.document && elem.document.nodeType !== 11 ) {
        return ret;
      }
    } catch (e) {}
  }

  return Sizzle( expr, document, null, [ elem ] ).length > 0;
};

Sizzle.contains = function( context, elem ) {
  // Set document vars if needed
  if ( ( context.ownerDocument || context ) !== document ) {
    setDocument( context );
  }
  return contains( context, elem );
};

Sizzle.attr = function( elem, name ) {
  // Set document vars if needed
  if ( ( elem.ownerDocument || elem ) !== document ) {
    setDocument( elem );
  }

  var fn = Expr.attrHandle[ name.toLowerCase() ],
    // Don't get fooled by Object.prototype properties (jQuery #13807)
    val = fn && hasOwn.call( Expr.attrHandle, name.toLowerCase() ) ?
      fn( elem, name, !documentIsHTML ) :
      undefined;

  return val !== undefined ?
    val :
    support.attributes || !documentIsHTML ?
      elem.getAttribute( name ) :
      (val = elem.getAttributeNode(name)) && val.specified ?
        val.value :
        null;
};

Sizzle.error = function( msg ) {
  throw new Error( "Syntax error, unrecognized expression: " + msg );
};

/**
 * Document sorting and removing duplicates
 * @param {ArrayLike} results
 */
Sizzle.uniqueSort = function( results ) {
  var elem,
    duplicates = [],
    j = 0,
    i = 0;

  // Unless we *know* we can detect duplicates, assume their presence
  hasDuplicate = !support.detectDuplicates;
  sortInput = !support.sortStable && results.slice( 0 );
  results.sort( sortOrder );

  if ( hasDuplicate ) {
    while ( (elem = results[i++]) ) {
      if ( elem === results[ i ] ) {
        j = duplicates.push( i );
      }
    }
    while ( j-- ) {
      results.splice( duplicates[ j ], 1 );
    }
  }

  // Clear input after sorting to release objects
  // See https://github.com/jquery/sizzle/pull/225
  sortInput = null;

  return results;
};

/**
 * Utility function for retrieving the text value of an array of DOM nodes
 * @param {Array|Element} elem
 */
getText = Sizzle.getText = function( elem ) {
  var node,
    ret = "",
    i = 0,
    nodeType = elem.nodeType;

  if ( !nodeType ) {
    // If no nodeType, this is expected to be an array
    while ( (node = elem[i++]) ) {
      // Do not traverse comment nodes
      ret += getText( node );
    }
  } else if ( nodeType === 1 || nodeType === 9 || nodeType === 11 ) {
    // Use textContent for elements
    // innerText usage removed for consistency of new lines (jQuery #11153)
    if ( typeof elem.textContent === "string" ) {
      return elem.textContent;
    } else {
      // Traverse its children
      for ( elem = elem.firstChild; elem; elem = elem.nextSibling ) {
        ret += getText( elem );
      }
    }
  } else if ( nodeType === 3 || nodeType === 4 ) {
    return elem.nodeValue;
  }
  // Do not include comment or processing instruction nodes

  return ret;
};

Expr = Sizzle.selectors = {

  // Can be adjusted by the user
  cacheLength: 50,

  createPseudo: markFunction,

  match: matchExpr,

  attrHandle: {},

  find: {},

  relative: {
    ">": { dir: "parentNode", first: true },
    " ": { dir: "parentNode" },
    "+": { dir: "previousSibling", first: true },
    "~": { dir: "previousSibling" }
  },

  preFilter: {
    "ATTR": function( match ) {
      match[1] = match[1].replace( runescape, funescape );

      // Move the given value to match[3] whether quoted or unquoted
      match[3] = ( match[3] || match[4] || match[5] || "" ).replace( runescape, funescape );

      if ( match[2] === "~=" ) {
        match[3] = " " + match[3] + " ";
      }

      return match.slice( 0, 4 );
    },

    "CHILD": function( match ) {
      /* matches from matchExpr["CHILD"]
        1 type (only|nth|...)
        2 what (child|of-type)
        3 argument (even|odd|\d*|\d*n([+-]\d+)?|...)
        4 xn-component of xn+y argument ([+-]?\d*n|)
        5 sign of xn-component
        6 x of xn-component
        7 sign of y-component
        8 y of y-component
      */
      match[1] = match[1].toLowerCase();

      if ( match[1].slice( 0, 3 ) === "nth" ) {
        // nth-* requires argument
        if ( !match[3] ) {
          Sizzle.error( match[0] );
        }

        // numeric x and y parameters for Expr.filter.CHILD
        // remember that false/true cast respectively to 0/1
        match[4] = +( match[4] ? match[5] + (match[6] || 1) : 2 * ( match[3] === "even" || match[3] === "odd" ) );
        match[5] = +( ( match[7] + match[8] ) || match[3] === "odd" );

      // other types prohibit arguments
      } else if ( match[3] ) {
        Sizzle.error( match[0] );
      }

      return match;
    },

    "PSEUDO": function( match ) {
      var excess,
        unquoted = !match[6] && match[2];

      if ( matchExpr["CHILD"].test( match[0] ) ) {
        return null;
      }

      // Accept quoted arguments as-is
      if ( match[3] ) {
        match[2] = match[4] || match[5] || "";

      // Strip excess characters from unquoted arguments
      } else if ( unquoted && rpseudo.test( unquoted ) &&
        // Get excess from tokenize (recursively)
        (excess = tokenize( unquoted, true )) &&
        // advance to the next closing parenthesis
        (excess = unquoted.indexOf( ")", unquoted.length - excess ) - unquoted.length) ) {

        // excess is a negative index
        match[0] = match[0].slice( 0, excess );
        match[2] = unquoted.slice( 0, excess );
      }

      // Return only captures needed by the pseudo filter method (type and argument)
      return match.slice( 0, 3 );
    }
  },

  filter: {

    "TAG": function( nodeNameSelector ) {
      var nodeName = nodeNameSelector.replace( runescape, funescape ).toLowerCase();
      return nodeNameSelector === "*" ?
        function() { return true; } :
        function( elem ) {
          return elem.nodeName && elem.nodeName.toLowerCase() === nodeName;
        };
    },

    "CLASS": function( className ) {
      var pattern = classCache[ className + " " ];

      return pattern ||
        (pattern = new RegExp( "(^|" + whitespace + ")" + className + "(" + whitespace + "|$)" )) &&
        classCache( className, function( elem ) {
          return pattern.test( typeof elem.className === "string" && elem.className || typeof elem.getAttribute !== "undefined" && elem.getAttribute("class") || "" );
        });
    },

    "ATTR": function( name, operator, check ) {
      return function( elem ) {
        var result = Sizzle.attr( elem, name );

        if ( result == null ) {
          return operator === "!=";
        }
        if ( !operator ) {
          return true;
        }

        result += "";

        return operator === "=" ? result === check :
          operator === "!=" ? result !== check :
          operator === "^=" ? check && result.indexOf( check ) === 0 :
          operator === "*=" ? check && result.indexOf( check ) > -1 :
          operator === "$=" ? check && result.slice( -check.length ) === check :
          operator === "~=" ? ( " " + result.replace( rwhitespace, " " ) + " " ).indexOf( check ) > -1 :
          operator === "|=" ? result === check || result.slice( 0, check.length + 1 ) === check + "-" :
          false;
      };
    },

    "CHILD": function( type, what, argument, first, last ) {
      var simple = type.slice( 0, 3 ) !== "nth",
        forward = type.slice( -4 ) !== "last",
        ofType = what === "of-type";

      return first === 1 && last === 0 ?

        // Shortcut for :nth-*(n)
        function( elem ) {
          return !!elem.parentNode;
        } :

        function( elem, context, xml ) {
          var cache, uniqueCache, outerCache, node, nodeIndex, start,
            dir = simple !== forward ? "nextSibling" : "previousSibling",
            parent = elem.parentNode,
            name = ofType && elem.nodeName.toLowerCase(),
            useCache = !xml && !ofType,
            diff = false;

          if ( parent ) {

            // :(first|last|only)-(child|of-type)
            if ( simple ) {
              while ( dir ) {
                node = elem;
                while ( (node = node[ dir ]) ) {
                  if ( ofType ?
                    node.nodeName.toLowerCase() === name :
                    node.nodeType === 1 ) {

                    return false;
                  }
                }
                // Reverse direction for :only-* (if we haven't yet done so)
                start = dir = type === "only" && !start && "nextSibling";
              }
              return true;
            }

            start = [ forward ? parent.firstChild : parent.lastChild ];

            // non-xml :nth-child(...) stores cache data on `parent`
            if ( forward && useCache ) {

              // Seek `elem` from a previously-cached index

              // ...in a gzip-friendly way
              node = parent;
              outerCache = node[ expando ] || (node[ expando ] = {});

              // Support: IE <9 only
              // Defend against cloned attroperties (jQuery gh-1709)
              uniqueCache = outerCache[ node.uniqueID ] ||
                (outerCache[ node.uniqueID ] = {});

              cache = uniqueCache[ type ] || [];
              nodeIndex = cache[ 0 ] === dirruns && cache[ 1 ];
              diff = nodeIndex && cache[ 2 ];
              node = nodeIndex && parent.childNodes[ nodeIndex ];

              while ( (node = ++nodeIndex && node && node[ dir ] ||

                // Fallback to seeking `elem` from the start
                (diff = nodeIndex = 0) || start.pop()) ) {

                // When found, cache indexes on `parent` and break
                if ( node.nodeType === 1 && ++diff && node === elem ) {
                  uniqueCache[ type ] = [ dirruns, nodeIndex, diff ];
                  break;
                }
              }

            } else {
              // Use previously-cached element index if available
              if ( useCache ) {
                // ...in a gzip-friendly way
                node = elem;
                outerCache = node[ expando ] || (node[ expando ] = {});

                // Support: IE <9 only
                // Defend against cloned attroperties (jQuery gh-1709)
                uniqueCache = outerCache[ node.uniqueID ] ||
                  (outerCache[ node.uniqueID ] = {});

                cache = uniqueCache[ type ] || [];
                nodeIndex = cache[ 0 ] === dirruns && cache[ 1 ];
                diff = nodeIndex;
              }

              // xml :nth-child(...)
              // or :nth-last-child(...) or :nth(-last)?-of-type(...)
              if ( diff === false ) {
                // Use the same loop as above to seek `elem` from the start
                while ( (node = ++nodeIndex && node && node[ dir ] ||
                  (diff = nodeIndex = 0) || start.pop()) ) {

                  if ( ( ofType ?
                    node.nodeName.toLowerCase() === name :
                    node.nodeType === 1 ) &&
                    ++diff ) {

                    // Cache the index of each encountered element
                    if ( useCache ) {
                      outerCache = node[ expando ] || (node[ expando ] = {});

                      // Support: IE <9 only
                      // Defend against cloned attroperties (jQuery gh-1709)
                      uniqueCache = outerCache[ node.uniqueID ] ||
                        (outerCache[ node.uniqueID ] = {});

                      uniqueCache[ type ] = [ dirruns, diff ];
                    }

                    if ( node === elem ) {
                      break;
                    }
                  }
                }
              }
            }

            // Incorporate the offset, then check against cycle size
            diff -= last;
            return diff === first || ( diff % first === 0 && diff / first >= 0 );
          }
        };
    },

    "PSEUDO": function( pseudo, argument ) {
      // pseudo-class names are case-insensitive
      // http://www.w3.org/TR/selectors/#pseudo-classes
      // Prioritize by case sensitivity in case custom pseudos are added with uppercase letters
      // Remember that setFilters inherits from pseudos
      var args,
        fn = Expr.pseudos[ pseudo ] || Expr.setFilters[ pseudo.toLowerCase() ] ||
          Sizzle.error( "unsupported pseudo: " + pseudo );

      // The user may use createPseudo to indicate that
      // arguments are needed to create the filter function
      // just as Sizzle does
      if ( fn[ expando ] ) {
        return fn( argument );
      }

      // But maintain support for old signatures
      if ( fn.length > 1 ) {
        args = [ pseudo, pseudo, "", argument ];
        return Expr.setFilters.hasOwnProperty( pseudo.toLowerCase() ) ?
          markFunction(function( seed, matches ) {
            var idx,
              matched = fn( seed, argument ),
              i = matched.length;
            while ( i-- ) {
              idx = indexOf( seed, matched[i] );
              seed[ idx ] = !( matches[ idx ] = matched[i] );
            }
          }) :
          function( elem ) {
            return fn( elem, 0, args );
          };
      }

      return fn;
    }
  },

  pseudos: {
    // Potentially complex pseudos
    "not": markFunction(function( selector ) {
      // Trim the selector passed to compile
      // to avoid treating leading and trailing
      // spaces as combinators
      var input = [],
        results = [],
        matcher = compile( selector.replace( rtrim, "$1" ) );

      return matcher[ expando ] ?
        markFunction(function( seed, matches, context, xml ) {
          var elem,
            unmatched = matcher( seed, null, xml, [] ),
            i = seed.length;

          // Match elements unmatched by `matcher`
          while ( i-- ) {
            if ( (elem = unmatched[i]) ) {
              seed[i] = !(matches[i] = elem);
            }
          }
        }) :
        function( elem, context, xml ) {
          input[0] = elem;
          matcher( input, null, xml, results );
          // Don't keep the element (issue #299)
          input[0] = null;
          return !results.pop();
        };
    }),

    "has": markFunction(function( selector ) {
      return function( elem ) {
        return Sizzle( selector, elem ).length > 0;
      };
    }),

    "contains": markFunction(function( text ) {
      text = text.replace( runescape, funescape );
      return function( elem ) {
        return ( elem.textContent || elem.innerText || getText( elem ) ).indexOf( text ) > -1;
      };
    }),

    // "Whether an element is represented by a :lang() selector
    // is based solely on the element's language value
    // being equal to the identifier C,
    // or beginning with the identifier C immediately followed by "-".
    // The matching of C against the element's language value is performed case-insensitively.
    // The identifier C does not have to be a valid language name."
    // http://www.w3.org/TR/selectors/#lang-pseudo
    "lang": markFunction( function( lang ) {
      // lang value must be a valid identifier
      if ( !ridentifier.test(lang || "") ) {
        Sizzle.error( "unsupported lang: " + lang );
      }
      lang = lang.replace( runescape, funescape ).toLowerCase();
      return function( elem ) {
        var elemLang;
        do {
          if ( (elemLang = documentIsHTML ?
            elem.lang :
            elem.getAttribute("xml:lang") || elem.getAttribute("lang")) ) {

            elemLang = elemLang.toLowerCase();
            return elemLang === lang || elemLang.indexOf( lang + "-" ) === 0;
          }
        } while ( (elem = elem.parentNode) && elem.nodeType === 1 );
        return false;
      };
    }),

    // Miscellaneous
    "target": function( elem ) {
      var hash = window.location && window.location.hash;
      return hash && hash.slice( 1 ) === elem.id;
    },

    "root": function( elem ) {
      return elem === docElem;
    },

    "focus": function( elem ) {
      return elem === document.activeElement && (!document.hasFocus || document.hasFocus()) && !!(elem.type || elem.href || ~elem.tabIndex);
    },

    // Boolean properties
    "enabled": function( elem ) {
      return elem.disabled === false;
    },

    "disabled": function( elem ) {
      return elem.disabled === true;
    },

    "checked": function( elem ) {
      // In CSS3, :checked should return both checked and selected elements
      // http://www.w3.org/TR/2011/REC-css3-selectors-20110929/#checked
      var nodeName = elem.nodeName.toLowerCase();
      return (nodeName === "input" && !!elem.checked) || (nodeName === "option" && !!elem.selected);
    },

    "selected": function( elem ) {
      // Accessing this property makes selected-by-default
      // options in Safari work properly
      if ( elem.parentNode ) {
        elem.parentNode.selectedIndex;
      }

      return elem.selected === true;
    },

    // Contents
    "empty": function( elem ) {
      // http://www.w3.org/TR/selectors/#empty-pseudo
      // :empty is negated by element (1) or content nodes (text: 3; cdata: 4; entity ref: 5),
      //   but not by others (comment: 8; processing instruction: 7; etc.)
      // nodeType < 6 works because attributes (2) do not appear as children
      for ( elem = elem.firstChild; elem; elem = elem.nextSibling ) {
        if ( elem.nodeType < 6 ) {
          return false;
        }
      }
      return true;
    },

    "parent": function( elem ) {
      return !Expr.pseudos["empty"]( elem );
    },

    // Element/input types
    "header": function( elem ) {
      return rheader.test( elem.nodeName );
    },

    "input": function( elem ) {
      return rinputs.test( elem.nodeName );
    },

    "button": function( elem ) {
      var name = elem.nodeName.toLowerCase();
      return name === "input" && elem.type === "button" || name === "button";
    },

    "text": function( elem ) {
      var attr;
      return elem.nodeName.toLowerCase() === "input" &&
        elem.type === "text" &&

        // Support: IE<8
        // New HTML5 attribute values (e.g., "search") appear with elem.type === "text"
        ( (attr = elem.getAttribute("type")) == null || attr.toLowerCase() === "text" );
    },

    // Position-in-collection
    "first": createPositionalPseudo(function() {
      return [ 0 ];
    }),

    "last": createPositionalPseudo(function( matchIndexes, length ) {
      return [ length - 1 ];
    }),

    "eq": createPositionalPseudo(function( matchIndexes, length, argument ) {
      return [ argument < 0 ? argument + length : argument ];
    }),

    "even": createPositionalPseudo(function( matchIndexes, length ) {
      var i = 0;
      for ( ; i < length; i += 2 ) {
        matchIndexes.push( i );
      }
      return matchIndexes;
    }),

    "odd": createPositionalPseudo(function( matchIndexes, length ) {
      var i = 1;
      for ( ; i < length; i += 2 ) {
        matchIndexes.push( i );
      }
      return matchIndexes;
    }),

    "lt": createPositionalPseudo(function( matchIndexes, length, argument ) {
      var i = argument < 0 ? argument + length : argument;
      for ( ; --i >= 0; ) {
        matchIndexes.push( i );
      }
      return matchIndexes;
    }),

    "gt": createPositionalPseudo(function( matchIndexes, length, argument ) {
      var i = argument < 0 ? argument + length : argument;
      for ( ; ++i < length; ) {
        matchIndexes.push( i );
      }
      return matchIndexes;
    })
  }
};

Expr.pseudos["nth"] = Expr.pseudos["eq"];

// Add button/input type pseudos
for ( i in { radio: true, checkbox: true, file: true, password: true, image: true } ) {
  Expr.pseudos[ i ] = createInputPseudo( i );
}
for ( i in { submit: true, reset: true } ) {
  Expr.pseudos[ i ] = createButtonPseudo( i );
}

// Easy API for creating new setFilters
function setFilters() {}
setFilters.prototype = Expr.filters = Expr.pseudos;
Expr.setFilters = new setFilters();

tokenize = Sizzle.tokenize = function( selector, parseOnly ) {
  var matched, match, tokens, type,
    soFar, groups, preFilters,
    cached = tokenCache[ selector + " " ];

  if ( cached ) {
    return parseOnly ? 0 : cached.slice( 0 );
  }

  soFar = selector;
  groups = [];
  preFilters = Expr.preFilter;

  while ( soFar ) {

    // Comma and first run
    if ( !matched || (match = rcomma.exec( soFar )) ) {
      if ( match ) {
        // Don't consume trailing commas as valid
        soFar = soFar.slice( match[0].length ) || soFar;
      }
      groups.push( (tokens = []) );
    }

    matched = false;

    // Combinators
    if ( (match = rcombinators.exec( soFar )) ) {
      matched = match.shift();
      tokens.push({
        value: matched,
        // Cast descendant combinators to space
        type: match[0].replace( rtrim, " " )
      });
      soFar = soFar.slice( matched.length );
    }

    // Filters
    for ( type in Expr.filter ) {
      if ( (match = matchExpr[ type ].exec( soFar )) && (!preFilters[ type ] ||
        (match = preFilters[ type ]( match ))) ) {
        matched = match.shift();
        tokens.push({
          value: matched,
          type: type,
          matches: match
        });
        soFar = soFar.slice( matched.length );
      }
    }

    if ( !matched ) {
      break;
    }
  }

  // Return the length of the invalid excess
  // if we're just parsing
  // Otherwise, throw an error or return tokens
  return parseOnly ?
    soFar.length :
    soFar ?
      Sizzle.error( selector ) :
      // Cache the tokens
      tokenCache( selector, groups ).slice( 0 );
};

function toSelector( tokens ) {
  var i = 0,
    len = tokens.length,
    selector = "";
  for ( ; i < len; i++ ) {
    selector += tokens[i].value;
  }
  return selector;
}

function addCombinator( matcher, combinator, base ) {
  var dir = combinator.dir,
    checkNonElements = base && dir === "parentNode",
    doneName = done++;

  return combinator.first ?
    // Check against closest ancestor/preceding element
    function( elem, context, xml ) {
      while ( (elem = elem[ dir ]) ) {
        if ( elem.nodeType === 1 || checkNonElements ) {
          return matcher( elem, context, xml );
        }
      }
    } :

    // Check against all ancestor/preceding elements
    function( elem, context, xml ) {
      var oldCache, uniqueCache, outerCache,
        newCache = [ dirruns, doneName ];

      // We can't set arbitrary data on XML nodes, so they don't benefit from combinator caching
      if ( xml ) {
        while ( (elem = elem[ dir ]) ) {
          if ( elem.nodeType === 1 || checkNonElements ) {
            if ( matcher( elem, context, xml ) ) {
              return true;
            }
          }
        }
      } else {
        while ( (elem = elem[ dir ]) ) {
          if ( elem.nodeType === 1 || checkNonElements ) {
            outerCache = elem[ expando ] || (elem[ expando ] = {});

            // Support: IE <9 only
            // Defend against cloned attroperties (jQuery gh-1709)
            uniqueCache = outerCache[ elem.uniqueID ] || (outerCache[ elem.uniqueID ] = {});

            if ( (oldCache = uniqueCache[ dir ]) &&
              oldCache[ 0 ] === dirruns && oldCache[ 1 ] === doneName ) {

              // Assign to newCache so results back-propagate to previous elements
              return (newCache[ 2 ] = oldCache[ 2 ]);
            } else {
              // Reuse newcache so results back-propagate to previous elements
              uniqueCache[ dir ] = newCache;

              // A match means we're done; a fail means we have to keep checking
              if ( (newCache[ 2 ] = matcher( elem, context, xml )) ) {
                return true;
              }
            }
          }
        }
      }
    };
}

function elementMatcher( matchers ) {
  return matchers.length > 1 ?
    function( elem, context, xml ) {
      var i = matchers.length;
      while ( i-- ) {
        if ( !matchers[i]( elem, context, xml ) ) {
          return false;
        }
      }
      return true;
    } :
    matchers[0];
}

function multipleContexts( selector, contexts, results ) {
  var i = 0,
    len = contexts.length;
  for ( ; i < len; i++ ) {
    Sizzle( selector, contexts[i], results );
  }
  return results;
}

function condense( unmatched, map, filter, context, xml ) {
  var elem,
    newUnmatched = [],
    i = 0,
    len = unmatched.length,
    mapped = map != null;

  for ( ; i < len; i++ ) {
    if ( (elem = unmatched[i]) ) {
      if ( !filter || filter( elem, context, xml ) ) {
        newUnmatched.push( elem );
        if ( mapped ) {
          map.push( i );
        }
      }
    }
  }

  return newUnmatched;
}

function setMatcher( preFilter, selector, matcher, postFilter, postFinder, postSelector ) {
  if ( postFilter && !postFilter[ expando ] ) {
    postFilter = setMatcher( postFilter );
  }
  if ( postFinder && !postFinder[ expando ] ) {
    postFinder = setMatcher( postFinder, postSelector );
  }
  return markFunction(function( seed, results, context, xml ) {
    var temp, i, elem,
      preMap = [],
      postMap = [],
      preexisting = results.length,

      // Get initial elements from seed or context
      elems = seed || multipleContexts( selector || "*", context.nodeType ? [ context ] : context, [] ),

      // Prefilter to get matcher input, preserving a map for seed-results synchronization
      matcherIn = preFilter && ( seed || !selector ) ?
        condense( elems, preMap, preFilter, context, xml ) :
        elems,

      matcherOut = matcher ?
        // If we have a postFinder, or filtered seed, or non-seed postFilter or preexisting results,
        postFinder || ( seed ? preFilter : preexisting || postFilter ) ?

          // ...intermediate processing is necessary
          [] :

          // ...otherwise use results directly
          results :
        matcherIn;

    // Find primary matches
    if ( matcher ) {
      matcher( matcherIn, matcherOut, context, xml );
    }

    // Apply postFilter
    if ( postFilter ) {
      temp = condense( matcherOut, postMap );
      postFilter( temp, [], context, xml );

      // Un-match failing elements by moving them back to matcherIn
      i = temp.length;
      while ( i-- ) {
        if ( (elem = temp[i]) ) {
          matcherOut[ postMap[i] ] = !(matcherIn[ postMap[i] ] = elem);
        }
      }
    }

    if ( seed ) {
      if ( postFinder || preFilter ) {
        if ( postFinder ) {
          // Get the final matcherOut by condensing this intermediate into postFinder contexts
          temp = [];
          i = matcherOut.length;
          while ( i-- ) {
            if ( (elem = matcherOut[i]) ) {
              // Restore matcherIn since elem is not yet a final match
              temp.push( (matcherIn[i] = elem) );
            }
          }
          postFinder( null, (matcherOut = []), temp, xml );
        }

        // Move matched elements from seed to results to keep them synchronized
        i = matcherOut.length;
        while ( i-- ) {
          if ( (elem = matcherOut[i]) &&
            (temp = postFinder ? indexOf( seed, elem ) : preMap[i]) > -1 ) {

            seed[temp] = !(results[temp] = elem);
          }
        }
      }

    // Add elements to results, through postFinder if defined
    } else {
      matcherOut = condense(
        matcherOut === results ?
          matcherOut.splice( preexisting, matcherOut.length ) :
          matcherOut
      );
      if ( postFinder ) {
        postFinder( null, results, matcherOut, xml );
      } else {
        push.apply( results, matcherOut );
      }
    }
  });
}

function matcherFromTokens( tokens ) {
  var checkContext, matcher, j,
    len = tokens.length,
    leadingRelative = Expr.relative[ tokens[0].type ],
    implicitRelative = leadingRelative || Expr.relative[" "],
    i = leadingRelative ? 1 : 0,

    // The foundational matcher ensures that elements are reachable from top-level context(s)
    matchContext = addCombinator( function( elem ) {
      return elem === checkContext;
    }, implicitRelative, true ),
    matchAnyContext = addCombinator( function( elem ) {
      return indexOf( checkContext, elem ) > -1;
    }, implicitRelative, true ),
    matchers = [ function( elem, context, xml ) {
      var ret = ( !leadingRelative && ( xml || context !== outermostContext ) ) || (
        (checkContext = context).nodeType ?
          matchContext( elem, context, xml ) :
          matchAnyContext( elem, context, xml ) );
      // Avoid hanging onto element (issue #299)
      checkContext = null;
      return ret;
    } ];

  for ( ; i < len; i++ ) {
    if ( (matcher = Expr.relative[ tokens[i].type ]) ) {
      matchers = [ addCombinator(elementMatcher( matchers ), matcher) ];
    } else {
      matcher = Expr.filter[ tokens[i].type ].apply( null, tokens[i].matches );

      // Return special upon seeing a positional matcher
      if ( matcher[ expando ] ) {
        // Find the next relative operator (if any) for proper handling
        j = ++i;
        for ( ; j < len; j++ ) {
          if ( Expr.relative[ tokens[j].type ] ) {
            break;
          }
        }
        return setMatcher(
          i > 1 && elementMatcher( matchers ),
          i > 1 && toSelector(
            // If the preceding token was a descendant combinator, insert an implicit any-element `*`
            tokens.slice( 0, i - 1 ).concat({ value: tokens[ i - 2 ].type === " " ? "*" : "" })
          ).replace( rtrim, "$1" ),
          matcher,
          i < j && matcherFromTokens( tokens.slice( i, j ) ),
          j < len && matcherFromTokens( (tokens = tokens.slice( j )) ),
          j < len && toSelector( tokens )
        );
      }
      matchers.push( matcher );
    }
  }

  return elementMatcher( matchers );
}

function matcherFromGroupMatchers( elementMatchers, setMatchers ) {
  var bySet = setMatchers.length > 0,
    byElement = elementMatchers.length > 0,
    superMatcher = function( seed, context, xml, results, outermost ) {
      var elem, j, matcher,
        matchedCount = 0,
        i = "0",
        unmatched = seed && [],
        setMatched = [],
        contextBackup = outermostContext,
        // We must always have either seed elements or outermost context
        elems = seed || byElement && Expr.find["TAG"]( "*", outermost ),
        // Use integer dirruns iff this is the outermost matcher
        dirrunsUnique = (dirruns += contextBackup == null ? 1 : Math.random() || 0.1),
        len = elems.length;

      if ( outermost ) {
        outermostContext = context === document || context || outermost;
      }

      // Add elements passing elementMatchers directly to results
      // Support: IE<9, Safari
      // Tolerate NodeList properties (IE: "length"; Safari: <number>) matching elements by id
      for ( ; i !== len && (elem = elems[i]) != null; i++ ) {
        if ( byElement && elem ) {
          j = 0;
          if ( !context && elem.ownerDocument !== document ) {
            setDocument( elem );
            xml = !documentIsHTML;
          }
          while ( (matcher = elementMatchers[j++]) ) {
            if ( matcher( elem, context || document, xml) ) {
              results.push( elem );
              break;
            }
          }
          if ( outermost ) {
            dirruns = dirrunsUnique;
          }
        }

        // Track unmatched elements for set filters
        if ( bySet ) {
          // They will have gone through all possible matchers
          if ( (elem = !matcher && elem) ) {
            matchedCount--;
          }

          // Lengthen the array for every element, matched or not
          if ( seed ) {
            unmatched.push( elem );
          }
        }
      }

      // `i` is now the count of elements visited above, and adding it to `matchedCount`
      // makes the latter nonnegative.
      matchedCount += i;

      // Apply set filters to unmatched elements
      // NOTE: This can be skipped if there are no unmatched elements (i.e., `matchedCount`
      // equals `i`), unless we didn't visit _any_ elements in the above loop because we have
      // no element matchers and no seed.
      // Incrementing an initially-string "0" `i` allows `i` to remain a string only in that
      // case, which will result in a "00" `matchedCount` that differs from `i` but is also
      // numerically zero.
      if ( bySet && i !== matchedCount ) {
        j = 0;
        while ( (matcher = setMatchers[j++]) ) {
          matcher( unmatched, setMatched, context, xml );
        }

        if ( seed ) {
          // Reintegrate element matches to eliminate the need for sorting
          if ( matchedCount > 0 ) {
            while ( i-- ) {
              if ( !(unmatched[i] || setMatched[i]) ) {
                setMatched[i] = pop.call( results );
              }
            }
          }

          // Discard index placeholder values to get only actual matches
          setMatched = condense( setMatched );
        }

        // Add matches to results
        push.apply( results, setMatched );

        // Seedless set matches succeeding multiple successful matchers stipulate sorting
        if ( outermost && !seed && setMatched.length > 0 &&
          ( matchedCount + setMatchers.length ) > 1 ) {

          Sizzle.uniqueSort( results );
        }
      }

      // Override manipulation of globals by nested matchers
      if ( outermost ) {
        dirruns = dirrunsUnique;
        outermostContext = contextBackup;
      }

      return unmatched;
    };

  return bySet ?
    markFunction( superMatcher ) :
    superMatcher;
}

compile = Sizzle.compile = function( selector, match /* Internal Use Only */ ) {
  var i,
    setMatchers = [],
    elementMatchers = [],
    cached = compilerCache[ selector + " " ];

  if ( !cached ) {
    // Generate a function of recursive functions that can be used to check each element
    if ( !match ) {
      match = tokenize( selector );
    }
    i = match.length;
    while ( i-- ) {
      cached = matcherFromTokens( match[i] );
      if ( cached[ expando ] ) {
        setMatchers.push( cached );
      } else {
        elementMatchers.push( cached );
      }
    }

    // Cache the compiled function
    cached = compilerCache( selector, matcherFromGroupMatchers( elementMatchers, setMatchers ) );

    // Save selector and tokenization
    cached.selector = selector;
  }
  return cached;
};

/**
 * A low-level selection function that works with Sizzle's compiled
 *  selector functions
 * @param {String|Function} selector A selector or a pre-compiled
 *  selector function built with Sizzle.compile
 * @param {Element} context
 * @param {Array} [results]
 * @param {Array} [seed] A set of elements to match against
 */
select = Sizzle.select = function( selector, context, results, seed ) {
  var i, tokens, token, type, find,
    compiled = typeof selector === "function" && selector,
    match = !seed && tokenize( (selector = compiled.selector || selector) );

  results = results || [];

  // Try to minimize operations if there is only one selector in the list and no seed
  // (the latter of which guarantees us context)
  if ( match.length === 1 ) {

    // Reduce context if the leading compound selector is an ID
    tokens = match[0] = match[0].slice( 0 );
    if ( tokens.length > 2 && (token = tokens[0]).type === "ID" &&
        support.getById && context.nodeType === 9 && documentIsHTML &&
        Expr.relative[ tokens[1].type ] ) {

      context = ( Expr.find["ID"]( token.matches[0].replace(runescape, funescape), context ) || [] )[0];
      if ( !context ) {
        return results;

      // Precompiled matchers will still verify ancestry, so step up a level
      } else if ( compiled ) {
        context = context.parentNode;
      }

      selector = selector.slice( tokens.shift().value.length );
    }

    // Fetch a seed set for right-to-left matching
    i = matchExpr["needsContext"].test( selector ) ? 0 : tokens.length;
    while ( i-- ) {
      token = tokens[i];

      // Abort if we hit a combinator
      if ( Expr.relative[ (type = token.type) ] ) {
        break;
      }
      if ( (find = Expr.find[ type ]) ) {
        // Search, expanding context for leading sibling combinators
        if ( (seed = find(
          token.matches[0].replace( runescape, funescape ),
          rsibling.test( tokens[0].type ) && testContext( context.parentNode ) || context
        )) ) {

          // If seed is empty or no tokens remain, we can return early
          tokens.splice( i, 1 );
          selector = seed.length && toSelector( tokens );
          if ( !selector ) {
            push.apply( results, seed );
            return results;
          }

          break;
        }
      }
    }
  }

  // Compile and execute a filtering function if one is not provided
  // Provide `match` to avoid retokenization if we modified the selector above
  ( compiled || compile( selector, match ) )(
    seed,
    context,
    !documentIsHTML,
    results,
    !context || rsibling.test( selector ) && testContext( context.parentNode ) || context
  );
  return results;
};

// One-time assignments

// Sort stability
support.sortStable = expando.split("").sort( sortOrder ).join("") === expando;

// Support: Chrome 14-35+
// Always assume duplicates if they aren't passed to the comparison function
support.detectDuplicates = !!hasDuplicate;

// Initialize against the default document
setDocument();

// Support: Webkit<537.32 - Safari 6.0.3/Chrome 25 (fixed in Chrome 27)
// Detached nodes confoundingly follow *each other*
support.sortDetached = assert(function( div1 ) {
  // Should return 1, but returns 4 (following)
  return div1.compareDocumentPosition( document.createElement("div") ) & 1;
});

// Support: IE<8
// Prevent attribute/property "interpolation"
// http://msdn.microsoft.com/en-us/library/ms536429%28VS.85%29.aspx
if ( !assert(function( div ) {
  div.innerHTML = "<a href='#'></a>";
  return div.firstChild.getAttribute("href") === "#" ;
}) ) {
  addHandle( "type|href|height|width", function( elem, name, isXML ) {
    if ( !isXML ) {
      return elem.getAttribute( name, name.toLowerCase() === "type" ? 1 : 2 );
    }
  });
}

// Support: IE<9
// Use defaultValue in place of getAttribute("value")
if ( !support.attributes || !assert(function( div ) {
  div.innerHTML = "<input/>";
  div.firstChild.setAttribute( "value", "" );
  return div.firstChild.getAttribute( "value" ) === "";
}) ) {
  addHandle( "value", function( elem, name, isXML ) {
    if ( !isXML && elem.nodeName.toLowerCase() === "input" ) {
      return elem.defaultValue;
    }
  });
}

// Support: IE<9
// Use getAttributeNode to fetch booleans when getAttribute lies
if ( !assert(function( div ) {
  return div.getAttribute("disabled") == null;
}) ) {
  addHandle( booleans, function( elem, name, isXML ) {
    var val;
    if ( !isXML ) {
      return elem[ name ] === true ? name.toLowerCase() :
          (val = elem.getAttributeNode( name )) && val.specified ?
          val.value :
        null;
    }
  });
}

return Sizzle;

})( window );



jQuery.find = Sizzle;
jQuery.expr = Sizzle.selectors;
jQuery.expr[ ":" ] = jQuery.expr.pseudos;
jQuery.uniqueSort = jQuery.unique = Sizzle.uniqueSort;
jQuery.text = Sizzle.getText;
jQuery.isXMLDoc = Sizzle.isXML;
jQuery.contains = Sizzle.contains;



var dir = function( elem, dir, until ) {
  var matched = [],
    truncate = until !== undefined;

  while ( ( elem = elem[ dir ] ) && elem.nodeType !== 9 ) {
    if ( elem.nodeType === 1 ) {
      if ( truncate && jQuery( elem ).is( until ) ) {
        break;
      }
      matched.push( elem );
    }
  }
  return matched;
};


var siblings = function( n, elem ) {
  var matched = [];

  for ( ; n; n = n.nextSibling ) {
    if ( n.nodeType === 1 && n !== elem ) {
      matched.push( n );
    }
  }

  return matched;
};


var rneedsContext = jQuery.expr.match.needsContext;

var rsingleTag = ( /^<([\w-]+)\s*\/?>(?:<\/\1>|)$/ );



var risSimple = /^.[^:#\[\.,]*$/;

// Implement the identical functionality for filter and not
function winnow( elements, qualifier, not ) {
  if ( jQuery.isFunction( qualifier ) ) {
    return jQuery.grep( elements, function( elem, i ) {
      /* jshint -W018 */
      return !!qualifier.call( elem, i, elem ) !== not;
    } );

  }

  if ( qualifier.nodeType ) {
    return jQuery.grep( elements, function( elem ) {
      return ( elem === qualifier ) !== not;
    } );

  }

  if ( typeof qualifier === "string" ) {
    if ( risSimple.test( qualifier ) ) {
      return jQuery.filter( qualifier, elements, not );
    }

    qualifier = jQuery.filter( qualifier, elements );
  }

  return jQuery.grep( elements, function( elem ) {
    return ( indexOf.call( qualifier, elem ) > -1 ) !== not;
  } );
}

jQuery.filter = function( expr, elems, not ) {
  var elem = elems[ 0 ];

  if ( not ) {
    expr = ":not(" + expr + ")";
  }

  return elems.length === 1 && elem.nodeType === 1 ?
    jQuery.find.matchesSelector( elem, expr ) ? [ elem ] : [] :
    jQuery.find.matches( expr, jQuery.grep( elems, function( elem ) {
      return elem.nodeType === 1;
    } ) );
};

jQuery.fn.extend( {
  find: function( selector ) {
    var i,
      len = this.length,
      ret = [],
      self = this;

    if ( typeof selector !== "string" ) {
      return this.pushStack( jQuery( selector ).filter( function() {
        for ( i = 0; i < len; i++ ) {
          if ( jQuery.contains( self[ i ], this ) ) {
            return true;
          }
        }
      } ) );
    }

    for ( i = 0; i < len; i++ ) {
      jQuery.find( selector, self[ i ], ret );
    }

    // Needed because $( selector, context ) becomes $( context ).find( selector )
    ret = this.pushStack( len > 1 ? jQuery.unique( ret ) : ret );
    ret.selector = this.selector ? this.selector + " " + selector : selector;
    return ret;
  },
  filter: function( selector ) {
    return this.pushStack( winnow( this, selector || [], false ) );
  },
  not: function( selector ) {
    return this.pushStack( winnow( this, selector || [], true ) );
  },
  is: function( selector ) {
    return !!winnow(
      this,

      // If this is a positional/relative selector, check membership in the returned set
      // so $("p:first").is("p:last") won't return true for a doc with two "p".
      typeof selector === "string" && rneedsContext.test( selector ) ?
        jQuery( selector ) :
        selector || [],
      false
    ).length;
  }
} );


// Initialize a jQuery object


// A central reference to the root jQuery(document)
var rootjQuery,

  // A simple way to check for HTML strings
  // Prioritize #id over <tag> to avoid XSS via location.hash (#9521)
  // Strict HTML recognition (#11290: must start with <)
  rquickExpr = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/,

  init = jQuery.fn.init = function( selector, context, root ) {
    var match, elem;

    // HANDLE: $(""), $(null), $(undefined), $(false)
    if ( !selector ) {
      return this;
    }

    // Method init() accepts an alternate rootjQuery
    // so migrate can support jQuery.sub (gh-2101)
    root = root || rootjQuery;

    // Handle HTML strings
    if ( typeof selector === "string" ) {
      if ( selector[ 0 ] === "<" &&
        selector[ selector.length - 1 ] === ">" &&
        selector.length >= 3 ) {

        // Assume that strings that start and end with <> are HTML and skip the regex check
        match = [ null, selector, null ];

      } else {
        match = rquickExpr.exec( selector );
      }

      // Match html or make sure no context is specified for #id
      if ( match && ( match[ 1 ] || !context ) ) {

        // HANDLE: $(html) -> $(array)
        if ( match[ 1 ] ) {
          context = context instanceof jQuery ? context[ 0 ] : context;

          // Option to run scripts is true for back-compat
          // Intentionally let the error be thrown if parseHTML is not present
          jQuery.merge( this, jQuery.parseHTML(
            match[ 1 ],
            context && context.nodeType ? context.ownerDocument || context : document,
            true
          ) );

          // HANDLE: $(html, props)
          if ( rsingleTag.test( match[ 1 ] ) && jQuery.isPlainObject( context ) ) {
            for ( match in context ) {

              // Properties of context are called as methods if possible
              if ( jQuery.isFunction( this[ match ] ) ) {
                this[ match ]( context[ match ] );

              // ...and otherwise set as attributes
              } else {
                this.attr( match, context[ match ] );
              }
            }
          }

          return this;

        // HANDLE: $(#id)
        } else {
          elem = document.getElementById( match[ 2 ] );

          // Support: Blackberry 4.6
          // gEBID returns nodes no longer in the document (#6963)
          if ( elem && elem.parentNode ) {

            // Inject the element directly into the jQuery object
            this.length = 1;
            this[ 0 ] = elem;
          }

          this.context = document;
          this.selector = selector;
          return this;
        }

      // HANDLE: $(expr, $(...))
      } else if ( !context || context.jquery ) {
        return ( context || root ).find( selector );

      // HANDLE: $(expr, context)
      // (which is just equivalent to: $(context).find(expr)
      } else {
        return this.constructor( context ).find( selector );
      }

    // HANDLE: $(DOMElement)
    } else if ( selector.nodeType ) {
      this.context = this[ 0 ] = selector;
      this.length = 1;
      return this;

    // HANDLE: $(function)
    // Shortcut for document ready
    } else if ( jQuery.isFunction( selector ) ) {
      return root.ready !== undefined ?
        root.ready( selector ) :

        // Execute immediately if ready is not present
        selector( jQuery );
    }

    if ( selector.selector !== undefined ) {
      this.selector = selector.selector;
      this.context = selector.context;
    }

    return jQuery.makeArray( selector, this );
  };

// Give the init function the jQuery prototype for later instantiation
init.prototype = jQuery.fn;

// Initialize central reference
rootjQuery = jQuery( document );


var rparentsprev = /^(?:parents|prev(?:Until|All))/,

  // Methods guaranteed to produce a unique set when starting from a unique set
  guaranteedUnique = {
    children: true,
    contents: true,
    next: true,
    prev: true
  };

jQuery.fn.extend( {
  has: function( target ) {
    var targets = jQuery( target, this ),
      l = targets.length;

    return this.filter( function() {
      var i = 0;
      for ( ; i < l; i++ ) {
        if ( jQuery.contains( this, targets[ i ] ) ) {
          return true;
        }
      }
    } );
  },

  closest: function( selectors, context ) {
    var cur,
      i = 0,
      l = this.length,
      matched = [],
      pos = rneedsContext.test( selectors ) || typeof selectors !== "string" ?
        jQuery( selectors, context || this.context ) :
        0;

    for ( ; i < l; i++ ) {
      for ( cur = this[ i ]; cur && cur !== context; cur = cur.parentNode ) {

        // Always skip document fragments
        if ( cur.nodeType < 11 && ( pos ?
          pos.index( cur ) > -1 :

          // Don't pass non-elements to Sizzle
          cur.nodeType === 1 &&
            jQuery.find.matchesSelector( cur, selectors ) ) ) {

          matched.push( cur );
          break;
        }
      }
    }

    return this.pushStack( matched.length > 1 ? jQuery.uniqueSort( matched ) : matched );
  },

  // Determine the position of an element within the set
  index: function( elem ) {

    // No argument, return index in parent
    if ( !elem ) {
      return ( this[ 0 ] && this[ 0 ].parentNode ) ? this.first().prevAll().length : -1;
    }

    // Index in selector
    if ( typeof elem === "string" ) {
      return indexOf.call( jQuery( elem ), this[ 0 ] );
    }

    // Locate the position of the desired element
    return indexOf.call( this,

      // If it receives a jQuery object, the first element is used
      elem.jquery ? elem[ 0 ] : elem
    );
  },

  add: function( selector, context ) {
    return this.pushStack(
      jQuery.uniqueSort(
        jQuery.merge( this.get(), jQuery( selector, context ) )
      )
    );
  },

  addBack: function( selector ) {
    return this.add( selector == null ?
      this.prevObject : this.prevObject.filter( selector )
    );
  }
} );

function sibling( cur, dir ) {
  while ( ( cur = cur[ dir ] ) && cur.nodeType !== 1 ) {}
  return cur;
}

jQuery.each( {
  parent: function( elem ) {
    var parent = elem.parentNode;
    return parent && parent.nodeType !== 11 ? parent : null;
  },
  parents: function( elem ) {
    return dir( elem, "parentNode" );
  },
  parentsUntil: function( elem, i, until ) {
    return dir( elem, "parentNode", until );
  },
  next: function( elem ) {
    return sibling( elem, "nextSibling" );
  },
  prev: function( elem ) {
    return sibling( elem, "previousSibling" );
  },
  nextAll: function( elem ) {
    return dir( elem, "nextSibling" );
  },
  prevAll: function( elem ) {
    return dir( elem, "previousSibling" );
  },
  nextUntil: function( elem, i, until ) {
    return dir( elem, "nextSibling", until );
  },
  prevUntil: function( elem, i, until ) {
    return dir( elem, "previousSibling", until );
  },
  siblings: function( elem ) {
    return siblings( ( elem.parentNode || {} ).firstChild, elem );
  },
  children: function( elem ) {
    return siblings( elem.firstChild );
  },
  contents: function( elem ) {
    return elem.contentDocument || jQuery.merge( [], elem.childNodes );
  }
}, function( name, fn ) {
  jQuery.fn[ name ] = function( until, selector ) {
    var matched = jQuery.map( this, fn, until );

    if ( name.slice( -5 ) !== "Until" ) {
      selector = until;
    }

    if ( selector && typeof selector === "string" ) {
      matched = jQuery.filter( selector, matched );
    }

    if ( this.length > 1 ) {

      // Remove duplicates
      if ( !guaranteedUnique[ name ] ) {
        jQuery.uniqueSort( matched );
      }

      // Reverse order for parents* and prev-derivatives
      if ( rparentsprev.test( name ) ) {
        matched.reverse();
      }
    }

    return this.pushStack( matched );
  };
} );
var rnotwhite = ( /\S+/g );



// Convert String-formatted options into Object-formatted ones
function createOptions( options ) {
  var object = {};
  jQuery.each( options.match( rnotwhite ) || [], function( _, flag ) {
    object[ flag ] = true;
  } );
  return object;
}

/*
 * Create a callback list using the following parameters:
 *
 *  options: an optional list of space-separated options that will change how
 *      the callback list behaves or a more traditional option object
 *
 * By default a callback list will act like an event callback list and can be
 * "fired" multiple times.
 *
 * Possible options:
 *
 *  once:     will ensure the callback list can only be fired once (like a Deferred)
 *
 *  memory:     will keep track of previous values and will call any callback added
 *          after the list has been fired right away with the latest "memorized"
 *          values (like a Deferred)
 *
 *  unique:     will ensure a callback can only be added once (no duplicate in the list)
 *
 *  stopOnFalse:  interrupt callings when a callback returns false
 *
 */
jQuery.Callbacks = function( options ) {

  // Convert options from String-formatted to Object-formatted if needed
  // (we check in cache first)
  options = typeof options === "string" ?
    createOptions( options ) :
    jQuery.extend( {}, options );

  var // Flag to know if list is currently firing
    firing,

    // Last fire value for non-forgettable lists
    memory,

    // Flag to know if list was already fired
    fired,

    // Flag to prevent firing
    locked,

    // Actual callback list
    list = [],

    // Queue of execution data for repeatable lists
    queue = [],

    // Index of currently firing callback (modified by add/remove as needed)
    firingIndex = -1,

    // Fire callbacks
    fire = function() {

      // Enforce single-firing
      locked = options.once;

      // Execute callbacks for all pending executions,
      // respecting firingIndex overrides and runtime changes
      fired = firing = true;
      for ( ; queue.length; firingIndex = -1 ) {
        memory = queue.shift();
        while ( ++firingIndex < list.length ) {

          // Run callback and check for early termination
          if ( list[ firingIndex ].apply( memory[ 0 ], memory[ 1 ] ) === false &&
            options.stopOnFalse ) {

            // Jump to end and forget the data so .add doesn't re-fire
            firingIndex = list.length;
            memory = false;
          }
        }
      }

      // Forget the data if we're done with it
      if ( !options.memory ) {
        memory = false;
      }

      firing = false;

      // Clean up if we're done firing for good
      if ( locked ) {

        // Keep an empty list if we have data for future add calls
        if ( memory ) {
          list = [];

        // Otherwise, this object is spent
        } else {
          list = "";
        }
      }
    },

    // Actual Callbacks object
    self = {

      // Add a callback or a collection of callbacks to the list
      add: function() {
        if ( list ) {

          // If we have memory from a past run, we should fire after adding
          if ( memory && !firing ) {
            firingIndex = list.length - 1;
            queue.push( memory );
          }

          ( function add( args ) {
            jQuery.each( args, function( _, arg ) {
              if ( jQuery.isFunction( arg ) ) {
                if ( !options.unique || !self.has( arg ) ) {
                  list.push( arg );
                }
              } else if ( arg && arg.length && jQuery.type( arg ) !== "string" ) {

                // Inspect recursively
                add( arg );
              }
            } );
          } )( arguments );

          if ( memory && !firing ) {
            fire();
          }
        }
        return this;
      },

      // Remove a callback from the list
      remove: function() {
        jQuery.each( arguments, function( _, arg ) {
          var index;
          while ( ( index = jQuery.inArray( arg, list, index ) ) > -1 ) {
            list.splice( index, 1 );

            // Handle firing indexes
            if ( index <= firingIndex ) {
              firingIndex--;
            }
          }
        } );
        return this;
      },

      // Check if a given callback is in the list.
      // If no argument is given, return whether or not list has callbacks attached.
      has: function( fn ) {
        return fn ?
          jQuery.inArray( fn, list ) > -1 :
          list.length > 0;
      },

      // Remove all callbacks from the list
      empty: function() {
        if ( list ) {
          list = [];
        }
        return this;
      },

      // Disable .fire and .add
      // Abort any current/pending executions
      // Clear all callbacks and values
      disable: function() {
        locked = queue = [];
        list = memory = "";
        return this;
      },
      disabled: function() {
        return !list;
      },

      // Disable .fire
      // Also disable .add unless we have memory (since it would have no effect)
      // Abort any pending executions
      lock: function() {
        locked = queue = [];
        if ( !memory ) {
          list = memory = "";
        }
        return this;
      },
      locked: function() {
        return !!locked;
      },

      // Call all callbacks with the given context and arguments
      fireWith: function( context, args ) {
        if ( !locked ) {
          args = args || [];
          args = [ context, args.slice ? args.slice() : args ];
          queue.push( args );
          if ( !firing ) {
            fire();
          }
        }
        return this;
      },

      // Call all the callbacks with the given arguments
      fire: function() {
        self.fireWith( this, arguments );
        return this;
      },

      // To know if the callbacks have already been called at least once
      fired: function() {
        return !!fired;
      }
    };

  return self;
};


jQuery.extend( {

  Deferred: function( func ) {
    var tuples = [

        // action, add listener, listener list, final state
        [ "resolve", "done", jQuery.Callbacks( "once memory" ), "resolved" ],
        [ "reject", "fail", jQuery.Callbacks( "once memory" ), "rejected" ],
        [ "notify", "progress", jQuery.Callbacks( "memory" ) ]
      ],
      state = "pending",
      promise = {
        state: function() {
          return state;
        },
        always: function() {
          deferred.done( arguments ).fail( arguments );
          return this;
        },
        then: function( /* fnDone, fnFail, fnProgress */ ) {
          var fns = arguments;
          return jQuery.Deferred( function( newDefer ) {
            jQuery.each( tuples, function( i, tuple ) {
              var fn = jQuery.isFunction( fns[ i ] ) && fns[ i ];

              // deferred[ done | fail | progress ] for forwarding actions to newDefer
              deferred[ tuple[ 1 ] ]( function() {
                var returned = fn && fn.apply( this, arguments );
                if ( returned && jQuery.isFunction( returned.promise ) ) {
                  returned.promise()
                    .progress( newDefer.notify )
                    .done( newDefer.resolve )
                    .fail( newDefer.reject );
                } else {
                  newDefer[ tuple[ 0 ] + "With" ](
                    this === promise ? newDefer.promise() : this,
                    fn ? [ returned ] : arguments
                  );
                }
              } );
            } );
            fns = null;
          } ).promise();
        },

        // Get a promise for this deferred
        // If obj is provided, the promise aspect is added to the object
        promise: function( obj ) {
          return obj != null ? jQuery.extend( obj, promise ) : promise;
        }
      },
      deferred = {};

    // Keep pipe for back-compat
    promise.pipe = promise.then;

    // Add list-specific methods
    jQuery.each( tuples, function( i, tuple ) {
      var list = tuple[ 2 ],
        stateString = tuple[ 3 ];

      // promise[ done | fail | progress ] = list.add
      promise[ tuple[ 1 ] ] = list.add;

      // Handle state
      if ( stateString ) {
        list.add( function() {

          // state = [ resolved | rejected ]
          state = stateString;

        // [ reject_list | resolve_list ].disable; progress_list.lock
        }, tuples[ i ^ 1 ][ 2 ].disable, tuples[ 2 ][ 2 ].lock );
      }

      // deferred[ resolve | reject | notify ]
      deferred[ tuple[ 0 ] ] = function() {
        deferred[ tuple[ 0 ] + "With" ]( this === deferred ? promise : this, arguments );
        return this;
      };
      deferred[ tuple[ 0 ] + "With" ] = list.fireWith;
    } );

    // Make the deferred a promise
    promise.promise( deferred );

    // Call given func if any
    if ( func ) {
      func.call( deferred, deferred );
    }

    // All done!
    return deferred;
  },

  // Deferred helper
  when: function( subordinate /* , ..., subordinateN */ ) {
    var i = 0,
      resolveValues = slice.call( arguments ),
      length = resolveValues.length,

      // the count of uncompleted subordinates
      remaining = length !== 1 ||
        ( subordinate && jQuery.isFunction( subordinate.promise ) ) ? length : 0,

      // the master Deferred.
      // If resolveValues consist of only a single Deferred, just use that.
      deferred = remaining === 1 ? subordinate : jQuery.Deferred(),

      // Update function for both resolve and progress values
      updateFunc = function( i, contexts, values ) {
        return function( value ) {
          contexts[ i ] = this;
          values[ i ] = arguments.length > 1 ? slice.call( arguments ) : value;
          if ( values === progressValues ) {
            deferred.notifyWith( contexts, values );
          } else if ( !( --remaining ) ) {
            deferred.resolveWith( contexts, values );
          }
        };
      },

      progressValues, progressContexts, resolveContexts;

    // Add listeners to Deferred subordinates; treat others as resolved
    if ( length > 1 ) {
      progressValues = new Array( length );
      progressContexts = new Array( length );
      resolveContexts = new Array( length );
      for ( ; i < length; i++ ) {
        if ( resolveValues[ i ] && jQuery.isFunction( resolveValues[ i ].promise ) ) {
          resolveValues[ i ].promise()
            .progress( updateFunc( i, progressContexts, progressValues ) )
            .done( updateFunc( i, resolveContexts, resolveValues ) )
            .fail( deferred.reject );
        } else {
          --remaining;
        }
      }
    }

    // If we're not waiting on anything, resolve the master
    if ( !remaining ) {
      deferred.resolveWith( resolveContexts, resolveValues );
    }

    return deferred.promise();
  }
} );


// The deferred used on DOM ready
var readyList;

jQuery.fn.ready = function( fn ) {

  // Add the callback
  jQuery.ready.promise().done( fn );

  return this;
};

jQuery.extend( {

  // Is the DOM ready to be used? Set to true once it occurs.
  isReady: false,

  // A counter to track how many items to wait for before
  // the ready event fires. See #6781
  readyWait: 1,

  // Hold (or release) the ready event
  holdReady: function( hold ) {
    if ( hold ) {
      jQuery.readyWait++;
    } else {
      jQuery.ready( true );
    }
  },

  // Handle when the DOM is ready
  ready: function( wait ) {

    // Abort if there are pending holds or we're already ready
    if ( wait === true ? --jQuery.readyWait : jQuery.isReady ) {
      return;
    }

    // Remember that the DOM is ready
    jQuery.isReady = true;

    // If a normal DOM Ready event fired, decrement, and wait if need be
    if ( wait !== true && --jQuery.readyWait > 0 ) {
      return;
    }

    // If there are functions bound, to execute
    readyList.resolveWith( document, [ jQuery ] );

    // Trigger any bound ready events
    if ( jQuery.fn.triggerHandler ) {
      jQuery( document ).triggerHandler( "ready" );
      jQuery( document ).off( "ready" );
    }
  }
} );

/**
 * The ready event handler and self cleanup method
 */
function completed() {
  document.removeEventListener( "DOMContentLoaded", completed );
  window.removeEventListener( "load", completed );
  jQuery.ready();
}

jQuery.ready.promise = function( obj ) {
  if ( !readyList ) {

    readyList = jQuery.Deferred();

    // Catch cases where $(document).ready() is called
    // after the browser event has already occurred.
    // Support: IE9-10 only
    // Older IE sometimes signals "interactive" too soon
    if ( document.readyState === "complete" ||
      ( document.readyState !== "loading" && !document.documentElement.doScroll ) ) {

      // Handle it asynchronously to allow scripts the opportunity to delay ready
      window.setTimeout( jQuery.ready );

    } else {

      // Use the handy event callback
      document.addEventListener( "DOMContentLoaded", completed );

      // A fallback to window.onload, that will always work
      window.addEventListener( "load", completed );
    }
  }
  return readyList.promise( obj );
};

// Kick off the DOM ready check even if the user does not
jQuery.ready.promise();




// Multifunctional method to get and set values of a collection
// The value/s can optionally be executed if it's a function
var access = function( elems, fn, key, value, chainable, emptyGet, raw ) {
  var i = 0,
    len = elems.length,
    bulk = key == null;

  // Sets many values
  if ( jQuery.type( key ) === "object" ) {
    chainable = true;
    for ( i in key ) {
      access( elems, fn, i, key[ i ], true, emptyGet, raw );
    }

  // Sets one value
  } else if ( value !== undefined ) {
    chainable = true;

    if ( !jQuery.isFunction( value ) ) {
      raw = true;
    }

    if ( bulk ) {

      // Bulk operations run against the entire set
      if ( raw ) {
        fn.call( elems, value );
        fn = null;

      // ...except when executing function values
      } else {
        bulk = fn;
        fn = function( elem, key, value ) {
          return bulk.call( jQuery( elem ), value );
        };
      }
    }

    if ( fn ) {
      for ( ; i < len; i++ ) {
        fn(
          elems[ i ], key, raw ?
          value :
          value.call( elems[ i ], i, fn( elems[ i ], key ) )
        );
      }
    }
  }

  return chainable ?
    elems :

    // Gets
    bulk ?
      fn.call( elems ) :
      len ? fn( elems[ 0 ], key ) : emptyGet;
};
var acceptData = function( owner ) {

  // Accepts only:
  //  - Node
  //    - Node.ELEMENT_NODE
  //    - Node.DOCUMENT_NODE
  //  - Object
  //    - Any
  /* jshint -W018 */
  return owner.nodeType === 1 || owner.nodeType === 9 || !( +owner.nodeType );
};




function Data() {
  this.expando = jQuery.expando + Data.uid++;
}

Data.uid = 1;

Data.prototype = {

  register: function( owner, initial ) {
    var value = initial || {};

    // If it is a node unlikely to be stringify-ed or looped over
    // use plain assignment
    if ( owner.nodeType ) {
      owner[ this.expando ] = value;

    // Otherwise secure it in a non-enumerable, non-writable property
    // configurability must be true to allow the property to be
    // deleted with the delete operator
    } else {
      Object.defineProperty( owner, this.expando, {
        value: value,
        writable: true,
        configurable: true
      } );
    }
    return owner[ this.expando ];
  },
  cache: function( owner ) {

    // We can accept data for non-element nodes in modern browsers,
    // but we should not, see #8335.
    // Always return an empty object.
    if ( !acceptData( owner ) ) {
      return {};
    }

    // Check if the owner object already has a cache
    var value = owner[ this.expando ];

    // If not, create one
    if ( !value ) {
      value = {};

      // We can accept data for non-element nodes in modern browsers,
      // but we should not, see #8335.
      // Always return an empty object.
      if ( acceptData( owner ) ) {

        // If it is a node unlikely to be stringify-ed or looped over
        // use plain assignment
        if ( owner.nodeType ) {
          owner[ this.expando ] = value;

        // Otherwise secure it in a non-enumerable property
        // configurable must be true to allow the property to be
        // deleted when data is removed
        } else {
          Object.defineProperty( owner, this.expando, {
            value: value,
            configurable: true
          } );
        }
      }
    }

    return value;
  },
  set: function( owner, data, value ) {
    var prop,
      cache = this.cache( owner );

    // Handle: [ owner, key, value ] args
    if ( typeof data === "string" ) {
      cache[ data ] = value;

    // Handle: [ owner, { properties } ] args
    } else {

      // Copy the properties one-by-one to the cache object
      for ( prop in data ) {
        cache[ prop ] = data[ prop ];
      }
    }
    return cache;
  },
  get: function( owner, key ) {
    return key === undefined ?
      this.cache( owner ) :
      owner[ this.expando ] && owner[ this.expando ][ key ];
  },
  access: function( owner, key, value ) {
    var stored;

    // In cases where either:
    //
    //   1. No key was specified
    //   2. A string key was specified, but no value provided
    //
    // Take the "read" path and allow the get method to determine
    // which value to return, respectively either:
    //
    //   1. The entire cache object
    //   2. The data stored at the key
    //
    if ( key === undefined ||
        ( ( key && typeof key === "string" ) && value === undefined ) ) {

      stored = this.get( owner, key );

      return stored !== undefined ?
        stored : this.get( owner, jQuery.camelCase( key ) );
    }

    // When the key is not a string, or both a key and value
    // are specified, set or extend (existing objects) with either:
    //
    //   1. An object of properties
    //   2. A key and value
    //
    this.set( owner, key, value );

    // Since the "set" path can have two possible entry points
    // return the expected data based on which path was taken[*]
    return value !== undefined ? value : key;
  },
  remove: function( owner, key ) {
    var i, name, camel,
      cache = owner[ this.expando ];

    if ( cache === undefined ) {
      return;
    }

    if ( key === undefined ) {
      this.register( owner );

    } else {

      // Support array or space separated string of keys
      if ( jQuery.isArray( key ) ) {

        // If "name" is an array of keys...
        // When data is initially created, via ("key", "val") signature,
        // keys will be converted to camelCase.
        // Since there is no way to tell _how_ a key was added, remove
        // both plain key and camelCase key. #12786
        // This will only penalize the array argument path.
        name = key.concat( key.map( jQuery.camelCase ) );
      } else {
        camel = jQuery.camelCase( key );

        // Try the string as a key before any manipulation
        if ( key in cache ) {
          name = [ key, camel ];
        } else {

          // If a key with the spaces exists, use it.
          // Otherwise, create an array by matching non-whitespace
          name = camel;
          name = name in cache ?
            [ name ] : ( name.match( rnotwhite ) || [] );
        }
      }

      i = name.length;

      while ( i-- ) {
        delete cache[ name[ i ] ];
      }
    }

    // Remove the expando if there's no more data
    if ( key === undefined || jQuery.isEmptyObject( cache ) ) {

      // Support: Chrome <= 35-45+
      // Webkit & Blink performance suffers when deleting properties
      // from DOM nodes, so set to undefined instead
      // https://code.google.com/p/chromium/issues/detail?id=378607
      if ( owner.nodeType ) {
        owner[ this.expando ] = undefined;
      } else {
        delete owner[ this.expando ];
      }
    }
  },
  hasData: function( owner ) {
    var cache = owner[ this.expando ];
    return cache !== undefined && !jQuery.isEmptyObject( cache );
  }
};
var dataPriv = new Data();

var dataUser = new Data();



//  Implementation Summary
//
//  1. Enforce API surface and semantic compatibility with 1.9.x branch
//  2. Improve the module's maintainability by reducing the storage
//    paths to a single mechanism.
//  3. Use the same single mechanism to support "private" and "user" data.
//  4. _Never_ expose "private" data to user code (TODO: Drop _data, _removeData)
//  5. Avoid exposing implementation details on user objects (eg. expando properties)
//  6. Provide a clear path for implementation upgrade to WeakMap in 2014

var rbrace = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
  rmultiDash = /[A-Z]/g;

function dataAttr( elem, key, data ) {
  var name;

  // If nothing was found internally, try to fetch any
  // data from the HTML5 data-* attribute
  if ( data === undefined && elem.nodeType === 1 ) {
    name = "data-" + key.replace( rmultiDash, "-$&" ).toLowerCase();
    data = elem.getAttribute( name );

    if ( typeof data === "string" ) {
      try {
        data = data === "true" ? true :
          data === "false" ? false :
          data === "null" ? null :

          // Only convert to a number if it doesn't change the string
          +data + "" === data ? +data :
          rbrace.test( data ) ? jQuery.parseJSON( data ) :
          data;
      } catch ( e ) {}

      // Make sure we set the data so it isn't changed later
      dataUser.set( elem, key, data );
    } else {
      data = undefined;
    }
  }
  return data;
}

jQuery.extend( {
  hasData: function( elem ) {
    return dataUser.hasData( elem ) || dataPriv.hasData( elem );
  },

  data: function( elem, name, data ) {
    return dataUser.access( elem, name, data );
  },

  removeData: function( elem, name ) {
    dataUser.remove( elem, name );
  },

  // TODO: Now that all calls to _data and _removeData have been replaced
  // with direct calls to dataPriv methods, these can be deprecated.
  _data: function( elem, name, data ) {
    return dataPriv.access( elem, name, data );
  },

  _removeData: function( elem, name ) {
    dataPriv.remove( elem, name );
  }
} );

jQuery.fn.extend( {
  data: function( key, value ) {
    var i, name, data,
      elem = this[ 0 ],
      attrs = elem && elem.attributes;

    // Gets all values
    if ( key === undefined ) {
      if ( this.length ) {
        data = dataUser.get( elem );

        if ( elem.nodeType === 1 && !dataPriv.get( elem, "hasDataAttrs" ) ) {
          i = attrs.length;
          while ( i-- ) {

            // Support: IE11+
            // The attrs elements can be null (#14894)
            if ( attrs[ i ] ) {
              name = attrs[ i ].name;
              if ( name.indexOf( "data-" ) === 0 ) {
                name = jQuery.camelCase( name.slice( 5 ) );
                dataAttr( elem, name, data[ name ] );
              }
            }
          }
          dataPriv.set( elem, "hasDataAttrs", true );
        }
      }

      return data;
    }

    // Sets multiple values
    if ( typeof key === "object" ) {
      return this.each( function() {
        dataUser.set( this, key );
      } );
    }

    return access( this, function( value ) {
      var data, camelKey;

      // The calling jQuery object (element matches) is not empty
      // (and therefore has an element appears at this[ 0 ]) and the
      // `value` parameter was not undefined. An empty jQuery object
      // will result in `undefined` for elem = this[ 0 ] which will
      // throw an exception if an attempt to read a data cache is made.
      if ( elem && value === undefined ) {

        // Attempt to get data from the cache
        // with the key as-is
        data = dataUser.get( elem, key ) ||

          // Try to find dashed key if it exists (gh-2779)
          // This is for 2.2.x only
          dataUser.get( elem, key.replace( rmultiDash, "-$&" ).toLowerCase() );

        if ( data !== undefined ) {
          return data;
        }

        camelKey = jQuery.camelCase( key );

        // Attempt to get data from the cache
        // with the key camelized
        data = dataUser.get( elem, camelKey );
        if ( data !== undefined ) {
          return data;
        }

        // Attempt to "discover" the data in
        // HTML5 custom data-* attrs
        data = dataAttr( elem, camelKey, undefined );
        if ( data !== undefined ) {
          return data;
        }

        // We tried really hard, but the data doesn't exist.
        return;
      }

      // Set the data...
      camelKey = jQuery.camelCase( key );
      this.each( function() {

        // First, attempt to store a copy or reference of any
        // data that might've been store with a camelCased key.
        var data = dataUser.get( this, camelKey );

        // For HTML5 data-* attribute interop, we have to
        // store property names with dashes in a camelCase form.
        // This might not apply to all properties...*
        dataUser.set( this, camelKey, value );

        // *... In the case of properties that might _actually_
        // have dashes, we need to also store a copy of that
        // unchanged property.
        if ( key.indexOf( "-" ) > -1 && data !== undefined ) {
          dataUser.set( this, key, value );
        }
      } );
    }, null, value, arguments.length > 1, null, true );
  },

  removeData: function( key ) {
    return this.each( function() {
      dataUser.remove( this, key );
    } );
  }
} );


jQuery.extend( {
  queue: function( elem, type, data ) {
    var queue;

    if ( elem ) {
      type = ( type || "fx" ) + "queue";
      queue = dataPriv.get( elem, type );

      // Speed up dequeue by getting out quickly if this is just a lookup
      if ( data ) {
        if ( !queue || jQuery.isArray( data ) ) {
          queue = dataPriv.access( elem, type, jQuery.makeArray( data ) );
        } else {
          queue.push( data );
        }
      }
      return queue || [];
    }
  },

  dequeue: function( elem, type ) {
    type = type || "fx";

    var queue = jQuery.queue( elem, type ),
      startLength = queue.length,
      fn = queue.shift(),
      hooks = jQuery._queueHooks( elem, type ),
      next = function() {
        jQuery.dequeue( elem, type );
      };

    // If the fx queue is dequeued, always remove the progress sentinel
    if ( fn === "inprogress" ) {
      fn = queue.shift();
      startLength--;
    }

    if ( fn ) {

      // Add a progress sentinel to prevent the fx queue from being
      // automatically dequeued
      if ( type === "fx" ) {
        queue.unshift( "inprogress" );
      }

      // Clear up the last queue stop function
      delete hooks.stop;
      fn.call( elem, next, hooks );
    }

    if ( !startLength && hooks ) {
      hooks.empty.fire();
    }
  },

  // Not public - generate a queueHooks object, or return the current one
  _queueHooks: function( elem, type ) {
    var key = type + "queueHooks";
    return dataPriv.get( elem, key ) || dataPriv.access( elem, key, {
      empty: jQuery.Callbacks( "once memory" ).add( function() {
        dataPriv.remove( elem, [ type + "queue", key ] );
      } )
    } );
  }
} );

jQuery.fn.extend( {
  queue: function( type, data ) {
    var setter = 2;

    if ( typeof type !== "string" ) {
      data = type;
      type = "fx";
      setter--;
    }

    if ( arguments.length < setter ) {
      return jQuery.queue( this[ 0 ], type );
    }

    return data === undefined ?
      this :
      this.each( function() {
        var queue = jQuery.queue( this, type, data );

        // Ensure a hooks for this queue
        jQuery._queueHooks( this, type );

        if ( type === "fx" && queue[ 0 ] !== "inprogress" ) {
          jQuery.dequeue( this, type );
        }
      } );
  },
  dequeue: function( type ) {
    return this.each( function() {
      jQuery.dequeue( this, type );
    } );
  },
  clearQueue: function( type ) {
    return this.queue( type || "fx", [] );
  },

  // Get a promise resolved when queues of a certain type
  // are emptied (fx is the type by default)
  promise: function( type, obj ) {
    var tmp,
      count = 1,
      defer = jQuery.Deferred(),
      elements = this,
      i = this.length,
      resolve = function() {
        if ( !( --count ) ) {
          defer.resolveWith( elements, [ elements ] );
        }
      };

    if ( typeof type !== "string" ) {
      obj = type;
      type = undefined;
    }
    type = type || "fx";

    while ( i-- ) {
      tmp = dataPriv.get( elements[ i ], type + "queueHooks" );
      if ( tmp && tmp.empty ) {
        count++;
        tmp.empty.add( resolve );
      }
    }
    resolve();
    return defer.promise( obj );
  }
} );
var pnum = ( /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/ ).source;

var rcssNum = new RegExp( "^(?:([+-])=|)(" + pnum + ")([a-z%]*)$", "i" );


var cssExpand = [ "Top", "Right", "Bottom", "Left" ];

var isHidden = function( elem, el ) {

    // isHidden might be called from jQuery#filter function;
    // in that case, element will be second argument
    elem = el || elem;
    return jQuery.css( elem, "display" ) === "none" ||
      !jQuery.contains( elem.ownerDocument, elem );
  };



function adjustCSS( elem, prop, valueParts, tween ) {
  var adjusted,
    scale = 1,
    maxIterations = 20,
    currentValue = tween ?
      function() { return tween.cur(); } :
      function() { return jQuery.css( elem, prop, "" ); },
    initial = currentValue(),
    unit = valueParts && valueParts[ 3 ] || ( jQuery.cssNumber[ prop ] ? "" : "px" ),

    // Starting value computation is required for potential unit mismatches
    initialInUnit = ( jQuery.cssNumber[ prop ] || unit !== "px" && +initial ) &&
      rcssNum.exec( jQuery.css( elem, prop ) );

  if ( initialInUnit && initialInUnit[ 3 ] !== unit ) {

    // Trust units reported by jQuery.css
    unit = unit || initialInUnit[ 3 ];

    // Make sure we update the tween properties later on
    valueParts = valueParts || [];

    // Iteratively approximate from a nonzero starting point
    initialInUnit = +initial || 1;

    do {

      // If previous iteration zeroed out, double until we get *something*.
      // Use string for doubling so we don't accidentally see scale as unchanged below
      scale = scale || ".5";

      // Adjust and apply
      initialInUnit = initialInUnit / scale;
      jQuery.style( elem, prop, initialInUnit + unit );

    // Update scale, tolerating zero or NaN from tween.cur()
    // Break the loop if scale is unchanged or perfect, or if we've just had enough.
    } while (
      scale !== ( scale = currentValue() / initial ) && scale !== 1 && --maxIterations
    );
  }

  if ( valueParts ) {
    initialInUnit = +initialInUnit || +initial || 0;

    // Apply relative offset (+=/-=) if specified
    adjusted = valueParts[ 1 ] ?
      initialInUnit + ( valueParts[ 1 ] + 1 ) * valueParts[ 2 ] :
      +valueParts[ 2 ];
    if ( tween ) {
      tween.unit = unit;
      tween.start = initialInUnit;
      tween.end = adjusted;
    }
  }
  return adjusted;
}
var rcheckableType = ( /^(?:checkbox|radio)$/i );

var rtagName = ( /<([\w:-]+)/ );

var rscriptType = ( /^$|\/(?:java|ecma)script/i );



// We have to close these tags to support XHTML (#13200)
var wrapMap = {

  // Support: IE9
  option: [ 1, "<select multiple='multiple'>", "</select>" ],

  // XHTML parsers do not magically insert elements in the
  // same way that tag soup parsers do. So we cannot shorten
  // this by omitting <tbody> or other required elements.
  thead: [ 1, "<table>", "</table>" ],
  col: [ 2, "<table><colgroup>", "</colgroup></table>" ],
  tr: [ 2, "<table><tbody>", "</tbody></table>" ],
  td: [ 3, "<table><tbody><tr>", "</tr></tbody></table>" ],

  _default: [ 0, "", "" ]
};

// Support: IE9
wrapMap.optgroup = wrapMap.option;

wrapMap.tbody = wrapMap.tfoot = wrapMap.colgroup = wrapMap.caption = wrapMap.thead;
wrapMap.th = wrapMap.td;


function getAll( context, tag ) {

  // Support: IE9-11+
  // Use typeof to avoid zero-argument method invocation on host objects (#15151)
  var ret = typeof context.getElementsByTagName !== "undefined" ?
      context.getElementsByTagName( tag || "*" ) :
      typeof context.querySelectorAll !== "undefined" ?
        context.querySelectorAll( tag || "*" ) :
      [];

  return tag === undefined || tag && jQuery.nodeName( context, tag ) ?
    jQuery.merge( [ context ], ret ) :
    ret;
}


// Mark scripts as having already been evaluated
function setGlobalEval( elems, refElements ) {
  var i = 0,
    l = elems.length;

  for ( ; i < l; i++ ) {
    dataPriv.set(
      elems[ i ],
      "globalEval",
      !refElements || dataPriv.get( refElements[ i ], "globalEval" )
    );
  }
}


var rhtml = /<|&#?\w+;/;

function buildFragment( elems, context, scripts, selection, ignored ) {
  var elem, tmp, tag, wrap, contains, j,
    fragment = context.createDocumentFragment(),
    nodes = [],
    i = 0,
    l = elems.length;

  for ( ; i < l; i++ ) {
    elem = elems[ i ];

    if ( elem || elem === 0 ) {

      // Add nodes directly
      if ( jQuery.type( elem ) === "object" ) {

        // Support: Android<4.1, PhantomJS<2
        // push.apply(_, arraylike) throws on ancient WebKit
        jQuery.merge( nodes, elem.nodeType ? [ elem ] : elem );

      // Convert non-html into a text node
      } else if ( !rhtml.test( elem ) ) {
        nodes.push( context.createTextNode( elem ) );

      // Convert html into DOM nodes
      } else {
        tmp = tmp || fragment.appendChild( context.createElement( "div" ) );

        // Deserialize a standard representation
        tag = ( rtagName.exec( elem ) || [ "", "" ] )[ 1 ].toLowerCase();
        wrap = wrapMap[ tag ] || wrapMap._default;
        tmp.innerHTML = wrap[ 1 ] + jQuery.htmlPrefilter( elem ) + wrap[ 2 ];

        // Descend through wrappers to the right content
        j = wrap[ 0 ];
        while ( j-- ) {
          tmp = tmp.lastChild;
        }

        // Support: Android<4.1, PhantomJS<2
        // push.apply(_, arraylike) throws on ancient WebKit
        jQuery.merge( nodes, tmp.childNodes );

        // Remember the top-level container
        tmp = fragment.firstChild;

        // Ensure the created nodes are orphaned (#12392)
        tmp.textContent = "";
      }
    }
  }

  // Remove wrapper from fragment
  fragment.textContent = "";

  i = 0;
  while ( ( elem = nodes[ i++ ] ) ) {

    // Skip elements already in the context collection (trac-4087)
    if ( selection && jQuery.inArray( elem, selection ) > -1 ) {
      if ( ignored ) {
        ignored.push( elem );
      }
      continue;
    }

    contains = jQuery.contains( elem.ownerDocument, elem );

    // Append to fragment
    tmp = getAll( fragment.appendChild( elem ), "script" );

    // Preserve script evaluation history
    if ( contains ) {
      setGlobalEval( tmp );
    }

    // Capture executables
    if ( scripts ) {
      j = 0;
      while ( ( elem = tmp[ j++ ] ) ) {
        if ( rscriptType.test( elem.type || "" ) ) {
          scripts.push( elem );
        }
      }
    }
  }

  return fragment;
}


( function() {
  var fragment = document.createDocumentFragment(),
    div = fragment.appendChild( document.createElement( "div" ) ),
    input = document.createElement( "input" );

  // Support: Android 4.0-4.3, Safari<=5.1
  // Check state lost if the name is set (#11217)
  // Support: Windows Web Apps (WWA)
  // `name` and `type` must use .setAttribute for WWA (#14901)
  input.setAttribute( "type", "radio" );
  input.setAttribute( "checked", "checked" );
  input.setAttribute( "name", "t" );

  div.appendChild( input );

  // Support: Safari<=5.1, Android<4.2
  // Older WebKit doesn't clone checked state correctly in fragments
  support.checkClone = div.cloneNode( true ).cloneNode( true ).lastChild.checked;

  // Support: IE<=11+
  // Make sure textarea (and checkbox) defaultValue is properly cloned
  div.innerHTML = "<textarea>x</textarea>";
  support.noCloneChecked = !!div.cloneNode( true ).lastChild.defaultValue;
} )();


var
  rkeyEvent = /^key/,
  rmouseEvent = /^(?:mouse|pointer|contextmenu|drag|drop)|click/,
  rtypenamespace = /^([^.]*)(?:\.(.+)|)/;

function returnTrue() {
  return true;
}

function returnFalse() {
  return false;
}

// Support: IE9
// See #13393 for more info
function safeActiveElement() {
  try {
    return document.activeElement;
  } catch ( err ) { }
}

function on( elem, types, selector, data, fn, one ) {
  var origFn, type;

  // Types can be a map of types/handlers
  if ( typeof types === "object" ) {

    // ( types-Object, selector, data )
    if ( typeof selector !== "string" ) {

      // ( types-Object, data )
      data = data || selector;
      selector = undefined;
    }
    for ( type in types ) {
      on( elem, type, selector, data, types[ type ], one );
    }
    return elem;
  }

  if ( data == null && fn == null ) {

    // ( types, fn )
    fn = selector;
    data = selector = undefined;
  } else if ( fn == null ) {
    if ( typeof selector === "string" ) {

      // ( types, selector, fn )
      fn = data;
      data = undefined;
    } else {

      // ( types, data, fn )
      fn = data;
      data = selector;
      selector = undefined;
    }
  }
  if ( fn === false ) {
    fn = returnFalse;
  } else if ( !fn ) {
    return elem;
  }

  if ( one === 1 ) {
    origFn = fn;
    fn = function( event ) {

      // Can use an empty set, since event contains the info
      jQuery().off( event );
      return origFn.apply( this, arguments );
    };

    // Use same guid so caller can remove using origFn
    fn.guid = origFn.guid || ( origFn.guid = jQuery.guid++ );
  }
  return elem.each( function() {
    jQuery.event.add( this, types, fn, data, selector );
  } );
}

/*
 * Helper functions for managing events -- not part of the public interface.
 * Props to Dean Edwards' addEvent library for many of the ideas.
 */
jQuery.event = {

  global: {},

  add: function( elem, types, handler, data, selector ) {

    var handleObjIn, eventHandle, tmp,
      events, t, handleObj,
      special, handlers, type, namespaces, origType,
      elemData = dataPriv.get( elem );

    // Don't attach events to noData or text/comment nodes (but allow plain objects)
    if ( !elemData ) {
      return;
    }

    // Caller can pass in an object of custom data in lieu of the handler
    if ( handler.handler ) {
      handleObjIn = handler;
      handler = handleObjIn.handler;
      selector = handleObjIn.selector;
    }

    // Make sure that the handler has a unique ID, used to find/remove it later
    if ( !handler.guid ) {
      handler.guid = jQuery.guid++;
    }

    // Init the element's event structure and main handler, if this is the first
    if ( !( events = elemData.events ) ) {
      events = elemData.events = {};
    }
    if ( !( eventHandle = elemData.handle ) ) {
      eventHandle = elemData.handle = function( e ) {

        // Discard the second event of a jQuery.event.trigger() and
        // when an event is called after a page has unloaded
        return typeof jQuery !== "undefined" && jQuery.event.triggered !== e.type ?
          jQuery.event.dispatch.apply( elem, arguments ) : undefined;
      };
    }

    // Handle multiple events separated by a space
    types = ( types || "" ).match( rnotwhite ) || [ "" ];
    t = types.length;
    while ( t-- ) {
      tmp = rtypenamespace.exec( types[ t ] ) || [];
      type = origType = tmp[ 1 ];
      namespaces = ( tmp[ 2 ] || "" ).split( "." ).sort();

      // There *must* be a type, no attaching namespace-only handlers
      if ( !type ) {
        continue;
      }

      // If event changes its type, use the special event handlers for the changed type
      special = jQuery.event.special[ type ] || {};

      // If selector defined, determine special event api type, otherwise given type
      type = ( selector ? special.delegateType : special.bindType ) || type;

      // Update special based on newly reset type
      special = jQuery.event.special[ type ] || {};

      // handleObj is passed to all event handlers
      handleObj = jQuery.extend( {
        type: type,
        origType: origType,
        data: data,
        handler: handler,
        guid: handler.guid,
        selector: selector,
        needsContext: selector && jQuery.expr.match.needsContext.test( selector ),
        namespace: namespaces.join( "." )
      }, handleObjIn );

      // Init the event handler queue if we're the first
      if ( !( handlers = events[ type ] ) ) {
        handlers = events[ type ] = [];
        handlers.delegateCount = 0;

        // Only use addEventListener if the special events handler returns false
        if ( !special.setup ||
          special.setup.call( elem, data, namespaces, eventHandle ) === false ) {

          if ( elem.addEventListener ) {
            elem.addEventListener( type, eventHandle );
          }
        }
      }

      if ( special.add ) {
        special.add.call( elem, handleObj );

        if ( !handleObj.handler.guid ) {
          handleObj.handler.guid = handler.guid;
        }
      }

      // Add to the element's handler list, delegates in front
      if ( selector ) {
        handlers.splice( handlers.delegateCount++, 0, handleObj );
      } else {
        handlers.push( handleObj );
      }

      // Keep track of which events have ever been used, for event optimization
      jQuery.event.global[ type ] = true;
    }

  },

  // Detach an event or set of events from an element
  remove: function( elem, types, handler, selector, mappedTypes ) {

    var j, origCount, tmp,
      events, t, handleObj,
      special, handlers, type, namespaces, origType,
      elemData = dataPriv.hasData( elem ) && dataPriv.get( elem );

    if ( !elemData || !( events = elemData.events ) ) {
      return;
    }

    // Once for each type.namespace in types; type may be omitted
    types = ( types || "" ).match( rnotwhite ) || [ "" ];
    t = types.length;
    while ( t-- ) {
      tmp = rtypenamespace.exec( types[ t ] ) || [];
      type = origType = tmp[ 1 ];
      namespaces = ( tmp[ 2 ] || "" ).split( "." ).sort();

      // Unbind all events (on this namespace, if provided) for the element
      if ( !type ) {
        for ( type in events ) {
          jQuery.event.remove( elem, type + types[ t ], handler, selector, true );
        }
        continue;
      }

      special = jQuery.event.special[ type ] || {};
      type = ( selector ? special.delegateType : special.bindType ) || type;
      handlers = events[ type ] || [];
      tmp = tmp[ 2 ] &&
        new RegExp( "(^|\\.)" + namespaces.join( "\\.(?:.*\\.|)" ) + "(\\.|$)" );

      // Remove matching events
      origCount = j = handlers.length;
      while ( j-- ) {
        handleObj = handlers[ j ];

        if ( ( mappedTypes || origType === handleObj.origType ) &&
          ( !handler || handler.guid === handleObj.guid ) &&
          ( !tmp || tmp.test( handleObj.namespace ) ) &&
          ( !selector || selector === handleObj.selector ||
            selector === "**" && handleObj.selector ) ) {
          handlers.splice( j, 1 );

          if ( handleObj.selector ) {
            handlers.delegateCount--;
          }
          if ( special.remove ) {
            special.remove.call( elem, handleObj );
          }
        }
      }

      // Remove generic event handler if we removed something and no more handlers exist
      // (avoids potential for endless recursion during removal of special event handlers)
      if ( origCount && !handlers.length ) {
        if ( !special.teardown ||
          special.teardown.call( elem, namespaces, elemData.handle ) === false ) {

          jQuery.removeEvent( elem, type, elemData.handle );
        }

        delete events[ type ];
      }
    }

    // Remove data and the expando if it's no longer used
    if ( jQuery.isEmptyObject( events ) ) {
      dataPriv.remove( elem, "handle events" );
    }
  },

  dispatch: function( event ) {

    // Make a writable jQuery.Event from the native event object
    event = jQuery.event.fix( event );

    var i, j, ret, matched, handleObj,
      handlerQueue = [],
      args = slice.call( arguments ),
      handlers = ( dataPriv.get( this, "events" ) || {} )[ event.type ] || [],
      special = jQuery.event.special[ event.type ] || {};

    // Use the fix-ed jQuery.Event rather than the (read-only) native event
    args[ 0 ] = event;
    event.delegateTarget = this;

    // Call the preDispatch hook for the mapped type, and let it bail if desired
    if ( special.preDispatch && special.preDispatch.call( this, event ) === false ) {
      return;
    }

    // Determine handlers
    handlerQueue = jQuery.event.handlers.call( this, event, handlers );

    // Run delegates first; they may want to stop propagation beneath us
    i = 0;
    while ( ( matched = handlerQueue[ i++ ] ) && !event.isPropagationStopped() ) {
      event.currentTarget = matched.elem;

      j = 0;
      while ( ( handleObj = matched.handlers[ j++ ] ) &&
        !event.isImmediatePropagationStopped() ) {

        // Triggered event must either 1) have no namespace, or 2) have namespace(s)
        // a subset or equal to those in the bound event (both can have no namespace).
        if ( !event.rnamespace || event.rnamespace.test( handleObj.namespace ) ) {

          event.handleObj = handleObj;
          event.data = handleObj.data;

          ret = ( ( jQuery.event.special[ handleObj.origType ] || {} ).handle ||
            handleObj.handler ).apply( matched.elem, args );

          if ( ret !== undefined ) {
            if ( ( event.result = ret ) === false ) {
              event.preventDefault();
              event.stopPropagation();
            }
          }
        }
      }
    }

    // Call the postDispatch hook for the mapped type
    if ( special.postDispatch ) {
      special.postDispatch.call( this, event );
    }

    return event.result;
  },

  handlers: function( event, handlers ) {
    var i, matches, sel, handleObj,
      handlerQueue = [],
      delegateCount = handlers.delegateCount,
      cur = event.target;

    // Support (at least): Chrome, IE9
    // Find delegate handlers
    // Black-hole SVG <use> instance trees (#13180)
    //
    // Support: Firefox<=42+
    // Avoid non-left-click in FF but don't block IE radio events (#3861, gh-2343)
    if ( delegateCount && cur.nodeType &&
      ( event.type !== "click" || isNaN( event.button ) || event.button < 1 ) ) {

      for ( ; cur !== this; cur = cur.parentNode || this ) {

        // Don't check non-elements (#13208)
        // Don't process clicks on disabled elements (#6911, #8165, #11382, #11764)
        if ( cur.nodeType === 1 && ( cur.disabled !== true || event.type !== "click" ) ) {
          matches = [];
          for ( i = 0; i < delegateCount; i++ ) {
            handleObj = handlers[ i ];

            // Don't conflict with Object.prototype properties (#13203)
            sel = handleObj.selector + " ";

            if ( matches[ sel ] === undefined ) {
              matches[ sel ] = handleObj.needsContext ?
                jQuery( sel, this ).index( cur ) > -1 :
                jQuery.find( sel, this, null, [ cur ] ).length;
            }
            if ( matches[ sel ] ) {
              matches.push( handleObj );
            }
          }
          if ( matches.length ) {
            handlerQueue.push( { elem: cur, handlers: matches } );
          }
        }
      }
    }

    // Add the remaining (directly-bound) handlers
    if ( delegateCount < handlers.length ) {
      handlerQueue.push( { elem: this, handlers: handlers.slice( delegateCount ) } );
    }

    return handlerQueue;
  },

  // Includes some event props shared by KeyEvent and MouseEvent
  props: ( "altKey bubbles cancelable ctrlKey currentTarget detail eventPhase " +
    "metaKey relatedTarget shiftKey target timeStamp view which" ).split( " " ),

  fixHooks: {},

  keyHooks: {
    props: "char charCode key keyCode".split( " " ),
    filter: function( event, original ) {

      // Add which for key events
      if ( event.which == null ) {
        event.which = original.charCode != null ? original.charCode : original.keyCode;
      }

      return event;
    }
  },

  mouseHooks: {
    props: ( "button buttons clientX clientY offsetX offsetY pageX pageY " +
      "screenX screenY toElement" ).split( " " ),
    filter: function( event, original ) {
      var eventDoc, doc, body,
        button = original.button;

      // Calculate pageX/Y if missing and clientX/Y available
      if ( event.pageX == null && original.clientX != null ) {
        eventDoc = event.target.ownerDocument || document;
        doc = eventDoc.documentElement;
        body = eventDoc.body;

        event.pageX = original.clientX +
          ( doc && doc.scrollLeft || body && body.scrollLeft || 0 ) -
          ( doc && doc.clientLeft || body && body.clientLeft || 0 );
        event.pageY = original.clientY +
          ( doc && doc.scrollTop  || body && body.scrollTop  || 0 ) -
          ( doc && doc.clientTop  || body && body.clientTop  || 0 );
      }

      // Add which for click: 1 === left; 2 === middle; 3 === right
      // Note: button is not normalized, so don't use it
      if ( !event.which && button !== undefined ) {
        event.which = ( button & 1 ? 1 : ( button & 2 ? 3 : ( button & 4 ? 2 : 0 ) ) );
      }

      return event;
    }
  },

  fix: function( event ) {
    if ( event[ jQuery.expando ] ) {
      return event;
    }

    // Create a writable copy of the event object and normalize some properties
    var i, prop, copy,
      type = event.type,
      originalEvent = event,
      fixHook = this.fixHooks[ type ];

    if ( !fixHook ) {
      this.fixHooks[ type ] = fixHook =
        rmouseEvent.test( type ) ? this.mouseHooks :
        rkeyEvent.test( type ) ? this.keyHooks :
        {};
    }
    copy = fixHook.props ? this.props.concat( fixHook.props ) : this.props;

    event = new jQuery.Event( originalEvent );

    i = copy.length;
    while ( i-- ) {
      prop = copy[ i ];
      event[ prop ] = originalEvent[ prop ];
    }

    // Support: Cordova 2.5 (WebKit) (#13255)
    // All events should have a target; Cordova deviceready doesn't
    if ( !event.target ) {
      event.target = document;
    }

    // Support: Safari 6.0+, Chrome<28
    // Target should not be a text node (#504, #13143)
    if ( event.target.nodeType === 3 ) {
      event.target = event.target.parentNode;
    }

    return fixHook.filter ? fixHook.filter( event, originalEvent ) : event;
  },

  special: {
    load: {

      // Prevent triggered image.load events from bubbling to window.load
      noBubble: true
    },
    focus: {

      // Fire native event if possible so blur/focus sequence is correct
      trigger: function() {
        if ( this !== safeActiveElement() && this.focus ) {
          this.focus();
          return false;
        }
      },
      delegateType: "focusin"
    },
    blur: {
      trigger: function() {
        if ( this === safeActiveElement() && this.blur ) {
          this.blur();
          return false;
        }
      },
      delegateType: "focusout"
    },
    click: {

      // For checkbox, fire native event so checked state will be right
      trigger: function() {
        if ( this.type === "checkbox" && this.click && jQuery.nodeName( this, "input" ) ) {
          this.click();
          return false;
        }
      },

      // For cross-browser consistency, don't fire native .click() on links
      _default: function( event ) {
        return jQuery.nodeName( event.target, "a" );
      }
    },

    beforeunload: {
      postDispatch: function( event ) {

        // Support: Firefox 20+
        // Firefox doesn't alert if the returnValue field is not set.
        if ( event.result !== undefined && event.originalEvent ) {
          event.originalEvent.returnValue = event.result;
        }
      }
    }
  }
};

jQuery.removeEvent = function( elem, type, handle ) {

  // This "if" is needed for plain objects
  if ( elem.removeEventListener ) {
    elem.removeEventListener( type, handle );
  }
};

jQuery.Event = function( src, props ) {

  // Allow instantiation without the 'new' keyword
  if ( !( this instanceof jQuery.Event ) ) {
    return new jQuery.Event( src, props );
  }

  // Event object
  if ( src && src.type ) {
    this.originalEvent = src;
    this.type = src.type;

    // Events bubbling up the document may have been marked as prevented
    // by a handler lower down the tree; reflect the correct value.
    this.isDefaultPrevented = src.defaultPrevented ||
        src.defaultPrevented === undefined &&

        // Support: Android<4.0
        src.returnValue === false ?
      returnTrue :
      returnFalse;

  // Event type
  } else {
    this.type = src;
  }

  // Put explicitly provided properties onto the event object
  if ( props ) {
    jQuery.extend( this, props );
  }

  // Create a timestamp if incoming event doesn't have one
  this.timeStamp = src && src.timeStamp || jQuery.now();

  // Mark it as fixed
  this[ jQuery.expando ] = true;
};

// jQuery.Event is based on DOM3 Events as specified by the ECMAScript Language Binding
// http://www.w3.org/TR/2003/WD-DOM-Level-3-Events-20030331/ecma-script-binding.html
jQuery.Event.prototype = {
  constructor: jQuery.Event,
  isDefaultPrevented: returnFalse,
  isPropagationStopped: returnFalse,
  isImmediatePropagationStopped: returnFalse,
  isSimulated: false,

  preventDefault: function() {
    var e = this.originalEvent;

    this.isDefaultPrevented = returnTrue;

    if ( e && !this.isSimulated ) {
      e.preventDefault();
    }
  },
  stopPropagation: function() {
    var e = this.originalEvent;

    this.isPropagationStopped = returnTrue;

    if ( e && !this.isSimulated ) {
      e.stopPropagation();
    }
  },
  stopImmediatePropagation: function() {
    var e = this.originalEvent;

    this.isImmediatePropagationStopped = returnTrue;

    if ( e && !this.isSimulated ) {
      e.stopImmediatePropagation();
    }

    this.stopPropagation();
  }
};

// Create mouseenter/leave events using mouseover/out and event-time checks
// so that event delegation works in jQuery.
// Do the same for pointerenter/pointerleave and pointerover/pointerout
//
// Support: Safari 7 only
// Safari sends mouseenter too often; see:
// https://code.google.com/p/chromium/issues/detail?id=470258
// for the description of the bug (it existed in older Chrome versions as well).
jQuery.each( {
  mouseenter: "mouseover",
  mouseleave: "mouseout",
  pointerenter: "pointerover",
  pointerleave: "pointerout"
}, function( orig, fix ) {
  jQuery.event.special[ orig ] = {
    delegateType: fix,
    bindType: fix,

    handle: function( event ) {
      var ret,
        target = this,
        related = event.relatedTarget,
        handleObj = event.handleObj;

      // For mouseenter/leave call the handler if related is outside the target.
      // NB: No relatedTarget if the mouse left/entered the browser window
      if ( !related || ( related !== target && !jQuery.contains( target, related ) ) ) {
        event.type = handleObj.origType;
        ret = handleObj.handler.apply( this, arguments );
        event.type = fix;
      }
      return ret;
    }
  };
} );

jQuery.fn.extend( {
  on: function( types, selector, data, fn ) {
    return on( this, types, selector, data, fn );
  },
  one: function( types, selector, data, fn ) {
    return on( this, types, selector, data, fn, 1 );
  },
  off: function( types, selector, fn ) {
    var handleObj, type;
    if ( types && types.preventDefault && types.handleObj ) {

      // ( event )  dispatched jQuery.Event
      handleObj = types.handleObj;
      jQuery( types.delegateTarget ).off(
        handleObj.namespace ?
          handleObj.origType + "." + handleObj.namespace :
          handleObj.origType,
        handleObj.selector,
        handleObj.handler
      );
      return this;
    }
    if ( typeof types === "object" ) {

      // ( types-object [, selector] )
      for ( type in types ) {
        this.off( type, selector, types[ type ] );
      }
      return this;
    }
    if ( selector === false || typeof selector === "function" ) {

      // ( types [, fn] )
      fn = selector;
      selector = undefined;
    }
    if ( fn === false ) {
      fn = returnFalse;
    }
    return this.each( function() {
      jQuery.event.remove( this, types, fn, selector );
    } );
  }
} );


var
  rxhtmlTag = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:-]+)[^>]*)\/>/gi,

  // Support: IE 10-11, Edge 10240+
  // In IE/Edge using regex groups here causes severe slowdowns.
  // See https://connect.microsoft.com/IE/feedback/details/1736512/
  rnoInnerhtml = /<script|<style|<link/i,

  // checked="checked" or checked
  rchecked = /checked\s*(?:[^=]|=\s*.checked.)/i,
  rscriptTypeMasked = /^true\/(.*)/,
  rcleanScript = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;

// Manipulating tables requires a tbody
function manipulationTarget( elem, content ) {
  return jQuery.nodeName( elem, "table" ) &&
    jQuery.nodeName( content.nodeType !== 11 ? content : content.firstChild, "tr" ) ?

    elem.getElementsByTagName( "tbody" )[ 0 ] ||
      elem.appendChild( elem.ownerDocument.createElement( "tbody" ) ) :
    elem;
}

// Replace/restore the type attribute of script elements for safe DOM manipulation
function disableScript( elem ) {
  elem.type = ( elem.getAttribute( "type" ) !== null ) + "/" + elem.type;
  return elem;
}
function restoreScript( elem ) {
  var match = rscriptTypeMasked.exec( elem.type );

  if ( match ) {
    elem.type = match[ 1 ];
  } else {
    elem.removeAttribute( "type" );
  }

  return elem;
}

function cloneCopyEvent( src, dest ) {
  var i, l, type, pdataOld, pdataCur, udataOld, udataCur, events;

  if ( dest.nodeType !== 1 ) {
    return;
  }

  // 1. Copy private data: events, handlers, etc.
  if ( dataPriv.hasData( src ) ) {
    pdataOld = dataPriv.access( src );
    pdataCur = dataPriv.set( dest, pdataOld );
    events = pdataOld.events;

    if ( events ) {
      delete pdataCur.handle;
      pdataCur.events = {};

      for ( type in events ) {
        for ( i = 0, l = events[ type ].length; i < l; i++ ) {
          jQuery.event.add( dest, type, events[ type ][ i ] );
        }
      }
    }
  }

  // 2. Copy user data
  if ( dataUser.hasData( src ) ) {
    udataOld = dataUser.access( src );
    udataCur = jQuery.extend( {}, udataOld );

    dataUser.set( dest, udataCur );
  }
}

// Fix IE bugs, see support tests
function fixInput( src, dest ) {
  var nodeName = dest.nodeName.toLowerCase();

  // Fails to persist the checked state of a cloned checkbox or radio button.
  if ( nodeName === "input" && rcheckableType.test( src.type ) ) {
    dest.checked = src.checked;

  // Fails to return the selected option to the default selected state when cloning options
  } else if ( nodeName === "input" || nodeName === "textarea" ) {
    dest.defaultValue = src.defaultValue;
  }
}

function domManip( collection, args, callback, ignored ) {

  // Flatten any nested arrays
  args = concat.apply( [], args );

  var fragment, first, scripts, hasScripts, node, doc,
    i = 0,
    l = collection.length,
    iNoClone = l - 1,
    value = args[ 0 ],
    isFunction = jQuery.isFunction( value );

  // We can't cloneNode fragments that contain checked, in WebKit
  if ( isFunction ||
      ( l > 1 && typeof value === "string" &&
        !support.checkClone && rchecked.test( value ) ) ) {
    return collection.each( function( index ) {
      var self = collection.eq( index );
      if ( isFunction ) {
        args[ 0 ] = value.call( this, index, self.html() );
      }
      domManip( self, args, callback, ignored );
    } );
  }

  if ( l ) {
    fragment = buildFragment( args, collection[ 0 ].ownerDocument, false, collection, ignored );
    first = fragment.firstChild;

    if ( fragment.childNodes.length === 1 ) {
      fragment = first;
    }

    // Require either new content or an interest in ignored elements to invoke the callback
    if ( first || ignored ) {
      scripts = jQuery.map( getAll( fragment, "script" ), disableScript );
      hasScripts = scripts.length;

      // Use the original fragment for the last item
      // instead of the first because it can end up
      // being emptied incorrectly in certain situations (#8070).
      for ( ; i < l; i++ ) {
        node = fragment;

        if ( i !== iNoClone ) {
          node = jQuery.clone( node, true, true );

          // Keep references to cloned scripts for later restoration
          if ( hasScripts ) {

            // Support: Android<4.1, PhantomJS<2
            // push.apply(_, arraylike) throws on ancient WebKit
            jQuery.merge( scripts, getAll( node, "script" ) );
          }
        }

        callback.call( collection[ i ], node, i );
      }

      if ( hasScripts ) {
        doc = scripts[ scripts.length - 1 ].ownerDocument;

        // Reenable scripts
        jQuery.map( scripts, restoreScript );

        // Evaluate executable scripts on first document insertion
        for ( i = 0; i < hasScripts; i++ ) {
          node = scripts[ i ];
          if ( rscriptType.test( node.type || "" ) &&
            !dataPriv.access( node, "globalEval" ) &&
            jQuery.contains( doc, node ) ) {

            if ( node.src ) {

              // Optional AJAX dependency, but won't run scripts if not present
              if ( jQuery._evalUrl ) {
                jQuery._evalUrl( node.src );
              }
            } else {
              jQuery.globalEval( node.textContent.replace( rcleanScript, "" ) );
            }
          }
        }
      }
    }
  }

  return collection;
}

function remove( elem, selector, keepData ) {
  var node,
    nodes = selector ? jQuery.filter( selector, elem ) : elem,
    i = 0;

  for ( ; ( node = nodes[ i ] ) != null; i++ ) {
    if ( !keepData && node.nodeType === 1 ) {
      jQuery.cleanData( getAll( node ) );
    }

    if ( node.parentNode ) {
      if ( keepData && jQuery.contains( node.ownerDocument, node ) ) {
        setGlobalEval( getAll( node, "script" ) );
      }
      node.parentNode.removeChild( node );
    }
  }

  return elem;
}

jQuery.extend( {
  htmlPrefilter: function( html ) {
    return html.replace( rxhtmlTag, "<$1></$2>" );
  },

  clone: function( elem, dataAndEvents, deepDataAndEvents ) {
    var i, l, srcElements, destElements,
      clone = elem.cloneNode( true ),
      inPage = jQuery.contains( elem.ownerDocument, elem );

    // Fix IE cloning issues
    if ( !support.noCloneChecked && ( elem.nodeType === 1 || elem.nodeType === 11 ) &&
        !jQuery.isXMLDoc( elem ) ) {

      // We eschew Sizzle here for performance reasons: http://jsperf.com/getall-vs-sizzle/2
      destElements = getAll( clone );
      srcElements = getAll( elem );

      for ( i = 0, l = srcElements.length; i < l; i++ ) {
        fixInput( srcElements[ i ], destElements[ i ] );
      }
    }

    // Copy the events from the original to the clone
    if ( dataAndEvents ) {
      if ( deepDataAndEvents ) {
        srcElements = srcElements || getAll( elem );
        destElements = destElements || getAll( clone );

        for ( i = 0, l = srcElements.length; i < l; i++ ) {
          cloneCopyEvent( srcElements[ i ], destElements[ i ] );
        }
      } else {
        cloneCopyEvent( elem, clone );
      }
    }

    // Preserve script evaluation history
    destElements = getAll( clone, "script" );
    if ( destElements.length > 0 ) {
      setGlobalEval( destElements, !inPage && getAll( elem, "script" ) );
    }

    // Return the cloned set
    return clone;
  },

  cleanData: function( elems ) {
    var data, elem, type,
      special = jQuery.event.special,
      i = 0;

    for ( ; ( elem = elems[ i ] ) !== undefined; i++ ) {
      if ( acceptData( elem ) ) {
        if ( ( data = elem[ dataPriv.expando ] ) ) {
          if ( data.events ) {
            for ( type in data.events ) {
              if ( special[ type ] ) {
                jQuery.event.remove( elem, type );

              // This is a shortcut to avoid jQuery.event.remove's overhead
              } else {
                jQuery.removeEvent( elem, type, data.handle );
              }
            }
          }

          // Support: Chrome <= 35-45+
          // Assign undefined instead of using delete, see Data#remove
          elem[ dataPriv.expando ] = undefined;
        }
        if ( elem[ dataUser.expando ] ) {

          // Support: Chrome <= 35-45+
          // Assign undefined instead of using delete, see Data#remove
          elem[ dataUser.expando ] = undefined;
        }
      }
    }
  }
} );

jQuery.fn.extend( {

  // Keep domManip exposed until 3.0 (gh-2225)
  domManip: domManip,

  detach: function( selector ) {
    return remove( this, selector, true );
  },

  remove: function( selector ) {
    return remove( this, selector );
  },

  text: function( value ) {
    return access( this, function( value ) {
      return value === undefined ?
        jQuery.text( this ) :
        this.empty().each( function() {
          if ( this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9 ) {
            this.textContent = value;
          }
        } );
    }, null, value, arguments.length );
  },

  append: function() {
    return domManip( this, arguments, function( elem ) {
      if ( this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9 ) {
        var target = manipulationTarget( this, elem );
        target.appendChild( elem );
      }
    } );
  },

  prepend: function() {
    return domManip( this, arguments, function( elem ) {
      if ( this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9 ) {
        var target = manipulationTarget( this, elem );
        target.insertBefore( elem, target.firstChild );
      }
    } );
  },

  before: function() {
    return domManip( this, arguments, function( elem ) {
      if ( this.parentNode ) {
        this.parentNode.insertBefore( elem, this );
      }
    } );
  },

  after: function() {
    return domManip( this, arguments, function( elem ) {
      if ( this.parentNode ) {
        this.parentNode.insertBefore( elem, this.nextSibling );
      }
    } );
  },

  empty: function() {
    var elem,
      i = 0;

    for ( ; ( elem = this[ i ] ) != null; i++ ) {
      if ( elem.nodeType === 1 ) {

        // Prevent memory leaks
        jQuery.cleanData( getAll( elem, false ) );

        // Remove any remaining nodes
        elem.textContent = "";
      }
    }

    return this;
  },

  clone: function( dataAndEvents, deepDataAndEvents ) {
    dataAndEvents = dataAndEvents == null ? false : dataAndEvents;
    deepDataAndEvents = deepDataAndEvents == null ? dataAndEvents : deepDataAndEvents;

    return this.map( function() {
      return jQuery.clone( this, dataAndEvents, deepDataAndEvents );
    } );
  },

  html: function( value ) {
    return access( this, function( value ) {
      var elem = this[ 0 ] || {},
        i = 0,
        l = this.length;

      if ( value === undefined && elem.nodeType === 1 ) {
        return elem.innerHTML;
      }

      // See if we can take a shortcut and just use innerHTML
      if ( typeof value === "string" && !rnoInnerhtml.test( value ) &&
        !wrapMap[ ( rtagName.exec( value ) || [ "", "" ] )[ 1 ].toLowerCase() ] ) {

        value = jQuery.htmlPrefilter( value );

        try {
          for ( ; i < l; i++ ) {
            elem = this[ i ] || {};

            // Remove element nodes and prevent memory leaks
            if ( elem.nodeType === 1 ) {
              jQuery.cleanData( getAll( elem, false ) );
              elem.innerHTML = value;
            }
          }

          elem = 0;

        // If using innerHTML throws an exception, use the fallback method
        } catch ( e ) {}
      }

      if ( elem ) {
        this.empty().append( value );
      }
    }, null, value, arguments.length );
  },

  replaceWith: function() {
    var ignored = [];

    // Make the changes, replacing each non-ignored context element with the new content
    return domManip( this, arguments, function( elem ) {
      var parent = this.parentNode;

      if ( jQuery.inArray( this, ignored ) < 0 ) {
        jQuery.cleanData( getAll( this ) );
        if ( parent ) {
          parent.replaceChild( elem, this );
        }
      }

    // Force callback invocation
    }, ignored );
  }
} );

jQuery.each( {
  appendTo: "append",
  prependTo: "prepend",
  insertBefore: "before",
  insertAfter: "after",
  replaceAll: "replaceWith"
}, function( name, original ) {
  jQuery.fn[ name ] = function( selector ) {
    var elems,
      ret = [],
      insert = jQuery( selector ),
      last = insert.length - 1,
      i = 0;

    for ( ; i <= last; i++ ) {
      elems = i === last ? this : this.clone( true );
      jQuery( insert[ i ] )[ original ]( elems );

      // Support: QtWebKit
      // .get() because push.apply(_, arraylike) throws
      push.apply( ret, elems.get() );
    }

    return this.pushStack( ret );
  };
} );


var iframe,
  elemdisplay = {

    // Support: Firefox
    // We have to pre-define these values for FF (#10227)
    HTML: "block",
    BODY: "block"
  };

/**
 * Retrieve the actual display of a element
 * @param {String} name nodeName of the element
 * @param {Object} doc Document object
 */

// Called only from within defaultDisplay
function actualDisplay( name, doc ) {
  var elem = jQuery( doc.createElement( name ) ).appendTo( doc.body ),

    display = jQuery.css( elem[ 0 ], "display" );

  // We don't have any data stored on the element,
  // so use "detach" method as fast way to get rid of the element
  elem.detach();

  return display;
}

/**
 * Try to determine the default display value of an element
 * @param {String} nodeName
 */
function defaultDisplay( nodeName ) {
  var doc = document,
    display = elemdisplay[ nodeName ];

  if ( !display ) {
    display = actualDisplay( nodeName, doc );

    // If the simple way fails, read from inside an iframe
    if ( display === "none" || !display ) {

      // Use the already-created iframe if possible
      iframe = ( iframe || jQuery( "<iframe frameborder='0' width='0' height='0'/>" ) )
        .appendTo( doc.documentElement );

      // Always write a new HTML skeleton so Webkit and Firefox don't choke on reuse
      doc = iframe[ 0 ].contentDocument;

      // Support: IE
      doc.write();
      doc.close();

      display = actualDisplay( nodeName, doc );
      iframe.detach();
    }

    // Store the correct default display
    elemdisplay[ nodeName ] = display;
  }

  return display;
}
var rmargin = ( /^margin/ );

var rnumnonpx = new RegExp( "^(" + pnum + ")(?!px)[a-z%]+$", "i" );

var getStyles = function( elem ) {

    // Support: IE<=11+, Firefox<=30+ (#15098, #14150)
    // IE throws on elements created in popups
    // FF meanwhile throws on frame elements through "defaultView.getComputedStyle"
    var view = elem.ownerDocument.defaultView;

    if ( !view || !view.opener ) {
      view = window;
    }

    return view.getComputedStyle( elem );
  };

var swap = function( elem, options, callback, args ) {
  var ret, name,
    old = {};

  // Remember the old values, and insert the new ones
  for ( name in options ) {
    old[ name ] = elem.style[ name ];
    elem.style[ name ] = options[ name ];
  }

  ret = callback.apply( elem, args || [] );

  // Revert the old values
  for ( name in options ) {
    elem.style[ name ] = old[ name ];
  }

  return ret;
};


var documentElement = document.documentElement;



( function() {
  var pixelPositionVal, boxSizingReliableVal, pixelMarginRightVal, reliableMarginLeftVal,
    container = document.createElement( "div" ),
    div = document.createElement( "div" );

  // Finish early in limited (non-browser) environments
  if ( !div.style ) {
    return;
  }

  // Support: IE9-11+
  // Style of cloned element affects source element cloned (#8908)
  div.style.backgroundClip = "content-box";
  div.cloneNode( true ).style.backgroundClip = "";
  support.clearCloneStyle = div.style.backgroundClip === "content-box";

  container.style.cssText = "border:0;width:8px;height:0;top:0;left:-9999px;" +
    "padding:0;margin-top:1px;position:absolute";
  container.appendChild( div );

  // Executing both pixelPosition & boxSizingReliable tests require only one layout
  // so they're executed at the same time to save the second computation.
  function computeStyleTests() {
    div.style.cssText =

      // Support: Firefox<29, Android 2.3
      // Vendor-prefix box-sizing
      "-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;" +
      "position:relative;display:block;" +
      "margin:auto;border:1px;padding:1px;" +
      "top:1%;width:50%";
    div.innerHTML = "";
    documentElement.appendChild( container );

    var divStyle = window.getComputedStyle( div );
    pixelPositionVal = divStyle.top !== "1%";
    reliableMarginLeftVal = divStyle.marginLeft === "2px";
    boxSizingReliableVal = divStyle.width === "4px";

    // Support: Android 4.0 - 4.3 only
    // Some styles come back with percentage values, even though they shouldn't
    div.style.marginRight = "50%";
    pixelMarginRightVal = divStyle.marginRight === "4px";

    documentElement.removeChild( container );
  }

  jQuery.extend( support, {
    pixelPosition: function() {

      // This test is executed only once but we still do memoizing
      // since we can use the boxSizingReliable pre-computing.
      // No need to check if the test was already performed, though.
      computeStyleTests();
      return pixelPositionVal;
    },
    boxSizingReliable: function() {
      if ( boxSizingReliableVal == null ) {
        computeStyleTests();
      }
      return boxSizingReliableVal;
    },
    pixelMarginRight: function() {

      // Support: Android 4.0-4.3
      // We're checking for boxSizingReliableVal here instead of pixelMarginRightVal
      // since that compresses better and they're computed together anyway.
      if ( boxSizingReliableVal == null ) {
        computeStyleTests();
      }
      return pixelMarginRightVal;
    },
    reliableMarginLeft: function() {

      // Support: IE <=8 only, Android 4.0 - 4.3 only, Firefox <=3 - 37
      if ( boxSizingReliableVal == null ) {
        computeStyleTests();
      }
      return reliableMarginLeftVal;
    },
    reliableMarginRight: function() {

      // Support: Android 2.3
      // Check if div with explicit width and no margin-right incorrectly
      // gets computed margin-right based on width of container. (#3333)
      // WebKit Bug 13343 - getComputedStyle returns wrong value for margin-right
      // This support function is only executed once so no memoizing is needed.
      var ret,
        marginDiv = div.appendChild( document.createElement( "div" ) );

      // Reset CSS: box-sizing; display; margin; border; padding
      marginDiv.style.cssText = div.style.cssText =

        // Support: Android 2.3
        // Vendor-prefix box-sizing
        "-webkit-box-sizing:content-box;box-sizing:content-box;" +
        "display:block;margin:0;border:0;padding:0";
      marginDiv.style.marginRight = marginDiv.style.width = "0";
      div.style.width = "1px";
      documentElement.appendChild( container );

      ret = !parseFloat( window.getComputedStyle( marginDiv ).marginRight );

      documentElement.removeChild( container );
      div.removeChild( marginDiv );

      return ret;
    }
  } );
} )();


function curCSS( elem, name, computed ) {
  var width, minWidth, maxWidth, ret,
    style = elem.style;

  computed = computed || getStyles( elem );
  ret = computed ? computed.getPropertyValue( name ) || computed[ name ] : undefined;

  // Support: Opera 12.1x only
  // Fall back to style even without computed
  // computed is undefined for elems on document fragments
  if ( ( ret === "" || ret === undefined ) && !jQuery.contains( elem.ownerDocument, elem ) ) {
    ret = jQuery.style( elem, name );
  }

  // Support: IE9
  // getPropertyValue is only needed for .css('filter') (#12537)
  if ( computed ) {

    // A tribute to the "awesome hack by Dean Edwards"
    // Android Browser returns percentage for some values,
    // but width seems to be reliably pixels.
    // This is against the CSSOM draft spec:
    // http://dev.w3.org/csswg/cssom/#resolved-values
    if ( !support.pixelMarginRight() && rnumnonpx.test( ret ) && rmargin.test( name ) ) {

      // Remember the original values
      width = style.width;
      minWidth = style.minWidth;
      maxWidth = style.maxWidth;

      // Put in the new values to get a computed value out
      style.minWidth = style.maxWidth = style.width = ret;
      ret = computed.width;

      // Revert the changed values
      style.width = width;
      style.minWidth = minWidth;
      style.maxWidth = maxWidth;
    }
  }

  return ret !== undefined ?

    // Support: IE9-11+
    // IE returns zIndex value as an integer.
    ret + "" :
    ret;
}


function addGetHookIf( conditionFn, hookFn ) {

  // Define the hook, we'll check on the first run if it's really needed.
  return {
    get: function() {
      if ( conditionFn() ) {

        // Hook not needed (or it's not possible to use it due
        // to missing dependency), remove it.
        delete this.get;
        return;
      }

      // Hook needed; redefine it so that the support test is not executed again.
      return ( this.get = hookFn ).apply( this, arguments );
    }
  };
}


var

  // Swappable if display is none or starts with table
  // except "table", "table-cell", or "table-caption"
  // See here for display values: https://developer.mozilla.org/en-US/docs/CSS/display
  rdisplayswap = /^(none|table(?!-c[ea]).+)/,

  cssShow = { position: "absolute", visibility: "hidden", display: "block" },
  cssNormalTransform = {
    letterSpacing: "0",
    fontWeight: "400"
  },

  cssPrefixes = [ "Webkit", "O", "Moz", "ms" ],
  emptyStyle = document.createElement( "div" ).style;

// Return a css property mapped to a potentially vendor prefixed property
function vendorPropName( name ) {

  // Shortcut for names that are not vendor prefixed
  if ( name in emptyStyle ) {
    return name;
  }

  // Check for vendor prefixed names
  var capName = name[ 0 ].toUpperCase() + name.slice( 1 ),
    i = cssPrefixes.length;

  while ( i-- ) {
    name = cssPrefixes[ i ] + capName;
    if ( name in emptyStyle ) {
      return name;
    }
  }
}

function setPositiveNumber( elem, value, subtract ) {

  // Any relative (+/-) values have already been
  // normalized at this point
  var matches = rcssNum.exec( value );
  return matches ?

    // Guard against undefined "subtract", e.g., when used as in cssHooks
    Math.max( 0, matches[ 2 ] - ( subtract || 0 ) ) + ( matches[ 3 ] || "px" ) :
    value;
}

function augmentWidthOrHeight( elem, name, extra, isBorderBox, styles ) {
  var i = extra === ( isBorderBox ? "border" : "content" ) ?

    // If we already have the right measurement, avoid augmentation
    4 :

    // Otherwise initialize for horizontal or vertical properties
    name === "width" ? 1 : 0,

    val = 0;

  for ( ; i < 4; i += 2 ) {

    // Both box models exclude margin, so add it if we want it
    if ( extra === "margin" ) {
      val += jQuery.css( elem, extra + cssExpand[ i ], true, styles );
    }

    if ( isBorderBox ) {

      // border-box includes padding, so remove it if we want content
      if ( extra === "content" ) {
        val -= jQuery.css( elem, "padding" + cssExpand[ i ], true, styles );
      }

      // At this point, extra isn't border nor margin, so remove border
      if ( extra !== "margin" ) {
        val -= jQuery.css( elem, "border" + cssExpand[ i ] + "Width", true, styles );
      }
    } else {

      // At this point, extra isn't content, so add padding
      val += jQuery.css( elem, "padding" + cssExpand[ i ], true, styles );

      // At this point, extra isn't content nor padding, so add border
      if ( extra !== "padding" ) {
        val += jQuery.css( elem, "border" + cssExpand[ i ] + "Width", true, styles );
      }
    }
  }

  return val;
}

function getWidthOrHeight( elem, name, extra ) {

  // Start with offset property, which is equivalent to the border-box value
  var valueIsBorderBox = true,
    val = name === "width" ? elem.offsetWidth : elem.offsetHeight,
    styles = getStyles( elem ),
    isBorderBox = jQuery.css( elem, "boxSizing", false, styles ) === "border-box";

  // Some non-html elements return undefined for offsetWidth, so check for null/undefined
  // svg - https://bugzilla.mozilla.org/show_bug.cgi?id=649285
  // MathML - https://bugzilla.mozilla.org/show_bug.cgi?id=491668
  if ( val <= 0 || val == null ) {

    // Fall back to computed then uncomputed css if necessary
    val = curCSS( elem, name, styles );
    if ( val < 0 || val == null ) {
      val = elem.style[ name ];
    }

    // Computed unit is not pixels. Stop here and return.
    if ( rnumnonpx.test( val ) ) {
      return val;
    }

    // Check for style in case a browser which returns unreliable values
    // for getComputedStyle silently falls back to the reliable elem.style
    valueIsBorderBox = isBorderBox &&
      ( support.boxSizingReliable() || val === elem.style[ name ] );

    // Normalize "", auto, and prepare for extra
    val = parseFloat( val ) || 0;
  }

  // Use the active box-sizing model to add/subtract irrelevant styles
  return ( val +
    augmentWidthOrHeight(
      elem,
      name,
      extra || ( isBorderBox ? "border" : "content" ),
      valueIsBorderBox,
      styles
    )
  ) + "px";
}

function showHide( elements, show ) {
  var display, elem, hidden,
    values = [],
    index = 0,
    length = elements.length;

  for ( ; index < length; index++ ) {
    elem = elements[ index ];
    if ( !elem.style ) {
      continue;
    }

    values[ index ] = dataPriv.get( elem, "olddisplay" );
    display = elem.style.display;
    if ( show ) {

      // Reset the inline display of this element to learn if it is
      // being hidden by cascaded rules or not
      if ( !values[ index ] && display === "none" ) {
        elem.style.display = "";
      }

      // Set elements which have been overridden with display: none
      // in a stylesheet to whatever the default browser style is
      // for such an element
      if ( elem.style.display === "" && isHidden( elem ) ) {
        values[ index ] = dataPriv.access(
          elem,
          "olddisplay",
          defaultDisplay( elem.nodeName )
        );
      }
    } else {
      hidden = isHidden( elem );

      if ( display !== "none" || !hidden ) {
        dataPriv.set(
          elem,
          "olddisplay",
          hidden ? display : jQuery.css( elem, "display" )
        );
      }
    }
  }

  // Set the display of most of the elements in a second loop
  // to avoid the constant reflow
  for ( index = 0; index < length; index++ ) {
    elem = elements[ index ];
    if ( !elem.style ) {
      continue;
    }
    if ( !show || elem.style.display === "none" || elem.style.display === "" ) {
      elem.style.display = show ? values[ index ] || "" : "none";
    }
  }

  return elements;
}

jQuery.extend( {

  // Add in style property hooks for overriding the default
  // behavior of getting and setting a style property
  cssHooks: {
    opacity: {
      get: function( elem, computed ) {
        if ( computed ) {

          // We should always get a number back from opacity
          var ret = curCSS( elem, "opacity" );
          return ret === "" ? "1" : ret;
        }
      }
    }
  },

  // Don't automatically add "px" to these possibly-unitless properties
  cssNumber: {
    "animationIterationCount": true,
    "columnCount": true,
    "fillOpacity": true,
    "flexGrow": true,
    "flexShrink": true,
    "fontWeight": true,
    "lineHeight": true,
    "opacity": true,
    "order": true,
    "orphans": true,
    "widows": true,
    "zIndex": true,
    "zoom": true
  },

  // Add in properties whose names you wish to fix before
  // setting or getting the value
  cssProps: {
    "float": "cssFloat"
  },

  // Get and set the style property on a DOM Node
  style: function( elem, name, value, extra ) {

    // Don't set styles on text and comment nodes
    if ( !elem || elem.nodeType === 3 || elem.nodeType === 8 || !elem.style ) {
      return;
    }

    // Make sure that we're working with the right name
    var ret, type, hooks,
      origName = jQuery.camelCase( name ),
      style = elem.style;

    name = jQuery.cssProps[ origName ] ||
      ( jQuery.cssProps[ origName ] = vendorPropName( origName ) || origName );

    // Gets hook for the prefixed version, then unprefixed version
    hooks = jQuery.cssHooks[ name ] || jQuery.cssHooks[ origName ];

    // Check if we're setting a value
    if ( value !== undefined ) {
      type = typeof value;

      // Convert "+=" or "-=" to relative numbers (#7345)
      if ( type === "string" && ( ret = rcssNum.exec( value ) ) && ret[ 1 ] ) {
        value = adjustCSS( elem, name, ret );

        // Fixes bug #9237
        type = "number";
      }

      // Make sure that null and NaN values aren't set (#7116)
      if ( value == null || value !== value ) {
        return;
      }

      // If a number was passed in, add the unit (except for certain CSS properties)
      if ( type === "number" ) {
        value += ret && ret[ 3 ] || ( jQuery.cssNumber[ origName ] ? "" : "px" );
      }

      // Support: IE9-11+
      // background-* props affect original clone's values
      if ( !support.clearCloneStyle && value === "" && name.indexOf( "background" ) === 0 ) {
        style[ name ] = "inherit";
      }

      // If a hook was provided, use that value, otherwise just set the specified value
      if ( !hooks || !( "set" in hooks ) ||
        ( value = hooks.set( elem, value, extra ) ) !== undefined ) {

        style[ name ] = value;
      }

    } else {

      // If a hook was provided get the non-computed value from there
      if ( hooks && "get" in hooks &&
        ( ret = hooks.get( elem, false, extra ) ) !== undefined ) {

        return ret;
      }

      // Otherwise just get the value from the style object
      return style[ name ];
    }
  },

  css: function( elem, name, extra, styles ) {
    var val, num, hooks,
      origName = jQuery.camelCase( name );

    // Make sure that we're working with the right name
    name = jQuery.cssProps[ origName ] ||
      ( jQuery.cssProps[ origName ] = vendorPropName( origName ) || origName );

    // Try prefixed name followed by the unprefixed name
    hooks = jQuery.cssHooks[ name ] || jQuery.cssHooks[ origName ];

    // If a hook was provided get the computed value from there
    if ( hooks && "get" in hooks ) {
      val = hooks.get( elem, true, extra );
    }

    // Otherwise, if a way to get the computed value exists, use that
    if ( val === undefined ) {
      val = curCSS( elem, name, styles );
    }

    // Convert "normal" to computed value
    if ( val === "normal" && name in cssNormalTransform ) {
      val = cssNormalTransform[ name ];
    }

    // Make numeric if forced or a qualifier was provided and val looks numeric
    if ( extra === "" || extra ) {
      num = parseFloat( val );
      return extra === true || isFinite( num ) ? num || 0 : val;
    }
    return val;
  }
} );

jQuery.each( [ "height", "width" ], function( i, name ) {
  jQuery.cssHooks[ name ] = {
    get: function( elem, computed, extra ) {
      if ( computed ) {

        // Certain elements can have dimension info if we invisibly show them
        // but it must have a current display style that would benefit
        return rdisplayswap.test( jQuery.css( elem, "display" ) ) &&
          elem.offsetWidth === 0 ?
            swap( elem, cssShow, function() {
              return getWidthOrHeight( elem, name, extra );
            } ) :
            getWidthOrHeight( elem, name, extra );
      }
    },

    set: function( elem, value, extra ) {
      var matches,
        styles = extra && getStyles( elem ),
        subtract = extra && augmentWidthOrHeight(
          elem,
          name,
          extra,
          jQuery.css( elem, "boxSizing", false, styles ) === "border-box",
          styles
        );

      // Convert to pixels if value adjustment is needed
      if ( subtract && ( matches = rcssNum.exec( value ) ) &&
        ( matches[ 3 ] || "px" ) !== "px" ) {

        elem.style[ name ] = value;
        value = jQuery.css( elem, name );
      }

      return setPositiveNumber( elem, value, subtract );
    }
  };
} );

jQuery.cssHooks.marginLeft = addGetHookIf( support.reliableMarginLeft,
  function( elem, computed ) {
    if ( computed ) {
      return ( parseFloat( curCSS( elem, "marginLeft" ) ) ||
        elem.getBoundingClientRect().left -
          swap( elem, { marginLeft: 0 }, function() {
            return elem.getBoundingClientRect().left;
          } )
        ) + "px";
    }
  }
);

// Support: Android 2.3
jQuery.cssHooks.marginRight = addGetHookIf( support.reliableMarginRight,
  function( elem, computed ) {
    if ( computed ) {
      return swap( elem, { "display": "inline-block" },
        curCSS, [ elem, "marginRight" ] );
    }
  }
);

// These hooks are used by animate to expand properties
jQuery.each( {
  margin: "",
  padding: "",
  border: "Width"
}, function( prefix, suffix ) {
  jQuery.cssHooks[ prefix + suffix ] = {
    expand: function( value ) {
      var i = 0,
        expanded = {},

        // Assumes a single number if not a string
        parts = typeof value === "string" ? value.split( " " ) : [ value ];

      for ( ; i < 4; i++ ) {
        expanded[ prefix + cssExpand[ i ] + suffix ] =
          parts[ i ] || parts[ i - 2 ] || parts[ 0 ];
      }

      return expanded;
    }
  };

  if ( !rmargin.test( prefix ) ) {
    jQuery.cssHooks[ prefix + suffix ].set = setPositiveNumber;
  }
} );

jQuery.fn.extend( {
  css: function( name, value ) {
    return access( this, function( elem, name, value ) {
      var styles, len,
        map = {},
        i = 0;

      if ( jQuery.isArray( name ) ) {
        styles = getStyles( elem );
        len = name.length;

        for ( ; i < len; i++ ) {
          map[ name[ i ] ] = jQuery.css( elem, name[ i ], false, styles );
        }

        return map;
      }

      return value !== undefined ?
        jQuery.style( elem, name, value ) :
        jQuery.css( elem, name );
    }, name, value, arguments.length > 1 );
  },
  show: function() {
    return showHide( this, true );
  },
  hide: function() {
    return showHide( this );
  },
  toggle: function( state ) {
    if ( typeof state === "boolean" ) {
      return state ? this.show() : this.hide();
    }

    return this.each( function() {
      if ( isHidden( this ) ) {
        jQuery( this ).show();
      } else {
        jQuery( this ).hide();
      }
    } );
  }
} );


function Tween( elem, options, prop, end, easing ) {
  return new Tween.prototype.init( elem, options, prop, end, easing );
}
jQuery.Tween = Tween;

Tween.prototype = {
  constructor: Tween,
  init: function( elem, options, prop, end, easing, unit ) {
    this.elem = elem;
    this.prop = prop;
    this.easing = easing || jQuery.easing._default;
    this.options = options;
    this.start = this.now = this.cur();
    this.end = end;
    this.unit = unit || ( jQuery.cssNumber[ prop ] ? "" : "px" );
  },
  cur: function() {
    var hooks = Tween.propHooks[ this.prop ];

    return hooks && hooks.get ?
      hooks.get( this ) :
      Tween.propHooks._default.get( this );
  },
  run: function( percent ) {
    var eased,
      hooks = Tween.propHooks[ this.prop ];

    if ( this.options.duration ) {
      this.pos = eased = jQuery.easing[ this.easing ](
        percent, this.options.duration * percent, 0, 1, this.options.duration
      );
    } else {
      this.pos = eased = percent;
    }
    this.now = ( this.end - this.start ) * eased + this.start;

    if ( this.options.step ) {
      this.options.step.call( this.elem, this.now, this );
    }

    if ( hooks && hooks.set ) {
      hooks.set( this );
    } else {
      Tween.propHooks._default.set( this );
    }
    return this;
  }
};

Tween.prototype.init.prototype = Tween.prototype;

Tween.propHooks = {
  _default: {
    get: function( tween ) {
      var result;

      // Use a property on the element directly when it is not a DOM element,
      // or when there is no matching style property that exists.
      if ( tween.elem.nodeType !== 1 ||
        tween.elem[ tween.prop ] != null && tween.elem.style[ tween.prop ] == null ) {
        return tween.elem[ tween.prop ];
      }

      // Passing an empty string as a 3rd parameter to .css will automatically
      // attempt a parseFloat and fallback to a string if the parse fails.
      // Simple values such as "10px" are parsed to Float;
      // complex values such as "rotate(1rad)" are returned as-is.
      result = jQuery.css( tween.elem, tween.prop, "" );

      // Empty strings, null, undefined and "auto" are converted to 0.
      return !result || result === "auto" ? 0 : result;
    },
    set: function( tween ) {

      // Use step hook for back compat.
      // Use cssHook if its there.
      // Use .style if available and use plain properties where available.
      if ( jQuery.fx.step[ tween.prop ] ) {
        jQuery.fx.step[ tween.prop ]( tween );
      } else if ( tween.elem.nodeType === 1 &&
        ( tween.elem.style[ jQuery.cssProps[ tween.prop ] ] != null ||
          jQuery.cssHooks[ tween.prop ] ) ) {
        jQuery.style( tween.elem, tween.prop, tween.now + tween.unit );
      } else {
        tween.elem[ tween.prop ] = tween.now;
      }
    }
  }
};

// Support: IE9
// Panic based approach to setting things on disconnected nodes
Tween.propHooks.scrollTop = Tween.propHooks.scrollLeft = {
  set: function( tween ) {
    if ( tween.elem.nodeType && tween.elem.parentNode ) {
      tween.elem[ tween.prop ] = tween.now;
    }
  }
};

jQuery.easing = {
  linear: function( p ) {
    return p;
  },
  swing: function( p ) {
    return 0.5 - Math.cos( p * Math.PI ) / 2;
  },
  _default: "swing"
};

jQuery.fx = Tween.prototype.init;

// Back Compat <1.8 extension point
jQuery.fx.step = {};




var
  fxNow, timerId,
  rfxtypes = /^(?:toggle|show|hide)$/,
  rrun = /queueHooks$/;

// Animations created synchronously will run synchronously
function createFxNow() {
  window.setTimeout( function() {
    fxNow = undefined;
  } );
  return ( fxNow = jQuery.now() );
}

// Generate parameters to create a standard animation
function genFx( type, includeWidth ) {
  var which,
    i = 0,
    attrs = { height: type };

  // If we include width, step value is 1 to do all cssExpand values,
  // otherwise step value is 2 to skip over Left and Right
  includeWidth = includeWidth ? 1 : 0;
  for ( ; i < 4 ; i += 2 - includeWidth ) {
    which = cssExpand[ i ];
    attrs[ "margin" + which ] = attrs[ "padding" + which ] = type;
  }

  if ( includeWidth ) {
    attrs.opacity = attrs.width = type;
  }

  return attrs;
}

function createTween( value, prop, animation ) {
  var tween,
    collection = ( Animation.tweeners[ prop ] || [] ).concat( Animation.tweeners[ "*" ] ),
    index = 0,
    length = collection.length;
  for ( ; index < length; index++ ) {
    if ( ( tween = collection[ index ].call( animation, prop, value ) ) ) {

      // We're done with this property
      return tween;
    }
  }
}

function defaultPrefilter( elem, props, opts ) {
  /* jshint validthis: true */
  var prop, value, toggle, tween, hooks, oldfire, display, checkDisplay,
    anim = this,
    orig = {},
    style = elem.style,
    hidden = elem.nodeType && isHidden( elem ),
    dataShow = dataPriv.get( elem, "fxshow" );

  // Handle queue: false promises
  if ( !opts.queue ) {
    hooks = jQuery._queueHooks( elem, "fx" );
    if ( hooks.unqueued == null ) {
      hooks.unqueued = 0;
      oldfire = hooks.empty.fire;
      hooks.empty.fire = function() {
        if ( !hooks.unqueued ) {
          oldfire();
        }
      };
    }
    hooks.unqueued++;

    anim.always( function() {

      // Ensure the complete handler is called before this completes
      anim.always( function() {
        hooks.unqueued--;
        if ( !jQuery.queue( elem, "fx" ).length ) {
          hooks.empty.fire();
        }
      } );
    } );
  }

  // Height/width overflow pass
  if ( elem.nodeType === 1 && ( "height" in props || "width" in props ) ) {

    // Make sure that nothing sneaks out
    // Record all 3 overflow attributes because IE9-10 do not
    // change the overflow attribute when overflowX and
    // overflowY are set to the same value
    opts.overflow = [ style.overflow, style.overflowX, style.overflowY ];

    // Set display property to inline-block for height/width
    // animations on inline elements that are having width/height animated
    display = jQuery.css( elem, "display" );

    // Test default display if display is currently "none"
    checkDisplay = display === "none" ?
      dataPriv.get( elem, "olddisplay" ) || defaultDisplay( elem.nodeName ) : display;

    if ( checkDisplay === "inline" && jQuery.css( elem, "float" ) === "none" ) {
      style.display = "inline-block";
    }
  }

  if ( opts.overflow ) {
    style.overflow = "hidden";
    anim.always( function() {
      style.overflow = opts.overflow[ 0 ];
      style.overflowX = opts.overflow[ 1 ];
      style.overflowY = opts.overflow[ 2 ];
    } );
  }

  // show/hide pass
  for ( prop in props ) {
    value = props[ prop ];
    if ( rfxtypes.exec( value ) ) {
      delete props[ prop ];
      toggle = toggle || value === "toggle";
      if ( value === ( hidden ? "hide" : "show" ) ) {

        // If there is dataShow left over from a stopped hide or show
        // and we are going to proceed with show, we should pretend to be hidden
        if ( value === "show" && dataShow && dataShow[ prop ] !== undefined ) {
          hidden = true;
        } else {
          continue;
        }
      }
      orig[ prop ] = dataShow && dataShow[ prop ] || jQuery.style( elem, prop );

    // Any non-fx value stops us from restoring the original display value
    } else {
      display = undefined;
    }
  }

  if ( !jQuery.isEmptyObject( orig ) ) {
    if ( dataShow ) {
      if ( "hidden" in dataShow ) {
        hidden = dataShow.hidden;
      }
    } else {
      dataShow = dataPriv.access( elem, "fxshow", {} );
    }

    // Store state if its toggle - enables .stop().toggle() to "reverse"
    if ( toggle ) {
      dataShow.hidden = !hidden;
    }
    if ( hidden ) {
      jQuery( elem ).show();
    } else {
      anim.done( function() {
        jQuery( elem ).hide();
      } );
    }
    anim.done( function() {
      var prop;

      dataPriv.remove( elem, "fxshow" );
      for ( prop in orig ) {
        jQuery.style( elem, prop, orig[ prop ] );
      }
    } );
    for ( prop in orig ) {
      tween = createTween( hidden ? dataShow[ prop ] : 0, prop, anim );

      if ( !( prop in dataShow ) ) {
        dataShow[ prop ] = tween.start;
        if ( hidden ) {
          tween.end = tween.start;
          tween.start = prop === "width" || prop === "height" ? 1 : 0;
        }
      }
    }

  // If this is a noop like .hide().hide(), restore an overwritten display value
  } else if ( ( display === "none" ? defaultDisplay( elem.nodeName ) : display ) === "inline" ) {
    style.display = display;
  }
}

function propFilter( props, specialEasing ) {
  var index, name, easing, value, hooks;

  // camelCase, specialEasing and expand cssHook pass
  for ( index in props ) {
    name = jQuery.camelCase( index );
    easing = specialEasing[ name ];
    value = props[ index ];
    if ( jQuery.isArray( value ) ) {
      easing = value[ 1 ];
      value = props[ index ] = value[ 0 ];
    }

    if ( index !== name ) {
      props[ name ] = value;
      delete props[ index ];
    }

    hooks = jQuery.cssHooks[ name ];
    if ( hooks && "expand" in hooks ) {
      value = hooks.expand( value );
      delete props[ name ];

      // Not quite $.extend, this won't overwrite existing keys.
      // Reusing 'index' because we have the correct "name"
      for ( index in value ) {
        if ( !( index in props ) ) {
          props[ index ] = value[ index ];
          specialEasing[ index ] = easing;
        }
      }
    } else {
      specialEasing[ name ] = easing;
    }
  }
}

function Animation( elem, properties, options ) {
  var result,
    stopped,
    index = 0,
    length = Animation.prefilters.length,
    deferred = jQuery.Deferred().always( function() {

      // Don't match elem in the :animated selector
      delete tick.elem;
    } ),
    tick = function() {
      if ( stopped ) {
        return false;
      }
      var currentTime = fxNow || createFxNow(),
        remaining = Math.max( 0, animation.startTime + animation.duration - currentTime ),

        // Support: Android 2.3
        // Archaic crash bug won't allow us to use `1 - ( 0.5 || 0 )` (#12497)
        temp = remaining / animation.duration || 0,
        percent = 1 - temp,
        index = 0,
        length = animation.tweens.length;

      for ( ; index < length ; index++ ) {
        animation.tweens[ index ].run( percent );
      }

      deferred.notifyWith( elem, [ animation, percent, remaining ] );

      if ( percent < 1 && length ) {
        return remaining;
      } else {
        deferred.resolveWith( elem, [ animation ] );
        return false;
      }
    },
    animation = deferred.promise( {
      elem: elem,
      props: jQuery.extend( {}, properties ),
      opts: jQuery.extend( true, {
        specialEasing: {},
        easing: jQuery.easing._default
      }, options ),
      originalProperties: properties,
      originalOptions: options,
      startTime: fxNow || createFxNow(),
      duration: options.duration,
      tweens: [],
      createTween: function( prop, end ) {
        var tween = jQuery.Tween( elem, animation.opts, prop, end,
            animation.opts.specialEasing[ prop ] || animation.opts.easing );
        animation.tweens.push( tween );
        return tween;
      },
      stop: function( gotoEnd ) {
        var index = 0,

          // If we are going to the end, we want to run all the tweens
          // otherwise we skip this part
          length = gotoEnd ? animation.tweens.length : 0;
        if ( stopped ) {
          return this;
        }
        stopped = true;
        for ( ; index < length ; index++ ) {
          animation.tweens[ index ].run( 1 );
        }

        // Resolve when we played the last frame; otherwise, reject
        if ( gotoEnd ) {
          deferred.notifyWith( elem, [ animation, 1, 0 ] );
          deferred.resolveWith( elem, [ animation, gotoEnd ] );
        } else {
          deferred.rejectWith( elem, [ animation, gotoEnd ] );
        }
        return this;
      }
    } ),
    props = animation.props;

  propFilter( props, animation.opts.specialEasing );

  for ( ; index < length ; index++ ) {
    result = Animation.prefilters[ index ].call( animation, elem, props, animation.opts );
    if ( result ) {
      if ( jQuery.isFunction( result.stop ) ) {
        jQuery._queueHooks( animation.elem, animation.opts.queue ).stop =
          jQuery.proxy( result.stop, result );
      }
      return result;
    }
  }

  jQuery.map( props, createTween, animation );

  if ( jQuery.isFunction( animation.opts.start ) ) {
    animation.opts.start.call( elem, animation );
  }

  jQuery.fx.timer(
    jQuery.extend( tick, {
      elem: elem,
      anim: animation,
      queue: animation.opts.queue
    } )
  );

  // attach callbacks from options
  return animation.progress( animation.opts.progress )
    .done( animation.opts.done, animation.opts.complete )
    .fail( animation.opts.fail )
    .always( animation.opts.always );
}

jQuery.Animation = jQuery.extend( Animation, {
  tweeners: {
    "*": [ function( prop, value ) {
      var tween = this.createTween( prop, value );
      adjustCSS( tween.elem, prop, rcssNum.exec( value ), tween );
      return tween;
    } ]
  },

  tweener: function( props, callback ) {
    if ( jQuery.isFunction( props ) ) {
      callback = props;
      props = [ "*" ];
    } else {
      props = props.match( rnotwhite );
    }

    var prop,
      index = 0,
      length = props.length;

    for ( ; index < length ; index++ ) {
      prop = props[ index ];
      Animation.tweeners[ prop ] = Animation.tweeners[ prop ] || [];
      Animation.tweeners[ prop ].unshift( callback );
    }
  },

  prefilters: [ defaultPrefilter ],

  prefilter: function( callback, prepend ) {
    if ( prepend ) {
      Animation.prefilters.unshift( callback );
    } else {
      Animation.prefilters.push( callback );
    }
  }
} );

jQuery.speed = function( speed, easing, fn ) {
  var opt = speed && typeof speed === "object" ? jQuery.extend( {}, speed ) : {
    complete: fn || !fn && easing ||
      jQuery.isFunction( speed ) && speed,
    duration: speed,
    easing: fn && easing || easing && !jQuery.isFunction( easing ) && easing
  };

  opt.duration = jQuery.fx.off ? 0 : typeof opt.duration === "number" ?
    opt.duration : opt.duration in jQuery.fx.speeds ?
      jQuery.fx.speeds[ opt.duration ] : jQuery.fx.speeds._default;

  // Normalize opt.queue - true/undefined/null -> "fx"
  if ( opt.queue == null || opt.queue === true ) {
    opt.queue = "fx";
  }

  // Queueing
  opt.old = opt.complete;

  opt.complete = function() {
    if ( jQuery.isFunction( opt.old ) ) {
      opt.old.call( this );
    }

    if ( opt.queue ) {
      jQuery.dequeue( this, opt.queue );
    }
  };

  return opt;
};

jQuery.fn.extend( {
  fadeTo: function( speed, to, easing, callback ) {

    // Show any hidden elements after setting opacity to 0
    return this.filter( isHidden ).css( "opacity", 0 ).show()

      // Animate to the value specified
      .end().animate( { opacity: to }, speed, easing, callback );
  },
  animate: function( prop, speed, easing, callback ) {
    var empty = jQuery.isEmptyObject( prop ),
      optall = jQuery.speed( speed, easing, callback ),
      doAnimation = function() {

        // Operate on a copy of prop so per-property easing won't be lost
        var anim = Animation( this, jQuery.extend( {}, prop ), optall );

        // Empty animations, or finishing resolves immediately
        if ( empty || dataPriv.get( this, "finish" ) ) {
          anim.stop( true );
        }
      };
      doAnimation.finish = doAnimation;

    return empty || optall.queue === false ?
      this.each( doAnimation ) :
      this.queue( optall.queue, doAnimation );
  },
  stop: function( type, clearQueue, gotoEnd ) {
    var stopQueue = function( hooks ) {
      var stop = hooks.stop;
      delete hooks.stop;
      stop( gotoEnd );
    };

    if ( typeof type !== "string" ) {
      gotoEnd = clearQueue;
      clearQueue = type;
      type = undefined;
    }
    if ( clearQueue && type !== false ) {
      this.queue( type || "fx", [] );
    }

    return this.each( function() {
      var dequeue = true,
        index = type != null && type + "queueHooks",
        timers = jQuery.timers,
        data = dataPriv.get( this );

      if ( index ) {
        if ( data[ index ] && data[ index ].stop ) {
          stopQueue( data[ index ] );
        }
      } else {
        for ( index in data ) {
          if ( data[ index ] && data[ index ].stop && rrun.test( index ) ) {
            stopQueue( data[ index ] );
          }
        }
      }

      for ( index = timers.length; index--; ) {
        if ( timers[ index ].elem === this &&
          ( type == null || timers[ index ].queue === type ) ) {

          timers[ index ].anim.stop( gotoEnd );
          dequeue = false;
          timers.splice( index, 1 );
        }
      }

      // Start the next in the queue if the last step wasn't forced.
      // Timers currently will call their complete callbacks, which
      // will dequeue but only if they were gotoEnd.
      if ( dequeue || !gotoEnd ) {
        jQuery.dequeue( this, type );
      }
    } );
  },
  finish: function( type ) {
    if ( type !== false ) {
      type = type || "fx";
    }
    return this.each( function() {
      var index,
        data = dataPriv.get( this ),
        queue = data[ type + "queue" ],
        hooks = data[ type + "queueHooks" ],
        timers = jQuery.timers,
        length = queue ? queue.length : 0;

      // Enable finishing flag on private data
      data.finish = true;

      // Empty the queue first
      jQuery.queue( this, type, [] );

      if ( hooks && hooks.stop ) {
        hooks.stop.call( this, true );
      }

      // Look for any active animations, and finish them
      for ( index = timers.length; index--; ) {
        if ( timers[ index ].elem === this && timers[ index ].queue === type ) {
          timers[ index ].anim.stop( true );
          timers.splice( index, 1 );
        }
      }

      // Look for any animations in the old queue and finish them
      for ( index = 0; index < length; index++ ) {
        if ( queue[ index ] && queue[ index ].finish ) {
          queue[ index ].finish.call( this );
        }
      }

      // Turn off finishing flag
      delete data.finish;
    } );
  }
} );

jQuery.each( [ "toggle", "show", "hide" ], function( i, name ) {
  var cssFn = jQuery.fn[ name ];
  jQuery.fn[ name ] = function( speed, easing, callback ) {
    return speed == null || typeof speed === "boolean" ?
      cssFn.apply( this, arguments ) :
      this.animate( genFx( name, true ), speed, easing, callback );
  };
} );

// Generate shortcuts for custom animations
jQuery.each( {
  slideDown: genFx( "show" ),
  slideUp: genFx( "hide" ),
  slideToggle: genFx( "toggle" ),
  fadeIn: { opacity: "show" },
  fadeOut: { opacity: "hide" },
  fadeToggle: { opacity: "toggle" }
}, function( name, props ) {
  jQuery.fn[ name ] = function( speed, easing, callback ) {
    return this.animate( props, speed, easing, callback );
  };
} );

jQuery.timers = [];
jQuery.fx.tick = function() {
  var timer,
    i = 0,
    timers = jQuery.timers;

  fxNow = jQuery.now();

  for ( ; i < timers.length; i++ ) {
    timer = timers[ i ];

    // Checks the timer has not already been removed
    if ( !timer() && timers[ i ] === timer ) {
      timers.splice( i--, 1 );
    }
  }

  if ( !timers.length ) {
    jQuery.fx.stop();
  }
  fxNow = undefined;
};

jQuery.fx.timer = function( timer ) {
  jQuery.timers.push( timer );
  if ( timer() ) {
    jQuery.fx.start();
  } else {
    jQuery.timers.pop();
  }
};

jQuery.fx.interval = 13;
jQuery.fx.start = function() {
  if ( !timerId ) {
    timerId = window.setInterval( jQuery.fx.tick, jQuery.fx.interval );
  }
};

jQuery.fx.stop = function() {
  window.clearInterval( timerId );

  timerId = null;
};

jQuery.fx.speeds = {
  slow: 600,
  fast: 200,

  // Default speed
  _default: 400
};


// Based off of the plugin by Clint Helfers, with permission.
// http://web.archive.org/web/20100324014747/http://blindsignals.com/index.php/2009/07/jquery-delay/
jQuery.fn.delay = function( time, type ) {
  time = jQuery.fx ? jQuery.fx.speeds[ time ] || time : time;
  type = type || "fx";

  return this.queue( type, function( next, hooks ) {
    var timeout = window.setTimeout( next, time );
    hooks.stop = function() {
      window.clearTimeout( timeout );
    };
  } );
};


( function() {
  var input = document.createElement( "input" ),
    select = document.createElement( "select" ),
    opt = select.appendChild( document.createElement( "option" ) );

  input.type = "checkbox";

  // Support: iOS<=5.1, Android<=4.2+
  // Default value for a checkbox should be "on"
  support.checkOn = input.value !== "";

  // Support: IE<=11+
  // Must access selectedIndex to make default options select
  support.optSelected = opt.selected;

  // Support: Android<=2.3
  // Options inside disabled selects are incorrectly marked as disabled
  select.disabled = true;
  support.optDisabled = !opt.disabled;

  // Support: IE<=11+
  // An input loses its value after becoming a radio
  input = document.createElement( "input" );
  input.value = "t";
  input.type = "radio";
  support.radioValue = input.value === "t";
} )();


var boolHook,
  attrHandle = jQuery.expr.attrHandle;

jQuery.fn.extend( {
  attr: function( name, value ) {
    return access( this, jQuery.attr, name, value, arguments.length > 1 );
  },

  removeAttr: function( name ) {
    return this.each( function() {
      jQuery.removeAttr( this, name );
    } );
  }
} );

jQuery.extend( {
  attr: function( elem, name, value ) {
    var ret, hooks,
      nType = elem.nodeType;

    // Don't get/set attributes on text, comment and attribute nodes
    if ( nType === 3 || nType === 8 || nType === 2 ) {
      return;
    }

    // Fallback to prop when attributes are not supported
    if ( typeof elem.getAttribute === "undefined" ) {
      return jQuery.prop( elem, name, value );
    }

    // All attributes are lowercase
    // Grab necessary hook if one is defined
    if ( nType !== 1 || !jQuery.isXMLDoc( elem ) ) {
      name = name.toLowerCase();
      hooks = jQuery.attrHooks[ name ] ||
        ( jQuery.expr.match.bool.test( name ) ? boolHook : undefined );
    }

    if ( value !== undefined ) {
      if ( value === null ) {
        jQuery.removeAttr( elem, name );
        return;
      }

      if ( hooks && "set" in hooks &&
        ( ret = hooks.set( elem, value, name ) ) !== undefined ) {
        return ret;
      }

      elem.setAttribute( name, value + "" );
      return value;
    }

    if ( hooks && "get" in hooks && ( ret = hooks.get( elem, name ) ) !== null ) {
      return ret;
    }

    ret = jQuery.find.attr( elem, name );

    // Non-existent attributes return null, we normalize to undefined
    return ret == null ? undefined : ret;
  },

  attrHooks: {
    type: {
      set: function( elem, value ) {
        if ( !support.radioValue && value === "radio" &&
          jQuery.nodeName( elem, "input" ) ) {
          var val = elem.value;
          elem.setAttribute( "type", value );
          if ( val ) {
            elem.value = val;
          }
          return value;
        }
      }
    }
  },

  removeAttr: function( elem, value ) {
    var name, propName,
      i = 0,
      attrNames = value && value.match( rnotwhite );

    if ( attrNames && elem.nodeType === 1 ) {
      while ( ( name = attrNames[ i++ ] ) ) {
        propName = jQuery.propFix[ name ] || name;

        // Boolean attributes get special treatment (#10870)
        if ( jQuery.expr.match.bool.test( name ) ) {

          // Set corresponding property to false
          elem[ propName ] = false;
        }

        elem.removeAttribute( name );
      }
    }
  }
} );

// Hooks for boolean attributes
boolHook = {
  set: function( elem, value, name ) {
    if ( value === false ) {

      // Remove boolean attributes when set to false
      jQuery.removeAttr( elem, name );
    } else {
      elem.setAttribute( name, name );
    }
    return name;
  }
};
jQuery.each( jQuery.expr.match.bool.source.match( /\w+/g ), function( i, name ) {
  var getter = attrHandle[ name ] || jQuery.find.attr;

  attrHandle[ name ] = function( elem, name, isXML ) {
    var ret, handle;
    if ( !isXML ) {

      // Avoid an infinite loop by temporarily removing this function from the getter
      handle = attrHandle[ name ];
      attrHandle[ name ] = ret;
      ret = getter( elem, name, isXML ) != null ?
        name.toLowerCase() :
        null;
      attrHandle[ name ] = handle;
    }
    return ret;
  };
} );




var rfocusable = /^(?:input|select|textarea|button)$/i,
  rclickable = /^(?:a|area)$/i;

jQuery.fn.extend( {
  prop: function( name, value ) {
    return access( this, jQuery.prop, name, value, arguments.length > 1 );
  },

  removeProp: function( name ) {
    return this.each( function() {
      delete this[ jQuery.propFix[ name ] || name ];
    } );
  }
} );

jQuery.extend( {
  prop: function( elem, name, value ) {
    var ret, hooks,
      nType = elem.nodeType;

    // Don't get/set properties on text, comment and attribute nodes
    if ( nType === 3 || nType === 8 || nType === 2 ) {
      return;
    }

    if ( nType !== 1 || !jQuery.isXMLDoc( elem ) ) {

      // Fix name and attach hooks
      name = jQuery.propFix[ name ] || name;
      hooks = jQuery.propHooks[ name ];
    }

    if ( value !== undefined ) {
      if ( hooks && "set" in hooks &&
        ( ret = hooks.set( elem, value, name ) ) !== undefined ) {
        return ret;
      }

      return ( elem[ name ] = value );
    }

    if ( hooks && "get" in hooks && ( ret = hooks.get( elem, name ) ) !== null ) {
      return ret;
    }

    return elem[ name ];
  },

  propHooks: {
    tabIndex: {
      get: function( elem ) {

        // elem.tabIndex doesn't always return the
        // correct value when it hasn't been explicitly set
        // http://fluidproject.org/blog/2008/01/09/getting-setting-and-removing-tabindex-values-with-javascript/
        // Use proper attribute retrieval(#12072)
        var tabindex = jQuery.find.attr( elem, "tabindex" );

        return tabindex ?
          parseInt( tabindex, 10 ) :
          rfocusable.test( elem.nodeName ) ||
            rclickable.test( elem.nodeName ) && elem.href ?
              0 :
              -1;
      }
    }
  },

  propFix: {
    "for": "htmlFor",
    "class": "className"
  }
} );

// Support: IE <=11 only
// Accessing the selectedIndex property
// forces the browser to respect setting selected
// on the option
// The getter ensures a default option is selected
// when in an optgroup
if ( !support.optSelected ) {
  jQuery.propHooks.selected = {
    get: function( elem ) {
      var parent = elem.parentNode;
      if ( parent && parent.parentNode ) {
        parent.parentNode.selectedIndex;
      }
      return null;
    },
    set: function( elem ) {
      var parent = elem.parentNode;
      if ( parent ) {
        parent.selectedIndex;

        if ( parent.parentNode ) {
          parent.parentNode.selectedIndex;
        }
      }
    }
  };
}

jQuery.each( [
  "tabIndex",
  "readOnly",
  "maxLength",
  "cellSpacing",
  "cellPadding",
  "rowSpan",
  "colSpan",
  "useMap",
  "frameBorder",
  "contentEditable"
], function() {
  jQuery.propFix[ this.toLowerCase() ] = this;
} );




var rclass = /[\t\r\n\f]/g;

function getClass( elem ) {
  return elem.getAttribute && elem.getAttribute( "class" ) || "";
}

jQuery.fn.extend( {
  addClass: function( value ) {
    var classes, elem, cur, curValue, clazz, j, finalValue,
      i = 0;

    if ( jQuery.isFunction( value ) ) {
      return this.each( function( j ) {
        jQuery( this ).addClass( value.call( this, j, getClass( this ) ) );
      } );
    }

    if ( typeof value === "string" && value ) {
      classes = value.match( rnotwhite ) || [];

      while ( ( elem = this[ i++ ] ) ) {
        curValue = getClass( elem );
        cur = elem.nodeType === 1 &&
          ( " " + curValue + " " ).replace( rclass, " " );

        if ( cur ) {
          j = 0;
          while ( ( clazz = classes[ j++ ] ) ) {
            if ( cur.indexOf( " " + clazz + " " ) < 0 ) {
              cur += clazz + " ";
            }
          }

          // Only assign if different to avoid unneeded rendering.
          finalValue = jQuery.trim( cur );
          if ( curValue !== finalValue ) {
            elem.setAttribute( "class", finalValue );
          }
        }
      }
    }

    return this;
  },

  removeClass: function( value ) {
    var classes, elem, cur, curValue, clazz, j, finalValue,
      i = 0;

    if ( jQuery.isFunction( value ) ) {
      return this.each( function( j ) {
        jQuery( this ).removeClass( value.call( this, j, getClass( this ) ) );
      } );
    }

    if ( !arguments.length ) {
      return this.attr( "class", "" );
    }

    if ( typeof value === "string" && value ) {
      classes = value.match( rnotwhite ) || [];

      while ( ( elem = this[ i++ ] ) ) {
        curValue = getClass( elem );

        // This expression is here for better compressibility (see addClass)
        cur = elem.nodeType === 1 &&
          ( " " + curValue + " " ).replace( rclass, " " );

        if ( cur ) {
          j = 0;
          while ( ( clazz = classes[ j++ ] ) ) {

            // Remove *all* instances
            while ( cur.indexOf( " " + clazz + " " ) > -1 ) {
              cur = cur.replace( " " + clazz + " ", " " );
            }
          }

          // Only assign if different to avoid unneeded rendering.
          finalValue = jQuery.trim( cur );
          if ( curValue !== finalValue ) {
            elem.setAttribute( "class", finalValue );
          }
        }
      }
    }

    return this;
  },

  toggleClass: function( value, stateVal ) {
    var type = typeof value;

    if ( typeof stateVal === "boolean" && type === "string" ) {
      return stateVal ? this.addClass( value ) : this.removeClass( value );
    }

    if ( jQuery.isFunction( value ) ) {
      return this.each( function( i ) {
        jQuery( this ).toggleClass(
          value.call( this, i, getClass( this ), stateVal ),
          stateVal
        );
      } );
    }

    return this.each( function() {
      var className, i, self, classNames;

      if ( type === "string" ) {

        // Toggle individual class names
        i = 0;
        self = jQuery( this );
        classNames = value.match( rnotwhite ) || [];

        while ( ( className = classNames[ i++ ] ) ) {

          // Check each className given, space separated list
          if ( self.hasClass( className ) ) {
            self.removeClass( className );
          } else {
            self.addClass( className );
          }
        }

      // Toggle whole class name
      } else if ( value === undefined || type === "boolean" ) {
        className = getClass( this );
        if ( className ) {

          // Store className if set
          dataPriv.set( this, "__className__", className );
        }

        // If the element has a class name or if we're passed `false`,
        // then remove the whole classname (if there was one, the above saved it).
        // Otherwise bring back whatever was previously saved (if anything),
        // falling back to the empty string if nothing was stored.
        if ( this.setAttribute ) {
          this.setAttribute( "class",
            className || value === false ?
            "" :
            dataPriv.get( this, "__className__" ) || ""
          );
        }
      }
    } );
  },

  hasClass: function( selector ) {
    var className, elem,
      i = 0;

    className = " " + selector + " ";
    while ( ( elem = this[ i++ ] ) ) {
      if ( elem.nodeType === 1 &&
        ( " " + getClass( elem ) + " " ).replace( rclass, " " )
          .indexOf( className ) > -1
      ) {
        return true;
      }
    }

    return false;
  }
} );




var rreturn = /\r/g,
  rspaces = /[\x20\t\r\n\f]+/g;

jQuery.fn.extend( {
  val: function( value ) {
    var hooks, ret, isFunction,
      elem = this[ 0 ];

    if ( !arguments.length ) {
      if ( elem ) {
        hooks = jQuery.valHooks[ elem.type ] ||
          jQuery.valHooks[ elem.nodeName.toLowerCase() ];

        if ( hooks &&
          "get" in hooks &&
          ( ret = hooks.get( elem, "value" ) ) !== undefined
        ) {
          return ret;
        }

        ret = elem.value;

        return typeof ret === "string" ?

          // Handle most common string cases
          ret.replace( rreturn, "" ) :

          // Handle cases where value is null/undef or number
          ret == null ? "" : ret;
      }

      return;
    }

    isFunction = jQuery.isFunction( value );

    return this.each( function( i ) {
      var val;

      if ( this.nodeType !== 1 ) {
        return;
      }

      if ( isFunction ) {
        val = value.call( this, i, jQuery( this ).val() );
      } else {
        val = value;
      }

      // Treat null/undefined as ""; convert numbers to string
      if ( val == null ) {
        val = "";

      } else if ( typeof val === "number" ) {
        val += "";

      } else if ( jQuery.isArray( val ) ) {
        val = jQuery.map( val, function( value ) {
          return value == null ? "" : value + "";
        } );
      }

      hooks = jQuery.valHooks[ this.type ] || jQuery.valHooks[ this.nodeName.toLowerCase() ];

      // If set returns undefined, fall back to normal setting
      if ( !hooks || !( "set" in hooks ) || hooks.set( this, val, "value" ) === undefined ) {
        this.value = val;
      }
    } );
  }
} );

jQuery.extend( {
  valHooks: {
    option: {
      get: function( elem ) {

        var val = jQuery.find.attr( elem, "value" );
        return val != null ?
          val :

          // Support: IE10-11+
          // option.text throws exceptions (#14686, #14858)
          // Strip and collapse whitespace
          // https://html.spec.whatwg.org/#strip-and-collapse-whitespace
          jQuery.trim( jQuery.text( elem ) ).replace( rspaces, " " );
      }
    },
    select: {
      get: function( elem ) {
        var value, option,
          options = elem.options,
          index = elem.selectedIndex,
          one = elem.type === "select-one" || index < 0,
          values = one ? null : [],
          max = one ? index + 1 : options.length,
          i = index < 0 ?
            max :
            one ? index : 0;

        // Loop through all the selected options
        for ( ; i < max; i++ ) {
          option = options[ i ];

          // IE8-9 doesn't update selected after form reset (#2551)
          if ( ( option.selected || i === index ) &&

              // Don't return options that are disabled or in a disabled optgroup
              ( support.optDisabled ?
                !option.disabled : option.getAttribute( "disabled" ) === null ) &&
              ( !option.parentNode.disabled ||
                !jQuery.nodeName( option.parentNode, "optgroup" ) ) ) {

            // Get the specific value for the option
            value = jQuery( option ).val();

            // We don't need an array for one selects
            if ( one ) {
              return value;
            }

            // Multi-Selects return an array
            values.push( value );
          }
        }

        return values;
      },

      set: function( elem, value ) {
        var optionSet, option,
          options = elem.options,
          values = jQuery.makeArray( value ),
          i = options.length;

        while ( i-- ) {
          option = options[ i ];
          if ( option.selected =
            jQuery.inArray( jQuery.valHooks.option.get( option ), values ) > -1
          ) {
            optionSet = true;
          }
        }

        // Force browsers to behave consistently when non-matching value is set
        if ( !optionSet ) {
          elem.selectedIndex = -1;
        }
        return values;
      }
    }
  }
} );

// Radios and checkboxes getter/setter
jQuery.each( [ "radio", "checkbox" ], function() {
  jQuery.valHooks[ this ] = {
    set: function( elem, value ) {
      if ( jQuery.isArray( value ) ) {
        return ( elem.checked = jQuery.inArray( jQuery( elem ).val(), value ) > -1 );
      }
    }
  };
  if ( !support.checkOn ) {
    jQuery.valHooks[ this ].get = function( elem ) {
      return elem.getAttribute( "value" ) === null ? "on" : elem.value;
    };
  }
} );




// Return jQuery for attributes-only inclusion


var rfocusMorph = /^(?:focusinfocus|focusoutblur)$/;

jQuery.extend( jQuery.event, {

  trigger: function( event, data, elem, onlyHandlers ) {

    var i, cur, tmp, bubbleType, ontype, handle, special,
      eventPath = [ elem || document ],
      type = hasOwn.call( event, "type" ) ? event.type : event,
      namespaces = hasOwn.call( event, "namespace" ) ? event.namespace.split( "." ) : [];

    cur = tmp = elem = elem || document;

    // Don't do events on text and comment nodes
    if ( elem.nodeType === 3 || elem.nodeType === 8 ) {
      return;
    }

    // focus/blur morphs to focusin/out; ensure we're not firing them right now
    if ( rfocusMorph.test( type + jQuery.event.triggered ) ) {
      return;
    }

    if ( type.indexOf( "." ) > -1 ) {

      // Namespaced trigger; create a regexp to match event type in handle()
      namespaces = type.split( "." );
      type = namespaces.shift();
      namespaces.sort();
    }
    ontype = type.indexOf( ":" ) < 0 && "on" + type;

    // Caller can pass in a jQuery.Event object, Object, or just an event type string
    event = event[ jQuery.expando ] ?
      event :
      new jQuery.Event( type, typeof event === "object" && event );

    // Trigger bitmask: & 1 for native handlers; & 2 for jQuery (always true)
    event.isTrigger = onlyHandlers ? 2 : 3;
    event.namespace = namespaces.join( "." );
    event.rnamespace = event.namespace ?
      new RegExp( "(^|\\.)" + namespaces.join( "\\.(?:.*\\.|)" ) + "(\\.|$)" ) :
      null;

    // Clean up the event in case it is being reused
    event.result = undefined;
    if ( !event.target ) {
      event.target = elem;
    }

    // Clone any incoming data and prepend the event, creating the handler arg list
    data = data == null ?
      [ event ] :
      jQuery.makeArray( data, [ event ] );

    // Allow special events to draw outside the lines
    special = jQuery.event.special[ type ] || {};
    if ( !onlyHandlers && special.trigger && special.trigger.apply( elem, data ) === false ) {
      return;
    }

    // Determine event propagation path in advance, per W3C events spec (#9951)
    // Bubble up to document, then to window; watch for a global ownerDocument var (#9724)
    if ( !onlyHandlers && !special.noBubble && !jQuery.isWindow( elem ) ) {

      bubbleType = special.delegateType || type;
      if ( !rfocusMorph.test( bubbleType + type ) ) {
        cur = cur.parentNode;
      }
      for ( ; cur; cur = cur.parentNode ) {
        eventPath.push( cur );
        tmp = cur;
      }

      // Only add window if we got to document (e.g., not plain obj or detached DOM)
      if ( tmp === ( elem.ownerDocument || document ) ) {
        eventPath.push( tmp.defaultView || tmp.parentWindow || window );
      }
    }

    // Fire handlers on the event path
    i = 0;
    while ( ( cur = eventPath[ i++ ] ) && !event.isPropagationStopped() ) {

      event.type = i > 1 ?
        bubbleType :
        special.bindType || type;

      // jQuery handler
      handle = ( dataPriv.get( cur, "events" ) || {} )[ event.type ] &&
        dataPriv.get( cur, "handle" );
      if ( handle ) {
        handle.apply( cur, data );
      }

      // Native handler
      handle = ontype && cur[ ontype ];
      if ( handle && handle.apply && acceptData( cur ) ) {
        event.result = handle.apply( cur, data );
        if ( event.result === false ) {
          event.preventDefault();
        }
      }
    }
    event.type = type;

    // If nobody prevented the default action, do it now
    if ( !onlyHandlers && !event.isDefaultPrevented() ) {

      if ( ( !special._default ||
        special._default.apply( eventPath.pop(), data ) === false ) &&
        acceptData( elem ) ) {

        // Call a native DOM method on the target with the same name name as the event.
        // Don't do default actions on window, that's where global variables be (#6170)
        if ( ontype && jQuery.isFunction( elem[ type ] ) && !jQuery.isWindow( elem ) ) {

          // Don't re-trigger an onFOO event when we call its FOO() method
          tmp = elem[ ontype ];

          if ( tmp ) {
            elem[ ontype ] = null;
          }

          // Prevent re-triggering of the same event, since we already bubbled it above
          jQuery.event.triggered = type;
          elem[ type ]();
          jQuery.event.triggered = undefined;

          if ( tmp ) {
            elem[ ontype ] = tmp;
          }
        }
      }
    }

    return event.result;
  },

  // Piggyback on a donor event to simulate a different one
  // Used only for `focus(in | out)` events
  simulate: function( type, elem, event ) {
    var e = jQuery.extend(
      new jQuery.Event(),
      event,
      {
        type: type,
        isSimulated: true
      }
    );

    jQuery.event.trigger( e, null, elem );
  }

} );

jQuery.fn.extend( {

  trigger: function( type, data ) {
    return this.each( function() {
      jQuery.event.trigger( type, data, this );
    } );
  },
  triggerHandler: function( type, data ) {
    var elem = this[ 0 ];
    if ( elem ) {
      return jQuery.event.trigger( type, data, elem, true );
    }
  }
} );


jQuery.each( ( "blur focus focusin focusout load resize scroll unload click dblclick " +
  "mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave " +
  "change select submit keydown keypress keyup error contextmenu" ).split( " " ),
  function( i, name ) {

  // Handle event binding
  jQuery.fn[ name ] = function( data, fn ) {
    return arguments.length > 0 ?
      this.on( name, null, data, fn ) :
      this.trigger( name );
  };
} );

jQuery.fn.extend( {
  hover: function( fnOver, fnOut ) {
    return this.mouseenter( fnOver ).mouseleave( fnOut || fnOver );
  }
} );




support.focusin = "onfocusin" in window;


// Support: Firefox
// Firefox doesn't have focus(in | out) events
// Related ticket - https://bugzilla.mozilla.org/show_bug.cgi?id=687787
//
// Support: Chrome, Safari
// focus(in | out) events fire after focus & blur events,
// which is spec violation - http://www.w3.org/TR/DOM-Level-3-Events/#events-focusevent-event-order
// Related ticket - https://code.google.com/p/chromium/issues/detail?id=449857
if ( !support.focusin ) {
  jQuery.each( { focus: "focusin", blur: "focusout" }, function( orig, fix ) {

    // Attach a single capturing handler on the document while someone wants focusin/focusout
    var handler = function( event ) {
      jQuery.event.simulate( fix, event.target, jQuery.event.fix( event ) );
    };

    jQuery.event.special[ fix ] = {
      setup: function() {
        var doc = this.ownerDocument || this,
          attaches = dataPriv.access( doc, fix );

        if ( !attaches ) {
          doc.addEventListener( orig, handler, true );
        }
        dataPriv.access( doc, fix, ( attaches || 0 ) + 1 );
      },
      teardown: function() {
        var doc = this.ownerDocument || this,
          attaches = dataPriv.access( doc, fix ) - 1;

        if ( !attaches ) {
          doc.removeEventListener( orig, handler, true );
          dataPriv.remove( doc, fix );

        } else {
          dataPriv.access( doc, fix, attaches );
        }
      }
    };
  } );
}
var location = window.location;

var nonce = jQuery.now();

var rquery = ( /\?/ );



// Support: Android 2.3
// Workaround failure to string-cast null input
jQuery.parseJSON = function( data ) {
  return JSON.parse( data + "" );
};


// Cross-browser xml parsing
jQuery.parseXML = function( data ) {
  var xml;
  if ( !data || typeof data !== "string" ) {
    return null;
  }

  // Support: IE9
  try {
    xml = ( new window.DOMParser() ).parseFromString( data, "text/xml" );
  } catch ( e ) {
    xml = undefined;
  }

  if ( !xml || xml.getElementsByTagName( "parsererror" ).length ) {
    jQuery.error( "Invalid XML: " + data );
  }
  return xml;
};


var
  rhash = /#.*$/,
  rts = /([?&])_=[^&]*/,
  rheaders = /^(.*?):[ \t]*([^\r\n]*)$/mg,

  // #7653, #8125, #8152: local protocol detection
  rlocalProtocol = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/,
  rnoContent = /^(?:GET|HEAD)$/,
  rprotocol = /^\/\//,

  /* Prefilters
   * 1) They are useful to introduce custom dataTypes (see ajax/jsonp.js for an example)
   * 2) These are called:
   *    - BEFORE asking for a transport
   *    - AFTER param serialization (s.data is a string if s.processData is true)
   * 3) key is the dataType
   * 4) the catchall symbol "*" can be used
   * 5) execution will start with transport dataType and THEN continue down to "*" if needed
   */
  prefilters = {},

  /* Transports bindings
   * 1) key is the dataType
   * 2) the catchall symbol "*" can be used
   * 3) selection will start with transport dataType and THEN go to "*" if needed
   */
  transports = {},

  // Avoid comment-prolog char sequence (#10098); must appease lint and evade compression
  allTypes = "*/".concat( "*" ),

  // Anchor tag for parsing the document origin
  originAnchor = document.createElement( "a" );
  originAnchor.href = location.href;

// Base "constructor" for jQuery.ajaxPrefilter and jQuery.ajaxTransport
function addToPrefiltersOrTransports( structure ) {

  // dataTypeExpression is optional and defaults to "*"
  return function( dataTypeExpression, func ) {

    if ( typeof dataTypeExpression !== "string" ) {
      func = dataTypeExpression;
      dataTypeExpression = "*";
    }

    var dataType,
      i = 0,
      dataTypes = dataTypeExpression.toLowerCase().match( rnotwhite ) || [];

    if ( jQuery.isFunction( func ) ) {

      // For each dataType in the dataTypeExpression
      while ( ( dataType = dataTypes[ i++ ] ) ) {

        // Prepend if requested
        if ( dataType[ 0 ] === "+" ) {
          dataType = dataType.slice( 1 ) || "*";
          ( structure[ dataType ] = structure[ dataType ] || [] ).unshift( func );

        // Otherwise append
        } else {
          ( structure[ dataType ] = structure[ dataType ] || [] ).push( func );
        }
      }
    }
  };
}

// Base inspection function for prefilters and transports
function inspectPrefiltersOrTransports( structure, options, originalOptions, jqXHR ) {

  var inspected = {},
    seekingTransport = ( structure === transports );

  function inspect( dataType ) {
    var selected;
    inspected[ dataType ] = true;
    jQuery.each( structure[ dataType ] || [], function( _, prefilterOrFactory ) {
      var dataTypeOrTransport = prefilterOrFactory( options, originalOptions, jqXHR );
      if ( typeof dataTypeOrTransport === "string" &&
        !seekingTransport && !inspected[ dataTypeOrTransport ] ) {

        options.dataTypes.unshift( dataTypeOrTransport );
        inspect( dataTypeOrTransport );
        return false;
      } else if ( seekingTransport ) {
        return !( selected = dataTypeOrTransport );
      }
    } );
    return selected;
  }

  return inspect( options.dataTypes[ 0 ] ) || !inspected[ "*" ] && inspect( "*" );
}

// A special extend for ajax options
// that takes "flat" options (not to be deep extended)
// Fixes #9887
function ajaxExtend( target, src ) {
  var key, deep,
    flatOptions = jQuery.ajaxSettings.flatOptions || {};

  for ( key in src ) {
    if ( src[ key ] !== undefined ) {
      ( flatOptions[ key ] ? target : ( deep || ( deep = {} ) ) )[ key ] = src[ key ];
    }
  }
  if ( deep ) {
    jQuery.extend( true, target, deep );
  }

  return target;
}

/* Handles responses to an ajax request:
 * - finds the right dataType (mediates between content-type and expected dataType)
 * - returns the corresponding response
 */
function ajaxHandleResponses( s, jqXHR, responses ) {

  var ct, type, finalDataType, firstDataType,
    contents = s.contents,
    dataTypes = s.dataTypes;

  // Remove auto dataType and get content-type in the process
  while ( dataTypes[ 0 ] === "*" ) {
    dataTypes.shift();
    if ( ct === undefined ) {
      ct = s.mimeType || jqXHR.getResponseHeader( "Content-Type" );
    }
  }

  // Check if we're dealing with a known content-type
  if ( ct ) {
    for ( type in contents ) {
      if ( contents[ type ] && contents[ type ].test( ct ) ) {
        dataTypes.unshift( type );
        break;
      }
    }
  }

  // Check to see if we have a response for the expected dataType
  if ( dataTypes[ 0 ] in responses ) {
    finalDataType = dataTypes[ 0 ];
  } else {

    // Try convertible dataTypes
    for ( type in responses ) {
      if ( !dataTypes[ 0 ] || s.converters[ type + " " + dataTypes[ 0 ] ] ) {
        finalDataType = type;
        break;
      }
      if ( !firstDataType ) {
        firstDataType = type;
      }
    }

    // Or just use first one
    finalDataType = finalDataType || firstDataType;
  }

  // If we found a dataType
  // We add the dataType to the list if needed
  // and return the corresponding response
  if ( finalDataType ) {
    if ( finalDataType !== dataTypes[ 0 ] ) {
      dataTypes.unshift( finalDataType );
    }
    return responses[ finalDataType ];
  }
}

/* Chain conversions given the request and the original response
 * Also sets the responseXXX fields on the jqXHR instance
 */
function ajaxConvert( s, response, jqXHR, isSuccess ) {
  var conv2, current, conv, tmp, prev,
    converters = {},

    // Work with a copy of dataTypes in case we need to modify it for conversion
    dataTypes = s.dataTypes.slice();

  // Create converters map with lowercased keys
  if ( dataTypes[ 1 ] ) {
    for ( conv in s.converters ) {
      converters[ conv.toLowerCase() ] = s.converters[ conv ];
    }
  }

  current = dataTypes.shift();

  // Convert to each sequential dataType
  while ( current ) {

    if ( s.responseFields[ current ] ) {
      jqXHR[ s.responseFields[ current ] ] = response;
    }

    // Apply the dataFilter if provided
    if ( !prev && isSuccess && s.dataFilter ) {
      response = s.dataFilter( response, s.dataType );
    }

    prev = current;
    current = dataTypes.shift();

    if ( current ) {

    // There's only work to do if current dataType is non-auto
      if ( current === "*" ) {

        current = prev;

      // Convert response if prev dataType is non-auto and differs from current
      } else if ( prev !== "*" && prev !== current ) {

        // Seek a direct converter
        conv = converters[ prev + " " + current ] || converters[ "* " + current ];

        // If none found, seek a pair
        if ( !conv ) {
          for ( conv2 in converters ) {

            // If conv2 outputs current
            tmp = conv2.split( " " );
            if ( tmp[ 1 ] === current ) {

              // If prev can be converted to accepted input
              conv = converters[ prev + " " + tmp[ 0 ] ] ||
                converters[ "* " + tmp[ 0 ] ];
              if ( conv ) {

                // Condense equivalence converters
                if ( conv === true ) {
                  conv = converters[ conv2 ];

                // Otherwise, insert the intermediate dataType
                } else if ( converters[ conv2 ] !== true ) {
                  current = tmp[ 0 ];
                  dataTypes.unshift( tmp[ 1 ] );
                }
                break;
              }
            }
          }
        }

        // Apply converter (if not an equivalence)
        if ( conv !== true ) {

          // Unless errors are allowed to bubble, catch and return them
          if ( conv && s.throws ) {
            response = conv( response );
          } else {
            try {
              response = conv( response );
            } catch ( e ) {
              return {
                state: "parsererror",
                error: conv ? e : "No conversion from " + prev + " to " + current
              };
            }
          }
        }
      }
    }
  }

  return { state: "success", data: response };
}

jQuery.extend( {

  // Counter for holding the number of active queries
  active: 0,

  // Last-Modified header cache for next request
  lastModified: {},
  etag: {},

  ajaxSettings: {
    url: location.href,
    type: "GET",
    isLocal: rlocalProtocol.test( location.protocol ),
    global: true,
    processData: true,
    async: true,
    contentType: "application/x-www-form-urlencoded; charset=UTF-8",
    /*
    timeout: 0,
    data: null,
    dataType: null,
    username: null,
    password: null,
    cache: null,
    throws: false,
    traditional: false,
    headers: {},
    */

    accepts: {
      "*": allTypes,
      text: "text/plain",
      html: "text/html",
      xml: "application/xml, text/xml",
      json: "application/json, text/javascript"
    },

    contents: {
      xml: /\bxml\b/,
      html: /\bhtml/,
      json: /\bjson\b/
    },

    responseFields: {
      xml: "responseXML",
      text: "responseText",
      json: "responseJSON"
    },

    // Data converters
    // Keys separate source (or catchall "*") and destination types with a single space
    converters: {

      // Convert anything to text
      "* text": String,

      // Text to html (true = no transformation)
      "text html": true,

      // Evaluate text as a json expression
      "text json": jQuery.parseJSON,

      // Parse text as xml
      "text xml": jQuery.parseXML
    },

    // For options that shouldn't be deep extended:
    // you can add your own custom options here if
    // and when you create one that shouldn't be
    // deep extended (see ajaxExtend)
    flatOptions: {
      url: true,
      context: true
    }
  },

  // Creates a full fledged settings object into target
  // with both ajaxSettings and settings fields.
  // If target is omitted, writes into ajaxSettings.
  ajaxSetup: function( target, settings ) {
    return settings ?

      // Building a settings object
      ajaxExtend( ajaxExtend( target, jQuery.ajaxSettings ), settings ) :

      // Extending ajaxSettings
      ajaxExtend( jQuery.ajaxSettings, target );
  },

  ajaxPrefilter: addToPrefiltersOrTransports( prefilters ),
  ajaxTransport: addToPrefiltersOrTransports( transports ),

  // Main method
  ajax: function( url, options ) {

    // If url is an object, simulate pre-1.5 signature
    if ( typeof url === "object" ) {
      options = url;
      url = undefined;
    }

    // Force options to be an object
    options = options || {};

    var transport,

      // URL without anti-cache param
      cacheURL,

      // Response headers
      responseHeadersString,
      responseHeaders,

      // timeout handle
      timeoutTimer,

      // Url cleanup var
      urlAnchor,

      // To know if global events are to be dispatched
      fireGlobals,

      // Loop variable
      i,

      // Create the final options object
      s = jQuery.ajaxSetup( {}, options ),

      // Callbacks context
      callbackContext = s.context || s,

      // Context for global events is callbackContext if it is a DOM node or jQuery collection
      globalEventContext = s.context &&
        ( callbackContext.nodeType || callbackContext.jquery ) ?
          jQuery( callbackContext ) :
          jQuery.event,

      // Deferreds
      deferred = jQuery.Deferred(),
      completeDeferred = jQuery.Callbacks( "once memory" ),

      // Status-dependent callbacks
      statusCode = s.statusCode || {},

      // Headers (they are sent all at once)
      requestHeaders = {},
      requestHeadersNames = {},

      // The jqXHR state
      state = 0,

      // Default abort message
      strAbort = "canceled",

      // Fake xhr
      jqXHR = {
        readyState: 0,

        // Builds headers hashtable if needed
        getResponseHeader: function( key ) {
          var match;
          if ( state === 2 ) {
            if ( !responseHeaders ) {
              responseHeaders = {};
              while ( ( match = rheaders.exec( responseHeadersString ) ) ) {
                responseHeaders[ match[ 1 ].toLowerCase() ] = match[ 2 ];
              }
            }
            match = responseHeaders[ key.toLowerCase() ];
          }
          return match == null ? null : match;
        },

        // Raw string
        getAllResponseHeaders: function() {
          return state === 2 ? responseHeadersString : null;
        },

        // Caches the header
        setRequestHeader: function( name, value ) {
          var lname = name.toLowerCase();
          if ( !state ) {
            name = requestHeadersNames[ lname ] = requestHeadersNames[ lname ] || name;
            requestHeaders[ name ] = value;
          }
          return this;
        },

        // Overrides response content-type header
        overrideMimeType: function( type ) {
          if ( !state ) {
            s.mimeType = type;
          }
          return this;
        },

        // Status-dependent callbacks
        statusCode: function( map ) {
          var code;
          if ( map ) {
            if ( state < 2 ) {
              for ( code in map ) {

                // Lazy-add the new callback in a way that preserves old ones
                statusCode[ code ] = [ statusCode[ code ], map[ code ] ];
              }
            } else {

              // Execute the appropriate callbacks
              jqXHR.always( map[ jqXHR.status ] );
            }
          }
          return this;
        },

        // Cancel the request
        abort: function( statusText ) {
          var finalText = statusText || strAbort;
          if ( transport ) {
            transport.abort( finalText );
          }
          done( 0, finalText );
          return this;
        }
      };

    // Attach deferreds
    deferred.promise( jqXHR ).complete = completeDeferred.add;
    jqXHR.success = jqXHR.done;
    jqXHR.error = jqXHR.fail;

    // Remove hash character (#7531: and string promotion)
    // Add protocol if not provided (prefilters might expect it)
    // Handle falsy url in the settings object (#10093: consistency with old signature)
    // We also use the url parameter if available
    s.url = ( ( url || s.url || location.href ) + "" ).replace( rhash, "" )
      .replace( rprotocol, location.protocol + "//" );

    // Alias method option to type as per ticket #12004
    s.type = options.method || options.type || s.method || s.type;

    // Extract dataTypes list
    s.dataTypes = jQuery.trim( s.dataType || "*" ).toLowerCase().match( rnotwhite ) || [ "" ];

    // A cross-domain request is in order when the origin doesn't match the current origin.
    if ( s.crossDomain == null ) {
      urlAnchor = document.createElement( "a" );

      // Support: IE8-11+
      // IE throws exception if url is malformed, e.g. http://example.com:80x/
      try {
        urlAnchor.href = s.url;

        // Support: IE8-11+
        // Anchor's host property isn't correctly set when s.url is relative
        urlAnchor.href = urlAnchor.href;
        s.crossDomain = originAnchor.protocol + "//" + originAnchor.host !==
          urlAnchor.protocol + "//" + urlAnchor.host;
      } catch ( e ) {

        // If there is an error parsing the URL, assume it is crossDomain,
        // it can be rejected by the transport if it is invalid
        s.crossDomain = true;
      }
    }

    // Convert data if not already a string
    if ( s.data && s.processData && typeof s.data !== "string" ) {
      s.data = jQuery.param( s.data, s.traditional );
    }

    // Apply prefilters
    inspectPrefiltersOrTransports( prefilters, s, options, jqXHR );

    // If request was aborted inside a prefilter, stop there
    if ( state === 2 ) {
      return jqXHR;
    }

    // We can fire global events as of now if asked to
    // Don't fire events if jQuery.event is undefined in an AMD-usage scenario (#15118)
    fireGlobals = jQuery.event && s.global;

    // Watch for a new set of requests
    if ( fireGlobals && jQuery.active++ === 0 ) {
      jQuery.event.trigger( "ajaxStart" );
    }

    // Uppercase the type
    s.type = s.type.toUpperCase();

    // Determine if request has content
    s.hasContent = !rnoContent.test( s.type );

    // Save the URL in case we're toying with the If-Modified-Since
    // and/or If-None-Match header later on
    cacheURL = s.url;

    // More options handling for requests with no content
    if ( !s.hasContent ) {

      // If data is available, append data to url
      if ( s.data ) {
        cacheURL = ( s.url += ( rquery.test( cacheURL ) ? "&" : "?" ) + s.data );

        // #9682: remove data so that it's not used in an eventual retry
        delete s.data;
      }

      // Add anti-cache in url if needed
      if ( s.cache === false ) {
        s.url = rts.test( cacheURL ) ?

          // If there is already a '_' parameter, set its value
          cacheURL.replace( rts, "$1_=" + nonce++ ) :

          // Otherwise add one to the end
          cacheURL + ( rquery.test( cacheURL ) ? "&" : "?" ) + "_=" + nonce++;
      }
    }

    // Set the If-Modified-Since and/or If-None-Match header, if in ifModified mode.
    if ( s.ifModified ) {
      if ( jQuery.lastModified[ cacheURL ] ) {
        jqXHR.setRequestHeader( "If-Modified-Since", jQuery.lastModified[ cacheURL ] );
      }
      if ( jQuery.etag[ cacheURL ] ) {
        jqXHR.setRequestHeader( "If-None-Match", jQuery.etag[ cacheURL ] );
      }
    }

    // Set the correct header, if data is being sent
    if ( s.data && s.hasContent && s.contentType !== false || options.contentType ) {
      jqXHR.setRequestHeader( "Content-Type", s.contentType );
    }

    // Set the Accepts header for the server, depending on the dataType
    jqXHR.setRequestHeader(
      "Accept",
      s.dataTypes[ 0 ] && s.accepts[ s.dataTypes[ 0 ] ] ?
        s.accepts[ s.dataTypes[ 0 ] ] +
          ( s.dataTypes[ 0 ] !== "*" ? ", " + allTypes + "; q=0.01" : "" ) :
        s.accepts[ "*" ]
    );

    // Check for headers option
    for ( i in s.headers ) {
      jqXHR.setRequestHeader( i, s.headers[ i ] );
    }

    // Allow custom headers/mimetypes and early abort
    if ( s.beforeSend &&
      ( s.beforeSend.call( callbackContext, jqXHR, s ) === false || state === 2 ) ) {

      // Abort if not done already and return
      return jqXHR.abort();
    }

    // Aborting is no longer a cancellation
    strAbort = "abort";

    // Install callbacks on deferreds
    for ( i in { success: 1, error: 1, complete: 1 } ) {
      jqXHR[ i ]( s[ i ] );
    }

    // Get transport
    transport = inspectPrefiltersOrTransports( transports, s, options, jqXHR );

    // If no transport, we auto-abort
    if ( !transport ) {
      done( -1, "No Transport" );
    } else {
      jqXHR.readyState = 1;

      // Send global event
      if ( fireGlobals ) {
        globalEventContext.trigger( "ajaxSend", [ jqXHR, s ] );
      }

      // If request was aborted inside ajaxSend, stop there
      if ( state === 2 ) {
        return jqXHR;
      }

      // Timeout
      if ( s.async && s.timeout > 0 ) {
        timeoutTimer = window.setTimeout( function() {
          jqXHR.abort( "timeout" );
        }, s.timeout );
      }

      try {
        state = 1;
        transport.send( requestHeaders, done );
      } catch ( e ) {

        // Propagate exception as error if not done
        if ( state < 2 ) {
          done( -1, e );

        // Simply rethrow otherwise
        } else {
          throw e;
        }
      }
    }

    // Callback for when everything is done
    function done( status, nativeStatusText, responses, headers ) {
      var isSuccess, success, error, response, modified,
        statusText = nativeStatusText;

      // Called once
      if ( state === 2 ) {
        return;
      }

      // State is "done" now
      state = 2;

      // Clear timeout if it exists
      if ( timeoutTimer ) {
        window.clearTimeout( timeoutTimer );
      }

      // Dereference transport for early garbage collection
      // (no matter how long the jqXHR object will be used)
      transport = undefined;

      // Cache response headers
      responseHeadersString = headers || "";

      // Set readyState
      jqXHR.readyState = status > 0 ? 4 : 0;

      // Determine if successful
      isSuccess = status >= 200 && status < 300 || status === 304;

      // Get response data
      if ( responses ) {
        response = ajaxHandleResponses( s, jqXHR, responses );
      }

      // Convert no matter what (that way responseXXX fields are always set)
      response = ajaxConvert( s, response, jqXHR, isSuccess );

      // If successful, handle type chaining
      if ( isSuccess ) {

        // Set the If-Modified-Since and/or If-None-Match header, if in ifModified mode.
        if ( s.ifModified ) {
          modified = jqXHR.getResponseHeader( "Last-Modified" );
          if ( modified ) {
            jQuery.lastModified[ cacheURL ] = modified;
          }
          modified = jqXHR.getResponseHeader( "etag" );
          if ( modified ) {
            jQuery.etag[ cacheURL ] = modified;
          }
        }

        // if no content
        if ( status === 204 || s.type === "HEAD" ) {
          statusText = "nocontent";

        // if not modified
        } else if ( status === 304 ) {
          statusText = "notmodified";

        // If we have data, let's convert it
        } else {
          statusText = response.state;
          success = response.data;
          error = response.error;
          isSuccess = !error;
        }
      } else {

        // Extract error from statusText and normalize for non-aborts
        error = statusText;
        if ( status || !statusText ) {
          statusText = "error";
          if ( status < 0 ) {
            status = 0;
          }
        }
      }

      // Set data for the fake xhr object
      jqXHR.status = status;
      jqXHR.statusText = ( nativeStatusText || statusText ) + "";

      // Success/Error
      if ( isSuccess ) {
        deferred.resolveWith( callbackContext, [ success, statusText, jqXHR ] );
      } else {
        deferred.rejectWith( callbackContext, [ jqXHR, statusText, error ] );
      }

      // Status-dependent callbacks
      jqXHR.statusCode( statusCode );
      statusCode = undefined;

      if ( fireGlobals ) {
        globalEventContext.trigger( isSuccess ? "ajaxSuccess" : "ajaxError",
          [ jqXHR, s, isSuccess ? success : error ] );
      }

      // Complete
      completeDeferred.fireWith( callbackContext, [ jqXHR, statusText ] );

      if ( fireGlobals ) {
        globalEventContext.trigger( "ajaxComplete", [ jqXHR, s ] );

        // Handle the global AJAX counter
        if ( !( --jQuery.active ) ) {
          jQuery.event.trigger( "ajaxStop" );
        }
      }
    }

    return jqXHR;
  },

  getJSON: function( url, data, callback ) {
    return jQuery.get( url, data, callback, "json" );
  },

  getScript: function( url, callback ) {
    return jQuery.get( url, undefined, callback, "script" );
  }
} );

jQuery.each( [ "get", "post" ], function( i, method ) {
  jQuery[ method ] = function( url, data, callback, type ) {

    // Shift arguments if data argument was omitted
    if ( jQuery.isFunction( data ) ) {
      type = type || callback;
      callback = data;
      data = undefined;
    }

    // The url can be an options object (which then must have .url)
    return jQuery.ajax( jQuery.extend( {
      url: url,
      type: method,
      dataType: type,
      data: data,
      success: callback
    }, jQuery.isPlainObject( url ) && url ) );
  };
} );


jQuery._evalUrl = function( url ) {
  return jQuery.ajax( {
    url: url,

    // Make this explicit, since user can override this through ajaxSetup (#11264)
    type: "GET",
    dataType: "script",
    async: false,
    global: false,
    "throws": true
  } );
};


jQuery.fn.extend( {
  wrapAll: function( html ) {
    var wrap;

    if ( jQuery.isFunction( html ) ) {
      return this.each( function( i ) {
        jQuery( this ).wrapAll( html.call( this, i ) );
      } );
    }

    if ( this[ 0 ] ) {

      // The elements to wrap the target around
      wrap = jQuery( html, this[ 0 ].ownerDocument ).eq( 0 ).clone( true );

      if ( this[ 0 ].parentNode ) {
        wrap.insertBefore( this[ 0 ] );
      }

      wrap.map( function() {
        var elem = this;

        while ( elem.firstElementChild ) {
          elem = elem.firstElementChild;
        }

        return elem;
      } ).append( this );
    }

    return this;
  },

  wrapInner: function( html ) {
    if ( jQuery.isFunction( html ) ) {
      return this.each( function( i ) {
        jQuery( this ).wrapInner( html.call( this, i ) );
      } );
    }

    return this.each( function() {
      var self = jQuery( this ),
        contents = self.contents();

      if ( contents.length ) {
        contents.wrapAll( html );

      } else {
        self.append( html );
      }
    } );
  },

  wrap: function( html ) {
    var isFunction = jQuery.isFunction( html );

    return this.each( function( i ) {
      jQuery( this ).wrapAll( isFunction ? html.call( this, i ) : html );
    } );
  },

  unwrap: function() {
    return this.parent().each( function() {
      if ( !jQuery.nodeName( this, "body" ) ) {
        jQuery( this ).replaceWith( this.childNodes );
      }
    } ).end();
  }
} );


jQuery.expr.filters.hidden = function( elem ) {
  return !jQuery.expr.filters.visible( elem );
};
jQuery.expr.filters.visible = function( elem ) {

  // Support: Opera <= 12.12
  // Opera reports offsetWidths and offsetHeights less than zero on some elements
  // Use OR instead of AND as the element is not visible if either is true
  // See tickets #10406 and #13132
  return elem.offsetWidth > 0 || elem.offsetHeight > 0 || elem.getClientRects().length > 0;
};




var r20 = /%20/g,
  rbracket = /\[\]$/,
  rCRLF = /\r?\n/g,
  rsubmitterTypes = /^(?:submit|button|image|reset|file)$/i,
  rsubmittable = /^(?:input|select|textarea|keygen)/i;

function buildParams( prefix, obj, traditional, add ) {
  var name;

  if ( jQuery.isArray( obj ) ) {

    // Serialize array item.
    jQuery.each( obj, function( i, v ) {
      if ( traditional || rbracket.test( prefix ) ) {

        // Treat each array item as a scalar.
        add( prefix, v );

      } else {

        // Item is non-scalar (array or object), encode its numeric index.
        buildParams(
          prefix + "[" + ( typeof v === "object" && v != null ? i : "" ) + "]",
          v,
          traditional,
          add
        );
      }
    } );

  } else if ( !traditional && jQuery.type( obj ) === "object" ) {

    // Serialize object item.
    for ( name in obj ) {
      buildParams( prefix + "[" + name + "]", obj[ name ], traditional, add );
    }

  } else {

    // Serialize scalar item.
    add( prefix, obj );
  }
}

// Serialize an array of form elements or a set of
// key/values into a query string
jQuery.param = function( a, traditional ) {
  var prefix,
    s = [],
    add = function( key, value ) {

      // If value is a function, invoke it and return its value
      value = jQuery.isFunction( value ) ? value() : ( value == null ? "" : value );
      s[ s.length ] = encodeURIComponent( key ) + "=" + encodeURIComponent( value );
    };

  // Set traditional to true for jQuery <= 1.3.2 behavior.
  if ( traditional === undefined ) {
    traditional = jQuery.ajaxSettings && jQuery.ajaxSettings.traditional;
  }

  // If an array was passed in, assume that it is an array of form elements.
  if ( jQuery.isArray( a ) || ( a.jquery && !jQuery.isPlainObject( a ) ) ) {

    // Serialize the form elements
    jQuery.each( a, function() {
      add( this.name, this.value );
    } );

  } else {

    // If traditional, encode the "old" way (the way 1.3.2 or older
    // did it), otherwise encode params recursively.
    for ( prefix in a ) {
      buildParams( prefix, a[ prefix ], traditional, add );
    }
  }

  // Return the resulting serialization
  return s.join( "&" ).replace( r20, "+" );
};

jQuery.fn.extend( {
  serialize: function() {
    return jQuery.param( this.serializeArray() );
  },
  serializeArray: function() {
    return this.map( function() {

      // Can add propHook for "elements" to filter or add form elements
      var elements = jQuery.prop( this, "elements" );
      return elements ? jQuery.makeArray( elements ) : this;
    } )
    .filter( function() {
      var type = this.type;

      // Use .is( ":disabled" ) so that fieldset[disabled] works
      return this.name && !jQuery( this ).is( ":disabled" ) &&
        rsubmittable.test( this.nodeName ) && !rsubmitterTypes.test( type ) &&
        ( this.checked || !rcheckableType.test( type ) );
    } )
    .map( function( i, elem ) {
      var val = jQuery( this ).val();

      return val == null ?
        null :
        jQuery.isArray( val ) ?
          jQuery.map( val, function( val ) {
            return { name: elem.name, value: val.replace( rCRLF, "\r\n" ) };
          } ) :
          { name: elem.name, value: val.replace( rCRLF, "\r\n" ) };
    } ).get();
  }
} );


jQuery.ajaxSettings.xhr = function() {
  try {
    return new window.XMLHttpRequest();
  } catch ( e ) {}
};

var xhrSuccessStatus = {

    // File protocol always yields status code 0, assume 200
    0: 200,

    // Support: IE9
    // #1450: sometimes IE returns 1223 when it should be 204
    1223: 204
  },
  xhrSupported = jQuery.ajaxSettings.xhr();

support.cors = !!xhrSupported && ( "withCredentials" in xhrSupported );
support.ajax = xhrSupported = !!xhrSupported;

jQuery.ajaxTransport( function( options ) {
  var callback, errorCallback;

  // Cross domain only allowed if supported through XMLHttpRequest
  if ( support.cors || xhrSupported && !options.crossDomain ) {
    return {
      send: function( headers, complete ) {
        var i,
          xhr = options.xhr();

        xhr.open(
          options.type,
          options.url,
          options.async,
          options.username,
          options.password
        );

        // Apply custom fields if provided
        if ( options.xhrFields ) {
          for ( i in options.xhrFields ) {
            xhr[ i ] = options.xhrFields[ i ];
          }
        }

        // Override mime type if needed
        if ( options.mimeType && xhr.overrideMimeType ) {
          xhr.overrideMimeType( options.mimeType );
        }

        // X-Requested-With header
        // For cross-domain requests, seeing as conditions for a preflight are
        // akin to a jigsaw puzzle, we simply never set it to be sure.
        // (it can always be set on a per-request basis or even using ajaxSetup)
        // For same-domain requests, won't change header if already provided.
        if ( !options.crossDomain && !headers[ "X-Requested-With" ] ) {
          headers[ "X-Requested-With" ] = "XMLHttpRequest";
        }

        // Set headers
        for ( i in headers ) {
          xhr.setRequestHeader( i, headers[ i ] );
        }

        // Callback
        callback = function( type ) {
          return function() {
            if ( callback ) {
              callback = errorCallback = xhr.onload =
                xhr.onerror = xhr.onabort = xhr.onreadystatechange = null;

              if ( type === "abort" ) {
                xhr.abort();
              } else if ( type === "error" ) {

                // Support: IE9
                // On a manual native abort, IE9 throws
                // errors on any property access that is not readyState
                if ( typeof xhr.status !== "number" ) {
                  complete( 0, "error" );
                } else {
                  complete(

                    // File: protocol always yields status 0; see #8605, #14207
                    xhr.status,
                    xhr.statusText
                  );
                }
              } else {
                complete(
                  xhrSuccessStatus[ xhr.status ] || xhr.status,
                  xhr.statusText,

                  // Support: IE9 only
                  // IE9 has no XHR2 but throws on binary (trac-11426)
                  // For XHR2 non-text, let the caller handle it (gh-2498)
                  ( xhr.responseType || "text" ) !== "text"  ||
                  typeof xhr.responseText !== "string" ?
                    { binary: xhr.response } :
                    { text: xhr.responseText },
                  xhr.getAllResponseHeaders()
                );
              }
            }
          };
        };

        // Listen to events
        xhr.onload = callback();
        errorCallback = xhr.onerror = callback( "error" );

        // Support: IE9
        // Use onreadystatechange to replace onabort
        // to handle uncaught aborts
        if ( xhr.onabort !== undefined ) {
          xhr.onabort = errorCallback;
        } else {
          xhr.onreadystatechange = function() {

            // Check readyState before timeout as it changes
            if ( xhr.readyState === 4 ) {

              // Allow onerror to be called first,
              // but that will not handle a native abort
              // Also, save errorCallback to a variable
              // as xhr.onerror cannot be accessed
              window.setTimeout( function() {
                if ( callback ) {
                  errorCallback();
                }
              } );
            }
          };
        }

        // Create the abort callback
        callback = callback( "abort" );

        try {

          // Do send the request (this may raise an exception)
          xhr.send( options.hasContent && options.data || null );
        } catch ( e ) {

          // #14683: Only rethrow if this hasn't been notified as an error yet
          if ( callback ) {
            throw e;
          }
        }
      },

      abort: function() {
        if ( callback ) {
          callback();
        }
      }
    };
  }
} );




// Install script dataType
jQuery.ajaxSetup( {
  accepts: {
    script: "text/javascript, application/javascript, " +
      "application/ecmascript, application/x-ecmascript"
  },
  contents: {
    script: /\b(?:java|ecma)script\b/
  },
  converters: {
    "text script": function( text ) {
      jQuery.globalEval( text );
      return text;
    }
  }
} );

// Handle cache's special case and crossDomain
jQuery.ajaxPrefilter( "script", function( s ) {
  if ( s.cache === undefined ) {
    s.cache = false;
  }
  if ( s.crossDomain ) {
    s.type = "GET";
  }
} );

// Bind script tag hack transport
jQuery.ajaxTransport( "script", function( s ) {

  // This transport only deals with cross domain requests
  if ( s.crossDomain ) {
    var script, callback;
    return {
      send: function( _, complete ) {
        script = jQuery( "<script>" ).prop( {
          charset: s.scriptCharset,
          src: s.url
        } ).on(
          "load error",
          callback = function( evt ) {
            script.remove();
            callback = null;
            if ( evt ) {
              complete( evt.type === "error" ? 404 : 200, evt.type );
            }
          }
        );

        // Use native DOM manipulation to avoid our domManip AJAX trickery
        document.head.appendChild( script[ 0 ] );
      },
      abort: function() {
        if ( callback ) {
          callback();
        }
      }
    };
  }
} );




var oldCallbacks = [],
  rjsonp = /(=)\?(?=&|$)|\?\?/;

// Default jsonp settings
jQuery.ajaxSetup( {
  jsonp: "callback",
  jsonpCallback: function() {
    var callback = oldCallbacks.pop() || ( jQuery.expando + "_" + ( nonce++ ) );
    this[ callback ] = true;
    return callback;
  }
} );

// Detect, normalize options and install callbacks for jsonp requests
jQuery.ajaxPrefilter( "json jsonp", function( s, originalSettings, jqXHR ) {

  var callbackName, overwritten, responseContainer,
    jsonProp = s.jsonp !== false && ( rjsonp.test( s.url ) ?
      "url" :
      typeof s.data === "string" &&
        ( s.contentType || "" )
          .indexOf( "application/x-www-form-urlencoded" ) === 0 &&
        rjsonp.test( s.data ) && "data"
    );

  // Handle iff the expected data type is "jsonp" or we have a parameter to set
  if ( jsonProp || s.dataTypes[ 0 ] === "jsonp" ) {

    // Get callback name, remembering preexisting value associated with it
    callbackName = s.jsonpCallback = jQuery.isFunction( s.jsonpCallback ) ?
      s.jsonpCallback() :
      s.jsonpCallback;

    // Insert callback into url or form data
    if ( jsonProp ) {
      s[ jsonProp ] = s[ jsonProp ].replace( rjsonp, "$1" + callbackName );
    } else if ( s.jsonp !== false ) {
      s.url += ( rquery.test( s.url ) ? "&" : "?" ) + s.jsonp + "=" + callbackName;
    }

    // Use data converter to retrieve json after script execution
    s.converters[ "script json" ] = function() {
      if ( !responseContainer ) {
        jQuery.error( callbackName + " was not called" );
      }
      return responseContainer[ 0 ];
    };

    // Force json dataType
    s.dataTypes[ 0 ] = "json";

    // Install callback
    overwritten = window[ callbackName ];
    window[ callbackName ] = function() {
      responseContainer = arguments;
    };

    // Clean-up function (fires after converters)
    jqXHR.always( function() {

      // If previous value didn't exist - remove it
      if ( overwritten === undefined ) {
        jQuery( window ).removeProp( callbackName );

      // Otherwise restore preexisting value
      } else {
        window[ callbackName ] = overwritten;
      }

      // Save back as free
      if ( s[ callbackName ] ) {

        // Make sure that re-using the options doesn't screw things around
        s.jsonpCallback = originalSettings.jsonpCallback;

        // Save the callback name for future use
        oldCallbacks.push( callbackName );
      }

      // Call if it was a function and we have a response
      if ( responseContainer && jQuery.isFunction( overwritten ) ) {
        overwritten( responseContainer[ 0 ] );
      }

      responseContainer = overwritten = undefined;
    } );

    // Delegate to script
    return "script";
  }
} );




// Argument "data" should be string of html
// context (optional): If specified, the fragment will be created in this context,
// defaults to document
// keepScripts (optional): If true, will include scripts passed in the html string
jQuery.parseHTML = function( data, context, keepScripts ) {
  if ( !data || typeof data !== "string" ) {
    return null;
  }
  if ( typeof context === "boolean" ) {
    keepScripts = context;
    context = false;
  }
  context = context || document;

  var parsed = rsingleTag.exec( data ),
    scripts = !keepScripts && [];

  // Single tag
  if ( parsed ) {
    return [ context.createElement( parsed[ 1 ] ) ];
  }

  parsed = buildFragment( [ data ], context, scripts );

  if ( scripts && scripts.length ) {
    jQuery( scripts ).remove();
  }

  return jQuery.merge( [], parsed.childNodes );
};


// Keep a copy of the old load method
var _load = jQuery.fn.load;

/**
 * Load a url into a page
 */
jQuery.fn.load = function( url, params, callback ) {
  if ( typeof url !== "string" && _load ) {
    return _load.apply( this, arguments );
  }

  var selector, type, response,
    self = this,
    off = url.indexOf( " " );

  if ( off > -1 ) {
    selector = jQuery.trim( url.slice( off ) );
    url = url.slice( 0, off );
  }

  // If it's a function
  if ( jQuery.isFunction( params ) ) {

    // We assume that it's the callback
    callback = params;
    params = undefined;

  // Otherwise, build a param string
  } else if ( params && typeof params === "object" ) {
    type = "POST";
  }

  // If we have elements to modify, make the request
  if ( self.length > 0 ) {
    jQuery.ajax( {
      url: url,

      // If "type" variable is undefined, then "GET" method will be used.
      // Make value of this field explicit since
      // user can override it through ajaxSetup method
      type: type || "GET",
      dataType: "html",
      data: params
    } ).done( function( responseText ) {

      // Save response for use in complete callback
      response = arguments;

      self.html( selector ?

        // If a selector was specified, locate the right elements in a dummy div
        // Exclude scripts to avoid IE 'Permission Denied' errors
        jQuery( "<div>" ).append( jQuery.parseHTML( responseText ) ).find( selector ) :

        // Otherwise use the full result
        responseText );

    // If the request succeeds, this function gets "data", "status", "jqXHR"
    // but they are ignored because response was set above.
    // If it fails, this function gets "jqXHR", "status", "error"
    } ).always( callback && function( jqXHR, status ) {
      self.each( function() {
        callback.apply( this, response || [ jqXHR.responseText, status, jqXHR ] );
      } );
    } );
  }

  return this;
};




// Attach a bunch of functions for handling common AJAX events
jQuery.each( [
  "ajaxStart",
  "ajaxStop",
  "ajaxComplete",
  "ajaxError",
  "ajaxSuccess",
  "ajaxSend"
], function( i, type ) {
  jQuery.fn[ type ] = function( fn ) {
    return this.on( type, fn );
  };
} );




jQuery.expr.filters.animated = function( elem ) {
  return jQuery.grep( jQuery.timers, function( fn ) {
    return elem === fn.elem;
  } ).length;
};




/**
 * Gets a window from an element
 */
function getWindow( elem ) {
  return jQuery.isWindow( elem ) ? elem : elem.nodeType === 9 && elem.defaultView;
}

jQuery.offset = {
  setOffset: function( elem, options, i ) {
    var curPosition, curLeft, curCSSTop, curTop, curOffset, curCSSLeft, calculatePosition,
      position = jQuery.css( elem, "position" ),
      curElem = jQuery( elem ),
      props = {};

    // Set position first, in-case top/left are set even on static elem
    if ( position === "static" ) {
      elem.style.position = "relative";
    }

    curOffset = curElem.offset();
    curCSSTop = jQuery.css( elem, "top" );
    curCSSLeft = jQuery.css( elem, "left" );
    calculatePosition = ( position === "absolute" || position === "fixed" ) &&
      ( curCSSTop + curCSSLeft ).indexOf( "auto" ) > -1;

    // Need to be able to calculate position if either
    // top or left is auto and position is either absolute or fixed
    if ( calculatePosition ) {
      curPosition = curElem.position();
      curTop = curPosition.top;
      curLeft = curPosition.left;

    } else {
      curTop = parseFloat( curCSSTop ) || 0;
      curLeft = parseFloat( curCSSLeft ) || 0;
    }

    if ( jQuery.isFunction( options ) ) {

      // Use jQuery.extend here to allow modification of coordinates argument (gh-1848)
      options = options.call( elem, i, jQuery.extend( {}, curOffset ) );
    }

    if ( options.top != null ) {
      props.top = ( options.top - curOffset.top ) + curTop;
    }
    if ( options.left != null ) {
      props.left = ( options.left - curOffset.left ) + curLeft;
    }

    if ( "using" in options ) {
      options.using.call( elem, props );

    } else {
      curElem.css( props );
    }
  }
};

jQuery.fn.extend( {
  offset: function( options ) {
    if ( arguments.length ) {
      return options === undefined ?
        this :
        this.each( function( i ) {
          jQuery.offset.setOffset( this, options, i );
        } );
    }

    var docElem, win,
      elem = this[ 0 ],
      box = { top: 0, left: 0 },
      doc = elem && elem.ownerDocument;

    if ( !doc ) {
      return;
    }

    docElem = doc.documentElement;

    // Make sure it's not a disconnected DOM node
    if ( !jQuery.contains( docElem, elem ) ) {
      return box;
    }

    box = elem.getBoundingClientRect();
    win = getWindow( doc );
    return {
      top: box.top + win.pageYOffset - docElem.clientTop,
      left: box.left + win.pageXOffset - docElem.clientLeft
    };
  },

  position: function() {
    if ( !this[ 0 ] ) {
      return;
    }

    var offsetParent, offset,
      elem = this[ 0 ],
      parentOffset = { top: 0, left: 0 };

    // Fixed elements are offset from window (parentOffset = {top:0, left: 0},
    // because it is its only offset parent
    if ( jQuery.css( elem, "position" ) === "fixed" ) {

      // Assume getBoundingClientRect is there when computed position is fixed
      offset = elem.getBoundingClientRect();

    } else {

      // Get *real* offsetParent
      offsetParent = this.offsetParent();

      // Get correct offsets
      offset = this.offset();
      if ( !jQuery.nodeName( offsetParent[ 0 ], "html" ) ) {
        parentOffset = offsetParent.offset();
      }

      // Add offsetParent borders
      parentOffset.top += jQuery.css( offsetParent[ 0 ], "borderTopWidth", true );
      parentOffset.left += jQuery.css( offsetParent[ 0 ], "borderLeftWidth", true );
    }

    // Subtract parent offsets and element margins
    return {
      top: offset.top - parentOffset.top - jQuery.css( elem, "marginTop", true ),
      left: offset.left - parentOffset.left - jQuery.css( elem, "marginLeft", true )
    };
  },

  // This method will return documentElement in the following cases:
  // 1) For the element inside the iframe without offsetParent, this method will return
  //    documentElement of the parent window
  // 2) For the hidden or detached element
  // 3) For body or html element, i.e. in case of the html node - it will return itself
  //
  // but those exceptions were never presented as a real life use-cases
  // and might be considered as more preferable results.
  //
  // This logic, however, is not guaranteed and can change at any point in the future
  offsetParent: function() {
    return this.map( function() {
      var offsetParent = this.offsetParent;

      while ( offsetParent && jQuery.css( offsetParent, "position" ) === "static" ) {
        offsetParent = offsetParent.offsetParent;
      }

      return offsetParent || documentElement;
    } );
  }
} );

// Create scrollLeft and scrollTop methods
jQuery.each( { scrollLeft: "pageXOffset", scrollTop: "pageYOffset" }, function( method, prop ) {
  var top = "pageYOffset" === prop;

  jQuery.fn[ method ] = function( val ) {
    return access( this, function( elem, method, val ) {
      var win = getWindow( elem );

      if ( val === undefined ) {
        return win ? win[ prop ] : elem[ method ];
      }

      if ( win ) {
        win.scrollTo(
          !top ? val : win.pageXOffset,
          top ? val : win.pageYOffset
        );

      } else {
        elem[ method ] = val;
      }
    }, method, val, arguments.length );
  };
} );

// Support: Safari<7-8+, Chrome<37-44+
// Add the top/left cssHooks using jQuery.fn.position
// Webkit bug: https://bugs.webkit.org/show_bug.cgi?id=29084
// Blink bug: https://code.google.com/p/chromium/issues/detail?id=229280
// getComputedStyle returns percent when specified for top/left/bottom/right;
// rather than make the css module depend on the offset module, just check for it here
jQuery.each( [ "top", "left" ], function( i, prop ) {
  jQuery.cssHooks[ prop ] = addGetHookIf( support.pixelPosition,
    function( elem, computed ) {
      if ( computed ) {
        computed = curCSS( elem, prop );

        // If curCSS returns percentage, fallback to offset
        return rnumnonpx.test( computed ) ?
          jQuery( elem ).position()[ prop ] + "px" :
          computed;
      }
    }
  );
} );


// Create innerHeight, innerWidth, height, width, outerHeight and outerWidth methods
jQuery.each( { Height: "height", Width: "width" }, function( name, type ) {
  jQuery.each( { padding: "inner" + name, content: type, "": "outer" + name },
    function( defaultExtra, funcName ) {

    // Margin is only for outerHeight, outerWidth
    jQuery.fn[ funcName ] = function( margin, value ) {
      var chainable = arguments.length && ( defaultExtra || typeof margin !== "boolean" ),
        extra = defaultExtra || ( margin === true || value === true ? "margin" : "border" );

      return access( this, function( elem, type, value ) {
        var doc;

        if ( jQuery.isWindow( elem ) ) {

          // As of 5/8/2012 this will yield incorrect results for Mobile Safari, but there
          // isn't a whole lot we can do. See pull request at this URL for discussion:
          // https://github.com/jquery/jquery/pull/764
          return elem.document.documentElement[ "client" + name ];
        }

        // Get document width or height
        if ( elem.nodeType === 9 ) {
          doc = elem.documentElement;

          // Either scroll[Width/Height] or offset[Width/Height] or client[Width/Height],
          // whichever is greatest
          return Math.max(
            elem.body[ "scroll" + name ], doc[ "scroll" + name ],
            elem.body[ "offset" + name ], doc[ "offset" + name ],
            doc[ "client" + name ]
          );
        }

        return value === undefined ?

          // Get width or height on the element, requesting but not forcing parseFloat
          jQuery.css( elem, type, extra ) :

          // Set width or height on the element
          jQuery.style( elem, type, value, extra );
      }, type, chainable ? margin : undefined, chainable, null );
    };
  } );
} );


jQuery.fn.extend( {

  bind: function( types, data, fn ) {
    return this.on( types, null, data, fn );
  },
  unbind: function( types, fn ) {
    return this.off( types, null, fn );
  },

  delegate: function( selector, types, data, fn ) {
    return this.on( types, selector, data, fn );
  },
  undelegate: function( selector, types, fn ) {

    // ( namespace ) or ( selector, types [, fn] )
    return arguments.length === 1 ?
      this.off( selector, "**" ) :
      this.off( types, selector || "**", fn );
  },
  size: function() {
    return this.length;
  }
} );

jQuery.fn.andSelf = jQuery.fn.addBack;




// Register as a named AMD module, since jQuery can be concatenated with other
// files that may use define, but not via a proper concatenation script that
// understands anonymous AMD modules. A named AMD is safest and most robust
// way to register. Lowercase jquery is used because AMD module names are
// derived from file names, and jQuery is normally delivered in a lowercase
// file name. Do this after creating the global so that if an AMD module wants
// to call noConflict to hide this version of jQuery, it will work.

// Note that for maximum portability, libraries that are not jQuery should
// declare themselves as anonymous modules, and avoid setting a global if an
// AMD loader is present. jQuery is a special case. For more information, see
// https://github.com/jrburke/requirejs/wiki/Updating-existing-libraries#wiki-anon

if ( typeof define === "function" && define.amd ) {
  define( "jquery", [], function() {
    return jQuery;
  } );
}



var

  // Map over jQuery in case of overwrite
  _jQuery = window.jQuery,

  // Map over the $ in case of overwrite
  _$ = window.$;

jQuery.noConflict = function( deep ) {
  if ( window.$ === jQuery ) {
    window.$ = _$;
  }

  if ( deep && window.jQuery === jQuery ) {
    window.jQuery = _jQuery;
  }

  return jQuery;
};

// Expose jQuery and $ identifiers, even in AMD
// (#7102#comment:10, https://github.com/jquery/jquery/pull/557)
// and CommonJS for browser emulators (#13566)
if ( !noGlobal ) {
  window.jQuery = window.$ = jQuery;
}

return jQuery;
}));
//     keymaster.js
//     (c) 2011-2013 Thomas Fuchs
//     keymaster.js may be freely distributed under the MIT license.

var keymasterify = function(global){
  var k,
    _handlers = {},
    _mods = { 16: false, 18: false, 17: false, 91: false },
    _scope = 'all',
    // modifier keys
    _MODIFIERS = {
      '⇧': 16, shift: 16,
      '⌥': 18, alt: 18, option: 18,
      '⌃': 17, ctrl: 17, control: 17,
      '⌘': 91, command: 91
    },
    // special keys
    _MAP = {
      backspace: 8, tab: 9, clear: 12,
      enter: 13, 'return': 13,
      esc: 27, escape: 27, space: 32,
      left: 37, up: 38,
      right: 39, down: 40,
      del: 46, 'delete': 46,
      home: 36, end: 35,
      minus: 109, plus: 107,
      pageup: 33, pagedown: 34,
      ',': 188, '.': 190, '/': 191,
      '`': 192, '-': 189, '=': 187,
      ';': 186, '\'': 222,
      '[': 219, ']': 221, '\\': 220
    },
    code = function(x){
      return _MAP[x] || x.toUpperCase().charCodeAt(0);
    },
    _downKeys = [];

  for(k=1;k<20;k++) _MAP['f'+k] = 111+k;

  // IE doesn't support Array#indexOf, so have a simple replacement
  function index(array, item){
    var i = array.length;
    while(i--) if(array[i]===item) return i;
    return -1;
  }

  // for comparing mods before unassignment
  function compareArray(a1, a2) {
    if (a1.length != a2.length) return false;
    for (var i = 0; i < a1.length; i++) {
        if (a1[i] !== a2[i]) return false;
    }
    return true;
  }

  var modifierMap = {
      16:'shiftKey',
      18:'altKey',
      17:'ctrlKey',
      91:'metaKey'
  };
  function updateModifierKey(event) {
      for(k in _mods) _mods[k] = event[modifierMap[k]];
  };

  // handle keydown event
  function dispatch(event) {
    var key, handler, k, i, modifiersMatch, scope;
    key = event.keyCode;

    if (index(_downKeys, key) == -1) {
        _downKeys.push(key);
    }

    // if a modifier key, set the key.<modifierkeyname> property to true and return
    if(key == 93 || key == 224) key = 91; // right command on webkit, command on Gecko
    if(key in _mods) {
      _mods[key] = true;
      // 'assignKey' from inside this closure is exported to window.key
      for(k in _MODIFIERS) if(_MODIFIERS[k] == key) assignKey[k] = true;
      return;
    }
    updateModifierKey(event);

    // see if we need to ignore the keypress (filter() can can be overridden)
    // by default ignore key presses if a select, textarea, or input is focused
    if(!assignKey.filter.call(this, event)) return;

    // abort if no potentially matching shortcuts found
    if (!(key in _handlers)) return;

    scope = getScope();

    // for each potential shortcut
    for (i = 0; i < _handlers[key].length; i++) {
      handler = _handlers[key][i];

      // see if it's in the current scope
      if(handler.scope == scope || handler.scope == 'all'){
        // check if modifiers match if any
        modifiersMatch = handler.mods.length > 0;
        for(k in _mods)
          if((!_mods[k] && index(handler.mods, +k) > -1) ||
            (_mods[k] && index(handler.mods, +k) == -1)) modifiersMatch = false;
        // call the handler and stop the event if neccessary
        if((handler.mods.length == 0 && !_mods[16] && !_mods[18] && !_mods[17] && !_mods[91]) || modifiersMatch){
          if(handler.method(event, handler)===false){
            if(event.preventDefault) event.preventDefault();
              else event.returnValue = false;
            if(event.stopPropagation) event.stopPropagation();
            if(event.cancelBubble) event.cancelBubble = true;
          }
        }
      }
    }
  };

  // unset modifier keys on keyup
  function clearModifier(event){
    var key = event.keyCode, k,
        i = index(_downKeys, key);

    // remove key from _downKeys
    if (i >= 0) {
        _downKeys.splice(i, 1);
    }

    if(key == 93 || key == 224) key = 91;
    if(key in _mods) {
      _mods[key] = false;
      for(k in _MODIFIERS) if(_MODIFIERS[k] == key) assignKey[k] = false;
    }
  };

  function resetModifiers() {
    for(k in _mods) _mods[k] = false;
    for(k in _MODIFIERS) assignKey[k] = false;
  };

  // parse and assign shortcut
  function assignKey(key, scope, method){
    var keys, mods;
    keys = getKeys(key);
    if (method === undefined) {
      method = scope;
      scope = 'all';
    }

    // for each shortcut
    for (var i = 0; i < keys.length; i++) {
      // set modifier keys if any
      mods = [];
      key = keys[i].split('+');
      if (key.length > 1){
        mods = getMods(key);
        key = [key[key.length-1]];
      }
      // convert to keycode and...
      key = key[0]
      key = code(key);
      // ...store handler
      if (!(key in _handlers)) _handlers[key] = [];
      _handlers[key].push({ shortcut: keys[i], scope: scope, method: method, key: keys[i], mods: mods });
    }
  };

  // unbind all handlers for given key in current scope
  function unbindKey(key, scope) {
    var multipleKeys, keys,
      mods = [],
      i, j, obj;

    multipleKeys = getKeys(key);

    for (j = 0; j < multipleKeys.length; j++) {
      keys = multipleKeys[j].split('+');

      if (keys.length > 1) {
        mods = getMods(keys);
      }

      key = keys[keys.length - 1];
      key = code(key);

      if (scope === undefined) {
        scope = getScope();
      }
      if (!_handlers[key]) {
        return;
      }
      for (i = 0; i < _handlers[key].length; i++) {
        obj = _handlers[key][i];
        // only clear handlers if correct scope and mods match
        if (obj.scope === scope && compareArray(obj.mods, mods)) {
          _handlers[key][i] = {};
        }
      }
    }
  };

  // Returns true if the key with code 'keyCode' is currently down
  // Converts strings into key codes.
  function isPressed(keyCode) {
      if (typeof(keyCode)=='string') {
        keyCode = code(keyCode);
      }
      return index(_downKeys, keyCode) != -1;
  }

  function getPressedKeyCodes() {
      return _downKeys.slice(0);
  }

  function filter(event){
    var tagName = (event.target || event.srcElement).tagName;
    // ignore keypressed in any elements that support keyboard data input
    return !(tagName == 'INPUT' || tagName == 'SELECT' || tagName == 'TEXTAREA');
  }

  // initialize key.<modifier> to false
  for(k in _MODIFIERS) assignKey[k] = false;

  // set current scope (default 'all')
  function setScope(scope){ _scope = scope || 'all' };
  function getScope(){ return _scope || 'all' };

  // delete all handlers for a given scope
  function deleteScope(scope){
    var key, handlers, i;

    for (key in _handlers) {
      handlers = _handlers[key];
      for (i = 0; i < handlers.length; ) {
        if (handlers[i].scope === scope) handlers.splice(i, 1);
        else i++;
      }
    }
  };

  // abstract key logic for assign and unassign
  function getKeys(key) {
    var keys;
    key = key.replace(/\s/g, '');
    keys = key.split(',');
    if ((keys[keys.length - 1]) == '') {
      keys[keys.length - 2] += ',';
    }
    return keys;
  }

  // abstract mods logic for assign and unassign
  function getMods(key) {
    var mods = key.slice(0, key.length - 1);
    for (var mi = 0; mi < mods.length; mi++)
    mods[mi] = _MODIFIERS[mods[mi]];
    return mods;
  }

  // cross-browser events
  function addEvent(object, event, method) {
    if (object.addEventListener)
      object.addEventListener(event, method, false);
    else if(object.attachEvent)
      object.attachEvent('on'+event, function(){ method(global.event) });
  };

  // set the handlers globally on document
  addEvent(global.document, 'keydown', function(event) { dispatch(event) }); // Passing _scope to a callback to ensure it remains the same by execution. Fixes #48
  addEvent(global.document, 'keyup', clearModifier);

  // reset modifiers to false whenever the window is (re)focused.
  addEvent(global, 'focus', resetModifiers);

  // store previously defined key
  var previousKey = global.key;

  // restore previously defined key and return reference to our key object
  function noConflict() {
    var k = global.key;
    global.key = previousKey;
    return k;
  }

  // set window.key and window.key.set/get/deleteScope, and the default filter
  global.key = assignKey;
  global.key.setScope = setScope;
  global.key.getScope = getScope;
  global.key.deleteScope = deleteScope;
  global.key.filter = filter;
  global.key.isPressed = isPressed;
  global.key.getPressedKeyCodes = getPressedKeyCodes;
  global.key.noConflict = noConflict;
  global.key.unbind = unbindKey;

  if(typeof module !== 'undefined') module.exports = assignKey;

}
/*! modernizr 3.3.1 (Custom Build) | MIT *
 * http://modernizr.com/download/?-flash-setclasses !*/
!function(e,n,t){function o(e,n){return typeof e===n}function i(){var e,n,t,i,a,s,r;for(var l in f)if(f.hasOwnProperty(l)){if(e=[],n=f[l],n.name&&(e.push(n.name.toLowerCase()),n.options&&n.options.aliases&&n.options.aliases.length))for(t=0;t<n.options.aliases.length;t++)e.push(n.options.aliases[t].toLowerCase());for(i=o(n.fn,"function")?n.fn():n.fn,a=0;a<e.length;a++)s=e[a],r=s.split("."),1===r.length?Modernizr[r[0]]=i:(!Modernizr[r[0]]||Modernizr[r[0]]instanceof Boolean||(Modernizr[r[0]]=new Boolean(Modernizr[r[0]])),Modernizr[r[0]][r[1]]=i),c.push((i?"":"no-")+r.join("-"))}}function a(e){var n=p.className,t=Modernizr._config.classPrefix||"";if(h&&(n=n.baseVal),Modernizr._config.enableJSClass){var o=new RegExp("(^|\\s)"+t+"no-js(\\s|$)");n=n.replace(o,"$1"+t+"js$2")}Modernizr._config.enableClasses&&(n+=" "+t+e.join(" "+t),h?p.className.baseVal=n:p.className=n)}function s(){return"function"!=typeof n.createElement?n.createElement(arguments[0]):h?n.createElementNS.call(n,"http://www.w3.org/2000/svg",arguments[0]):n.createElement.apply(n,arguments)}function r(){var e=n.body;return e||(e=s(h?"svg":"body"),e.fake=!0),e}function l(e,n){if("object"==typeof e)for(var t in e)d(e,t)&&l(t,e[t]);else{e=e.toLowerCase();var o=e.split("."),i=Modernizr[o[0]];if(2==o.length&&(i=i[o[1]]),"undefined"!=typeof i)return Modernizr;n="function"==typeof n?n():n,1==o.length?Modernizr[o[0]]=n:(!Modernizr[o[0]]||Modernizr[o[0]]instanceof Boolean||(Modernizr[o[0]]=new Boolean(Modernizr[o[0]])),Modernizr[o[0]][o[1]]=n),a([(n&&0!=n?"":"no-")+o.join("-")]),Modernizr._trigger(e,n)}return Modernizr}var c=[],f=[],u={_version:"3.3.1",_config:{classPrefix:"",enableClasses:!0,enableJSClass:!0,usePrefixes:!0},_q:[],on:function(e,n){var t=this;setTimeout(function(){n(t[e])},0)},addTest:function(e,n,t){f.push({name:e,fn:n,options:t})},addAsyncTest:function(e){f.push({name:null,fn:e})}},Modernizr=function(){};Modernizr.prototype=u,Modernizr=new Modernizr;var d,p=n.documentElement,h="svg"===p.nodeName.toLowerCase();!function(){var e={}.hasOwnProperty;d=o(e,"undefined")||o(e.call,"undefined")?function(e,n){return n in e&&o(e.constructor.prototype[n],"undefined")}:function(n,t){return e.call(n,t)}}(),u._l={},u.on=function(e,n){this._l[e]||(this._l[e]=[]),this._l[e].push(n),Modernizr.hasOwnProperty(e)&&setTimeout(function(){Modernizr._trigger(e,Modernizr[e])},0)},u._trigger=function(e,n){if(this._l[e]){var t=this._l[e];setTimeout(function(){var e,o;for(e=0;e<t.length;e++)(o=t[e])(n)},0),delete this._l[e]}},Modernizr._q.push(function(){u.addTest=l}),Modernizr.addAsyncTest(function(){var t,o,i=function(e){p.contains(e)||p.appendChild(e)},a=function(e){e.fake&&e.parentNode&&e.parentNode.removeChild(e)},c=function(e,n){var t=!!e;if(t&&(t=new Boolean(t),t.blocked="blocked"===e),l("flash",function(){return t}),n&&g.contains(n)){for(;n.parentNode!==g;)n=n.parentNode;g.removeChild(n)}};try{o="ActiveXObject"in e&&"Pan"in new e.ActiveXObject("ShockwaveFlash.ShockwaveFlash")}catch(f){}if(t=!("plugins"in navigator&&"Shockwave Flash"in navigator.plugins||o),t||h)c(!1);else{var u,d,v=s("embed"),g=r();if(v.type="application/x-shockwave-flash",g.appendChild(v),!("Pan"in v||o))return i(g),c("blocked",v),void a(g);u=function(){return i(g),p.contains(g)?(p.contains(v)?(d=v.style.cssText,""!==d?c("blocked",v):c(!0,v)):c("blocked"),void a(g)):(g=n.body||g,v=s("embed"),v.type="application/x-shockwave-flash",g.appendChild(v),setTimeout(u,1e3))},setTimeout(u,10)}}),i(),a(c),delete u.addTest,delete u.addAsyncTest;for(var v=0;v<Modernizr._q.length;v++)Modernizr._q[v]();e.Modernizr=Modernizr}(window,document);
//;( function() {
// Main class
window.WhiteChili = function( editor ) {
  if ( editor ) {
    // activate white chili by referencing the editor
    WhiteChili.ci.editor = editor

    // register events
    new WhiteChili.Events

    // register shortcuts
    WhiteChili.ci.shortcut = new WhiteChili.Shortcuts

  } else {
    console.error( 'WhiteChili: FATAL ERROR ( Editor object not received )' )
  }
}

// Namespaces
WhiteChili.Models = {}
WhiteChili.Traits = {}
WhiteChili.Collections = {}
// Add maps module
WhiteChili.map = {
  // Zoom steps
  zooms:         [ 5, 10, 15, 20, 40, 60, 80, 100, 150, 200, 250, 300, 350, 400, 500 ]
, cropper_zooms: [ 100, 125, 150, 200, 250, 300, 350, 400, 450, 500, 600, 700, 800, 900, 1000 ]
  
  // Stroke edge mapping
, edges: {
    straight: {
      caps: 'none'
    , join: 'miter'
    }
  , round: {
      caps: 'round'
    , join: 'round'
    }
  , bevel: {
      caps: 'none'
    , join: 'bevel'
    }
  }
  
  // Page labels per page counts
, pages: {
    two:  [ 'Recto', 'Verso' ]
  , many: 'Pagina {{number}}'
  }

  // Stroke join and caps mapping
, capjoin: {
    miter: 'straight'
  , none:  'straight'
  , round: 'round'
  , bevel: 'bevel'
  }

  // Align settings
, align: [
    'left'
  , 'center'
  , 'right'
  , 'justify'
  ]

  // Settings map for each frame type
, settings: {
    all:        [ 'menu:fill', 'menu:stroke', 'menu:color', 'menu:align', 'menu:font', 'menu:paragraph', 'menu:size', 'done', 'replace', 'background:set', 'background:unset', 'menu:crop', 'crop', 'cropper', 'menu:organize', 'destroy', 'info' ]
  , rectangle:  [ 'menu:fill', 'menu:stroke',                                                                                                                                                                  'menu:organize', 'destroy'         ]
  , text:       [                             'menu:color', 'menu:align', 'menu:font', 'menu:paragraph', 'menu:size',                                                                                          'menu:organize', 'destroy'         ]
  , image:      [                                                                                                             'replace', 'background:set',                     'menu:crop', 'crop', 'cropper', 'menu:organize', 'destroy'         ]
  , shape:      [ 'menu:fill', 'menu:stroke',                                                                                                                                                                  'menu:organize', 'destroy'         ]
  , snippet:    [                                                                                                                                                                                              'menu:organize', 'destroy'         ]
  , video:      [                                                                                                                                                                                              'menu:organize', 'destroy'         ]
  , circle:     [ 'menu:fill', 'menu:stroke',                                                                                                                                                                  'menu:organize', 'destroy'         ]
  , line:       [              'menu:stroke',                                                                                                                                                                  'menu:organize', 'destroy'         ]
  , ad:         [                                                                                                                                                                                              'menu:organize', 'destroy'         ]
  , barcode:    [ 'menu:fill',                                                                                                                                                                                 'menu:organize', 'destroy'         ]
  , table:      [ 'menu:fill', 'menu:stroke',                                                                                                                                                                  'menu:organize', 'destroy'         ]
  , format:     [                             'menu:color', 'menu:align', 'menu:font',                   'menu:size', 'done',                                                                                                   'destroy'         ]
  , background: [                                                                                                             'replace',                   'background:unset', 'menu:crop',                                     'destroy'         ]
  }
  
  // Frame types
, frames: [
    'rectangle'
  , 'text'
  , 'image'
  , 'shape'
  , 'snippet'
  , 'video'
  , 'circle'
  , 'line'
  , 'ad'
  , 'barcode'
  , 'table'
  ]

  // Fillable frames
, fillable: [
    'rectangle'
  , 'shape'
  , 'circle'
  , 'line'
  ]

  // Text frames
, textable: [
    'text'
  ]

, // Fit modes with anchor option
  anchorable: [
    'proportional_outside'
  , 'proportional'
  ]
  // Cursor types

, cursors: [
    'pointer'
  , 'hand'
  , 'zoom'
  , 'selection'
  , 'text'
  , 'chain'
  , 'unchain'
  ]

  // Attributes that should be casted (for WhiteChili xml api)
, castable: {
    gradientColor1: { model: 'Color', root: 'document colors item' }
  , gradientColor2: { model: 'Color', root: 'document colors item' }
  , color:          { model: 'Color', root: 'document colors item' }
  }

  // Attributes that should not be type casted
, stale: [
    'chiliVersion'
  , 'javaScriptDescriptor'
  , 'name'
  , 'pageNum'
  ]

  // Render engines
, engines: [
    '-webkit-'
  , '-moz-'
  , '-ms-'
  , '-o-'
  , ''
  ]

  // Throttle delays
, throttle: {
    text:  200
  , other: 50
  }

, xmlFlow: '<TextFlow xmlns="http://ns.adobe.com/textLayout/2008" whiteSpaceCollapse="preserve"><p><span>{{text}}</span></p></TextFlow>'
}
// Chili Interface module
WhiteChili.ci = {

  // create status object
  status: {
    env:            'development' // app environment
  , page:           0             // page index (not name!!)
  , loader:         0             // loader progress
  , moving:         false         // frame moving status
  , stats:          {}            // statistics store
  , update:         {}            // temporary storage to detect change for settings
  , has_settings:   true          // register settings
  , has_help_layer: true          // register help layer
  }

  // cache store for multiple applications
, cache: {
    api:   {} // XML api selector to object cache
  , mixed: {} // storage for arbitrary data
  }

  // create css array
, css: []

  // Create xml parser
, parser: document.getElementById( 'white_chili_parser' )
  
  // Perform Chili query
, query: function( query ) {
    return this.editor.GetObject( query )
  }

  // Perform api query (replacement for chili query)
, get: function( selector, raw ) {
    return this.xml( raw ).find( selector )
  }
  
  // Execute Chili command
, execute: function() {
    return this.editor.ExecuteFunction.apply( this.editor, arguments )
  }

  // Set property on chili frame
, property: function( query, property, value ) {
    // if an object is given, distribute method as individual calls
    if ( typeof property == 'object' )
      for ( p in property ) this.property( query, p, property[p] )

    // apply new property values
    else
      this.editor.SetProperty( query, property, value.toString() )

    return this
  }

  // Log message
, log: function( message, type ) { // message:string, type:string[log|warn|error]
    if ( WhiteChili.ci.config.env != 'production' )
      console[ type || 'log' ]( 'WhiteChili: ' + message )
    
    return this
  }

  // Benchmark the execution of a given function
, bench: function( closure, output ) {
    var start = ( new Date ).getTime()
      , message

    // perform actions
    closure()

    // build message
    message = 'Executed in ' + ( ( new Date ).getTime() - start ) + 'ms'

    // print to console as well
    if ( output ) console.log( message )

    return message
  }

  // Keep statistics
, stats: function( namespace, instance ) {
    if ( WhiteChili.ci.config.env != 'production' ) {
      if ( ! this.status.stats[namespace] )
        this.status.stats[namespace] = {}

      if ( ! this.status.stats[namespace][instance] )
        this.status.stats[namespace][instance] = 0

      this.status.stats[namespace][instance]++      
    }
  }

  // Get the document
, document: function() {
    if ( ! this.cache.mixed.doc )
      this.cache.mixed.doc = new WhiteChili.Document
    
    return this.cache.mixed.doc
  }

  // Get the document (shorter version)
, doc: function() {
    return this.document()
  }
  
  // Get xml api (jQuery object)
, api: function() {
    if ( ! this.cache.mixed.api )
      this.cache.mixed.api = new WhiteChili.Models.Api
    
    return this.cache.mixed.api
  }

  // Get the xml DOM (returns jQuery object)
, xml: function( regenerate ) {
    if ( regenerate === true || ! this.cache.xml )
      this.cache.xml = $( $.parseXML( this.execute( 'document','GetTempXML' ) ) )
    
    return this.cache.xml
  }

  // Regenerate cache for dynamic objects
, recache: function() {
    if ( ! this.status.has_settings && this.api().importantPreflightResults().empty() ) {
      this.log( 'No XML recache required' )

      return false
    }

    // clear api DOM cache
    this.cache.api = {}

    // get new xml document
    this.cache.xml = this.xml( true )

    // perform live preflight
    if ( this.config.enable_live_preflight )
      this.controller.perform( 'live_preflight' )

    this.log( 'Recached XML document' )

    return this
  }

}
;( function( ci, regex ) {

  WhiteChili.i18n = {
    // Get locale
    t: function( key, values ) {
      var locale = ci.config.locales[key]
        , absolute

      // objectify values
      if ( typeof values === 'boolean' )
        absolute = values

      // interpolate locales
      if ( typeof values === 'object'  ) {
        var name

        // pluck absolute value
        absolute = !! values.absolute
        delete values.absolute

        // interpolate string
        if ( locale )
          for ( name in values )
            locale = locale.replace( new RegExp( '\{\{' + name + '\}\}' ), values[name] )
      }

      // add version
      if ( locale ) 
        locale = locale.replace( '{{version}}', ci.config.version )

      return locale || ( absolute ? false : 't(' + key + ')' )
    }

  }

})( WhiteChili.ci, WhiteChili.regex )
// Add regex module
WhiteChili.regex = {
  // number with possible unit
  number:         /^(-?[\d\.]+)\s?([a-z%]{0,2})$/

  // integer value
, integer:        /^(-?\d+)$/

  // camel case conversion from slug
, camel:          /-+(.)?/g

  // slug conversion from snake
, slug:           /_/g

  // chili publish color object
, cp_color:       /cp_object\:document\.colors\[/

  // chili publish font object
, cp_font:        /cp_object\:document\.fonts\[/
  
  // chili publish object
, cp_object:      /cp_object\:\[/
  
  // non-chili color
, defined_color:  /^[^\[]/
  
  // uuid matcher
, uuid:           /[0-9A-F]{8}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{12}/i
  
  // frame action pattern
, frame_action:   /white_chili\.([a-z0-9_]+)(\.([a-z0-9_]+))?/i

  // Help layer name
, help_layer:     /^layer\.help\.[a-z]{2}$/
  
  // I18n interpolations
, interpolations: /\{\{[^\}]+\}\}/

  // Test file name
, file_name:      /\w{1,}\.\w{2,}/
  
  // Replace http(s)
, http:           /^https?:/
  
  // test temporary document name
, tmp_name:       /[a-z0-9\-]+_tmp_[\d]+/i

  // default name on Chili resources
, default_name:   /^\[default\]$/i

  // check if a url string has params
, has_params:     /\w+\?[^=]+/
  
  // check if file name is web safe image
, is_image:       /\.(jpe?g|png|gif)$/i
  
  // check if a variable name is destined to be an option
, is_option:      /^white_chili_option_\d+$/
  
}
;( function( ci, map, regex ) {

  // Add utilities module
  WhiteChili.utils = {
    // Extend a given prototype
    extend: function( target, source ) {
      for ( var method in source )
        target.prototype[method] = source[method]

      return target
    }

    // Type cast attribute values
  , cast: function( value, key, api ) { // value:string|number|boolean
      // prevent some attributes from being converted
      if ( map.stale.indexOf( key ) > -1 )
        return value

      // cast strings
      if ( typeof value === 'string' ) {
        // convert to boolean
        if ( value === 'true' )
          return true
        else if ( value === 'false' )
          return false
        // ignore blank values as well
        else if ( value === '' )
          return null
        // convert to cursor instance
        else if ( key === 'cursor' || key === 'Cursor' )
          return new WhiteChili.Cursor
        // convert to number instance
        else if ( regex.number.test( value ) )
          return new WhiteChili.Number( value )
        // convert color instance
        else if ( regex.cp_color.test( value ) )
          return api ? new WhiteChili.Models.Color( this.p2s( value ) ) : new WhiteChili.Color( value )
        // cast model from id
        else if ( map.castable[key] )
          return this.model( map.castable[key], this.uuid( value ) )

      // cast objects
      } else if ( typeof value == 'object' && ! Array.isArray( value ) ) {
        // deep casting for objects
        for ( var key in value )
          if ( value.hasOwnProperty( key ) )
            value[key] = this.cast( value[key], key, api )
      }

      return value
    }

    // Create model form attribute
  , model: function( map, id ) {
      return new WhiteChili.Models[map.model]( map.root + '[id=' + id + ']' )
    } 

    // Closest number in numerical array
  , closest: function( array, goal ) {
      return array.reduce( function ( prev, curr ) {
        return ( Math.abs( curr - goal ) < Math.abs( prev - goal ) ? curr : prev )
      })
    }

    // Difference of two arrays
  , difference: function( a, b ) {
      var c = a.filter( function( i ) { return b.indexOf( i ) < 0 } )
        , d = b.filter( function( i ) { return a.indexOf( i ) < 0 } )

      return c.concat( d )
    }

    // Unique array
  , unique: function( a ) {
      return a.reduce( function( p, c ) {
        if ( p.indexOf( c ) < 0 ) p.push( c )
        return p
      }, [] )
    }

    // Intersect multiple arrays
  , intersection: function( arrays ) {
      if ( arrays.length == 0 )
        return []

      return arrays.shift().reduce( function( res, v ) {
        if ( res.indexOf( v ) === -1 && arrays.every( function( a ) {
          return a.indexOf( v ) !== -1
        }))

        res.push( v )
        return res
      }, [])
    }

    // Get attributes from node
  , attr: function( node ) {
      var attr = {}

      if ( node )
        for ( var i = node.attributes.length - 1; i >= 0; i-- )
          attr[node.attributes[i].nodeName] = node.attributes[i].nodeValue
      
      return attr
    }

    // Map array to selectors
  , selectors: function( array, settings ) {
      var i, s, a, pair, selector

      // ensure array is array
      if ( ! Array.isArray( array ) )
        array = [ array ]
      
      // ensure settings object
      settings = settings || {}

      // ensure default selector
      selector = settings.selector || '.menu [data-wc-action={{action}}]'

      // build selectors
      for ( i = 0, s = []; i < array.length; i++ ) {
        // detect value or action/value pair
        pair = array[i].split( ':' )

        // build selector
        a = selector.replace( '{{action}}', pair[0] )

        // add value selector as well
        if ( pair.length == 2 )
          a += ( '[data-wc-value={{value}}]' ).replace( '{{value}}', pair[1] )

        // add prefix if given
        if ( settings.prefix )
          a = settings.prefix + ' ' + a

        // add suffix if given
        if ( settings.suffix )
          a = a + ' ' + settings.suffix
        
        s.push( a )
      }

      return s.join( ', ' )
    }

    // build s3 url
  , s3: function() {
      // get parts
      var parts   = [].slice.call( arguments )
        , options = {}

      // try to find additional configuration
      if ( typeof parts[parts.length - 1] == 'object' )
        options = parts.pop()

      return ci.config.bucket + parts.join( '/' ) + '?r=' + ( options.cache_key || ci.config.cache_key )
    }

    // Build thumbnail url for page name
  , thumbFor: function( name ) {
      return ci.config.thumbs.replace( '###', name )
    }

    // Build url with params
  , urlWithParams: function( url, params ) {
      var glue = regex.has_params.test( url ) ? '&' : '?'
        , ps   = []
        , p
        
      // gather params as key/value strings
      for ( p in params )
        if ( params[p] )
          ps.push( p + '=' + params[p] )

      // glue everything together
      return url + glue + ps.join( '&' )
    }

    // Capitalize first letter
  , capital: function( string ) {
      return string.charAt( 0 ).toUpperCase() + string.slice( 1 )
    }

    // Camelize string
  , camel: function( string ) {
      return this.slug( string ).replace( regex.camel, function( match, chr ) {
        return chr ? chr.toUpperCase() : ''
      })
    }

    // Classify string
  , studly: function( string ) {
      return this.capital( this.camel( this.slug( string ) ) )
    }

    // Dasherize string
  , slug: function( string ) {
      return string.replace( regex.slug, '-' )
    }

    // Underscore string
  , snake: function( string ) {
      return string.replace( /::/g, '/' )
        .replace( /([A-Z]+)([A-Z][a-z])/g, '$1_$2' )
        .replace( /([a-z\d])([A-Z])/g, '$1_$2' )
        .replace( /-/g, '_' )
        .toLowerCase()
    }

    // WhiteChili selector to Chili path
  , s2p: function( selector ) {
      return selector
        .replace( 'document', 'cp_object:document' )
        .replace( ' > item[id=', '[' )
        .replace( ' > ', '.' )
    }

    // Chili path to WhiteChili selector
  , p2s: function( path ) {
      return path
        .replace( /^cp_object\:/, '' )
        .replace( /\./g, ' > ' )
        .replace( /\[/g, ' > item[id=' )
    }
    
    // Convert pixels to millimetres
  , px2mm: function( pixels, dpi ) {
      return ( pixels * 25.4 ) / ( dpi || 72 )
    }

    // Parse resource id
  , uuid: function( value ) {
      var match = value.match( regex.uuid )

      return match ? match[0] : value
    }

    // Convert millimetres to pixels
  , mm2px: function( mm, dpi ) {
      return ( mm * ( dpi || 72 ) ) / 25.4
    }

    // Rotate geo object from portrait to landscape clockwise
  , portland: function( geo, orientation ) {
      var o = ci.cache.orientation

      // calculate rotation
      if ( orientation != o.original ) {
        var rotated = {
          x:        o.height - geo.height - geo.y
        , y:        geo.x
        , width:    geo.height
        , height:   geo.width
        }

        // add layer and fit mode if present
        if ( geo.layer )
          rotated.layer = geo.layer
        if ( geo.fitMode )
          rotated.fitMode = geo.fitMode

        geo = rotated
      }
      
      return geo
    }
    
    // Pluralize string
  , plural: function( string, revert ) {
      // prepare variables
      var plural, singular, irregular, uncountable, word, pattern, replace, array, reg

      // plural rules
      plural = {
        '(quiz)$'                       : '$1zes'
      , '^(ox)$'                        : '$1en'
      , '([m|l])ouse$'                  : '$1ice'
      , '(matr|vert|ind)ix|ex$'         : '$1ices'
      , '(x|ch|ss|sh)$'                 : '$1es'
      , '([^aeiouy]|qu)y$'              : '$1ies'
      , '(hive)$'                       : '$1s'
      , '(?:([^f])fe|([lr])f)$'         : '$1$2ves'
      , '(shea|lea|loa|thie)f$'         : '$1ves'
      , 'sis$'                          : 'ses'
      , '([ti])um$'                     : '$1a'
      , '(tomat|potat|ech|her|vet)o$'   : '$1oes'
      , '(bu)s$'                        : '$1ses'
      , '(alias)$'                      : '$1es'
      , '(octop)us$'                    : '$1i'
      , '(ax|test)is$'                  : '$1es'
      , '(us)$'                         : '$1es'
      , '^$'                            : ''
      , '$'                             : 's'
      }

      // singular rules
      singular = {
        '(quiz)zes$'                    : '$1'
      , '(matr)ices$'                   : '$1ix'
      , '(vert|ind)ices$'               : '$1ex'
      , '^(ox)en$'                      : '$1'
      , '(alias)es$'                    : '$1'
      , '(octop|vir)i$'                 : '$1us'
      , '(cris|ax|test)es$'             : '$1is'
      , '(shoe)s$'                      : '$1'
      , '(o)es$'                        : '$1'
      , '(bus)es$'                      : '$1'
      , '([m|l])ice$'                   : '$1ouse'
      , '(x|ch|ss|sh)es$'               : '$1'
      , '(m)ovies$'                     : '$1ovie'
      , '(s)eries$'                     : '$1eries'
      , '([^aeiouy]|qu)ies$'            : '$1y'
      , '([lr])ves$'                    : '$1f'
      , '(tive)s$'                      : '$1'
      , '(hive)s$'                      : '$1'
      , '(li|wi|kni)ves$'               : '$1fe'
      , '(shea|loa|lea|thie)ves$'       : '$1f'
      , '(^analy)ses$'                  : '$1sis'
      , '((a)naly|(b)a|(d)iagno|(p)arenthe|(p)rogno|(s)ynop|(t)he)ses$': '$1$2sis'
      , '([ti])a$'                      : '$1um'
      , '(n)ews$'                       : '$1ews'
      , '(h|bl)ouses$'                  : '$1ouse'
      , '(corpse)s$'                    : '$1'
      , '(us)es$'                       : '$1'
      , 's$'                            : ''
      }
  
      // irregular rules
      irregular = {
        'move'   : 'moves'
      , 'foot'   : 'feet'
      , 'goose'  : 'geese'
      , 'sex'    : 'sexes'
      , 'child'  : 'children'
      , 'man'    : 'men'
      , 'tooth'  : 'teeth'
      , 'person' : 'people'
      }
  
      // uncountable rules
      uncountable = [
        'sheep'
      , 'fish'
      , 'deer'
      , 'series'
      , 'species'
      , 'money'
      , 'rice'
      , 'information'
      , 'equipment'
      ]
  
      // save some time in the case that singular and plural are the same
      if( uncountable.indexOf( string.toLowerCase() ) >= 0 )
        return string
  
      // check for irregular forms
      for ( word in irregular ){
        if ( revert ) {
          pattern = new RegExp( irregular[word] + '$', 'i' )
          replace = word
        } else { 
          pattern = new RegExp( word + '$', 'i' )
          replace = irregular[word]
        }
  
        if ( pattern.test( string ) )
          return string.replace( pattern, replace )
      }
  
      // grab array to work with
      array = revert ? singular : plural
  
      // check for matches using regular expressions
      for ( reg in array ) {
        pattern = new RegExp( reg, 'i' )
  
        if ( pattern.test( string ) )
          return string.replace( pattern, array[reg] )
      }
  
      return string
    }

    // Singularize string
  , singular: function( string ) {
      return this.plural( string , true )
    }

  }

})( WhiteChili.ci, WhiteChili.map, WhiteChili.regex )
;( function( ci, map ) {

  var store = {}    // throttle store
    , delay = 1000  // default throttle execution delay
    , timeout

  // Event throttler
  WhiteChili.throttle = function( key, closure, d ) {
    // store delayed action
    store[key] = closure

    // clear current timeout
    if ( timeout )
      clearTimeout( timeout )

    // set new timeout
    timeout = setTimeout( function() {
      for ( var action in store )
        store[action]()

      store = {}
    }, d || map.throttle[ci.api().cursor()] || map.throttle.other || delay )
  }

  // Perform throttled commadn right now
  WhiteChili.unthrottle = function( key, closure ) {
    // perform now
    if ( store[key] ) store[key]()

    // delete closure
    delete store[key]

    // perform closure
    if ( closure ) closure()
  }

})( WhiteChili.ci, WhiteChili.map )
;( function( ci ) {

  // Add dom selector module for caching
  WhiteChili.dom = {
    // open cache store
    cache: {}

    // Perform dom query with caching
  , $: function( selector ) {
      // build event statistics
      ci.stats( 'dom', selector )
      
      // find cached selector
      if ( this.cache[selector] )
        return this.cache[selector]

      // build new cache
      return this.cache[selector] = $( selector )
    }

  }

})( WhiteChili.ci );
;( function( ci, map, utils, regex ) {
  // define lists
  var colorPalettes = [ 'fill', 'stroke', 'color' ]
    , schemeColors  = [ 'color_dark', 'color', 'color_light', 'color_lighter', 'color_lightbox' ]

  // Add builder module
  WhiteChili.build = {
    // Color palette
    colorPalettes: function() {
      colorPalettes.forEach( function( palette ) {
        $( utils.selectors( 'menu:' + palette, { suffix: '.dropdown' } ) )
          .prepend( new WhiteChili.ColorComponent().html( palette ) )
      })
    }

    // Pages sidebar
  , pagesSidebar: function() {
      var many = ci.doc().pages().many()

      // show or hide sidebar
      $( 'body' )
        .toggleClass( 'open-sidebar', many )
      
      // rebuild pages sidebar
      if ( many )
        $( '.pages-sidebar' )
          .html( new WhiteChili.PagesComponent().html() )
    }

    // Layouts dropdown
  , layoutsDropdown: function() {
      if ( ci.doc().customAlternateLayouts().size() > 1 )
        $( utils.selectors( 'menu:layouts', { suffix: '.dropdown' } ) )
          .html( new WhiteChili.LayoutsComponent().html() )
      else
        $( utils.selectors( 'menu:layouts' ) ).hide()
    }

    // Fonts dropdown
  , fontsDropdown: function() {
      $( utils.selectors( 'menu:font', { suffix: '.dropdown' } ) )
        .html( new WhiteChili.FontsComponent().html() )
    }
    
    // Paragraphs dropdown
  , paragraphsDropdown: function() {
      $( utils.selectors( 'menu:paragraph', { suffix: '.dropdown' } ) )
        .html( new WhiteChili.ParagraphsComponent().html() )
    }

    // Text sizes presets
  , textSizes: function() {
      $( utils.selectors( 'menu:size', { suffix: '.dropdown' } ) )
        .prepend( new WhiteChili.TextSize().html() )
    }

    // Zoom slider
  , zoomSlider: function() {
      var slider = new WhiteChili.ZoomComponent({
        value: function() {
          return ci.api().attr( 'zoom' )
        }
      , perform: function( zoom ) {
          ci.controller.perform( 'zoom', zoom )
        }
      , min: map.zooms[0]
      , max: map.zooms[map.zooms.length - 1]
      })

      $( '.zoom-slider' )
        .append( slider.html() )
    }

    // Build splash screen
  , splash: function() {
      new WhiteChili.Splash()

      ci.log( 'Prepared splash screen' )
    }

    // Build coach mark slider
  , coachMark: function() {
      // make sure coach mark data is given
      if ( ci.config.coach_mark ) {
        new WhiteChili.CoachMark( utils.s3 )

        ci.log( 'Built coach mark' )

      // remove coach mark wrapper if no data is given
      } else {
        $( '.coach-mark' ).remove()

        ci.log( 'No coach mark defined so no need to build one' )
      }
    }

    // Get background template data
  , backgroundTemplates: function() {
      var doc = ci.api()

      // gather background layer data
      if ( doc.layers().where( 'name', 'layer.background' ).any() ) {
        doc.pages().each( function() {
          // find background templates
          var templates = this.tempFrames()
            , key       = 'backgroundTemplate.' + this.attr( 'id' )
            , source
            
          if ( source = templates.first() ) {
            // get background template geometry
            ci.cache.mixed[key] = {
              x:      source.attr( 'x' ).valueOf()
            , y:      source.attr( 'y' ).valueOf()
            , width:  source.attr( 'width' ).valueOf()
            , height: source.attr( 'height' ).valueOf()
            }

            // make sure they are removed
            templates.destroy()
          } else {
            // get document data
            source = doc.attr()

            // get document geometry
            ci.cache.mixed[key] = {
              x:      - source.bleedLeft
            , y:      - source.bleedTop
            , width:  source.bleedLeft + source.width  + source.bleedRight
            , height: source.bleedTop  + source.height + source.bleedBottom
            }
          }

          // store fitmode
          ci.cache.mixed[key].fitMode = 'proportional_outside'
        })
        
      // remove background layer buttons 
      } else {
        $( utils.selectors( 'background' ) ).remove()
      }
    }
    
    // Build css stylesheet
  , css: function() {
      var value

      // add body css class
      $( 'body' ).addClass( 'skin-' + ci.config.scheme.skin + ' skin-' + ci.config.scheme.name )

      // add checkout css class
      $( '.button.nav.primary' ).addClass( utils.slug( ci.config.scheme.checkout ) )

      // add color scheme
      schemeColors.forEach( function( color ) {
        var base = '.' + utils.slug( color )

        // background color
        ci.css.push( base + '-bg { background-color: ' + ci.config.scheme[color] + '; }' )

        // text color
        ci.css.push( ( base == '.color' ? '.text-color' : base ) + ' { color: ' + ci.config.scheme[color] + '; }' )

        // hover background color
        ci.css.push( base + '-hover-bg:hover, ' + base + '-hover-bg.active { background-color: ' + ci.config.scheme[color] + ' !important; }' )
      })

      // add logo
      if ( regex.file_name.test( ci.config.scheme.logo_file_name ) )
        ci.css.push( '.logo { background-image: url( "' + utils.s3( 'logos', ci.config.scheme.logo_file_name ) + '" ); }' )

      // add splash
      if ( regex.file_name.test( ci.config.scheme.splash_file_name ) )
        ci.css.push( '.splash .logo { background-image: url( "' + utils.s3( 'logos', ci.config.scheme.splash_file_name ) + '" ); }' )

      // add icon file
      if ( regex.file_name.test( ci.config.scheme.icon_file_name ) )
        ci.css.push( '.page, .button, .button.nav .icon { background-image: url( "' + utils.s3( 'images', ci.config.scheme.icon_file_name ) + '" ); }' )

      // add multiple colors question mark
      ci.css.push( '.button.multiple .swatch { background-image: url( "' + utils.s3( 'images', 'multiple.svg' ) + '" ); }' )

      // add processing file
      ci.css.push( '.processing .bar { background-image: url( "' + utils.s3( 'images', 'processing.svg' ) + '" ); }' )

      // add image loader
      ci.css.push( '.layer.loading, .dialog-box.loading { background-image: url( "' + utils.s3( 'images', 'loader.svg' ) + '" ); }' )
      
      // add font preview file
      if ( regex.file_name.test( ci.config.font_file ) )
        ci.css.push( '.fonts [data-wc-action] { background-image: url( "' + utils.s3( 'images', ci.config.font_file ) + '" ); }' )

      // uploader image
      ci.css.push( '.dz-drag-hover .uploader { background-image: url( "' + utils.s3( 'images', 'upload.svg' ) + '" ); }' )

      // add font preview offsets
      ci.config.fonts.forEach( function( font ) {
        // font group
        ci.css.push( '.group-' + font.item_id + ' { background-position: 0 -' + ( font.offset * 1.25 ) + 'em  }' )

        // font variant
        ci.css.push( '.variant-' + font.item_id + ' { background-position: -11.5em -' + ( font.variant.offset * 1.25 ) + 'em  }' )
      })

      // add paragraph styles
      ci.doc().paragraphStyles().each( function() {
        ci.css.push( '.paragraph-' + this.attr( 'id' ) + ' { color: ' + this.color().hex() + ' }' )
      })
      
      // create stylesheet
      $( '<style></style>' )
        .html( ci.css.join( '' ) )
        .appendTo( $( 'head' ) )

      ci.log( 'Built dynamic stylesheet' )
    }

  }

})( WhiteChili.ci, WhiteChili.map, WhiteChili.utils, WhiteChili.regex )
;( function( ci, map, dom, utils, regex, i18n, throttle ) {

  // Add enabler module
  WhiteChili.enable = {
    // Enable all action buttons
    buttons: function() {
      var frameConstraints = ci.api().frameConstraints()

      // remove all buttons that are not selected
      if ( ci.config.buttons.length > 0 ) {
        $( '.menu [data-wc-action], .menu [data-wc-slider]' )
          .addClass( 'pending' )

        ci.config.buttons.forEach( function( b ) {
          var action   = b.button_type.name == 'slider' ? 'slider' : 'action'
            , selector = '.menu [data-wc-' + action + '=' + b.action + ']'

          if ( b.value )
            selector += '[data-wc-value=' + b.value + ']'

          $( selector ).removeClass( 'pending' )
        })

        $( '.menu [data-wc-action].pending, .menu [data-wc-slider].pending' ).remove()
      }

      // remove all settings if they're locked
      if ( frameConstraints.hasLockOn( 'frameSettings' ) ) {
        $( 'header.frame-settings' ).remove()

      } else {
        // remove paragraph style button
        if ( ci.doc().paragraphStyles().one() )
          $( '.menu [data-wc-action=menu][data-wc-value=paragraph]' ).remove()

        // remove delete button
        if ( frameConstraints.hasLockOn( 'delete' ) )
          $( '.menu [data-wc-action=destroy]' ).remove()

        // remove replace button
        if ( frameConstraints.hasLockOn( 'content' ) )
          $( '.menu [data-wc-action=replace]' ).remove()
      }

      // enable buttons
      $( '.menu [data-wc-action]' )
        .addClass( 'enabled' )

      
      // activate buttons
      $( document ).on( 'click', '[data-wc-action], [data-wc-action] .bubble', function( event ) {
        // get action and value
        var action = $( this ).data( 'wc-action' )
          , value  = $( this ).data( 'wc-value' )
          , target = $( event.target )
          
        // find target or bubbling target
        if ( target.hasClass( 'bubble' ) )
          target = $( target[0] ).parent()
          
        // perform action only if clicked element carries the action
        if ( target.hasClass( 'enabled' ) && target.data( 'wc-action' ) == action )
          WhiteChili.ci.controller.perform( action, value, event )
      })

      // remove superfluous buttons
      if ( ! ci.config.remote || ! ci.config.remote.checkout )
        $( '[data-wc-action=checkout]' ).remove()
    }

    // Enable all action inputs
  , inputs: function() {
      var constraint = ci.doc().frameContentConstraints()
        , inputs = $( '[data-wc-input]' )

      // make sure inputs are required
      inputs.each( function() {
        // set text size constraint
        if ( $( this ).data( 'wc-input' ) == 'text_size') {
          // remove input if there is a list
          if ( constraint.hasLimitOn( 'fontSize' ) && constraint.attrIs( 'fontInputType', 'pulldown' ) ) {
            $( this )
              .closest( '.section' )
              .remove()

          // set value rounded and with a minimum of 1
          } else {
            $( this ).attr({
              min:  Math.ceil( constraint.attr( 'minFontSize' ) )
            , max:  Math.ceil( constraint.attr( 'maxFontSize' ) )
            , step: Math.ceil( constraint.attr( 'fontStepSize' ) )
            })
          }
        }
      })

      // activate inputs
      inputs.on( 'keyup input change', function() {
        var action = $( this ).data( 'wc-input' )
          , value  = this.value

        throttle( 'input' + action, function() {
          WhiteChili.ci.controller.perform( action, value )
        }, 50 )
      })
    }

    // Enable zoom limits
  , zoom: function() {
      var min   = ci.api().viewPreferences().attr( 'minZoom' ) || 20
        , max   = ci.api().viewPreferences().attr( 'maxZoom' ) || 300
        , zooms = []
        , il    = map.zooms.length
        , i

      // find only eligable zooms
      for ( i = 0; i < il; i++ )
        if ( map.zooms[i] >= min && map.zooms[i] <= max )
          zooms.push( map.zooms[i] )

      // redefine zoom map
      map.zooms = zooms
    }

    // Set history limit
  , history: function() {
      ci.status.history_offset = ci.api().history().status().undos
    }

    // Prepare orientation data
  , orientation: function() {
      // cache original orientation
      ci.cache.orientation = {
        width:  ci.api().attr( 'width' )
      , height: ci.api().attr( 'height' )
      }

      ci.cache.orientation.ratio     = ci.cache.orientation.width / ci.cache.orientation.height
      ci.cache.orientation.portrait  = ci.cache.orientation.ratio < 1
      ci.cache.orientation.landscape = ci.cache.orientation.ratio > 1
      ci.cache.orientation.square    = ci.cache.orientation.ratio == 1
      ci.cache.orientation.original  = ci.cache.orientation.portrait  ? 'portrait'  :
                                       ci.cache.orientation.landscape ? 'landscape' : 'square'
      // set current destination
      ci.status.orientation = ci.cache.orientation.original

      if ( ! ci.config.allow_orientation )
        dom.$( '.menu [data-wc-action=menu][data-wc-value=orientation]' ).remove()
    }

    // Watch window resize
  , resize: function() {
      var timeout, resize
      
      resize = function() {
        // update iframe
        WhiteChili.ui.updateIframe()

        // zoom to fit
        setTimeout( function() {
          ci.doc().zoom( 'fit' )
        }, 50 )
      }

      $( window ).resize( function() {
        clearTimeout( timeout )
        timeout = setTimeout( resize, 300 )
      })

      resize()
    }

    // Enable animated dropdowns
  , dropdowns: function() {
      var dropdowns = dom.$( '.button .dropdown' )

      // calculate size of every dropdown
      dropdowns.each( function() {
        var button, hidden

        // find button and open it
        button = $( this )
          .closest( '.button' )
          .addClass( 'open' )

        // check if button is hidden or not
        if ( hidden = button.hasClass( 'hidden' ) )
          button.removeClass( 'hidden' )

        // calculate height
        $( this ).data({
          w: $( this ).width()
        , h: $( this ).height()
        })
        
        // close button again
        button.removeClass( 'open' )

        // add hidden class again if required
        if ( hidden )
          button.addClass( 'hidden' )
      })

      // add animated class
      dropdowns.addClass( 'animated' )
    }

    // Enable help buttons
  , help: function() {
      var help_layers = ci.api().helpLayers()

      // show help layer button
      if ( help_layers.any() )
        dom.$( '[data-wc-action=help][data-wc-value=show]' )
          .removeClass( 'hidden' )

      // show coach mark button
      if ( ci.config.coach_mark )
        dom.$( '[data-wc-action=coach_mark][data-wc-value=show]' )
          .removeClass( 'hidden' )

      // hide menu if none are present
      if ( help_layers.empty() && ! ci.config.coach_mark )
        dom.$( '[data-wc-action=menu][data-wc-value=help]' )
          .addClass( 'hidden' )
    }

    // Enable download
  , download: function() {
      if ( ci.config.allow_downloads ) {
        ci.doc().pages().each( function() {
          $( '<iframe id="downloader_' + this.attr( 'name' ) + '">' )
            .appendTo( dom.$( 'body' ) )
            .addClass( 'downloader' )
            .attr( 'src', '/blank' )
        })
      } else {
        $( '[data-wc-action=download]' ).remove()
      }
    }

    // Enable viewer
  , viewer: function() {
      if ( ci.doc().hasViewer() )
        dom.$( '[data-wc-action=viewer][data-wc-value=show]' )
          .removeClass( 'hidden' )
    }

    // Enable endpoints
  , endpoints: function() {
      if ( ci.config.remote ) {
        [ 'save', 'checkout' ].forEach( function( action ) {
          if ( ci.config.remote[action] )
            ci.config.remote[action] = ci.config.remote[action].replace( ':id', ci.doc().attr( 'id' ) )
        })
      }
    }

    // Register loaded buttons to ensure absolute performance
  , lightness: function() {
      // check settings menu
      if ( $( 'header.frame-settings .menu .button' ).size() == 0 ) {
        $( 'header.frame-settings' ).remove()

        ci.status.has_settings = false
      }

      // check help layers
      ci.status.has_help_layer = ci.api().helpLayers().any()
    }
    
    // Remove the splash screen with a bit of a delay
  , clarity: function() {
      // update splash
      WhiteChili.ui.loaderGrowth( 'done', i18n.t( 'loader.splash.done' ) )

      setTimeout( function() {
        $( '.splash' ).addClass( 'dying' )

        setTimeout( function() {
          $( '.splash' ).remove()

          setTimeout( function() {
            ci.doc().zoom( 'fit' )
          }, 1000 )
        }, 1000 )
      }, 1000 )
    }

    // Hide all irrelevant help layers
  , helpLayer: function() {
      ci.doc().helpLayers().each( function() {
        if ( regex.help_layer.test( this.attr( 'name' ) ) && this.attr( 'name' ) != 'layer.help.' + ci.config.locale )
          this.attr( 'name', 'inactive' )
      })
    }

    // Warn users before they leave when hitting the back button
  , exitWarning: function() {
      window.onbeforeunload = function() {
        if ( ci.status.exiting !== true )
          return i18n.t( 'home.warning' )
      }
    }

    // Initialize name on document
  , name: function() {
      if ( ci.config.name )
        ci.api().attr( 'name', ci.config.name )
    }

    // Dropzone uploader (code borrowed from dropzone example)
  , uploader: function() {
      if ( ( ci.config.disable_uploads * 1 ) === 1 ) {
        $( '#upload_template, #uploader' ).remove()
        $( '[data-wc-action=upload][data-wc-value=new]' ).remove()
        return
      }

      var node, template
        , url = [ '', ci.config.chili_env, 'templates', ci.doc().attr( 'id' ), 'images.json' ].join( '/' )

      // reference template
      node = document.querySelector( '#upload_template' )
      node.id = ''
      template = node.parentNode.innerHTML
      node.parentNode.removeChild( node )

      // instantiate dropzone for both bodies
      ;[ document, document.getElementById( 'editor' ).contentWindow.document ].forEach( function( doc, i ) {
        // instantiate dropzone
        var zone = new Dropzone( doc.body, {
          url:                url
        , thumbnailWidth:     80
        , thumbnailHeight:    80
        , parallelUploads:    1
        , previewTemplate:    template
        , acceptedFiles:      'image/*,application/pdf,.ai'
        , autoQueue:          true
        , previewsContainer:  '#uploader_previews'
        , clickable:          ( i == 0 ? '#uploader_picker' : null )
        })

        // drop event
        zone.on( 'drop', function( file ) {
          $( '#uploader .loader' ).addClass( 'visible' )

          ci.log( 'Dropped file!' )
        })

        // file receive event
        zone.on( 'addedfile', function( file ) {
          // hide upload instructions
          $( '#uploader' ).removeClass( 'instruct' )

          // show loading bar
          $( '#uploader .loader' )
            .removeClass( 'processing' )
            .addClass( 'visible' )

          // reset loader status
          ci.status.loader = 0
          
          ci.log( 'Added "' + file.name + '" for upload' )
        })

        // detected dragged file entering
        zone.on( 'dragover', function() {
          // make sure the coach mark is closed
          ci.controller.perform( 'coach_mark', 'hide' )

          // open the upload screen
          WhiteChili.ui.openUpload()
        })

        // detected dragged file leaving
        zone.on( 'dragleave', function() {
          $( '#uploader' ).addClass( 'instruct' )
        })

        // update the total progress bar
        zone.on( 'totaluploadprogress', function( progress ) {
          if ( progress >= 100 )
            WhiteChili.ui.loaderUpdate( 101, 'Beeld verwerken...' )
          else
            WhiteChili.ui.loaderUpdate( progress, ~~progress + '%' )

          ci.log( 'File uploaded for ' + ~~progress + '%' )
        })

        // show the total progress bar when upload starts
        zone.on( 'sending', function( file ) {
          ci.log( 'Uploading "' + file.name + '"...' )
        })

        // successful upload
        zone.on( 'success', function( xhr, response ) {
          ci.controller.perform( 'image', response )
          
          ci.log( 'Upload successfully completed!' )
        })

        // errorful upload upload
        zone.on( 'error', function( xhr, message ) {
          // show message
          alert( message )

          // hide uploader
          ci.controller.perform( 'upload', 'hide' )

          ci.log( 'Upload error: ' + message )
        })
      })

      // hide uploader container
      $( '#uploader' ).hide()

      ci.log( 'Enabled dropzone uploader' )
    }

  }
  
})( WhiteChili.ci, WhiteChili.map, WhiteChili.dom, WhiteChili.utils, WhiteChili.regex, WhiteChili.i18n, WhiteChili.throttle )





















;( function( ci, i18n, utils, dom ) {

  WhiteChili.load = {
    // Preload preflight image
    preflight: function() {
      this.pre( utils.s3( 'images', ci.config.preflight_file ) )
    }

    // Preload all images
  , images: function() {
      var preload = this.pre

      ci.api().images().each( function() {
        preload( this.previewUrl() )
      })
    }

    // Download an image
  , down: function( page ) {
      // create iframe and load it
      dom.$( '#downloader_' + page.attr( 'name' ) )
        .attr( 'src', page.downloadUrl() )
        
      ci.log( 'Initiating download for "' + page.downloadUrl() + '"' )
    }

    // Get viewer url
  , viewer: function() {
      // prepare variables
      var model = ci.doc().attr( 'threeDModelID' )
        , fold  = ci.doc().attr( 'foldSettingID' )
        , item  = fold || model
        , env   = ci.config.chili_env
        , key   = fold ? 'viewer' : 'three_d'
        , id    = ci.doc().attr( 'id' )
        , url   = '/' + env + '/' + key + '/' + id + '/' + item + '.json'

        console.log( 'loading ' + url )
      // load url
      $.ajax({
        dataType: 'json'
      , url:      url
      , success:  function( response ) {
          console.log( 'got ' + response.viewer )
          dom.$( '#viewer' ).attr( 'src', response.viewer )
          
          ci.log( 'Starting to load 3D viewer' )
        }
      })

      ci.log( 'Fetching 3D viewer url for current alternate layout' )
    }

    // Preload an image
  , pre: function( src ) {
      var img = new Image()
      img.src = src
    }
    
  }

})( WhiteChili.ci, WhiteChili.i18n, WhiteChili.utils, WhiteChili.dom )
;( function( ci ) {

  // Add request module
  WhiteChili.req = {

    // Perform a post request
    post: function( name, after, data ) {
      // ensure data
      data = data || {}

      // add destination if required
      if ( ci.config.remote ) {
        if ( ci.config.remote[name] ) {
          data.destination = ci.config.remote.source + ci.config.remote[name]
        }
        if ( ci.config.remote.auth_token ) {
          data.data = { auth_token: ci.config.remote.auth_token }
        }
      }

      ci.log( 'Posting data to "' + name + '" endpoint' )
      
      // perform request
      $.post( ci.config.endpoints[name], data, function( res ) {
        // store new auth token if given
        if ( res.auth_token ) {
          if ( ! ci.config.remote ) {
            ci.config.remote = {}
          }

          ci.config.remote.auth_token = res.auth_token
        }

        // activate after callback if given
        if ( after )
          after( res )

        ci.log( 'Request to "' + name + '" succeeded!' )
        
      }, 'json' )
      .fail( function( res ) {
        ci.log( 'Request to "' + name + '" failed!', 'error' )
      })
    }

  }

})( WhiteChili.ci );
;( function( ci, map, utils, load, build, enable, regex, dom ) {
  var ui

  // Add user interface module
  ui = WhiteChili.ui = {
    // Store status
    initialized: false

    // Set initial interface status
  , initialize: function() {
      // make sure initialisation doesn't happen twice
      if ( this.initialized === true ) return false
      this.initialized = true

      // build ui components
      build.coachMark()
      build.fontsDropdown()
      build.paragraphsDropdown()
      build.textSizes()
      build.colorPalettes()
      build.pagesSidebar()
      build.layoutsDropdown()
      build.zoomSlider()
      build.css()
      build.backgroundTemplates()

      // // enable ui components
      enable.exitWarning()
      enable.buttons()
      enable.inputs()
      enable.resize()
      enable.zoom()
      enable.orientation()
      enable.dropdowns()
      enable.help()
      enable.name()
      enable.viewer()
      enable.endpoints()
      enable.uploader()
      enable.download()
      enable.helpLayer()
      enable.lightness()
      enable.history()
      enable.clarity()

      // // preload images
      load.preflight()
      load.images()

      // // initialize status of ui elements
      this.setInitialLayout()
      this.historyUpdate()
      this.selectPage() //-> HTML EDITOR PROBLEM
      this.selectLayout()
      this.helpUpdate()
      this.cursorUpdate()
      this.zoomUpdate()
      this.orientationUpdate()
      this.pagesUpdate()
      this.showAnimated()
      this.saveButtonUpdate( false )
    }

    // Set initial layout
  , setInitialLayout: function() {
      var layout = ci.doc().alternateLayouts().find( 'id', ci.config.layout_id )

      if ( layout )
        ci.doc().attr( 'selectedAlternateLayout', layout )
    }

    // Update selected page
  , selectPage: function() {
      var page = ci.query( 'document.selectedPage' )

      // select page
      $( '.pages-sidebar .page' )
        .removeClass( 'active' )

      $( '.pages-sidebar [data-wc-value=' + page.name + ']' )
        .addClass( 'active' )

      // store selected page status
      ci.status.page = page.index * 1
    }

    // Update selected layout
  , selectLayout: function() {
      var layout = ci.query( 'document.selectedAlternateLayout' )

      // deactivate all and activate selected
      dom.$( '.menu [data-wc-action=menu][data-wc-value=layouts] [data-wc-action=layout]' )
        .removeClass( 'active' )
      dom.$( '.menu [data-wc-action=menu][data-wc-value=layouts] [data-wc-value=' + layout.index + ']' )
        .addClass( 'active' )
    }

    // Update pages drawer
  , pagesUpdate: function() {
      // show/hide pages component
      dom.$( 'body' ).toggleClass( 'open-sidebar', ci.doc().pages().many() )
    }

    // Regenerate page thumbs
  , regeneratePages: function() {
      // rebuild pages sidebar
      build.pagesSidebar()

      // update iframe size and position
      this.updateIframe()
    }

    // Update history buttons
  , historyUpdate: function() {
      // get current history stack status
      var status = ci.api().history().status()

      // update buttons
      dom.$( '.menu [data-wc-action=undo]' )
        .toggleClass( 'enabled', status.undos > ci.status.history_offset )
      dom.$( '.menu [data-wc-action=redo]' )
        .toggleClass( 'enabled', status.redos > 0 )
    }

    // Resize iframe
  , updateIframe: function() {
      // set iframe size
      var header = dom.$( 'section header' )
        , aside  = dom.$( 'section aside' )
        , doc    = $( document )
        , x = aside.width() - 1
        , y = header.height() - 1
        , w = doc.width() - x + 18
        , h = doc.height() - y * 2

      // update iframe
      dom.$( '#editor' ).css({
        left:   x + 'px'
      , top:    y + 'px'
      , width:  w + 'px'
      , height: h + 'px'
      })

      // update pages sidebar
      aside.css({
        top:    y + 'px'
      , height: h + 'px'
      })
    }

    // Update zoom buttons
  , zoomUpdate: function() {
      this.zoomButtonsUpdate({
        target: 'zoom'
      , zoom:   ci.doc().attr( 'zoom' )
      , min:    map.zooms[0]
      , max:    map.zooms[map.zooms.length - 1]
      })
    }

    // Update cropper zoom buttons
  , cropperZoomUpdate: function() {
      if ( ci.status.cropper ) {
        this.zoomButtonsUpdate({
          target: 'cropper_zoom'
        , zoom:   ci.status.cropper.zoomLevel()
        , min:    ci.status.cropper.min()
        , max:    ci.status.cropper.max()
        })
      }
    }

    // Zoom buttons update
  , zoomButtonsUpdate: function( options ) {
      // get current zoom
      var klass  = utils.slug( options.target )
        , label  = $( '.' + klass + '-slider label' ).first()
        , width  = $( '.' + klass + '-slider .slider-bar' ).first().width()
        , handle = $( '.' + klass + '-slider .slider-handle' ).first()
        , offset = Math.floor( ( ( options.zoom - options.min ) / ( options.max - options.min ) ) * width )

      // normalize zoom value
      if ( options.zoom > options.max )
        options.zoom = options.max
      else if ( options.zoom < options.min )
        options.zoom = options.min

      // update buttons
      $( '.menu [data-wc-action=' + options.target + '][data-wc-value=in]' )
        .toggleClass( 'enabled', options.zoom != options.max )
      $( '.menu [data-wc-action=' + options.target + '][data-wc-value=out]' )
        .toggleClass( 'enabled', options.zoom != options.min )

      // update zoom slider
      handle.css( 'left', offset + 'px' )

      // update zoom label
      label.html( ~~options.zoom + '%' )
    }

    // Update cursor buttons
  , cursorUpdate: function() {
      // get current cursor (must be fetched from the doc(), not the api()!!!)
      ci.status.cursor = ci.doc().cursor()

      // show all cursor buttons
      dom.$( '.menu [data-wc-action=cursor]' )
        .removeClass( 'enabled' )

      // hide where necessary
      if ( ci.api().cursorIsnt( 'pointer' ) )
        dom.$( '.menu [data-wc-action=cursor][data-wc-value=pointer]' )
          .addClass( 'enabled' )

      if ( ci.api().cursorIs( 'pointer' ) )
        dom.$( '.menu [data-wc-action=cursor][data-wc-value=hand]' )
          .addClass( 'enabled' )

    }

  , orientationUpdate: function( orientation ) {
      if ( ci.config.allow_orientation ) {
        orientation = orientation || ci.cache.orientation.original

        // deactivate all buttons
        dom.$( '.menu [data-wc-action=orientation]' )
          .removeClass( 'active' )

        // activate button
        dom.$( '.menu [data-wc-action=orientation][data-wc-value=' + orientation + ']' )
          .addClass( 'active' )

        // update mask visibility

      }
    }

    // Update help buttons
  , helpUpdate: function() {
      if ( ci.status.has_help_layer && ci.api().helpLayers().any() ) {
        // get visible help layer
        var layers = ci.api().helpLayers( true )

        dom.$( '[data-wc-action=help][data-wc-value=show]' )
          .toggleClass( 'hidden', layers.any() )
        dom.$( '[data-wc-action=help][data-wc-value=hide]' )
          .toggleClass( 'hidden', layers.empty() )
      }
    }

    // Update 3D viewer
  , viewerUpdate: function( value ) {
      var viewer = dom.$( '#viewer' )
        , layer  = dom.$( '.layer-viewer' )

      // make sure viewer button is updated
      if ( value == 'show' ) {
        // save document and add callback
        ci.doc().save( function() {
          // make sure the iframe is loaded
          load.viewer()

          layer
            .addClass( 'loading' )
            .show()

          viewer.get( 0 ).onload = function() {
            setTimeout( function() {
              layer.removeClass( 'loading' )
              viewer.addClass( 'show' )
            }, 500 )

            ci.log( 'Viewer loaded' )
          }

          setTimeout( function() {
            layer.addClass( 'open' )
          }, 20 )
        })

      } else {
        layer.removeClass( 'open' )
        viewer.removeClass( 'show' )

        setTimeout( function() {
          layer.hide()
          viewer.attr( 'src', '/blank' )
        }, 500 )
      }
    }

    // Update modal
  ,  modalUpdate: function( value ) {
      var modal = dom.$( '#modal' )
        , layer = dom.$( '.layer-modal' )

      // make sure viewer button is updated
      if ( value == 'hide' ) {
        layer.removeClass( 'open' )
        modal.removeClass( 'show' )

        setTimeout( function() {
          layer.hide()
          modal.attr( 'src', '/blank' )
        }, 500 )

      } else {
        // make sure the iframe is loaded
        modal
          .attr( 'src', '/blank' )
          .attr( 'src', value )

        layer.show()

        modal.get( 0 ).onload = function() {
          setTimeout( function() {
            modal.addClass( 'show' )
          }, 500 )

          ci.log( 'Modal loaded "' + $( this ).attr( 'src' ) + '"' )
        }

        setTimeout( function() {
          layer.addClass( 'open' )
        }, 20 )
      }
    }

    // Update ui elements affected by variables
  , variableUpdate: function() {
      // update pages sidebar
      setTimeout( function() {
        $( '.pages-sidebar [data-wc-action=page]' )
          .removeClass( 'hidden' )

        ci.doc().pages().each( function() {
          if ( this.attrIs( 'includeInOutput', false ) )
            $( '.pages-sidebar [data-wc-action=page][data-wc-value=' + this.attr( 'name' ) + ']' )
              .addClass( 'hidden' )
        }, 300 )
      })

    }

    // Enable transition animations on ui
  , showAnimated: function() {
      ci.doc().zoom( 'fit' )

      dom.$( 'body' ).addClass( 'animated' )
    }

    // Show menu for given action
  , openMenu: function( action ) {
      // open menu
      dom.$( utils.selectors( 'menu:' + action ) )
        .addClass( 'open' )

      // enable close event
      $( document ).on( 'mouseup', closeMenu )

      // show frame shield
      dom.$( '.frame-shield' ).addClass( 'active' )

      // update status
      ci.status.last_menu = action
      ci.status.menu_open = true
    }

    // Hide menu
  , closeMenu: function( event ) {
      // get open menu panel and value
      var panel = $( '.open[data-wc-action=menu]' ).first()
        , value = panel.data( 'wc-value' )

      if ( ! event || ! value || clickedOutside( event, panel ) ) {
        // close all menus menu
        dom.$( '[data-wc-action=menu]' )
          .removeClass( 'open' )
          .find( '.open' )
          .removeClass( 'open' )

        // unbind click
        $( document ).off( 'mouseup', closeMenu )

        // hide frame shield
        dom.$( '.frame-shield' ).removeClass( 'active' )
      }

      // update menu status
      setTimeout( function() {
        if ( ci.status.last_menu === value && ci.status.menu_open === true ) {
          delete ci.status.last_menu
          delete ci.status.menu_open
        }
      }, 10 )
    }

    // Open upload screen
  , openUpload: function() {
      // reset contents
      dom.$( '#uploader_previews, #uploader .message' ).html( '' )

      // reset preloader bar
      dom.$( '#uploader .loader' ).removeClass( 'processing visible' )
      dom.$( '#uploader .bar' ).css( 'width', 0 )

      // show uploader
      dom.$( '#uploader' ).addClass( 'instruct' ).show()

      setTimeout( function() {
        dom.$( '#uploader' ).addClass( 'open' )
      }, 10 )
    }

    // Close upload screen
  , closeUpload: function() {
      dom.$( '#uploader' ).removeClass( 'open ' )

      setTimeout( function() {
        dom.$( '#uploader' ).removeClass( 'instruct' ).hide()
      }, 299 )
    }

  , openFont: function( id ) {
      // close open fonts
      dom.$( '.fonts .font' ).removeClass( 'open' )
      dom.$( '.fonts .group' ).removeClass( 'active' )

      // open selected font
      dom.$( '.font-' + id ).addClass( 'open' )
    }

  , selectFontVariant: function( id ) {
      // deactivate font variants and groups
      dom.$( '.fonts .variant, .fonts .group' ).removeClass( 'active' )

      // activate currently selected group and variant
      dom.$( '.fonts .variant-' + id )
        .addClass( 'active' )
        .closest( '.font' )
        .find( '.group' )
        .addClass( 'active' )
    }

    // Update loader
  , loaderUpdate: function( percent, message ) {
      // update loader status
      percent = percent == 'done' ? 100 : percent

      // make sure loader is visible
      dom.$( '.loader' )
        .addClass( 'visible' )
        .removeClass( 'processing' )

      // normalise value
      if ( percent > 100 ) {
        dom.$( '.loader' ).addClass( 'processing' )

        ci.status.loader = 100
      }

      // update bar
      dom.$( '.loader .bar' ).css( 'width', percent + '%' )

      // update message
      dom.$( '.loader .message' ).html( message )
    }

    // Relatively grow loader
  , loaderGrowth: function( growth, message ) {
      if ( typeof growth == 'number' ) {
        ci.status.loader += growth
        growth = ci.status.loader
      }

      this.loaderUpdate( growth, message )
    }

    // Update slider
  , sliderUpdate: function( subject, value ) {
    // reference coachmark element
      var slider = $( '.' + subject )
        , active = slider.find( '.button.active' ).data( 'wc-value' ) * 1
        , total  = slider.find( '.buttons .button' ).size()
        , end

      // show a given slide number
      if ( regex.number.test( value ) ) {
        // convert value
        value *= 1

        // hide if no slides are left
        if ( value >= total )
          return this.sliderUpdate( subject, 'hide' )

        // detect last slide
        end = value == total - 1

        // update dots
        slider.find( '.button' ).removeClass( 'active' )
        slider.find( '.button[data-wc-value=' + value + ']' ).addClass( 'active' )
        slider.find( '.images' ).css( 'left', ( value * -100 ) + '%' )

        // toggle previous button
        slider.find( '.nav.previous' ).toggleClass( 'enabled', value > 0 )

        // toggle next button
        slider.find( '.nav.next' ).toggleClass( 'end', end )

      // show next slide
      } else if ( value == 'next' ) {
        slider.removeClass( 'loading' )
        this.sliderUpdate( subject, active + 1 )

      // show previous slide
      } else if ( value == 'previous' ) {
        slider.removeClass( 'loading' )
        this.sliderUpdate( subject, active - 1 )

      // hide slider
      } else if ( value == 'hide' ) {
        slider.addClass( 'hidden' )
        setTimeout( function() { slider.hide() }, 500 )

      // show slider
      } else if ( value == 'show') {
        this.sliderUpdate( subject, 0 )
        slider.show()
        setTimeout( function() { slider.removeClass( 'hidden' ) }, 20 )

        // empty exiting previews if necessary
        if ( subject == 'layer-preview' ) {
          slider
            .addClass( 'loading' )
            .find( '.image' )
            .css({ backgroundImage: 'url( data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7 )' })
        }
      }
    }

    // Show dialog
  , openDialog: function( content, name ) {
      var dialog = dom.$( '.dialog-box' ).show()

      setTimeout( function() {
        dialog.addClass( 'open' )
      }, 10 )

      // show close button
      dialog.find( 'button.close' ).show()

      // set keymaster scope
      ci.shortcut.scope( 'modal' )

      // generate content
      content = content.html()

      // set title and message and make sure dialog content is centered
      dialog.children( '.content' )
        .html( content )
        .css( 'margin-top',  ( - content.height() / 2 ) + 'px' )
        .css( 'margin-left', ( - content.width()  / 2 ) + 'px' )

      // set current dialog in status
      ci.status.dialog = name
    }

    // Hide dialog
  , closeDialog: function( name ) {
      if ( ! name || ! ci.status.dialog || ci.status.dialog == name ) {
        var dialog = dom.$( '.dialog-box' ).removeClass( 'open' )

        // clear keymaster enter actions
        key.unbind( 'enter', 'modal' )

        // set keymaster scope
        ci.shortcut.scope( 'main' )

        setTimeout( function() {
          dialog.hide()
        }, 500 )

        // unset current dialog in status
        delete ci.status.dialog
      }
    }

    // Perform a frame action if found on a frame
  , frameAction: function() {

      if ( ci.api().cursorIs( 'pointer' ) && ci.api().selected().one() ) {
        // get tag on selected frame
        var frame = ci.api().selected( 0 )
          , tag   = frame.attr( 'tag' )
          , deselect, persist, implode

        // process all tags
        if ( typeof tag == 'string' ) {
          for ( var i = 0, p = tag.split( ',' ), il = p.length, m; i < il; i++ ) {
            if ( m = p[i].match( regex.frame_action ) ) {
              // make persistent
              if ( m[1] == 'persist' )
                persist = true

              // detect implosion
              else if ( m[1] == 'implode' )
                implode = true

              // perform frame action
              else
                ci.controller.perform( m[1], m[3] )

              // mark for deselection
              deselect = true

              ci.log( 'Found frame action "' + m[0] + '"' )
            }
          }

          // erase frame actions if required
          if ( implode ) {
            var tags = []

            for ( i = 0, il = p.length; i < il; i++ )
              if ( ! regex.frame_action.test( p[i] ) )
                tags.push( p[i] )

            frame.attr( 'tag', tags.join( ',' ) )
          }
        }

        // deselect frame
        if ( deselect && ! persist )
          setTimeout( function() { ci.doc().deselect() }, 50 )
      }
    }

    // Open cropper if frame is croppable
  , frameCropper: function() {
      var selected = ci.api().selected()

      if ( selected.one() && selected.first().isCropable() )
        ci.controller.perform( 'cropper' )
    }

    // Update save button enabled status
  , saveButtonUpdate: function( enabled ) {
      if ( ! ci.status.saving ) {
        if ( typeof enabled != 'boolean' )
          enabled = ci.doc().dirty()

        dom.$( '[data-wc-action=save]' )
          .toggleClass( 'enabled', enabled  )
      }
    }

    // Update frame settings panel
  , settingsUpdate: function() {
      // make sure settings are present
      if ( ! ci.status.has_settings ) {
        ci.log( 'No settings update required' )

        return false
      }

      // get selected
      var selected        = ci.api().selected()
        , hasSelectedText = ci.api().hasSelectedText()
        , stroked         = new WhiteChili.Collections.Frame() // new WhiteChili.FrameSet()
        , imaged          = new WhiteChili.Collections.Frame() // new WhiteChili.FrameSet()
        , texed           = new WhiteChili.Collections.Frame() // new WhiteChili.FrameSet()
        , value, constraints, delete_locks

      // collect all frames with fill/border attribute support
      selected.each( function() {
        if ( map.fillable.indexOf( this.attr( 'type' ) ) > -1 )
          return stroked.add( this )
      })

      // collect all image frames
      selected.each( function() {
        if ( this.attrIs( 'type', 'image' ) )
          return imaged.add( this )
      })

      // collect all frames with text attribute support
      selected.each( function() {
        if ( map.textable.indexOf( this.attr( 'type' ) ) > -1 )
          return texed.add( this )
      })

      // collect all delete locks
      delete_locks = selected.collect( function() {
        return this.frameConstraints().hasLockOn( 'delete' )
      })

      // show all settings
      dom.$( utils.selectors( map.settings.all ) )
        .removeClass( 'hidden' )

      // hide all settings if selection is empty
      if ( selected.empty() )
        dom.$( utils.selectors( map.settings.all ) )
          .addClass( 'hidden' )

      // visualise settings according to frame constraints
      applyFrameConstraints( stroked, texed )

      // get constrains
      if ( selected.any() )
        constraints = selected.first().frameConstraints()

      // hide background buttons if many frames are selected
      if ( selected.many() )
        dom.$( utils.selectors( 'background' ) )
          .addClass( 'hidden' )

      // stroke settings
      if ( stroked.any() ) {
        // find capjoin value
        var capjoin = map.capjoin[stroked.first().attr( 'borderJoin' )]

        // update fill color settings
        swatchUpdate( 'fill', selected.fill() )

        // update stroke color settings
        swatchUpdate( 'stroke', selected.stroke() )

        // update stroke width
        dom.$( '.menu [data-wc-input=stroke_width]' )
          .val( stroked.first().strokeWidth() )

        // update stroke edge
        if ( stroked.one() && stroked.first().attrIs( 'type', 'circle' ) ) {
          dom.$( utils.selectors( 'stroke_edge' ) )
            .addClass( 'hidden' )

        } else {
          dom.$( utils.selectors( 'stroke_edge' ) )
            .removeClass( 'active hidden' )

          dom.$( '.menu [data-wc-action=stroke_edge][data-wc-value=' + capjoin + ']' )
            .addClass( 'active' )
        }
      }

      // image frame settings
      if ( imaged.any() ) {
        var cropable = imaged.first().isCropable()

        // update fit mode
        dom.$( utils.selectors( 'crop' ) )
          .removeClass( 'active' )

        dom.$( '.menu [data-wc-action=crop][data-wc-value=' + imaged.first().attr( 'fitMode' ) + ']' )
          .addClass( 'active' )

        // update fit point
        dom.$( '.buttons.crop_anchor' )
          .toggle( imaged.first().attrIn( 'fitMode', map.anchorable ) )

        // update anchor point
        dom.$( utils.selectors( 'crop_anchor' ) )
          .removeClass( 'active' )

        dom.$( '.menu [data-wc-action=crop_anchor][data-wc-value=' + imaged.first().attr( 'fitPoint' ) + ']' )
          .addClass( 'active' )

        // check for cropable frames
        if ( imaged.many() || ( constraints && constraints.hasLockOn( 'content' ) ) ) {
          dom.$( utils.selectors( 'menu:crop' ) ).addClass( 'hidden' )
          dom.$( utils.selectors( 'cropper'   ) ).addClass( 'hidden' )
        } else {
          dom.$( utils.selectors( 'menu:crop' ) ).toggleClass( 'hidden', cropable )
          dom.$( utils.selectors( 'cropper'   ) ).toggleClass( 'hidden', ! cropable )
        }

      }

      // text selection in text cursor mode
      if ( hasSelectedText ) {
        // cache selected text
        var selectedText = ci.api().selectedText()
        
        if ( selectedText.attr( 'length' ) > 0 ) {
          // find textalign value
          var textAlign = selectedText.align()
            , font = selectedText.font()

          // update stroke color settings
          swatchUpdate( 'color', selectedText.colors() )

          // update text size
          dom.$( '.menu [data-wc-input=text_size]' ).val( selectedText.size() )

          // update text alignment
          dom.$( utils.selectors( 'text_align' ) )
            .removeClass( 'active' )

          dom.$( '.menu [data-wc-action=text_align][data-wc-value=' + ( textAlign || 'left' ) + ']' )
            .addClass( 'active' )

          // update font menu
          this.selectFontVariant( font.attr( 'id' ) )

          // update text align menu icon
          dom.$( '[data-wc-action=menu][data-wc-value=align]' )
            .removeClass( 'align-left align-center align-right align-justify' )
            .addClass( 'align-' + textAlign )
        }

      // text frame in pointer mode
      } else if ( texed.any() ) {
        // find textalign value
        var textAlign = texed.first().textAligns()
          , fonts = texed.first().fonts()

        // update text color settings
        swatchUpdate( 'color', selected.textColors() )

        // update text size
        dom.$( '.menu [data-wc-input=text_size]' ).val( texed.textSizes()[0] || 16 )

        // update text alignment
        dom.$( utils.selectors( 'text_align' ) )
          .removeClass( 'active' )

        dom.$( '.menu [data-wc-action=text_align][data-wc-value=' + ( textAlign[0] || 'left' ) + ']' )
          .addClass( 'active' )

        // update font menu
        if ( fonts[0] )
          this.selectFontVariant( fonts[0].attr( 'id' ) )

        // update text align menu icon
        dom.$( '[data-wc-action=menu][data-wc-value=align]' )
          .removeClass( 'align-left align-center align-right align-justify' )
          .addClass( 'align-' + textAlign[0] )
      }

      // hide settings according to capabilities of selected frames
      if ( ci.api().hasSelectedText() ) {
        // only show text options when in text edit mode
        dom.$( utils.selectors( utils.difference( map.settings.all, map.settings.format ) ) )
          .addClass( 'hidden' )

      } else if ( selected.one() && selected.first().isBackground() ) {
        // only show text options when in text edit mode
        dom.$( utils.selectors( utils.difference( map.settings.all, map.settings.background ) ) )
          .addClass( 'hidden' )

      } else {
        selected.each( function() {
          // get frame type and non allowed settings
          var type = this.attr( 'type' )

          if ( map.settings[type] ) {
            var diff = utils.difference( map.settings.all, map.settings[type] )

            // hide buttons for non allowed settings
            dom.$( utils.selectors( diff ) )
              .addClass( 'hidden' )
          }

        })
      }

      // delete button
      dom.$( utils.selectors( 'destroy' ) )
        .toggleClass( 'hidden', selected.empty() || delete_locks.length > 1 || delete_locks[0] === true )

      // show info button
      dom.$( utils.selectors( 'info' ) )
        .toggleClass( 'hidden', ! selected.empty() )

      ci.log( 'Updating frame settings' )
    }
  }

  //////////////////////////////
  ////////// helpers ///////////
  //////////////////////////////

  // Apply frame constraints to ui selections
  function applyFrameConstraints( stroked, texed ) {
    var lists = { fill: [], color: [], stroke: [] }
      , list

    // reset all blinded features
    dom.$( '[data-wc-value=fill] .color, [data-wc-value=stroke] .color, [data-wc-value=color] .color' )
      .removeClass( 'hidden' )
      .addClass( 'pending' )

    // check stroked constraints
    stroked.each( function() {
      lists.fill.push( this.frameContentConstraints().colorsFor( 'fill' ).collect( 'id' ) )
      lists.stroke.push( this.frameContentConstraints().colorsFor( 'stroke' ).collect( 'id' ) )
    })

    // check texed constraints
    texed.each( function() {
      lists.color.push( this.frameContentConstraints().colorsFor( 'color' ).collect( 'id' ) )
    })

    // perform intersections
    for ( list in lists ) {
      dom.$( utils.intersection( lists[list] ).map( function( c ) {
        return '[data-wc-value=' + list + '] .color-' + c.toLowerCase()
      }).join( ', ' ) ).removeClass( 'pending' )

      $( '[data-wc-value=' + list + '] .color.pending' )
        .addClass( 'hidden' )
    }

    // remove all pending
    dom.$( '[data-wc-value=fill] .color, [data-wc-value=stroke] .color, [data-wc-value=color] .color' )
      .removeClass( 'pending' )
  }

  // Update swatch color based on selected elements
  function swatchUpdate( action, colors ) {
    // get button and collect colors from selected elements
    var selected = colors.unique()
      , button   = dom.$( utils.selectors( 'menu:' + action ) ).removeClass( 'multiple' )

    // update color selection
    dom.$( utils.selectors( 'menu:' + action, { suffix: '.colors .color' } ) )
      .removeClass( 'active' )

    // show selected color
    if ( selected.one() ) {
      // instantiate color
      var color = selected.first()

      // update swatch
      button.find( '.swatch' )
        .removeClass()
        .addClass( 'swatch' )
        .addClass( color.className( true ) )

      // activate color
      dom.$( utils.selectors( 'menu:' + action, { suffix: '.' + color.className() } ) )
        .addClass( 'active' )

    // don't show anything with mutilple values
    } else {
      // update swatch
      button.addClass( 'multiple' )
    }
  }

  // Detect if clicked outside a panel
  function clickedOutside( event, panel ) {
    var d = panel.find( '.dropdown' )
      , s = d.hasClass( 'slippery' )
      , p = d.offset()
      , x = event.pageX
      , y = event.pageY
      , l = p.left
      , t = p.top
      , w = d.width()
      , h = d.height()

    return s || x < l || x > l + w || y < t || y > t + h
  }

  // Close menu
  function closeMenu( event ) {
    ui.closeMenu( event )
  }

})( WhiteChili.ci, WhiteChili.map, WhiteChili.utils, WhiteChili.load, WhiteChili.build, WhiteChili.enable, WhiteChili.regex, WhiteChili.dom )

;( function( ci, ui, load, i18n, throttle ) {

  // Add main event handler
  WhiteChili.handler = function( type, target_id ) {
    // log current call
    ci.log( 'Received "' + type + '" event' + ( target_id ? ' with target id "' + target_id + '"' : '' ) )

    // build event statistics
    ci.stats( 'events', type )
    
    try {
      switch ( type ) {
        case 'CursorChanged':
          // update cursor
          ui.cursorUpdate()
          
          // change shortcut modus
          ci.shortcut.scope( ci.api().cursorIs( 'text' ) ? 'text' : 'main' )
        break
        case 'DocumentDimensionsChanged':
          
        break
        case 'DocumentFullyLoaded':
          var doc = ci.doc()

          // make sure only the current layout is saved to prevent layout rotation
          doc.attr( 'saveInactiveTextPositions', false )

          // cache for speed
          ci.recache()

          // initialize UI
          ui.initialize()

          // select the first page
          doc.select( '1' )

          // HACK: make first text frame and delete it
          // ci.controller.perform( 'frame', 'text' )

          // setTimeout( function() {
          //   ci.controller.perform( 'destroy' )
          // }, 2000 )
        break
        case 'DocumentSaved':
          // update save button visibility
          ui.saveButtonUpdate( false )

          // register saving process
          ci.status.saving = false

          // perform callback if present
          if ( typeof ci.status.afterSave === 'function' )
            ci.status.afterSave()

          delete ci.status.afterSave
        break
        case 'FrameAdded':
          throttle( 'RecacheXML', function() {
            ci.recache()
          }, 300 )

          clearSelectionCache()
        break
        case 'FrameIndexChanged':
          throttle( 'RecacheXML', function() {
            ci.recache()
          }, 0 )
        break
        case 'FrameMoveInProgress':
          // mark as frame moving
          ci.status.moving = true
        break
        case 'FrameAnchorChanged':
        case 'FrameBorderChanged':
        case 'FrameDropShadowChanged':
        case 'FrameFillChanged':
        case 'FrameOpacityChanged':
        case 'FrameTextVariablesChanged':
        case 'FrameWrapChanged':
          throttle( 'RecacheXML', function() {
            ci.recache()
          })
          
          throttle( 'FrameUpdate', function() {
            ui.settingsUpdate()
          })
        break
        case 'FrameDeleted':
          // make sure selection is cleared
          ci.api().deselect()

          // recache xml document
          throttle( 'RecacheXML', function() {
            ci.recache()
          }, 300 )

          // update visibility of frame settings
          throttle( 'FrameUpdate', function() {
            ui.settingsUpdate()
          })
        break
        case 'LayerVisibilityChanged':
          // update ui
          ui.helpUpdate()
        break
        case 'PageAdded':
        case 'PageDeleted':
          // regenerate page thumbs
          ui.regeneratePages()
        break
        case 'PageControlsLoaded':
          var doc   = ci.doc()
            , pages = doc.pages()

          // update pages in config
          ci.config.pages = pages.collect( 'name' )

          // update loader
          ui.loaderGrowth( 40 / pages.size(), i18n.t( 'loader.splash.prepare_pages' ) )

          // save document if required
          if ( ci.config.save_on_load && pages.last().attrIs( 'id', target_id ) ) {
            setTimeout( function() {
              doc.save()
            }, 100 )
            delete ci.config.save_on_load
          }
        break
        case 'SelectedAlternateLayoutChanged':
          // recache xml document
          throttle( 'RecacheXML', function() {
            ci.recache()
          }, 300 )

          // update ui
          setTimeout( function() {
            ui.selectLayout()
          }, 1000 )
        break
        case 'SelectedFrameChanged':
          // clear selected frames cache
          clearSelectionCache()
          
          // perform white chili action if one is given
          if ( ! ci.api().hasSelectedText() ) {
            // detect frame actions
            ui.frameAction()
            
            // update visibility of frame settings
            throttle( 'FrameUpdate', function() {
              ui.settingsUpdate()
            })

            // perform live preflight
            setTimeout( function() {
              if ( ci.config.enable_live_preflight )
                ci.controller.perform( 'live_preflight' )
            }, 500 )
          }
        break
        case 'SelectedPageChanged':
          // update ui
          ui.selectPage()
        break
        case 'TextSelectionChanged':
          throttle( type, function() {
            var doc = ci.api()
            
            // update visibility of frame settings
            ui.settingsUpdate()

            // return to pointer mode if deselected
            if ( ! doc.selected( 0 ) && doc.cursorIs( 'text' ) )
              doc.cursor( 'pointer' )
          })
        break
        case 'FrameMoveFinished':
        case 'UndoStackChanged':
          if ( type == 'FrameMoveFinished' || ! ci.status.moving ) {
            // update history buttons
            ui.historyUpdate()

            // update save button visibility
            ui.saveButtonUpdate( true )

            // regenerate xml cache
            throttle( 'RecacheXML', function() {
              ci.recache()
            }, 300 )

            // mark as frame moving
            ci.status.moving = false
          }

          if ( type == 'UndoStackChanged' ) {
            clearSelectionCache()

            // regenerate xml cache
            throttle( 'RecacheXML', function() {
              ci.recache()
            }, 300 )

            // re-open cropper if required
            setTimeout( function() {
              if ( ci.status.cropper && ci.api().selected().one() && ci.api().selected( 0 ).isCropable()  )
                ci.controller.perform( 'cropper' )

              // make sure settings are updated
              ui.settingsUpdate()
            }, 1000 )
          }
        break
        case 'VariableValueChanged':
          // update ui where variables may have impact
          ui.variableUpdate()
        break
        case 'ZoomChanged':
          throttle( type, function() {
            // recache document
            ci.recache()

            // update history buttons
            ui.zoomUpdate()
          }, 30 )
        break
      }
    } catch( e ) {
      console.error( e )
    }
  }

  //////////////////////////////
  ////////// helpers ///////////
  //////////////////////////////

  function clearSelectionCache() {
    delete ci.cache.mixed.selected
    delete ci.cache.mixed.selected_cp
  }

})( WhiteChili.ci, WhiteChili.ui, WhiteChili.load, WhiteChili.i18n, WhiteChili.throttle )
;( function( ci, ui, utils, map, req, regex, throttle, unthrottle ) {

  // Define local variables
  var settings  = [ 'fill', 'stroke', 'align', 'color', 'font', 'size' ]
    , clipboard = [ 'copy', 'paste',  'cut'                            ]

  // Add main controller
  ci.controller = {
    // Invoke given action
    perform: function( action, value, event ) {
      // store current event
      this.event = event

      // make sure action is camel cased
      action = utils.camel( utils.slug( action ) )

      // Log action and value
      ci.log( 'Calling action "' + action + ( value == null ? '"' : '" with value "' + value + '"' ) )

      // try to perform called action
      if ( this[action] )
        this[action]( value )
      else
        ci.log( 'Action "' + action + '" not yet implemented!', 'warn'  )

      // clean up
      delete this.event
    }

    // Frame creation
  , frame: function( type ) {
      // select layer for given type
      ci.doc().selectLayerFor( type )

      // create frame
      new WhiteChili.Frame( type )
    }

    // Replace image
  , replace: function() {
      this.upload()
    }

    // Set/unset image as background
  , background: function( value ) {
      var frame      = ci.api().selected( 0 )
        , constraint = frame.frameConstraints()
        , f_key      = frame.backgroundKey()
        , c_key      = 'originalFrameConstraints.' + frame.attr( 'id' )

      if ( value == 'set' ) {
        var background = ci.api().layers().where( 'name', 'layer.background' ).first()
          , page       = ci.api().selectedPage()

        // store original frame and constraint data
        ci.cache.mixed[f_key] = frame.geo()
        ci.cache.mixed[c_key] = constraint.geoLocks()

        // remove any existing background frames
        page.bgFrames().destroy()

        // set background template geometry to frame and move to background layer
        frame.attr( 'fitMode', 'proportional_outside' )
        frame.attr( 'layer', background )
        frame.attr( 'index', 0 )
        frame.attr( utils.portland( page.backgroundTemplate(), ci.status.orientation ) )

        // lock geometry
        constraint.lockGeo()
      } else {
        // restore original frame and constraint data
        frame.attr( ci.cache.mixed[f_key] )
        constraint.attr( ci.cache.mixed[c_key] )

        // delete unnecessary references
        delete ci.cache.mixed[f_key]
        delete ci.cache.mixed[c_key]
      }

      unthrottle( 'RecacheXML', function() {
        ui.settingsUpdate()
      })
    }

    // Define image cropping
  , crop: function( value ) {
      ci.doc().selected().attr( 'fitMode', value )

      unthrottle( 'RecacheXML', function() {
        ui.settingsUpdate()
      })
    }

    // Define image cropping anchor
  , cropAnchor: function( value ) {
      ci.doc().selected().attr( 'fitPoint', value )

      unthrottle( 'RecacheXML', function() {
        ui.settingsUpdate()
      })
    }

    // Show cropper
  , cropper: function( value ) {
      if ( value == 'hide' ) {
        ui.closeDialog( 'cropper' )
      } else {
        // invoke cropper
        ci.status.cropper = new WhiteChili.Cropper

        // open dialog
        ui.openDialog( ci.status.cropper, 'cropper' )

        // build cropper
        ci.status.cropper.build( 1000 )
      }
    }

    // Zoom cropper
  , cropperZoom: function( value ) {
      if ( ci.status.cropper ) {
        ci.status.cropper.zoomLevel( value )

        ui.cropperZoomUpdate()
      }
    }

    // Rotate cropper image
  , cropperRotate: function( value ) {
      if ( ci.status.cropper )
        ci.status.cropper.rotate( value )
    }

    // Replace cropper image
  , cropperReplace: function() {
      if ( ci.status.cropper ) {
        this.perform( 'dialog', 'hide' )
        this.perform( 'upload' )
      }
    }

    // Done cropping
  , cropperDone: function() {
      if ( ci.status.cropper ) {
        ci.status.cropper.done()

        ci.api().deselect()

        this.perform( 'cropper', 'hide' )
      }
    }

    // Cancel cropping
  , cropperCancel: function() {
      if ( ci.status.cropper ) {
        ci.status.cropper.vanish()

        ci.api().deselect()

        this.perform( 'cropper', 'hide' )
      }
    }

    // Show/hide upload screen
  , upload: function( type ) {
      // make sure frames are deselected if a new one needs to be created
      if ( type == 'new' )
        ci.doc().deselect()

      // show or hide the upload screen
      type == 'hide' ? ui.closeUpload() : ui.openUpload()
    }

    // Show dialog
  , dialog: function( dialog ) {
      dialog == 'hide' ? ui.closeDialog() : ui.openDialog( dialog )
    }

    // Create image frame
  , image: function( response ) {
      // get currently selected frame
      var frame = ci.api().selected( 0 )
        , image, tag

      // create a frame if no image frame is selected
      if ( ! frame || ! frame.attrIs( 'type', 'image' ) ) {
        // make sure everything is deselected
        ci.doc().deselect()

        // create new frame
        this.perform( 'frame', 'image' )
      }
      
      // add image to frame
      ci.execute( 'document.selectedFrame', 'LoadContentFromXmlString', response.xml )

      setTimeout( function() {
        // re-reference frame
        frame = ci.api().selected( 0 )

        // type cast response data
        image = utils.cast( response.image )

        // set fit mode
        frame.attr( 'fitMode', ci.api().viewPreferences().attr( 'defaultImageFit' ) )

        // // tag as pixel frame if required
        frame.isPixelFile() ? frame.addTag( 'is_pixel_file' ) : frame.removeTag( 'is_pixel_file' )

        // redefine frame size
        if ( frame.attrIs( 'fitMode', 'frame' ) ) {
          var ratio = frame.attr( 'preserveAspectRatio' )

          // set image width, height
          frame
            .attr( 'preserveAspectRatio', false )
            .attr( 'height', frame.attr( 'width' ) / ( image.width * 1 ) * ( image.height * 1 ) )
            .attr( 'preserveAspectRatio', ratio )
        }

        // make sure upload screen is closed
        ui.closeUpload()

      }, 1000 )
    }

    // Zooom to a given value
  , zoom: function( value ) {
      ci.doc().zoom( value )
    }

    // Select a given page
  , page: function( name ) {
      ci.doc().deselect().select( name )
    }

    // Select a given layout
  , layout: function( index ) {
      ci.doc().attr( 'selectedAlternateLayout', ci.doc().alternateLayouts( +index ).path )

      this.zoom( 'fit' )
    }

    // Redefine page orentation
  , orientation: function( value ) {
      if ( ci.config.allow_orientation ) {
        // store new orientation in status
        ci.status.orientation = value

        // set orientation
        ci.api().orientation( value )

        // update orientation menu
        ui.orientationUpdate( value )

        // redefine zoom
        this.perform( 'zoom', 'fit' )
      }
    }

    // Select previous page
  , pageup: function() {
      ci.doc().pages( Math.max( ci.doc().selectedPage( 'index' ) - 1, 0 ) ).select()
    }

    // Select next page
  , pagedown: function() {
      var pages = ci.doc().pages()

      pages.get( Math.min( ci.doc().selectedPage( 'index' ) + 1, pages.size() - 1 ) ).select()
    }

    // Select first page
  , pagefirst: function() {
      ci.doc().pages().first().select()
    }

    // Select last page
  , pagelast: function() {
      ci.doc().pages().last().select()
    }

    // show document naming input
  , name: function( value ) {
      if ( value == 'hide' )
        ui.closeDialog( 'name' )
      else
        ui.openDialog( new WhiteChili.Rename( value ), 'name' )
    }

    // send rename request to server
  , rename: function( input ) {
      // build post params
      var data = {
        name:    input.name
      , item_id: ci.doc().attr( 'id' )
      }

      // rename document
      ci.doc().attr( 'name', input.name )

      // rename document in local database
      req.post( 'rename', function( res ) {
        ci.controller.perform( input.next )
        ci.controller.perform( res.action, res.value )
      }, data )

      // update save button visibility
      ui.saveButtonUpdate( false )
    }

    // Save document
  , save: function() {
      ci.status.exiting = false

      if ( ci.doc().needsName() ) {
        ci.controller.perform( 'name', 'save' )

      } else {
        if ( ci.config.remote )
          req.post( 'save', function( res ) {
            ci.controller.perform( res.action, res.value )

            ci.doc().save()
          })
        else
          ci.doc().save()

        // update save button visibility
        ui.saveButtonUpdate( false )
      }
    }

    // Chekcout button
  , checkout: function() {
      ci.status.exiting = false

      if ( ci.doc().needsName() ) {
        ci.controller.perform( 'name', 'checkout' )

      } else if ( ci.status.skip_preflight || ci.api().importantPreflightResults().empty() ) {
        var thumbs = {}
          , pages  = ci.doc().pagesForOutput()
          , name   = ci.doc().attr( 'name' )
          , url

        // save document
        ci.status.exiting = true
        ci.doc().save()

        // collect page thumbs
        pages.each( function() {
          thumbs[this.attr( 'name' )] = utils.thumbFor( this.attr( 'name' ) )
        })

        // build url
        url = utils.urlWithParams( ci.config.remote.source + ci.config.remote.checkout, {
          thumbs:      btoa( JSON.stringify( thumbs ) )
        , pages:       pages.collect( 'name' ).join( ',' )
        , width:       ci.api().attr( 'width' ).value
        , height:      ci.api().attr( 'height' ).value
        , options:     ci.api().selectedOptionVariableIds()
        , sides:       ci.config.sides
        , description: ( regex.tmp_name.test( name ) ? '' : name )
        , auth_token:  ci.config.remote.auth_token
        })

        ci.controller.perform( 'modal', url )
      } else {
        ci.controller.perform( 'preflight' )
      }
    }

    // Back home
  , home: function() {
      ci.status.exiting = false

      if ( ci.config.remote )
        window.location.href = ci.config.remote.source + ci.config.remote.home
      else
        window.close()
    }

    // Preview download
  , download: function() {
      // save template then download all pages
      ci.doc().save().download()
    }

    // Destroy selected frames
  , destroy: function() {
      // make sure we're in cursor mode
      this.cursor( 'pointer' )

      // destroy selected frames
      ci.api().selected().destroy()
    }

    // Set color of attribute in selected frames
  , color: function( value ) {
      if ( ci.api().cursorIs( 'text' ) ) {
        // apply color to text selection
        ci.doc()
          .selectedText()
          .textFormat()
          .attr( 'color', value.color )

        ui.settingsUpdate()

      } else {
        ci.api().selected().each( function() {
          console.log(value)
          // set overprint
          this.attr( 'fillOverprint', value.overprint || false )

          // apply color to complete text element
          if ( this.attrIs( 'type', 'text' ) ) {
            this.textFlow( function( flow ) {
              // update color
              flow.find( 'p, [color]' ).attr({
                'color':    value.color.hex()
              , 'my_color': value.color.attr( 'id' )
              })

              return flow
            })

            ui.settingsUpdate()

          // apply color to fill or stroke
          } else {
            this[value.type]( value.color )
          }
        })
      }
    }

    // Set stroke width
  , strokeWidth: function( value ) {
      ci.api().selected().strokeWidth( value )
    }

    // Set stroke edge
  , strokeEdge: function( type ) {
      ci.api().selected().strokeEdge( type )
    }

    // Toggle hand cursor
  , handtoggle: function() {
      ci.api().cursorIs( 'pointer' ) ? this.cursor( 'hand' ) : this.cursor( 'pointer' )
    }

    // Switch back to initial cursor and clear any other modi
  , escape: function() {
      this.cursor( 'pointer' )
      ci.api().deselect()
      ui.closeMenu()
    }

    // Toggle help layer
  , help: function( value ) {
      ci.api().helpLayers().attr( 'visible', value == 'show' )
      ui.helpUpdate()
    }

    // Switch back to default mode
  , done: function() {
      ci.api().cursor( 'pointer' )
    }

    // Toggle viewer layer
  , viewer: function( value ) {
      ui.viewerUpdate( value )
    }

    // Control modal layer
  , modal: function( value ) {
      ui.modalUpdate( value )
    }

    // Align text
  , textAlign: function( value ) {
      // apply to text selection
      if ( ci.api().cursorIs( 'text' ) ) {
        ci.api()
          .selectedText()
          .textFormat()
          .attr( 'textAlign', value )

      // apply to complete frame
      } else {
        ci.api().selected().each( function() {
          this.textFlow( function( flow ) {
            // update text alignment
            flow.find( 'p, [textAlign]' ).attr({
              'textAlign': value
            })

            return flow
          })
        })
      }

      ui.settingsUpdate()
    }

    // Text size
  , textSize: function( value ) {
      if ( value != '' ) {
        // apply to text selection
        if ( ci.api().cursorIs( 'text' ) ) {
          ci.api()
            .selectedText()
            .textFormat()
            .attr( 'fontSize', value )

        // apply to complete frame
        } else {
          ci.api().selected().each( function() {
            this.textFlow( function( flow ) {
              flow.find( 'p, [fontSize]' )
                .attr({ 'fontSize': value })

              return flow
            })
          })
        }

        ui.settingsUpdate()
      }
    }

    // Change font to variant
  , font: function( id ) {
      // get font
      var font = ci.api().fonts().find( 'id', id )

      // apply to text selection
      if ( ci.api().cursorIs( 'text' ) ) {
        ci.api()
          .selectedText()
          .textFormat()
          .attr( 'font', font )

      // apply to text frame
      } else {
        ci.api().selected().each( function() {
          this.textFlow( function( flow ) {
            // update font family
            flow.find( '[fontFamily]' ).attr({
              'fontFamily': font.name()
            })

            return flow
          })
        })
      }

      ui.selectFontVariant( id )
      ui.settingsUpdate()
    }

    // Change paragraph style
  , paragraph: function( id ) {
      // get format
      var style  = ci.doc().paragraphStyles().find( 'id', id )
        , color  = style.attr( 'color' )
        , format = style.format().object()
        , a

      // remove null attributes
      for ( a in format )
        if ( format[a] == null )
          delete format[a]

      // add color attributes
      format.color    = color.hex()
      format.my_color = color.attr( 'id' )

      // apply to text selection
      if ( ci.api().cursorIs( 'text' ) ) {
        var text_format = ci.doc().selectedText().textFormat()

      // apply to text frame
      } else {
        ci.doc().selected().each( function() {
          this.textFlow( function( flow ) {
            // remove inline styling completely and add the new ones on paragraph level
            flow.find( 'p' ).each( function() {
              $( this )
                .html( $( this ).text() )
                .attr( format )
            })

            return flow
          })
        })
      }

      ui.settingsUpdate()
    }

    // Document information
  , info: function( value ) {
      if ( value == 'hide' )
        ui.closeDialog( 'info' )
      else
        ui.openDialog( new WhiteChili.Info, 'info' )
    }

    // Cursor change
  , cursor: function( value ) {
      if ( ci.api().cursorIsnt( value ) )
        ci.api().cursor( value )
    }

    // Organize selected frame
  , send: function( value ) {
      ci.api().selected()[value]()
    }

    // Open menu
  , menu: function( value ) {
      // close menu anyway
      ui.closeMenu()

      // only open menu again if it wasn't open before
      if ( ! ( ci.status.last_menu === value && ci.status.menu_open === true ) )
        ui.openMenu( value )

      // reset cursor
      if ( settings.indexOf( value ) == -1 && ci.api().cursorIsnt( 'pointer' ) )
        this.cursor( 'pointer' )
    }

    // open fonts menu
  , fonts: function( id ) {
      ui.openFont( id )
    }

    // control coach mark
  , coachMark: function( value ) {
      ui.sliderUpdate( 'coach-mark', value )
    }

    // control preview
  , preview: function( value ) {
      var edit = ci.api().viewPreferences().attrIs( 'viewMode', 'edit' )
        , o = ci.api().attr( 'width' ) < ci.api().attr( 'height' ) ? 'portrait' : 'landscape'
        , a = edit ? 'preview' : 'edit'
        , b = edit ? 'edit' : 'preview'

      // toggle guides
      ci.api().viewPreferences().attr( 'viewMode', edit ? 'preview' : 'edit' )

      // toggle ui
      $( 'html' ).toggleClass( 'no-ui preview-ui', edit )

      // switch mask layers
      ci.api().layers().where( 'name', 'layer.mask.' + a + '.' + o ).attr( 'visible', true )
      ci.api().layers().where( 'name', 'layer.mask.' + b + '.' + o ).attr( 'visible', false )

      // re-cache XML
      setTimeout( function() {
        ci.recache()
      }, 100 )
    }

    // show preflight warnings
  , preflight: function( value ) {
      value == 'hide' ? ui.closeDialog( 'preflight' ) : ui.openDialog( new WhiteChili.Preflight, 'preflight' )
    }

    // perform a live preflight
  , livePreflight: function() {
      new WhiteChili.LivePreflight( ci.api().importantPreflightResults() )
    }

    // redirect to destination
  , redirect: function( url ) {
      window.location.href = url
    }

    // log to console
  , console: function( message ) {
      console.log( message )
    }

    // Undo
  , undo: function() {
      ci.api().history().undo()
      ci.recache()
    }

    // Redo
  , redo: function() {
      ci.api().history().redo()
    }

  }

  // Clipbard actions
  clipboard.forEach( function( method ) {
    ci.controller[method] = function() {
      ci.api()[method]()
    }
  })

})( WhiteChili.ci, WhiteChili.ui, WhiteChili.utils, WhiteChili.map, WhiteChili.req, WhiteChili.regex, WhiteChili.throttle, WhiteChili.unthrottle )

;( function( ci, regex ) {

  // events to register
  var events = [
      'CursorChanged'
    , 'DocumentDimensionsChanged'
    , 'DocumentSaved' 
    , 'FrameAdded'
    , 'FrameAnchorChanged'
    , 'FrameBorderChanged'
    , 'FrameDeleted'
    , 'FrameDropShadowChanged'
    , 'FrameFillChanged'
    , 'FrameIndexChanged'
    , 'FrameMoveFinished'
    , 'FrameMoveInProgress'
    , 'FrameOpacityChanged'
    , 'FrameTextVariablesChanged'
    , 'FrameWrapChanged'
    , 'LayerVisibilityChanged'
    , 'PageAdded'
    , 'PageControlsLoaded'
    , 'PageDeleted'
    , 'SelectedAlternateLayoutChanged'
    , 'SelectedFrameChanged'
    , 'TextSelectionChanged'
    , 'UndoStackChanged'
    , 'VariableValueChanged'
    , 'ZoomChanged'
  ]

  // Event initializer
  WhiteChili.Events = function() {
    for ( var i = events.length - 1; i >= 0; i-- )
      ci.editor.AddListener( events[i] )
  }

  // PostMessage listener
  window.addEventListener( 'message', function( event ) {
    if ( ci.config.remote ){
      if ( event.origin.replace( regex.http, '' ) === ci.config.remote.source.replace( regex.http, '' ) ) {
        try {
          // parse data
          var data = JSON.parse( event.data )

          // store new auth token if given
          if ( data.auth_token )
            ci.config.remote.auth_token = data.auth_token

          ci.log( 'Received message from iframe' )

          // perform action
          ci.controller.perform( data.action, data.value )
          
        } catch ( e ) {
          ci.log( 'Error: ' + e, 'error' )  
        }
      } else {
        ci.log( 'Cross origin messages are not allowed!', 'error' )
      }
    }
  }, false )

  // register for global errors
  window.onerror = function( message, source, lineno, colno, error ) {
    console.log( message, source, lineno, colno, error )
    ci.log( 'Something went wrong', 'error' )
  }

  // register all events for debugging purposes
  // events = [
  //   'AdTypeOverlayViewerVisibilityChanged'
  // , 'AdTypeOverlayVisibilityChanged'
  // , 'AlternateLayoutAdded'
  // , 'AlternateLayoutDefinitionChanged'
  // , 'AlternateLayoutDeleted'
  // , 'AlternateLayoutIndexChanged'
  // , 'AlternateLayoutNameChanged'
  // , 'AlternateLayoutSelectionChanged'
  // , 'AnnotationAdded'
  // , 'AnnotationDefinitionChanged'
  // , 'AnnotationDeleted'
  // , 'AnnotationMoved'
  // , 'AutoTextCursorChanged'
  // , 'BarcodeFrameVariableChanged'
  // , 'BaseLineGridOptionsChanged'
  // , 'BaseLineGridVisibilityChanged'
  // , 'BleedVisibilityChanged'
  // , 'BookMarkAdded'
  // , 'BookMarkDefinitionChanged'
  // , 'BookMarkDeleted'
  // , 'BookMarkIndexChanged'
  // , 'BookMarkLinkSettingsChanged'
  // , 'BookMarkNameChanged'
  // , 'BookViewNavigationChanged'
  // , 'BookViewPreferencesChanged'
  // , 'BorderVisibilityChanged'
  // , 'ChainVisibilityChanged'
  // , 'CharacterStyleAdded'
  // , 'CharacterStyleColorChanged'
  // , 'CharacterStyleDefinitionChanged'
  // , 'CharacterStyleDeleted'
  // , 'CharacterStyleIndexChanged'
  // , 'CharacterStyleNameChanged'
  // , 'CharacterStyleUnderLineChanged'
  // , 'ColorAdded'
  // , 'ColorDefinitionChanged'
  // , 'ColorDeleted'
  // , 'ColorIndexChanged'
  // , 'ColorNameChanged'
  // , 'CopyFrameChanged'
  // , 'CursorChanged'
  // , 'CustomButtonAdded'
  // , 'CustomButtonDefinitionChanged'
  // , 'CustomButtonDeleted'
  // , 'CustomButtonIndexChanged'
  // , 'DataSourceChanged'
  // , 'DataSourceRecordChanged'
  // , 'DisplayQualityChanged'
  // , 'DocumentActionChanged'
  // , 'DocumentAdSettingsChanged'
  // , 'DocumentAnnotationSettingsChanged'
  // , 'DocumentBleedChanged'
  // , 'DocumentClosing'
  // , 'DocumentConstraintsChanged'
  // , 'DocumentDefaultDirectoriesChanged'
  // , 'DocumentDimensionsChanged'
  // , 'DocumentDirtyStateChanged'
  // , 'DocumentEventActionAdded'
  // , 'DocumentEventActionDefinitionChanged'
  // , 'DocumentEventActionDeleted'
  // , 'DocumentEventActionIndexChanged'
  // , 'DocumentEventActionNameChanged'
  // , 'DocumentFlapChanged'
  // , 'DocumentFullyLoaded'
  // , 'DocumentLinkSettingsChanged'
  // , 'DocumentLoaded'
  // , 'DocumentLoading'
  // , 'DocumentSaved'
  // , 'DocumentSaveRequested'
  // , 'DocumentSerialized'
  // , 'DocumentSlugChanged'
  // , 'DocumentSpineChanged'
  // , 'FontAdded'
  // , 'FontDeleted'
  // , 'FontIndexChanged'
  // , 'FontLoaded'
  // , 'FontNameChanged'
  // , 'FrameAdded'
  // , 'FrameAdNotesChanged'
  // , 'FrameAdSettingsChanged'
  // , 'FrameAlternateLayoutSelectionChanged'
  // , 'FrameAnchorChanged'
  // , 'FrameBarcodeSettingsChanged'
  // , 'FrameBorderChanged'
  // , 'FrameChainChanged'
  // , 'FrameConstraintsChanged'
  // , 'FrameDeleted'
  // , 'FrameDisplayQualityChanged'
  // , 'FrameDropShadowChanged'
  // , 'FrameFillChanged'
  // , 'FrameHandlePreferencesChanged'
  // , 'FrameIndexChanged'
  // , 'FrameLayerChanged'
  // , 'FrameLinkSettingsChanged'
  // , 'FrameMoved'
  // , 'FrameMoveFinished'
  // , 'FrameMoveInProgress'
  // , 'FrameMoveRegionChanged'
  // , 'FrameOpacityChanged'
  // , 'FrameRotated'
  // , 'FrameTagChanged'
  // , 'FrameTextVariablesChanged'
  // , 'FrameWrapChanged'
  // , 'GridSettingsChanged'
  // , 'HelpLayerVisibilityChanged'
  // , 'HiddenCharacterVisibilityChanged'
  // , 'IconSetDownloaded'
  // , 'IconSetDownloadStarted'
  // , 'IconSetFullDownloaded'
  // , 'Idle'
  // , 'ImageFrameVariableDefinitionChanged'
  // , 'InlineFrameAdded'
  // , 'InlineFrameDeleted'
  // , 'InlinePanelVisibilityChanged'
  // , 'InlineToolBarVisibilityChanged'
  // , 'LanguageCacheRequiresClearing'
  // , 'LanguageDownloaded'
  // , 'LanguageDownloadStarted'
  // , 'LayerAdded'
  // , 'LayerAlternateLayoutSelectionChanged'
  // , 'LayerContentTypeChanged'
  // , 'LayerDeleted'
  // , 'LayerIndexChanged'
  // , 'LayerMoveRegionChanged'
  // , 'LayerNameChanged'
  // , 'LayerOpacityChanged'
  // , 'LayerPrintingChanged'
  // , 'LayerTextWrapChanged'
  // , 'LayerVisibilityChanged'
  // , 'LayerVisibleVariableChanged'
  // , 'LinkSettingsChanged'
  // , 'LinkSettingsVariableChanged'
  // , 'MarginVisibilityChanged'
  // , 'MeasurementUnitsChanged'
  // , 'MoveRegionVisibilityChanged'
  // , 'ObjectReplaceRequested'
  // , 'PageAdded'
  // , 'PageAlternateLayoutSelectionChanged'
  // , 'PageBackgroundColorChanged'
  // , 'PageBookMarkSettingsChanged'
  // , 'PageCanvasScrollChanged'
  // , 'PageControlsLoaded'
  // , 'PageDeleted'
  // , 'PageIncludeInLayoutChanged'
  // , 'PageIncludeInOutputChanged'
  // , 'PageIndexChanged'
  // , 'PageLabelVisibilityChanged'
  // , 'PageMarginsChanged'
  // , 'PageModeChanged'
  // , 'PageRulerVisibilityChanged'
  // , 'PageSectionChanged'
  // , 'PageVisibilityModeChanged'
  // , 'PageVisibleVariableChanged'
  // , 'PaginationEditorVisibilityChanged'
  // , 'PanelAdded'
  // , 'PanelControlAdded'
  // , 'PanelControlDeleted'
  // , 'PanelControlIndexChanged'
  // , 'PanelControlTitleChanged'
  // , 'PanelControlViewPreferencesChanged'
  // , 'PanelControlVisibilityChanged'
  // , 'PanelDeleted'
  // , 'PanelIndexChanged'
  // , 'PanelTitleChanged'
  // , 'PanelViewPreferencesChanged'
  // , 'ParagraphStyleAdded'
  // , 'ParagraphStyleBackgroundDefinitionChanged'
  // , 'ParagraphStyleColorChanged'
  // , 'ParagraphStyleDefinitionChanged'
  // , 'ParagraphStyleDeleted'
  // , 'ParagraphStyleDiagonalRuleDefinitionChanged'
  // , 'ParagraphStyleIndexChanged'
  // , 'ParagraphStyleNameChanged'
  // , 'ParagraphStyleRuleDefinitionChanged'
  // , 'PathAdded'
  // , 'PathChanged'
  // , 'PathCursorChanged'
  // , 'PathDeleted'
  // , 'PathPointAdded'
  // , 'PathPointDeleted'
  // , 'PathPointMoved'
  // , 'PDFSettingsChanged'
  // , 'PluginAdded'
  // , 'PluginDeleted'
  // , 'PluginIndexChanged'
  // , 'PluginNameChanged'
  // , 'PreflightPreferencesChanged'
  // , 'PreflightResultAdded'
  // , 'PreflightResultDeleted'
  // , 'PreflightResultDescriptionChanged'
  // , 'RotateViewChanged'
  // , 'SearchCompleted'
  // , 'SearchResultAdded'
  // , 'SearchResultDeleted'
  // , 'SearchResultsSettingsChanged'
  // , 'SearchResultsStatusChanged'
  // , 'SearchStarted'
  // , 'SectionBreakVisibilityChanged'
  // , 'SectionVisibleVariableChanged'
  // , 'SelectedAlternateLayoutChanged'
  // , 'SelectedAnnotationChanged'
  // , 'SelectedBookMarkChanged'
  // , 'SelectedCharacterStyleChanged'
  // , 'SelectedColorChanged'
  // , 'SelectedDataSourceChanged'
  // , 'SelectedDocumentEventActionChanged'
  // , 'SelectedFontChanged'
  // , 'SelectedFrameChanged'
  // , 'SelectedImportWarningChanged'
  // , 'SelectedLayerChanged'
  // , 'SelectedPageChanged'
  // , 'SelectedPageMasterChanged'
  // , 'SelectedPanelChanged'
  // , 'SelectedParagraphStyleChanged'
  // , 'SelectedParagraphStyleListSettingsChanged'
  // , 'SelectedPathChanged'
  // , 'SelectedPathPointChanged'
  // , 'SelectedPluginChanged'
  // , 'SelectedSearchResultChanged'
  // , 'SelectedTableCellChanged'
  // , 'SelectedTableColumnChanged'
  // , 'SelectedTableRowChanged'
  // , 'SelectedVariableChanged'
  // , 'SelectionContentChanged'
  // , 'SlugVisibilityChanged'
  // , 'SnapSettingsChanged'
  // , 'SnippetVariablesChanged'
  // , 'SourceViewChanged'
  // , 'SpellCheckDictionariesLoaded'
  // , 'SpellCheckHunSpellFilesLoaded'
  // , 'SpellCheckPreferencesChanged'
  // , 'StoryFrameChanged'
  // , 'StoryViewPreferencesChanged'
  // , 'TableBorderAndFillChanged'
  // , 'TableCellBorderAndFillChanged'
  // , 'TableCellDefinitionChanged'
  // , 'TableColumnAdded'
  // , 'TableColumnBorderAndFillChanged'
  // , 'TableColumnDefinitionChanged'
  // , 'TableColumnDeleted'
  // , 'TableColumnIndexChanged'
  // , 'TableFrameBorderAndFillChanged'
  // , 'TableRowAdded'
  // , 'TableRowBorderAndFillChanged'
  // , 'TableRowDefinitionChanged'
  // , 'TableRowDeleted'
  // , 'TableRowIndexChanged'
  // , 'TabRulerControlVisibilityChanged'
  // , 'TabRulerMarkerSelectionChanged'
  // , 'TabRulerVisibilityChanged'
  // , 'TextColumnGuideVisibilityChanged'
  // , 'TextFormatChanged'
  // , 'TextFrameCopyFittingAppliedPercentageChanged'
  // , 'TextFrameCustomBaseLineGridChanged'
  // , 'TextFrameFlowComposed'
  // , 'TextFrameLinksUpdated'
  // , 'TextFramePathSettingChanged'
  // , 'TextFrameReadOnlyFlowComposed'
  // , 'TextFrameStrokeChanged'
  // , 'TextSelectionChanged'
  // , 'ToolBarItemAdded'
  // , 'ToolBarItemDeleted'
  // , 'ToolBarItemIndexChanged'
  // , 'ToolBarItemViewPreferencesChanged'
  // , 'UndoStackChanged'
  // , 'VariableAdded'
  // , 'VariableDefinitionChanged'
  // , 'VariableDeleted'
  // , 'VariableIndexChanged'
  // , 'VariableNameChanged'
  // , 'VariableRowAdded'
  // , 'VariableRowDeleted'
  // , 'VariableRowIndexChanged'
  // , 'VariableTextDirectionChanged'
  // , 'VariableValueChanged'
  // , 'VariableVisibilityChanged'
  // , 'ViewModeChanged'
  // , 'WarningVisibilityChanged'
  // , 'WorkSpaceChanged'
  // , 'WorkSpaceColorChanged'
  // , 'WorkSpaceInfoChanged'
  // , 'WrapMarginVisibilityChanged'
  // , 'ZoomAfterChange'
  // , 'ZoomBeforeChange'
  // , 'ZoomChanged'
  // , 'ZoomSettingsChanged'
  // ]

})( WhiteChili.ci, WhiteChili.regex )
;( function( ci, utils, regex ) {

  // Model class
  WhiteChili.Model = function() {}

  // Add resource trait
  utils.extend( WhiteChili.Model, WhiteChili.Traits.Resource )

  // Cacheable by default
  WhiteChili.Model.prototype.cacheable = true

  // Get to source object
  WhiteChili.Model.prototype.object = function( selector ) {
    // ensure selector
    selector = selector || this.selector
    
    if ( this.cacheable ) {
      if ( ! ci.cache.api[selector] )
        ci.cache.api[selector] = ci.get( selector )

      return ci.cache.api[selector]
    }

    return ci.get( selector )
  }

  // Get raw chili object
  WhiteChili.Model.prototype.raw = function() {
    return ci.query( this.path() )
  }

  // Get an attribute
  WhiteChili.Model.prototype.attr = function( key, value ) {
    // act as a setter with multiple key/value pairs
    if ( typeof key == 'object' )
      for ( value in key )
        this.attr( value, key[value] )

    // act as a setter with one key/value pair
    if ( value != null )
      ci.property( this.path(), key, value )

    // act as a full getter
    else if ( key == null )
      return utils.cast( utils.attr( this.object().get( 0 ), null, true ) )

    // make exception for index getter
    else if ( key == 'index' )
      return this.index()      

    // act as a getter without a value
    else if ( value == null )
      return utils.cast( this.object().attr( key ), key, true )

    return this
  }

  // Test the value of an attribute
  WhiteChili.Model.prototype.attrIs = function( key, value ) {
    return this.attr( key ) == value
  }

  // Test the presence of the attriubte value in a list
  WhiteChili.Model.prototype.attrIn = function( key, values ) {
    return values.indexOf( this.attr( key ) ) > -1
  }

  // Build color from attribute
  WhiteChili.Model.prototype.attrColor = function( subject ) {
    var item_id = this.attr( subject + 'Color' )

    if ( item_id )
      return new WhiteChili.Models.Color( 'document > colors > item[id=' + item_id + ']' )

    return ci.api().defaultColor( subject )
  }

  // Dynamically build a single model relation
  WhiteChili.Model.prototype.hasOne = function( name, options ) {
    // ensure options
    options = options || {}

    // prepare variables
    var studly   = utils.studly( name )
      , camel    = utils.camel( name )
      , entity   = ensureEntity( studly )
      , selector = ( options.selector || this.selector ) + ' > ' + camel
    
    // loader method
    this[camel] = function( a ) {
      // otherwise build a string or model based entity
      var model = new entity( selector )

      return typeof a === 'string' ? model.attr( a ) : model
    }

    return this
  }

  // Dynamically build a collection model relation
  WhiteChili.Model.prototype.hasMany = function( name, options ) {
    // ensure options
    options = options || {}

    // prepare variables
    var studly   = utils.studly( options.model || name )
      , camel    = utils.camel( options.model || name )
      , plural   = utils.plural( camel )
      , method   = utils.plural( utils.camel( name ) )
      , entity   = ensureEntity( studly )
      , selector = options.selector || ( this.selector + ' > ' + plural + ' > item' )
    
    // loader
    this[method] = function( i ) {
      // if an index or uuid is given, return the resource at that index or uuid
      if ( typeof i === 'string' )
        return new entity( selector + '[id=' + i + ']' )
      
      // count total resources
      var proto = WhiteChili.Collections[studly]
        , set   = proto ? new proto : new WhiteChili.Set

      // collect all resources
      ci.get( selector ).each( function() {
        set.add( new entity( selector + '[id=' + $( this ).attr( 'id' ) + ']' ) )
      })

      // return a given index within the set
      if ( typeof i === 'number' )
        return set.get( i )

      // ensure sort order if required
      if ( options.order )
        set.sortBy( options.order )
      
      return set
    }

    return this
  }

  // Dynamically build a parent relation
  WhiteChili.Model.prototype.belongsTo = function( name ) {
    // prepare variables
    var selector = this.selector
      , studly   = utils.studly( name )
      , camel    = utils.camel( name )
      , plural   = utils.plural( camel )
      , entity   = ensureEntity( studly )
      , parent   = selector + ' < parent'

    this[camel] = function() {
      // make sure parent is cached
      if ( ! ci.cache.api[parent] ) {
        // prepare loacal varibales
        var scope = plural + ' > item'
          , id    = ci.get( selector ).closest( scope ).attr( 'id' )

        // get parent base selector
        selector = selector.split( scope )[0]

        ci.cache.api[parent] = new entity( selector + ' ' + scope + '[id=' + id + ']' )
      }
      
      return ci.cache.api[parent]
    }

    return this
  }

  // Get index of node 
  WhiteChili.Model.prototype.index = function( index ) {
    // act as getter
    if ( typeof index !== 'number' )
      return ci.get( this.selector ).index()

    // continue as setter
    if ( this.index() != index )
      this.attr( 'index', index )

    return this
  }

  // Dynamically build Chili object path
  WhiteChili.Model.prototype.path = function() {
    return utils.s2p( this.selector )
  }

  // Test existance of resource
  WhiteChili.Model.prototype.exists = function() {
    return ci.get( this.selector ).size() != 0
  }

  // Return path when converted to string
  WhiteChili.Model.prototype.toString = function() {
    return this.path()
  }

  //////////////////////////////
  ////////// helpers ///////////
  //////////////////////////////

  // Ensure a prototype exists for the given name
  function ensureEntity( name ) {
    if ( ! WhiteChili.Models[name] ) {
      WhiteChili.Models[name] = function( selector ) {
        this.selector = selector
      }

      // Inherit from WhiteChili.Model
      WhiteChili.Models[name].prototype = new WhiteChili.Model
    }

    return WhiteChili.Models[name]
  }

})( WhiteChili.ci, WhiteChili.utils, WhiteChili.regex )
;( function( ci, utils ) {

  // Object class
  WhiteChili.Object = function( path ) {
    if ( path )
      this.object = ci.query( path )
  }

  // Add resource trait
  utils.extend( WhiteChili.Object, WhiteChili.Traits.Resource )

  // Get an attribute
  WhiteChili.Object.prototype.attr = function( key, value ) {
    // act as a setter with multiple key/value pairs
    if ( typeof key == 'object' )
      for ( value in key )
        this.attr( value, key[value] )

    // act as a setter with one key/value pair
    if ( value != null )
      ci.property( this.path(), key, value )

    // act as a full getter
    else if ( key == null )
      return utils.cast( this.object, null, true )

    // act as a getter without a value
    else if ( value == null )
      return utils.cast( this.object[key], key, true )

    return this
  }

  // Test the value of an attribute
  WhiteChili.Object.prototype.attrIs = function( key, value ) {
    return this.attr( key ) == value
  }

  // Test the presence of the attriubte value in a list
  WhiteChili.Object.prototype.attrIn = function( key, values ) {
    return values.indexOf( this.attr( key ) ) > -1
  }

  // Return javascript descriptor
  WhiteChili.Object.prototype.toString = function() {
    return this.object.javaScriptDescriptor
  }

  // Return path
  WhiteChili.Object.prototype.path = function() {
    return this.toString()
  }

})( WhiteChili.ci, WhiteChili.utils )
;( function( ci, utils, regex ) {
  
  // Resource class
  WhiteChili.Resource = function() {}

  // Add resource trait
  utils.extend( WhiteChili.Resource, WhiteChili.Traits.Resource )

  // Get to object
  WhiteChili.Resource.prototype.object = function( path ) {
    return ci.query( path || this.path ) || {}
  }

  // Get an attribute
  WhiteChili.Resource.prototype.attr = function( key, value ) {
    // act as a setter with multiple key/value pairs
    if ( typeof key == 'object' )
      for ( value in key )
        this.attr( value, key[value] )
    
    // act as a setter with one key/value pair
    else if ( value != null )
      ci.property( this.path, key, value )

    // act as a full getter without a key
    else if ( key == null )
      return utils.cast( this.object(), key )

    // act as a getter without a value
    else if ( value == null )
      return utils.cast( this.object()[key], key )

    return this
  }

  // Test the value of an attribute
  WhiteChili.Resource.prototype.attrIs = function( key, value ) {
    return this.attr( key ) == value
  }

  // Test the presence of the attriubte value in a list
  WhiteChili.Resource.prototype.attrIn = function( key, values ) {
    return values.indexOf( this.attr( key ) ) > -1
  }

  // Dynamically build a selected resource relation
  WhiteChili.Resource.prototype.hasSelected = function( name ) {
    var studly = utils.studly( name )
      , entity = ensureEntity( studly )

    // loader
    this['selected' + studly] = function( a ) {
      var resource = new entity( this.attr( 'selected' + studly ) )

      return typeof a === 'string' ? resource.attr( a ) : resource
    }

    return this
  }

  // Dynamically build a selected resource relation
  WhiteChili.Resource.prototype.hasOne = function( name, options ) {
    // ensure options
    options = options || {}

    // prepare variables
    var studly = utils.studly( name )
      , camel  = utils.camel( name )
      , entity = ensureEntity( studly )
      , root   = this.path
    
    // loader method
    this[camel] = function( a ) {
      // if a root is defined, use it to build the path directly
      if ( options.root )
        return new entity( options.root + '.' + camel )

      // otherwise build a string or resource based entity
      var resource = new entity( this.attr( camel ) )

      return typeof a === 'string' ? resource.attr( a ) : resource
    }

    // make sure build constructor is present on relation
    if ( typeof entity.prototype.build != 'function' ) {
      entity.prototype.build = function( o ) {
        // if the value is an empty cp_object, build a source from name
        if ( o == 'cp_object:' )
          return root + '.' + camel

        return o
      }
    }

    return this
  }

  // Dynamically build a collection resource relation
  WhiteChili.Resource.prototype.hasMany = function( name, options ) {
    // ensure options
    options = options || {}

    // prepare variables
    var studly = utils.studly( name )
      , camel  = utils.camel( name )
      , plural = utils.plural( camel )
      , entity = ensureEntity( studly )
      , root   = options.root || ( this.path + '.' + plural )
    
    // loader
    this[plural] = function( i ) {
      // if an index or uuid is given, return the resource at that index or uuid
      if ( typeof i === 'number' || typeof i === 'string' && i.length == 36 )
        return new entity( root + '[' + i + ']' )
      
      // count total resources
      var path, object
        , proto = WhiteChili[studly + 'Set']
        , set   = proto ? new proto : new WhiteChili.Set
        , total = ci.query( root ).length

      // collect all resources
      for ( i = 0; i < +total; i++ )
        set.add( new entity( root + '[' + i + ']' ) )
      
      return set
    }

    // make sure build constructor is present on relation
    if ( typeof entity.prototype.build != 'function' ) {
      entity.prototype.build = function( o ) {
        // if a string is given, return as is
        if ( typeof o === 'string' && ! o.match( regex.cp_object ) )
          return o

        // if the value is an empty cp_object, build a source from name
        if ( o == 'cp_object:' )
          return root + '.' + camel

        // if the given object is null, build a new instance
        var i = ci.query( root ).length

        // create
        o = ci.execute( root, 'Add' )

        // access property
        return root + '[' + o.id + ']'
      }
    }

    // relation builder
    this['build' + studly] = function( a, b ) {
      return new entity( a, b )
    }

    return this
  }
  
  // Test existance of resource
  WhiteChili.Resource.prototype.exists = function() {
    return !! this.object()
  }

  // Convert object to cp_object string
  WhiteChili.Resource.prototype.toString = function() {
    return this.path
  }

  //////////////////////////////
  ////////// helpers ///////////
  //////////////////////////////

  // Ensure a prototype exists for the given name
  function ensureEntity( name ) {
    if ( ! WhiteChili[name] ) {
      WhiteChili[name] = function( ref ) {
        // reference chili object
        this.path = this.build( ref )
      }

      // Inherit from WhiteChili.Resource
      WhiteChili[name].prototype = new WhiteChili.Resource
    }

    return WhiteChili[name]
  }

})( WhiteChili.ci, WhiteChili.utils, WhiteChili.regex )



















 ;( function( utils ) {

  // Class for handling members sets
  WhiteChili.Set = function() {
    this.members = []
  }

  // Iterate over all members
  WhiteChili.Set.prototype.each = function( block ) {
    for ( var i = 0, il = this.members.length; i < il; i++ )
      block.apply( this.members[i], [ i, this.members ])

    return this
  }

  // Proxy attribute method as a setter on members
  WhiteChili.Set.prototype.attr = function( key, value ) {
    for ( var i = 0, il = this.members.length; i < il; i++ )
      this.members[i].attr( key, value )

    return this
  }

  // Collect arbitrary data from members
  WhiteChili.Set.prototype.collect = function( block ) {
    // if a string value is given, assume it is an attribute name
    if ( typeof block == 'string' )
      return this.collect( function() { return this.attr( block ) })

    // perform block-based collection
    for ( var v, i = 0, il = this.members.length, d = []; i < il; i++ ) {
      // get value
      v = block.apply( this.members[i], [ i, this.members ])

      // make sure value exists before pushing it to the data array
      if ( v != null ) d.push( v )
    }
    
    return d
  }
  
  // Reduce members conditionally (destructive)
  WhiteChili.Set.prototype.reduce = function( block ) {
    // perform block-based collection
    for ( var v, i = 0, il = this.members.length, d = []; i < il; i++ ) {
      // make sure value exists before pushing it to the data array
      if ( block.apply( this.members[i], [ i, this.members ]) ) 
        d.push( this.members[i] )
    }

    // store new members
    this.members = d

    return this
  }

  // Find all members with a specific attribute value
  WhiteChili.Set.prototype.where = function( key, value ) {
    return this.reduce( function() {
      return this.attr( key ) == value
    })
  }

  // Find all members within an array of attribute values
  WhiteChili.Set.prototype.whereIn = function( key, values ) {
    return this.reduce( function() {
      return values.indexOf( this.attr( key ) ) > -1
    })
  }

  // Find a single member by a specific attribute value
  WhiteChili.Set.prototype.find = function( key, value ) {
    return this.where( key, value ).first()
  }

  // Make members in set unique (destructive)
  WhiteChili.Set.prototype.unique = function() {
    for ( var i = 0, il = this.members.length, s = [], m = []; i < il; i++) {
      // only add unique paths
      if ( s.indexOf( this.members[i].path ) == -1 ) {
        s.push( this.members[i].path )
        m.push( this.members[i] )
      }
    }
    
    // store new members
    this.members = m
    
    return this
  }

  // Reverse members
  WhiteChili.Set.prototype.reverse = function() {
    this.members.reverse()

    return this
  }

  // Sort by attribute value
  WhiteChili.Set.prototype.sortBy = function( k ) {
    this.members.sort( function( a, b ) {
      a = a.attr( k )
      b = b.attr( k )

      return typeof a == 'number' ? a - b : ( ( a < b ) ? -1 : ( ( a > b ) ? 1 : 0 ) )
    })

    return this
  }
  
  // Add members to set
  WhiteChili.Set.prototype.add = function( member ) {
    // if an array is provided, add members individually
    if ( Array.isArray( member ) ) {
      for ( var m in member ) this.add( m )

    // if a set instance is given, add the set memers
    } else if ( member instanceof WhiteChili.Set ) {
      this.members = this.members.concat( member.members )

    // add members to set
    } else {
      // make sure member is wrapped in member class, if it is defined
      if ( this.member_class && ! ( member instanceof this.member_class ) )
        member = new this.member_class( member )

      // add member
      this.members.push( member )

      // make circular reference to set instance on member
      if ( member.sets && member.sets.indexOf( this ) == -1 )
        member.sets.push( this )
    }
    
    return this
  }

  // Remove member from set
  WhiteChili.Set.prototype.remove = function( member ) {
    var i = this.index( member )
    
    // remove given child
    if ( i > -1 )
      this.members.splice( i, 1 )

    return this
  }

  // Retuns index of given member in set
  WhiteChili.Set.prototype.index = function( member ) {
    return this.members.indexOf( member )
  }

  // Get member at given index
  WhiteChili.Set.prototype.get = function( index ) {
    return this.members[index]
  }

  // Checks if a given member is present in set
  WhiteChili.Set.prototype.has = function( member ) {
    return this.index( member ) >= 0
  }

  // Get first member
  WhiteChili.Set.prototype.first = function() {
    return this.get( 0 )
  }

  // Get last member
  WhiteChili.Set.prototype.last = function() {
    return this.get( this.members.length - 1 )
  }

  // Default value
  WhiteChili.Set.prototype.valueOf = function() {
    return this.members
  }

  // Restore to defaults
  WhiteChili.Set.prototype.clear = function() {
    // initialize store
    this.members = []

    return this
  }

  // Get size of members array
  WhiteChili.Set.prototype.size = function() {
    return this.members.length
  }

  // Test if set is empty
  WhiteChili.Set.prototype.empty = function() {
    return this.members.length == 0
  }

  // Test if set is not empty
  WhiteChili.Set.prototype.any = function() {
    return ! this.empty()
  }

  // Test if set has only one item
  WhiteChili.Set.prototype.one = function() {
    return this.members.length == 1
  }

  // Test if set has more than one item
  WhiteChili.Set.prototype.many = function() {
    return this.members.length > 1
  }

})( WhiteChili.utils )




;( function( ci ) {
  // get windows
  var window_1 = window
    , window_2 = document.getElementById( 'editor' ).contentWindow

  // Enable shortcuts
  WhiteChili.Shortcuts = function() {
    // activate shortcuts
    this.activate()

    // define scope
    this.scope( 'main' )
  }

  WhiteChili.Shortcuts.prototype.activate = function() {
    // save document
    shortcut( '⌘+s, ctrl+s', 'main', 'save' )
    shortcut( '⌘+s, ctrl+s', 'text', 'save' )

    // zoom fit
    shortcut( '⌘+=, ctrl+=', 'main', 'zoom', 'fit' )

    // zoom in
    shortcut( '⌘+plus, ctrl+plus', 'main', 'zoom', 'in' )

    // zoom out
    shortcut( '⌘+minus, ctrl+minus', 'main', 'zoom', 'out' )

    // delete frame
    //-D shortcut( 'backspace', 'main', 'destroy' )

    // undo
    shortcut( '⌘+z, ctrl+z', 'main', 'undo' )
    shortcut( '⌘+z, ctrl+z', 'text', 'undo' )

    // redo
    shortcut( '⌘+shift+z, ctrl+shift+z', 'main', 'redo' )
    shortcut( '⌘+shift+z, ctrl+shift+z', 'text', 'redo' )

    // copy
    shortcut( '⌘+c, ctrl+c', 'main', 'copy' )

    // cut
    shortcut( '⌘+x, ctrl+x', 'main', 'cut' )

    // paste
    shortcut( '⌘+v, ctrl+v', 'main', 'paste' )

    // page up
    shortcut( 'pageup', 'main', 'pageup' )

    // page down
    shortcut( 'pagedown', 'main', 'pagedown' )

    // first page
    shortcut( 'home', 'main', 'pagefirst' )

    // last page
    shortcut( 'end', 'main', 'pagelast' )

    // toggle hand/pointer
    //-F shortcut( 'space', 'main', 'handtoggle' )

    // escape back to initial modi
    shortcut( 'escape', 'main', 'escape' )
    shortcut( 'escape', 'text', 'escape' )
    shortcut( 'escape', 'modal', 'dialog', 'hide' )

  }

  // define scope
  WhiteChili.Shortcuts.prototype.scope = function( scope ) {
    window_1.key.setScope( scope )
    window_2.key.setScope( scope )
  }

  //////////////////////////////
  ////////// helpers ///////////
  //////////////////////////////

  // create shortcut
  function shortcut( keys, scope, action, value ) {
    // define handler
    var handler = function( event ) {
      ci.controller.perform( action, value, event )

      return false
    }

    // attach to windows
    window_1.key( keys, scope, handler )
    window_2.key( keys, scope, handler )
  }

})( WhiteChili.ci )
;( function( ci, utils ) {

  // Class for building a coach mark
  WhiteChili.CoachMark = function() {
    // get and build data and wrappers
    var wrapper = $( '.coach-mark' )
      , coach   = ci.config.coach_mark
      , images  = wrapper.find( '.images' )
      , next    = wrapper.find( '.buttons .next' )

    // add images
    coach.images.forEach( function( image, i ) {
      // build full image url
      var src = utils.s3( 'coach_marks', coach.source_dir_name, image.source_file_name, { cache_key: image.cache_key })

      // create image
      $( '<li class="image plus-size coach-mark-slide-' + i + '">slide ' + i + '</li>' )
        .appendTo( images )

      // create button
      $( '<li class="button enabled" data-wc-action="coach_mark" data-wc-value="' + i + '">slide ' + i + '</li>' )
        .insertBefore( next )
        .toggleClass( 'active', i == 0 )

      // add css
      ci.css.push( '.coach-mark-slide-' + i + ' { background-image: url( "' + src + '" ); left: ' + ( i * 100 ) + '%; }' )
    })
  }

})( WhiteChili.ci, WhiteChili.utils )
;( function( ci, i18n ) {
  // define prude color lists
  var prude = [ 'stroke', 'color' ]
  
  // Class for building a color palette
  WhiteChili.ColorComponent = function() {
    // define color classes
    ci.api().colors().each( function() {
      ci.css.push( this.css() )
    })
  }

  // Generate palette html
  WhiteChili.ColorComponent.prototype.html = function( purpose ) {
    // build wrapper
    var wrapper = $( '<ul class="colors section"></ul>' )

    // add colors
    ci.api().frameContentConstraints().colorsFor( purpose ).each( function() {
      // filter out some unsupported stroke and text colors
      if ( prude.indexOf( purpose ) > -1 && ( this.none() || this.gradient() ) )
        return false

      // build color element
      var parts  = this.attr( 'name' ).split( '.' )
        , name   = /^u[23456]{1}$/.test( parts[2] ) ? i18n.t( 'colors.' + parts[1] ) : this.attr( 'name' )
        , button = $( '<li class="color ' + this.className( true ) + '"></li>' )
        , label  = $( '<span class="color-label">' + name + '</span>' ).appendTo( button )
        , color  = this

      // add data attributes
      button.on( 'click', function() {
        var parent = $( this ).closest( '[data-wc-action=menu]' )
          , type   = parent.data( 'wc-value' )
        
        // perform action
        ci.controller.perform( 'color', {
          type:      type
        , color:     color
        , overprint: parts.indexOf( 'overprint' ) > -1
        })
      })

      // add color
      wrapper.append( button )
    })

    return wrapper
  }

})( WhiteChili.ci, WhiteChili.i18n )
;( function( ci, ui, map, i18n, regex, utils ) {

  // Class for building a croppre component
  WhiteChili.Cropper = function() {
    this.wrapper = $( '<div class="cropper modal">' )
    this.dialog  = $( '.dialog-box' )
  }

  // Generate cropper html
  WhiteChili.Cropper.prototype.html = function() {
    var button, menu

    // build menu
    this.footer = $( '<footer>' ).addClass( 'skin-light skin-nested' ).appendTo( this.wrapper )
    menu = $( '<ul>' ).addClass( 'buttons menu' ).appendTo( this.footer )

    // build zoom-out button
    button = $( '<li data-wc-action="cropper_zoom" data-wc-value="out">' ).addClass( 'button enabled' ).appendTo( menu )
    $( '<label>' ).addClass( 'bubble' ).html( i18n.t( 'buttons.cropper_zoom.out' ) ).appendTo( button )

    // build zoom slider
    button = $( '<li data-wc-slider="cropper_zoom">' ).addClass( 'button-slider cropper-zoom-slider' ).appendTo( menu )
    $( '<label>' ).addClass( 'bubble' ).html( '100%' ).appendTo( button )

    // build zoom-in button
    button = $( '<li data-wc-action="cropper_zoom" data-wc-value="in">' ).addClass( 'button enabled' ).appendTo( menu )
    $( '<label>' ).addClass( 'bubble' ).html( i18n.t( 'buttons.cropper_zoom.in' ) ).appendTo( button )

    // build rotate left button
    button = $( '<li data-wc-action="cropper_rotate" data-wc-value="left">' ).addClass( 'button spacer enabled' ).appendTo( menu )
    $( '<label>' ).addClass( 'bubble' ).html( i18n.t( 'buttons.cropper_rotate.left' ) ).appendTo( button )

    // build rotate right button
    button = $( '<li data-wc-action="cropper_rotate" data-wc-value="right">' ).addClass( 'button enabled' ).appendTo( menu )
    $( '<label>' ).addClass( 'bubble' ).html( i18n.t( 'buttons.cropper_rotate.right' ) ).appendTo( button )

    // build replace button
    button = $( '<li data-wc-action="cropper_replace">' ).addClass( 'button spacer enabled' ).appendTo( menu )
    $( '<label>' ).addClass( 'bubble' ).html( i18n.t( 'buttons.cropper_replace' ) ).appendTo( button )

    // build done button
    button = $( '<li data-wc-action="cropper_done">' ).addClass( 'button right enabled' ).appendTo( menu )
    $( '<label>' ).addClass( 'bubble' ).html( i18n.t( 'buttons.cropper_done' ) ).appendTo( button )

    // build done button
    button = $( '<li data-wc-action="cropper_cancel">' ).addClass( 'button right enabled' ).appendTo( menu )
    $( '<label>' ).addClass( 'bubble' ).html( i18n.t( 'buttons.cropper_cancel' ) ).appendTo( button )

    return this.wrapper
  }

  // Build cropper_done
  WhiteChili.Cropper.prototype.build = function( delay ) {
    var width_in_mm
      , height_in_mm
      , cropper_frame
      , ghost_image
      , resolution
      , max_resize
      , available
      , frame
      , real

    // build and reference elements
    cropper_frame = $( '<div class="cropper-frame">' ).appendTo( this.wrapper )
    this.frame    = ci.api().selected().first()
    this.actor    = $( '<img>' ).attr( 'src', this.frame.previewUrl() ).appendTo( cropper_frame )
    ghost_image   = this.actor.clone().addClass( 'ghost' ).appendTo( this.wrapper )
    this.actors   = $( this.actor ).add( ghost_image )
    this.doc      = $( document )

    // get available width and height
    available = {
      width:  this.wrapper.width()
    , height: this.wrapper.height()
    }

    // get frame size
    frame = {
      x:        this.frame.attr( 'x' )
    , y:        this.frame.attr( 'y' )
    , width:    this.frame.attr( 'width' )
    , height:   this.frame.attr( 'height' )
    }
    
    // get real width and height
    real = {
      width:    this.frame.attr( 'realWidthPixels' )
    , height:   this.frame.attr( 'realHeightPixels' )
    }

    // get current dimensions
    this.image = {
      x:        this.frame.attr( 'imgX' )
    , y:        this.frame.attr( 'imgY' )
    , width:    this.frame.attr( 'imgWidth' )
    , height:   this.frame.attr( 'imgHeight' )
    , rotation: this.frame.attr( 'imgRotation' )
    }

    // calculate ratios
    available.ratio      = available.width / available.height
    frame.ratio          = frame.width / frame.height
    this.image.ratio     = real.width / real.height

    // initialize delta and start values
    this.delta  = { x: 0, y: 0 }
    this.start  = { x: 0, y: 0 }

    // initialize temporary drag data storage
    this.old    = this.result
    this.box    = {}

    // calculate cropper frame
    this.cropper = {}

    if ( available.ratio < frame.ratio ) {
      this.cropper.width  = available.width
      this.cropper.height = frame.height / frame.width * available.width
      this.cropper.x      = 0
      this.cropper.y      = ( available.height - this.cropper.height ) / 2
    } else {
      this.cropper.width  = frame.width / frame.height * available.height
      this.cropper.height = available.height
      this.cropper.x      = ( available.width - this.cropper.width ) / 2
      this.cropper.y      = 0
    }

    // calculate center point
    this.cropper.cx = this.cropper.x + this.cropper.width  / 2
    this.cropper.cy = this.cropper.y + this.cropper.height / 2
    
    this.scale           = this.image.ratio < frame.ratio ? this.cropper.width / this.image.width : this.cropper.height / this.image.height
    this.zoom            = this.image.ratio < frame.ratio ? this.image.width / frame.width     : this.image.height / frame.height

    // define result variables
    this.calculateResult()

    // define min and max values
    resolution      = ci.api().preflightPreferences().attr( 'minOutputResolution' )
    max_resize      = ci.api().preflightPreferences().attr( 'maxResizePercentage' )
    width_in_mm     = utils.px2mm( real.width,  resolution ) / frame.width
    height_in_mm    = utils.px2mm( real.height, resolution ) / frame.height
    this.min_zoom   = map.cropper_zooms[0]
    this.max_zoom   = Math.floor( Math.min( width_in_mm, height_in_mm ) * 100 ) * ( max_resize / 100 )

    // make sure max value is at least the max resize percentage
    if ( this.max_zoom < max_resize ) this.max_zoom = max_resize
    
    // set cropper frame size and position
    cropper_frame.css({
      left:       px( this.cropper.x )
    , top:        px( this.cropper.y )
    , width:      px( this.cropper.width )
    , height:     px( this.cropper.height )
    })
    
    // set ghost offset
    ghost_image.css({
      marginLeft: px( this.cropper.x )
    , marginTop:  px( this.cropper.y )
    })

    // position menu
    this.footer.css({
      top:        px( this.cropper.y + this.cropper.height )
    })

    // set initial position and rotation
    this.rotate( this.image.rotation )

    // activate drag functionality
    this.draggable()

    // build slider
    this.slideable()
  }

  // Calculate result values
  WhiteChili.Cropper.prototype.calculateResult = function() {
    // store result
    this.result = {
      x:        this.image.x      * this.scale * this.zoom
    , y:        this.image.y      * this.scale * this.zoom
    , width:    this.image.width  * this.scale * this.zoom
    , height:   this.image.height * this.scale * this.zoom
    , rotation: this.image.rotation
    }

    // calculate scale multiplier
    this.multiplier = this.image.width / this.result.width
  }

  // Calculate constraint box
  WhiteChili.Cropper.prototype.calculateBox = function() {
    // calculate new position
    this.box.max_x = this.cropper.width  - this.result.width
    this.box.max_y = this.cropper.height - this.result.height
    this.box.min_x = 0
    this.box.min_y = 0

    // rotate constraint box if required
    if ( ( this.result.rotation % 180 ) / 90 == 1 ) {
      this.box.max_x = this.cropper.width  - this.result.height
      this.box.max_y = this.cropper.height - this.result.width
    }

    // define constraint box offset introduced by rotation
    switch( this.result.rotation ) {
      case 90:
        this.box.min_x =  this.result.height
        this.box.max_x += this.box.min_x
      break
      case 180:
        this.box.min_x =  this.result.width
        this.box.max_x += this.box.min_x
        this.box.min_y =  this.result.height
        this.box.max_y += this.box.min_y
      break
      case 270:
        this.box.min_y =  this.result.width
        this.box.max_y += this.box.min_y
      break
    }
  }

  // Zoom control
  WhiteChili.Cropper.prototype.zoomLevel = function( value ) {
    if ( typeof value == 'number' || regex.integer.test( value ) ) {
      // normalize zoom
      if ( value <= this.min_zoom )
        value = this.min_zoom
      else if ( value >= this.max_zoom )
        value = this.max_zoom

      // apply zoom
      this.zoom = value / 100

      this.resize()

      ci.log( 'Zoomed cropper to ' + value + '%' )

    } else if ( value == 'in' || value == 'out' ) {
      var goal  = value == 'in' ? 1 : -1
        , value  = this.zoom * 100
        , index = map.cropper_zooms.indexOf( utils.closest( map.cropper_zooms, value ) )

      this.zoomLevel( map.cropper_zooms[index + goal] || map.cropper_zooms[index] )

    } else if ( value == null ) {
      return this.zoom * 100

    }
  }

  // Rotate image
  WhiteChili.Cropper.prototype.rotate = function( value ) {
    // make a snapshot of the current situation
    var radius   = this.result.width / 2
      , previous = this.result.rotation
      , radians, ref_x, ref_y, new_x, new_y

    // absolute rotation
    if ( typeof value == 'number' || regex.integer.test( value ) )
      this.result.rotation = value * 1
    // relative rotation
    else
      this.result.rotation += value == 'right' ? 90 : -90

    // make sure rotation angle is inbetween 0º and 360º
    this.result.rotation = ( this.result.rotation + 360 ) % 360

    // calculate angle to radians
    radians = this.result.rotation * ( Math.PI / 180 )

    // calculate position of reference point
    ref_x = radius * Math.cos( radians ) + this.cropper.cx  - this.cropper.x
    ref_y = radius * Math.sin( radians ) + this.cropper.cy  - this.cropper.y

    // make rotation position aware
    if ( this.result.rotation != previous ) {
      switch( this.result.rotation ) {
        case 0:
          this.result.x = ref_x - this.result.width
          this.result.y = ref_y - this.result.height / 2
        break
        case 90:
          this.result.x = ref_x + this.result.height / 2
          this.result.y = ref_y - this.result.width
        break
        case 180:
          this.result.x = ref_x + this.result.width
          this.result.y = ref_y + this.result.height / 2
        break
        case 270:
          this.result.x = ref_x - this.result.height / 2
          this.result.y = ref_y + this.result.width
        break
      }
    }

    this.render()

    ci.log( 'Rotated cropper to ' + this.result.rotation + 'º' )
  }

  // Resize image
  WhiteChili.Cropper.prototype.resize = function() {
    // calculate offset percentage of image according to cropper viewport
    var multiplier_x = Math.abs( this.result.x ) / ( this.result.width  - this.cropper.width )
      , multiplier_y = Math.abs( this.result.y ) / ( this.result.height - this.cropper.height )
      , new_x, new_y

    // make sure multipliers are centered when starting from 100% zoom
    if ( this.result.x == 0 && this.result.width == this.cropper.width )
      multiplier_x = 0.5
    if ( this.result.y == 0 && this.result.height == this.cropper.height )
      multiplier_y = 0.5

    // calculate new size
    this.result.width  = this.image.width  * this.scale * this.zoom
    this.result.height = this.image.height * this.scale * this.zoom

    // make position rotation aware
    switch( this.result.rotation ) {
      case 0:
        new_x = ( this.cropper.width  - this.result.width  ) * multiplier_x
        new_y = ( this.cropper.height - this.result.height ) * multiplier_y
      break
      default:
        new_x = this.result.x
        new_y = this.result.y
      break
    }

    // calculate new constraint box
    this.calculateBox()

    // calculate new position
    this.result.x = normalize( new_x, this.box.min_x, this.box.max_x )
    this.result.y = normalize( new_y, this.box.min_y, this.box.max_y )

    this.render()

    ci.log( 'Resized cropper to ' + this.zoom + '%' )
  }

  // Resize image
  WhiteChili.Cropper.prototype.move = function() {
    // calculate new position within box
    this.result.x = normalize( this.delta.x + this.old.x, this.box.min_x, this.box.max_x )
    this.result.y = normalize( this.delta.y + this.old.y, this.box.min_y, this.box.max_y )

    this.render()

    ci.log( 'Moved cropper to destined location' )
  }

  // Min value
  WhiteChili.Cropper.prototype.min = function() {
    return this.min_zoom
  }

  // Max value
  WhiteChili.Cropper.prototype.max = function() {
    return this.max_zoom
  }

  // Zoom control
  WhiteChili.Cropper.prototype.done = function( zoom ) {
    var element = this.actor[0]

    // remove references
    this.vanish()

    // update frame
    this.frame.attr({
      fitMode:     'manual'
    , imgX:        element.offsetLeft   * this.multiplier
    , imgY:        element.offsetTop    * this.multiplier
    , imgWidth:    element.offsetWidth  * this.multiplier
    , imgHeight:   element.offsetHeight * this.multiplier
    , imgRotation: this.result.rotation
    })
  }

  // Close cropper
  WhiteChili.Cropper.prototype.vanish = function() {
    delete ci.status.cropper
  }

  // Render new settings
  WhiteChili.Cropper.prototype.render = function() {
    this.actors.css({
      left:       px( this.result.x )
    , top:        px( this.result.y )
    , width:      px( this.result.width )
    , height:     px( this.result.height )
    , transform:  'rotate( ' + this.result.rotation + 'deg )'
    })
  }

  // Make draggable
  WhiteChili.Cropper.prototype.draggable = function() {
    var self = this

    // make images draggable
    this.actors.on( 'mousedown', dragstart )

    // start dragging
    function dragstart( event ) {
      // store start values
      self.start.x = event.clientX
      self.start.y = event.clientY

      // make image draggable
      self.doc.on( 'mousemove', dragging )
      self.doc.on( 'mouseup',   dragend )

      // store current values
      self.old = JSON.parse( JSON.stringify( self.result ) )

      // calculate new constraint box
      self.calculateBox()

      event.preventDefault()
    }

    // while dragging
    function dragging( event ) {
      // calculate delta
      self.delta.x = event.clientX - self.start.x
      self.delta.y = event.clientY - self.start.y

      // move image
      self.move()
    }

    // stop dragging
    function dragend( event ) {
      // stop draggable
      self.doc.off( 'mousemove', dragging )
      self.doc.off( 'mouseup',   dragend )

      // restore delta
      self.delta = { x: 0, y: 0 }
      self.box   = {}
      self.old   = self.result
    }
  }

  // Build slider
  WhiteChili.Cropper.prototype.slideable = function() {
    var settings

    // build an active slider only if the image is large enough
    if ( this.max_zoom > this.min_zoom ) {      
      settings = {
        value: function() {
          return this.zoom * 100
        }
      , perform: function( zoom ) {
          ci.controller.perform( 'cropper_zoom', zoom )
        }
      , min: this.min_zoom
      , max: this.max_zoom
      , val: this.zoom * 100
      }

    } else {
      $( '[data-wc-action=cropper_zoom], [data-wc-slider=cropper_zoom]' )
        .removeClass( 'enabled' )

      settings = {
        enabled: false
      }
    }

    // add it to the ui
    $( '.cropper-zoom-slider' )
      .append( ( new WhiteChili.ZoomComponent( settings ) ).html() )

    // hide close button 
    this.dialog
      .find( 'button.close' )
      .hide()

    // set slider value
    ui.cropperZoomUpdate()
  }

  //////////////////////////////
  ////////// helpers ///////////
  //////////////////////////////

  // Normalize value
  function normalize( value, mi, ma ) {
    if ( value <= ma )
      value = ma
    else if ( value >= mi )
      value = mi

    return value
  }

  // Render value in pixels
  function px( value ) {
    return value + 'px'
  }


})( WhiteChili.ci, WhiteChili.ui, WhiteChili.map, WhiteChili.i18n, WhiteChili.regex, WhiteChili.utils )
;( function( ci ) {

  // Class for building a fonts component
  WhiteChili.FontsComponent = function() {}

  // Generate palette html
  WhiteChili.FontsComponent.prototype.html = function() {
    // prepare variables and build wrapper
    var row, variants, current
      , fonts   = ci.config.fonts
      , wrapper = $( '<ul class="fonts"></ul>' )

    // detect global font constraint
    if ( ci.api().frameContentConstraints().hasLimitOn( 'fonts' ) ) {
      var i, allowed = ci.api().frameContentConstraints().attr( 'allowedFonts' ).split( ';' )

      fonts = fonts.filter( function( font ) {
        return allowed.indexOf( font.item_id ) > -1
      })
        
    }

    // add fonts
    fonts.forEach( function( font ) {
      if ( font.name != current ) {
        current = font.name

        // create row
        row = $( '<li class="font"></li>' )
          .addClass( 'font-' + font.item_id )
          .appendTo( wrapper )

        // add font group button
        $( '<div class="group" data-wc-action="fonts" data-wc-value="' + font.item_id + '"></div>' )
          .addClass( 'group-' + font.item_id )
          .html( font.name )
          .appendTo( row )

        // add font variant
        variants = $( '<ul class="variants"></ul>' )
          .appendTo( row )
      }

      // add font variant
      $( '<li class="variant" data-wc-action="font" data-wc-value="' + font.item_id + '"></li>' )
        .html( font.variant.name )
        .addClass( 'variant-' + font.item_id )
        .appendTo( variants )
    })

    return wrapper
  }

})( WhiteChili.ci )
;( function( ci, i18n, enable ) {

  // Class for building a layouts component
  WhiteChili.Info = function() {}

  // Generate palette html
  WhiteChili.Info.prototype.html = function() {
    // build wrapper
    var wrapper = $( '<div class="information modal">' )
      , fields  = $( '<div class="fields">' ).appendTo( wrapper )
      , doc     = ci.doc().attr()

    // create title
    $( '<h3>' )
      .addClass( 'zow-underline' )
      .html( i18n.t( 'info.title' ) )
      .appendTo( fields )

    // create document base info
    $( '<div class="basic-info">' )
      .html( i18n.t( 'info.document', {
        width:        doc.width
      , height:       doc.height
      , pages:        ci.api().pages().size()
      , name:         ci.doc().needsName() ? i18n.t( 'info.nameless' ) : doc.name
      , original:     ci.config.original.name
      , rename:       i18n.t( 'info.rename' )
      }))
      .appendTo( fields )

    // create additonal information
    if ( ci.config.original.information ) {
      $( '<div class="document-info">' )
        .html( ci.config.original.information )
        .appendTo( fields )
    }

    return wrapper
  }

})( WhiteChili.ci, WhiteChili.i18n, WhiteChili.enable )
;( function( ci, i18n, regex ) {

  // Class for building a layouts component
  WhiteChili.LayoutsComponent = function() {}

  // Generate palette html
  WhiteChili.LayoutsComponent.prototype.html = function() {
    // build wrapper
    var wrapper = $( '<ul class="buttons"></ul>' )
      , layouts = ci.doc().alternateLayouts()
      , length  = layouts.size()

    // add layouts
    layouts.each( function( i ) {
      if ( ! regex.default_name.test( this.attr( 'name' ) ) ) {
        // build button
        var button, label
          , name = i18n.t( 'layouts.' + this.attr( 'name' ), true ) || 'layout ' + String.fromCharCode( 65 + i )

        // build button
        button = $( '<li class="button enabled" data-wc-action="layout" data-wc-value="' + i + '"></li>' )
          .appendTo( wrapper )

        // build label
        $( '<label class="bubble">' + name + '</label>' )
          .appendTo( button )
      }
    })

    return wrapper
  }

})( WhiteChili.ci, WhiteChili.i18n, WhiteChili.regex )
;( function( ci, dom, utils ) {

  // Class for building a live preflight
  WhiteChili.LivePreflight = function( warnings ) {
    // reference wrapper
    var wrapper  = dom.$( '#live_preflight' )
      , selected = ci.api().selected().collect( 'id' )
      , messages = wrapper.find( '.message' )
      , current  = messages.map( function() { return $( this ).data( 'id' ) }).toArray()
      , wids     = warnings.collect( 'id' )

    // clear wrapper
    messages.each( function() {
      var id    = $( this ).data( 'id' )
        , frame = $( this ).data( 'frame' )

      if ( selected.length == 0 || wids.indexOf( id ) == -1 || selected.indexOf( frame ) == -1 )
        killMessage( $( this ) )
    })

    warnings.each( function() {
      // only live preflight on selected frames
      if ( this.attrIn( 'frame', selected ) ) {
        var message
        
        if ( this.attrIn( 'id', current ) ) {
          // update message
          message = wrapper
            .find( '[data-id=' + this.attr( 'id' ) + ']' )
            .html( this.locale() )

        } else {
          // build message
          message = $( '<div>' )
            .html( this.locale() )
            .addClass( 'message color-lightbox-bg' )
            .addClass( this.className() )
            .attr( 'data-id', this.attr( 'id' ) )
            .attr( 'data-frame', this.frame().attr( 'id' ) )
            .appendTo( wrapper )

          // make message visible
          setTimeout( function() {
            message.addClass( 'alive' )
          }, 5 )
        }

        // build close button
        $( '<div>' )
          .html( '✕' )
          .addClass( 'close-button' )
          .addClass( this.attrIs( 'type', 'warning' ) ? 'zow-tangerine-bg' : 'zow-red-bg' )
          .prependTo( message )
          .click( function() {
            killMessage( message )
          })
      }
    })
  }

  //////////////////////////////
  ////////// helpers ///////////
  //////////////////////////////

  function killMessage( message ) {
    message
      .removeClass( 'alive' )
      .addClass( 'dying' )

    setTimeout( function() {
      message.remove()
    }, 500 )
  }

})( WhiteChili.ci, WhiteChili.dom, WhiteChili.utils )
;( function( ci, map, i18n ) {

  // Class for building a pages component
  WhiteChili.PagesComponent = function() {}

  // Generate palette html
  WhiteChili.PagesComponent.prototype.html = function() {
    // build wrapper
    var wrapper = $( '<ul class="pages"></ul>' )
      , pages   = ci.api().pages()
      , length  = pages.size()
      , value, id

    // add pages
    pages.each( function( i ) {
      // build button
      var button, label, icon
        , name = this.attr( 'name' )

      // build button
      button = $( '<li class="page enabled" data-wc-action="page" data-wc-value="' + name + '"></li>' )
        .appendTo( wrapper )
        
      // build label
      if ( ! ( label = i18n.t( 'pages.' + name, true ) ) )
        label = length == 2 ? map.pages.two[i] : map.pages.many.replace( '{{number}}', name )

      $( '<label class="bubble">' + label + '</label>' )
        .appendTo( button )
    })
    
    return wrapper
  }

})( WhiteChili.ci, WhiteChili.map, WhiteChili.i18n )
;( function( ci, i18n ) {

  // Class for building a paragraphs component
  WhiteChili.ParagraphsComponent = function() {}

  // Generate palette html
  WhiteChili.ParagraphsComponent.prototype.html = function() {
    // prepare variables and build wrapper
    var row
      , wrapper = $( '<ul class="paragraphs"></ul>' )
    
    // add paragrpah styles
    ci.api().frameContentConstraints().paragraphStyles().each( function() {
      // create row
      if ( this.attr( 'name' ) != '[None]' )
        row = $( '<li class="paragraph" data-wc-action="paragraph" data-wc-value="' + this.attr( 'id' ) + '"></li>' )
          .addClass( 'paragraph-' + this.attr( 'id' ) )
          .appendTo( wrapper )
          .html( i18n.t( 'paragraph.' + this.attr( 'name' ) ) )
    })

    return wrapper
  }

})( WhiteChili.ci, WhiteChili.i18n )
;( function( ci, utils, i18n, regex ) {

  // Class for new preflight check
  WhiteChili.Preflight = function() {
    // get preflight data
    this.warnings = ci.api().importantPreflightResults()
  }

  // Build content output
  WhiteChili.Preflight.prototype.html = function() {
    // prepare wrapper and get warnings
    var wrapper = $( '<div class="preflight">' )
      , first   = this.warnings.first()
      , list    = $( '<ul class="warnings">' ).appendTo( wrapper )
      , title, buttons

    // build title
    if ( this.warnings.one() )
      title = i18n.t( 'preflight.title.singular' )
    else
      title = i18n.t( 'preflight.title.plural', { amount: this.warnings.size() })
    
    // create title
    $( '<h1>' )
      .html( title )
      .prependTo( wrapper )

    // create image
    $( '<img alt="preflight illustration">' )
      .attr({ src: utils.s3( 'images', ci.config.preflight_file ) })
      .prependTo( wrapper )

    // add warning blobs
    this.warnings.each( function() {
      // create warning
      $( '<li>' )
        .appendTo( list )
        .html( this.locale() )
    })

    // create button row
    buttons = $( '<div class="buttons">' )
      .appendTo( wrapper )

    $( '<div class="zow-button preflight-cancel nav zow-grey-bg">' )
      .append( $( '<span>' ).html( i18n.t( 'preflight.fix_issue' ) ) )
      .appendTo( buttons )
      .on( 'click', function() {
        first.frame().select()
        ci.controller.perform( 'preflight', 'hide' )
      })

    $( '<div class="zow-button preflight-agree nav zow-lime-bg">' )
      .append( $( '<span>' ).html( i18n.t( 'preflight.ignore_issue' ) ) )
      .appendTo( buttons )
      .on( 'click', function() {
        ci.status.skip_preflight = true
        ci.controller.perform( 'preflight', 'hide' )
        ci.controller.perform( 'checkout' )
      })

    return wrapper
  }

})( WhiteChili.ci, WhiteChili.utils, WhiteChili.i18n, WhiteChili.regex )
;( function( ci, i18n, regex  ) {

  // Class for document namer
  WhiteChili.Rename = function( next ) {
    this.next = next || 'save'
  }

  // Build content output
  WhiteChili.Rename.prototype.html = function() {
    // prepare wrapper
    var wrapper = $( '<div class="rename modal">' )
      , fields  = $( '<div class="fields">' ).appendTo( wrapper )
      , name    = ci.doc().attr( 'name' )
      , next    = this.next
      , input

    // create title
    $( '<h3>' )
      .addClass( 'zow-underline' )
      .html( i18n.t( 'rename.title' ) )
      .appendTo( fields )

    // create description
    $( '<p>' )
      .addClass( 'zow-description' )
      .html( i18n.t( 'rename.description' ) )
      .appendTo( fields )

    // create input
    input = $( '<input type="text" placeholder="' + i18n.t( 'rename.placeholder' ) + '">' )
      .appendTo( fields )
      .on( 'keyup', function( event ) {
        if ( event.keyCode == 27 ) {
          // exit on cancel
          ci.controller.perform( 'dialog', 'hide' )

        } else if ( input.val() != '' ) {
          input.removeClass( 'with-errors' )

          // submit on enter
          if ( event.keyCode == 13 )
            rename()
        }
      })

    // set name
    if ( ! regex.tmp_name.test( name ) )
      input.val( ci.doc().attr( 'name' ) )

    // create devider
    $( '<hr>' )
      .addClass( 'zow-softer-grey-bg' )
      .appendTo( fields )

    // add enter key listener
    key( 'enter', 'modal', rename )

    // create button
    $( '<button name="button" type="submit" required="required">' )
      .addClass( 'zow-button zow-button-primary' )
      .text( i18n.t( 'buttons.' + next ) )
      .appendTo( fields )
      .on( 'click', rename )

    // rename action
    function rename() {
      if ( input.val() == '' ) {
        input.addClass( 'with-errors' )
      } else {
        ci.controller.perform( 'rename', {
          name: input.removeClass( 'with-errors' ).val()
        , next: next
        })
      }
    }

    // focus on input
    setTimeout( function() {
      input.focus()
    }, 100 )

    return wrapper
  }

})( WhiteChili.ci, WhiteChili.i18n, WhiteChili.regex )
;( function( ci, utils ) {

  // Class for building a splash screen
  WhiteChili.Splash = function() {
    var scheme = ci.config.scheme
      , logo   = utils.s3( 'logos', scheme.splash_file_name || scheme.logo_file_name )

    // splash text color
    $( '.splash h1, .splash .message, .splash .footer' ).css( 'color', scheme.color_dark )

    // splash color
    $( '.splash' ).css( 'background-color', scheme.color )

    // splash logo
    $( '.splash .logo' ).css( 'background-image', 'url(' + logo + ')' )

    // splash bar color
    $( '.splash .bar' ).css( 'background-color', scheme.color_lighter )

    // splash bar color
    $( '.splash .loader' ).css({
      borderColor:     scheme.color_dark
    , backgroundColor: scheme.color_dark
    })
    
  }

})( WhiteChili.ci, WhiteChili.utils )
;( function( ci, utils ) {

  // Class for building a font size dropdown
  WhiteChili.TextSize = function() {
    // get sizes
    this.sizes = ci.api()
      .frameContentConstraints()
      .attr( 'fontPulldownValues' )
      .toString()
      .split( ';' )
  }

  // Generate menu options
  WhiteChili.TextSize.prototype.html = function() {
    if ( this.sizes.length > 1 ) {
      // create wrapper
      var wrapper = $( '<ul class="text-sizes">' )
        , button

      // create options
      this.sizes.forEach( function( size ) {
        button = $( '<li data-wc-action="text_size" data-wc-value="' + size + '">' )
          .addClass( 'enabled color-dark' )
          .html( size + ' <small class="bubble">pt</small>' )

        wrapper.append( button )
      })

      return wrapper
    }
  }

})( WhiteChili.ci, WhiteChili.utils )
;( function( ci, map ) {

  // Class for building a zoom slider component
  WhiteChili.ZoomComponent = function( settings ) {
    this.settings = settings
  }

  // Generate palette html
  WhiteChili.ZoomComponent.prototype.html = function() {
    // build wrapper
    var wrapper  = this.wrapper = $( '<div class="slider-bar"></div>' ).appendTo( document.body )
      , handle   = this.handle  = $( '<div class="slider-handle">zoom handle</div>' )
      , settings = this.settings
      , value    = settings.val || 0
      
    // enable slider behavior
    if ( settings.enabled == null || settings.enabled === true ) {
      handle.on( 'mousedown', function( start ) {
        var move, end
          , width = wrapper.width()
          , left  = parseInt( handle.css( 'left' ) )
          , last  = settings.value()
          , doc   = $( document )
          , min   = settings.min
          , max   = settings.max
        
        // move callback
        move = function( event ) {
          // calcualte offset
          var zoom
            , offset = left + event.pageX - start.pageX

          // normalize offset
          if ( offset < 0 )
            offset = 0
          else if ( offset > width )
            offset = width

          // calculate zoom
          zoom = min + ( offset / width * ( max - min ) )

          // perform zoom
          if ( zoom != last ) {
            settings.perform( zoom )
            last = zoom
          }
        }

        // end callback
        end = function() {
          doc.off( 'mousemove', move )
          doc.off( 'mouseup', end )
        }
        
        // attache move
        doc.on( 'mousemove', move )
        doc.on( 'mouseup', end )
      })
    }

    return wrapper.append( handle )
  }

  // Generate set handle value
  WhiteChili.ZoomComponent.prototype.val = function( val ) {
    if ( val ) {
      // get settings
      var s = this.settings

      // calculate handle position
      val = Math.floor( ( ( val - s.min ) / ( s.max - s.min ) ) * this.wrapper.width() )
      
      // set handle
      this.handle.css( 'left', val + 'px' )
    }
  }

})( WhiteChili.ci, WhiteChili.map )
;( function( ci, map, utils ) {

  WhiteChili.Traits.Color = {
    // Get hex value from color
    hex: function() {
      return this.attr( 'hexValue' )
    }

    // Return color
  , solid: function() {
      return 'background-color: ' + this.hex() + ';';
    }

    // return value for the hasXX attribute
  , has: function() {
      return ! this.none()
    }

    // Return none, if it's none
  , none: function() {
      if ( this.attr( 'name' ) == '[None]' )
        return 'background: white url( "' + utils.s3( 'images', 'none.svg' ) + '" );';
    }

    // Return gradient, if it's a gradient
  , gradient: function() {
      if ( this.attr( 'type' ).toLowerCase() == 'gradient' ) {
        // detect gradient type
        var type  = this.attr( 'gradientType' ).split( '_' )[1].toLowerCase()
          , col_1 = this.attr( 'gradientColor1' ).hex()
          , col_2 = this.attr( 'gradientColor2' ).hex()
          , loc_1 = this.attr( 'gradientLocation1' )
          , loc_2 = this.attr( 'gradientLocation2' )
          , value = col_1 + ' ' + loc_1 + '%, ' + col_2 + ' ' + loc_2 + '%'

        // add angle for linear gradient
        if ( type == 'linear' )
          value = ( this.attr( 'gradientAngle' ) + 90 ) + 'deg, ' + value

        // build background for every browser engine
        for ( i = 0, g = ''; i < map.engines.length; i++ )
          g += 'background: ' + map.engines[i] + type + '-gradient(' + value + ');'

        return g
      }
    }

    // Return css declaration
  , css: function() {
      return this.classSelector( false ) + '{ ' + this.background() + ' }'
    }

    // Return css selector
  , classSelector: function( lightness ) {
      return '.color-' + this.attr( 'id' ).toLowerCase()
    }

    // Return css class name
  , className: function( lightness ) {
      // build base class name
      var name = 'color-' + this.attr( 'id' ).toLowerCase()
        , i, parts

      // get name parts
      parts = this.attr( 'name' ).split( '.' )
      parts.shift()

      for ( i = parts.length - 1; i >= 0; i-- )
        name += ' color-' + parts[i]

      // add extra lightness class in some cases
      if ( lightness && this.light() )
        name += ' color-light'

      return name
    }

    // Test if it's a light background
  , light: function() {
      return this.attr( 'name' ) == '[None]' || this.brightness() > 0.8
    }

    // Convert object to background
  , background: function() {
      return this.none() || this.gradient() || this.solid() 
    }

    // Calculate true brightness
  , brightness: function() {
      // get rgb object
      var rgb = this.rgb()

      return ( rgb.r / 255 * 0.30 )
           + ( rgb.g / 255 * 0.59 )
           + ( rgb.b / 255 * 0.11 )
    }

    // Convert hex value to rgb
  , rgb: function() {
      // fond color parts
      var match = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec( this.hex() )
      
      return {
        r: match ? parseInt( match[1], 16 ) : 0
      , g: match ? parseInt( match[2], 16 ) : 0
      , b: match ? parseInt( match[3], 16 ) : 0
      }
    }
  }

})( WhiteChili.ci, WhiteChili.map, WhiteChili.utils )
;( function( ci, map, utils, regex ) {

  WhiteChili.Traits.Document = {
    // Get default color
    defaultColor: function( type ) { // type:string|null
      if ( ! ci.cache.mixed['defaultColor.' + type] ) {
        ci.cache.mixed['defaultColor.' + type] = findColor( this, type || 'default' ) ||
          findColor( this, 'default' ) ||
          findColor( this ) ||
          this.colors().find( 'name', '[Black]' )
      }

      return ci.cache.mixed['defaultColor.' + type]
    }

    // Test current cursor
  , cursorIs: function( type ) { // type:string
      return this.cursor() == type
    }
  , cursorIsnt: function( type ) { // type:string
      return ! this.cursorIs( type )
    }

    // Deselect all selected frames
  , deselect: function() {
      ci.execute( 'document.selectedFrames', 'Clear' )

      return this
    }

    // Copy frames
  , copy: function() {
      ci.execute( 'document.selectedFrames', 'Copy' )

      return this
    }

    // Cut frames
  , cut: function() {
      this.copy()
      this.selected().destroy()
      
      return this
    }

    // Paste frames
  , paste: function() {
      ci.execute( 'document', 'PasteFrames' )

      return this
    }

    // Get the history
  , history: function() {
      return new WhiteChili.History
    }

    // Select layer for frame type
  , selectLayerFor: function( type ) {
      // find layer
      var i
        , names  = [ 'default', type ]
        , length = names.length

      // loop through every suggestion
      for ( i = 0; i < length; i++) {
        this.layers().each( function() {
          if ( this.attrIs( 'translatedName', 'layer.' + names[i] ) )
            ci.doc().attr( 'selectedLayer', this.toString() )
        })
      }
      
      return this
    }

    // Get preflight results that should be shown
  , importantPreflightResults: function() {
      return this.preflightResults().reduce( function() {
        var frame = this.frame()

        return frame.exists()
          && ! regex.help_layer.test( frame.layer().attr( 'name' ) )
          && ! this.ignore()
      })
    }

    // Test editor on unsaved changes
  , dirty: function() {
      return ci.editor.GetDirtyState()
    }

    // Set orientation
  , orientation: function( value ) {
      var o = ci.cache.orientation
        , v = ci.api().viewPreferences().attrIs( 'viewMode', 'preview' ) ? 'preview' : 'edit'
        , w = value == 'landscape' ? 'portrait'  : 'landscape'

      // rotate canvas
      if ( value == o.original )
        this.attr({ width: o.width, height: o.height })
      else
        this.attr({ width: o.height, height: o.width })

      // rotate backgrounds
      this.pages().each( function() {
        this.orientation( value )
      })

      // switch mask layers
      ci.api().layers().where( 'name','layer.mask.' + v + '.' + value ).attr( 'visible', true )
      ci.api().layers().where( 'name','layer.mask.' + v + '.' + w ).attr( 'visible', false )
    }

    // Get all variables that are destined to be options
  , optionVariables: function() {
      return this.variables().reduce( function() {
        return regex.is_option.test( this.attr( 'name' ) )
      })
    }

    // Get all selected option variables
  , selectedOptionVariables: function() {
      return this.variables().reduce( function() {
        return regex.is_option.test( this.attr( 'name' ) ) && this.attr( 'value' ) == 1
      })
    }

    // Get ids from selected variable options
  , selectedOptionVariableIds: function() {
      return this.selectedOptionVariables().collect( function() {
        return this.attr( 'name' ).split( '_' ).pop()
      })
    }
    
  }

  //////////////////////////////
  ////////// helpers ///////////
  //////////////////////////////

  function findColor( source, value ) {
    // build rgex
    value = value ? new RegExp( '.*\.' + value, 'i' ) : regex.defined_color

    // find matching color
    return source.colors().reduce( function() {
      return value.test( this.attr( 'name' ) )
    }).first()
  }

})( WhiteChili.ci, WhiteChili.map, WhiteChili.utils, WhiteChili.regex )
;( function( ci, map, utils, regex ) {

  WhiteChili.Traits.Frame = {
    // Get frame textFlow (only operate on text frames)
    textFlow: function( flow ) { // flow:null|WhiteChili.Models.Flow|function|string|boolean
      if ( this.attrIs( 'type', 'text' ) ) {
        // get path (method on api, string on doc)
        var path = typeof this.path === 'function' ? this.path() : this.path

        // act as a setter (if a new flow is given)
        if ( flow instanceof WhiteChili.Models.Flow || typeof flow == 'string' ) {
          ci.execute( path, 'ImportTextFlow', flow.toString() )
  
          return this
  
        // perform a block operation to set attributes for example
        } else if ( typeof flow == 'function' ) {
          // re-apply modified flow
          return this.textFlow( flow( this.textFlow() ) )
        }
        
        // act as a getter (use a boolean flow to toggle plainText/textFlow mode)
        flow = ci.execute( path, 'GetText', typeof flow == 'boolean' ? flow : true, true )
  
        // always return as WhiteChili instance
        return new WhiteChili.Models.Flow( flow )
      }
    }

    // Build background key
  , backgroundKey: function() {
      return 'originalFrameGeometry.' + this.attr( 'id' )
    }

    // Check if this frame is a background image
  , isBackground: function() {
      return this.layer().attrIs( 'name', 'layer.background' )
    }

    // Check if the visual cropper can be used
  , isCropable: function() {
      return this.isPixelFile()
    }

    // Check if it is a pixel file
  , isPixelFile: function() {
      return regex.is_image.test( this.attr( 'externalName' ) )
    }

    // Get frame image url
  , previewUrl: function() {
      return /* ci.config.engine + '/' + */ this.attr( 'previewURL' )
    }

    // Send frame to the front
  , front: function() {
      // get help and mask frames
      var tops = ci.api().selectedPage().topFrames()
        , pos  = 999999

      // insert before tops
      if ( tops.any() )
        pos = tops.first().index() - 1
      
      // make sure not to insert at the current index
      if ( this.index() !== pos )
        this.index( pos )

      return this
    }

    // Send frame one step forward
  , forward: function() {
      // get status and help frames
      var page = ci.api().selectedPage()
        , top  = page.frames().size()
        , pos  = this.index()
        , tops = page.topFrames()
        
      // normalise value
      if ( pos < top ) pos += 1

      // make sure the frame does not surpass the first help layer frame
      if ( tops.any() ) {
        var index = tops.first().index()

        if ( pos >= index ) pos = index - 1
      }
      // make sure not to insert at the current index
      if ( this.index() != pos )
        this.index( pos )

      return this
    }

    // Send frame one step backward
  , backward: function() {
      // get top of the stack
      var pos   = this.index()
        , frame = this.page().bgFrames().last()
        , index = frame ? frame.index() + 1 : 0

      // normalise value
      if ( pos > index ) pos -= 1

      return this.index( pos )
    }
    
    // Send frame to the back
  , back: function() {
      var frame = this.page().bgFrames().last()
        , index = frame ? frame.index() + 1 : 0

      return this.index( index )
    }

    // Insert frame before another frame (index - 1 )
  , before: function( target ) {
      return this.index( target.index() - 1 )
    }

    // Insert frame after another frame (index + 1)
  , after: function( target ) {
      return this.index( target.index() + 1 )
    }

    // Return array of tags on the frame
  , tags: function() {
      var attr = this.attr( 'tag' )

      return attr == null ? [] : attr.trim().replace( /\s+/, ' ' ).split( ',' )
    }

    // Return true if tag exists on the frame, false otherwise
  , hasTag: function( name ) {
      return this.tags().indexOf( name ) != -1
    }

    // Add tag to the frame
  , addTag: function( name ) {
      if ( ! this.hasTag( name ) ) {
        var array = this.tags()
        array.push( name )
        this.attr( 'tag', array.join( ',' ) )
      }

      return this
    }

    // Remove tag from the frame
  , removeTag: function( name ) {
      if ( this.hasTag( name ) ) {
        this.attr( 'tag', this.tags().filter( function( t ) {
          return t != name
        }).join( ',' ) )
      }

      return this
    }

    // Toggle the presence of a tag on the frame
  , toggleTag: function(name) {
      return this.hasTag( name ) ? this.removeTag( name ) : this.addTag( name )
    }

    // Get all colors from a text frame
  , textColors: function() {
      var flow

      // collect colors from textflow
      if ( flow = this.textFlow() ) {
        return ci.api()
          .colors()
          .whereIn( 'id', utils.unique( flow.collect( 'my_color' ) ) )
      }
    }

    // Get all text sizes
  , textSizes: function() {
      var flow

      // collect sizes from textflow
      if ( flow = this.textFlow() ) {
        var sizes = utils.unique( flow.collect( 'fontSize' ) ).map( Number )
        sizes.sort()
        
        return sizes
      }
    }

    // Get all text sizes
  , textAligns: function() {
      var flow

      // collect sizes from textflow
      if ( flow = this.textFlow() )
        return utils.unique( flow.collect( 'textAlign' ) )
    }

    // Get all fonts
  , fonts: function() {
      var flow

      // collect sizes from textflow
      if ( flow = this.textFlow() ) {
        var fonts = []

        utils.unique( flow.collect( 'fontFamily' ) ).forEach( function( font ) {
          if ( font = ci.api().fonts().find( 'name', font.replace( '_', ' ' ) ) )
            fonts.push( font )
        })
        
        return fonts
      }
    }

    // Get all font sizes
  , fontFamilies: function() {
      var flow

      // collect sizes from textflow
      if ( flow = this.textFlow() )
        return utils.unique( flow.collect( 'fontFamily' ) )
    }

    // Frame fill
  , fill: function( color ) {
      // act as a setter
      if ( color )
        return this.attr({ fillColor: color, hasFill: color.has() })

      // act as getter
      return this.attr( 'fillColor' )
    }

    // Stroke color
  , stroke: function( color ) {
      // act as a setter
      if ( color ) {
        // ensure border width
        if ( color.has() && this.attrIs( 'borderWidth', 0 ) )
          this.attr( 'borderWidth', 1 )

        // set color and broder apprearance
        return this.attr({
          borderColor: color
        , hasBorder:   color.has()
        })
      }
      
      // act as getter
      return this.attr( 'borderColor' )
    }

    // Stroke width
  , strokeWidth: function( value ) {
      if ( value == null )
        return this.raw().borderWidth

      return this.attr( 'hasBorder', value > 0 ).attr( 'borderWidth', value )
    }
    
    // Stroke edge
  , strokeEdge: function( type ) {
      return this.attr({
        borderCaps: map.edges[type].caps
      , borderJoin: map.edges[type].join
      })
    }


  }

})( WhiteChili.ci, WhiteChili.map, WhiteChili.utils, WhiteChili.regex )
;( function( utils ) {

  WhiteChili.Traits.FrameConstraints = {
    // Get original geometry locks
    geoLocks: function() {
      return {
        lockMoveHorizontal:   this.attr( 'lockMoveHorizontal' )
      , lockMoveVertical:     this.attr( 'lockMoveVertical' )
      , lockResizeHorizontal: this.attr( 'lockResizeHorizontal' )
      , lockResizeVertical:   this.attr( 'lockResizeVertical' )
      , lockRotate:           this.attr( 'lockRotate' )
      }
    }

    // Lock geometry
  , lockGeo: function() {
      this.attr({
        lockMoveHorizontal:   'yes'
      , lockMoveVertical:     'yes'
      , lockResizeHorizontal: 'yes'
      , lockResizeVertical:   'yes'
      , lockRotate:           'yes'
      })

      return this
    }

    // Test lock on certain area
  , hasLockOn: function( value ) {
      // get attribute value
      value = this.attr( 'lock' + utils.capital( value ) )

      // note: inherit should not be yes per sé, but for the time being this is a fine solution
      return value != 'inherit' && value != 'no' && value != null
    }

  }

})( WhiteChili.utils )
;( function( ci, utils ) {

  WhiteChili.Traits.FrameContentConstraints = {
    // Parse colors for purpose
    colorsFor: function( value ) {
      if ( this.hasColor( value ) ) {
        // get attribute value
        value = this.attr( 'allowed' + attrValue( value ) ).split( ';' )

        // collect only allowed colors
        return ci.api().colors().reduce( function() {
          return value.indexOf( this.attr( 'id' ) ) > -1
        })
      }
      
      // return full color list if no constraint is found
      return ci.api().colors()
    }

    // Parse paragraph styles
  , paragraphStyles: function() {
      if ( this.hasLimitOn( 'paragraphStyles' ) ) {
        // get attribute value
        var ids = this.attr( 'allowedParagraphStyles' ).split( ';' )

        // collect only allowed styles
        return ci.doc().paragraphStyles().reduce( function() {
          return ids.indexOf( this.attr( 'id' ) ) > -1
        })
      }

      // return full list if no constraint is found
      return ci.doc().paragraphStyles()
    }

    // Test presence of color constraint
  , hasColor: function( value ) {
      return this.hasLimitOn( attrValue( value ) )
    }

    // Test limit on certain area
  , hasLimitOn: function( value ) {
      // get attribute value
      value = this.attr( 'limit' + utils.capital( value ) )

      // note: inherit should not be yes per sé, but for the time being this is a fine solution
      return value != 'inherit' && value != 'no'
    }
  }

  //////////////////////////////
  ////////// helpers ///////////
  //////////////////////////////

  // Normalize attribute value
  function attrValue( attr, prefix ) {
    // normalize attribute name
    attr = attr == 'stroke' ? 'border' : attr == 'color' ? 'text' : attr

    // return list
    return utils.capital( attr ) + 'Colors'
  }
  
})( WhiteChili.ci, WhiteChili.utils )
;( function( ci, i18n, regex, utils ) {

  WhiteChili.Traits.PreflightResult = {
    // Get to page
    page: function() {
      return new WhiteChili.Models.Page( 'document > pages > item[name=' + this.attr( 'pageNum' ) + ']' )
    }

    // Get to frame
  , frame: function() {
      return new WhiteChili.Models.Frame( 'document frames > item[id=' + this.attr( 'frame' ) + ']' )
    }

    // Check if I should be ignored
  , ignore: function() {
      return ci.api()
        .preflightPreferences()
        .attrIs( this.attr( 'name' ), 'ignore' )
    }

    // Get localized value
  , locale: function() {
      var type    = utils.snake( this.attr( 'name' ) )
        , values  = {}
        
      // prepare message variables
      switch ( type ) {
        case 'image_output_resolution':
          // get required values
          var args  = this.attr( 'description' ).match( /\d+/g )
            , frame = this.frame()

          // add calculated values
          values.is            = args[0]
          values.should        = args[1]
          values.dpi           = frame.attr( 'realResolution' )
          values.width         = frame.attr( 'realWidthPixels' )
          values.height        = frame.attr( 'realHeightPixels' )
          values.width_should  = Math.ceil( frame.attr( 'realWidthPixels' )  / args[0] * args[1] )
          values.height_should = Math.ceil( frame.attr( 'realHeightPixels' ) / args[0] * args[1] )
        break
      }

      return i18n.t( 'preflight.' + type, values )
    }

    // Get class name
  , className: function() {
      return this.attr( 'frame' ) + '-' + this.attr( 'name' )
    }
  }

})( WhiteChili.ci, WhiteChili.i18n, WhiteChili.regex, WhiteChili.utils )
;( function( ci, map, utils ) {

  // Color class
  WhiteChili.Color = function( path ) {
    // reference chili color
    this.path = this.build( path )
  }

  // Inherit from WhiteChili.Resource
  WhiteChili.Color.prototype = new WhiteChili.Resource

  // Add colors trait
  utils.extend( WhiteChili.Color, WhiteChili.Traits.Color )

  // Convert object to cp_object string
  WhiteChili.Color.prototype.toString = function() {
    return this.path
  }

})( WhiteChili.ci, WhiteChili.map, WhiteChili.utils )
;( function() {

  // Cursor class
  WhiteChili.Cursor = function() {
    // reference chili object
    this.path = 'document.Cursor'
  }

  // Inherit from WhiteChili.Resource
  WhiteChili.Cursor.prototype = new WhiteChili.Resource
  
})()
;( function( ci, ui, map, load, utils, regex ) {
  
  // Document class
  WhiteChili.Document = function() {
    // define path
    this.path = 'cp_object:document'

    // add resources
    this.hasOne( 'work_space' )
        .hasOne( 'view_preferences' )
        .hasOne( 'constraints' )
        .hasOne( 'frame_constraints' )
        .hasOne( 'frame_content_constraints' )
        .hasOne( 'preflight_preferences' )
        .hasMany( 'alternate_layout' )
        .hasMany( 'color' )
        .hasMany( 'font' )
        .hasMany( 'layer' )
        .hasMany( 'page' )
        .hasMany( 'frame', { root: 'document.allFrames' } )
        .hasMany( 'variable' )
        .hasMany( 'alternate_layout_property' )
        .hasMany( 'preflight_result' )
        .hasMany( 'paragraph_style' )
        .hasSelected( 'alternate_layout' )
        .hasSelected( 'page' )
        .hasSelected( 'text' )
        .hasSelected( 'layer' )
        .hasSelected( 'font' )
  }

  // Inherit from WhiteChili.Resource
  WhiteChili.Document.prototype = new WhiteChili.Resource

  // Add document trait
  utils.extend( WhiteChili.Document, WhiteChili.Traits.Document )

  // Get to object and correct some inconsitencies
  WhiteChili.Document.prototype.object = function() {
    // get raw object
    var object = ci.query( this.path )

    // set lower case version of cursor as well for consistency
    object.cursor = object.Cursor

    return object
  }

  // Find all help layers (optionally only visible)
  WhiteChili.Document.prototype.helpLayers = function( visible ) { // visible:boolean
    // prepare set
    var set = new WhiteChili.Set
      , is_help_layer

    // collect
    this.layers().each( function() {
      // find out if it's a help layer
      is_help_layer = this.attr( 'isHelpLayer' ) || regex.help_layer.test( this.attr( 'name' ) )
      
      if ( is_help_layer && ( visible !== true || this.attr( 'visible' ) === true ) )
        set.add( this )
    })

    return set
  }

  // Test presence of folding
  WhiteChili.Document.prototype.hasFolding = function() {
    return !! this.attr( 'foldSettingID' )
  }

  // Test presence of 3D model
  WhiteChili.Document.prototype.hasThreeD = function() {
    return !! this.attr( 'threeDModelID' )
  }

  // Test need for the viewer of 3D model
  WhiteChili.Document.prototype.hasViewer = function() {
    return !! ( this.hasFolding() || this.hasThreeD() )
  }

  // Test presence of selected text
  WhiteChili.Document.prototype.hasSelectedText = function() {
    return this.selectedText().exists()
  }

  // Test if document requires to be renamed
  WhiteChili.Document.prototype.needsName = function() {
    return regex.tmp_name.test( this.attr( 'name' ) ) && ! ci.config.allow_nameless
  }
  
  // Select a given page number
  WhiteChili.Document.prototype.select = function( page ) { // page:string
    // select page
    ci.editor.SetSelectedPage( page )

    return this.zoom( 'fit' )
  }

  // Set zoom
  WhiteChili.Document.prototype.zoom = function( z ) { // z:integer|string
    // zoom to an absolute value
    if ( typeof z == 'number' || regex.integer.test( z ) ) {
      this.attr( 'zoom', z )

      ci.log( 'Zoomed to ' + z + '%' )

    // zoom to fit
    } else if ( z.substr( 0, 3 ) == 'fit' ) {
      ci.execute( 'document.editor', 'Fit', z.split(':')[1] || 'page' )

      ci.log( 'Zoomed to ' + z + ' (' + this.attr( 'zoom' ) + '%)' )

    // zoom in or out
    } else if ( z == 'in' || z == 'out' ) {
      var goal  = z == 'in' ? 1 : -1
        , zoom  = this.attr( 'zoom' )
        , index = map.zooms.indexOf( utils.closest( map.zooms, zoom ) )

      this.zoom( map.zooms[index + goal] || map.zooms[index] )
    }

    return this
  }

  // Set orientation
  WhiteChili.Document.prototype.orientation = function( value ) {
    if ( value == ci.cache.orientation.original )
      ci.doc().attr({ width: ci.cache.orientation.width, height: ci.cache.orientation.height })
    else
      ci.doc().attr({ width: ci.cache.orientation.height, height: ci.cache.orientation.width })
  }

  // Set/get the cursor type
  WhiteChili.Document.prototype.cursor = function( type ) { // type:null|string
    // act as a getter if typs isn't given
    if ( ! type )
      return this.attr( 'cursor' ).attr( 'name' )

    // make sure given cursor type is present in the array
    if ( map.cursors.indexOf( type ) == -1 )
      type = map.cursors[0]

    ci.execute( 'document', 'SetCursor', type )
  }

  // Get properly mapped alternate layout properties
  WhiteChili.Document.prototype.layoutProperties = function() {
    var properties = {}

    // prepare properties objects
    this.alternateLayouts().each( function() {
      var id = this.attr( 'id' )

      // prepare property object
      properties[id] = { name: this.attr( 'name' ) }

      // add original width and height in case of the default layout
      if ( this.attrIs( 'name', '[Default]' ) ) {
        properties[id].width  = utils.cast( ci.api().attr( 'previousWidth' ), 'previousWidth' )
        properties[id].height = utils.cast( ci.api().attr( 'previousHeight' ), 'previousHeight' )
      }
    })

    // collect alternate layout properties
    this.alternateLayoutProperties().each( function() {
      // get layout id
      var match = this.attr( 'layout' ).match( regex.uuid )
        , name  = this.attr( 'name' )
        , id    = match[0]
        
      properties[id][name] = this.attr( 'value' )
    })

    return properties
  }

  // Collect custom created alternate layouts
  WhiteChili.Document.prototype.customAlternateLayouts = function() {
    return this.alternateLayouts().reduce( function() {
      return ! regex.default_name.test( this.attr( 'name' ) )
    })
  }
  
  // Get pages marked for output
  WhiteChili.Document.prototype.pagesForOutput =  function() {
    return this.pages().reduce( function() {
      return this.attrIs( 'includeInOutput', true )
    })
  }

  // Get all table frames
  WhiteChili.Document.prototype.tables = function( i ) {
    var tables = new WhiteChili.Set

    this.frames().where( 'type', 'table' ).each( function() {
      tables.add( new WhiteChili.Table( this.path ) )
    })

    return typeof i == 'number' ? tables.get( i ) : tables
  }

  // Perform save
  WhiteChili.Document.prototype.save = function( after ) {
    if ( this.dirty() ) {
      if ( ! ci.status.saving ) {
        // register saving process
        ci.status.saving = true

        // save document
        ci.execute( 'document', 'Save' )

        // after saving
        ci.status.afterSave = after
      }
    } else if ( typeof after === 'function' ) {
      after()
    }
    
    return this
  }

  // Revert to last saved
  WhiteChili.Document.prototype.revert = function() {
    ci.execute( 'document', 'Revert' )

    return this
  }
  
  // Download all pages
  WhiteChili.Document.prototype.download = function() {
    this.pages().each( function() {
      load.down( this )
    })

    return this
  }

  // Get selected chili frames
  WhiteChili.Document.prototype.selected = function( i ) { // i:number
    // if an index is given, return frame at given index
    if ( typeof i === 'number' )
      return this.selected().get( i )
    
    // prepare set
    var set = new WhiteChili.FrameSet
      , frame

    // try to acces frame on selected text
    if ( ci.query( 'document.selectedText' ) ) {
      frame = ci.query( 'document.selectedText.frame' )

      return set.add( frame.page + '.frames[' + frame.index + ']' )
    }

    if ( ! ci.cache.mixed.selected_cp ) {
      // get the total selected frames
      var length = ci.query( 'document.selectedFrames' ).length

      for ( i = 0; i < +length; i++ ) {
        // get frame
        frame = ci.execute( 'document.selectedFrames', 'GetItemAt', i )

        // add frame
        set.add( new WhiteChili.Frame( frame.page + '.frames[' + frame.index + ']' ) )
      }

      ci.cache.mixed.selected_cp = set.sortBy( 'index' )
    }

    return ci.cache.mixed.selected_cp
  }
  
})( WhiteChili.ci, WhiteChili.ui, WhiteChili.map, WhiteChili.load, WhiteChili.utils, WhiteChili.regex )












;( function() {

  // Font class
  WhiteChili.Font = function( path ) {
    // reference chili object
    this.path = this.build( path )
  }

  // Inherit from WhiteChili.Resource
  WhiteChili.Font.prototype = new WhiteChili.Resource

  // Build font name as used as font family attribute
  WhiteChili.Font.prototype.name = function() {
    return [ this.attr( 'family' ), this.attr( 'style' ) ].join( '_' )
  }
  
})()
;( function( ci, ui, map, utils ) {

  // Frame class
  WhiteChili.Frame = function( frame, geo ) {
    // make sure at least a frame type is selected
    frame = frame || map.frames[0]

    // if a frame type or nothing is given, create a new frame
    this.path = map.frames.indexOf( frame ) > -1 ? this.build( frame, geo ) : frame.javaScriptDescriptor || frame

    // initialize internals
    this.sets = []

    // add paths a resources
    this.hasOne( 'layer' )
        .hasOne( 'frame_constraints', { root: this.path })
        .hasOne( 'frame_content_constraints', { root: this.path })
        .hasMany( 'path' )
  }

  // Inherit from WhiteChili.Resource
  WhiteChili.Frame.prototype = new WhiteChili.Resource

  // Add document trait
  utils.extend( WhiteChili.Frame, WhiteChili.Traits.Frame )

  // Build frame
  WhiteChili.Frame.prototype.build = function( type, geo ) {
    var page  = ci.api().selectedPage()
      // build frame path
      , path  = 'document.pages[' + page.index() + ']' + '.frames'
      // get default values
      , def   = ci.config.default_value || {}
      // define geometry
      , geo   = ensureGeometry( geo )
      // create frame
      , obj   = ci.execute( path, 'Add', type, geo.x, geo.y, geo.width, geo.height )
      // reference new frame
      , frame = ci.doc().selected( 0 )
      // get help layer frames
      , tops  = page.topFrames()

    setTimeout( function() {
      var f = ci.api().selected(0)
      // add basic text in case of a text frame
      if ( type == 'text' ) {
        // get placeholder
        var text  = ci.api().viewPreferences( 'customPlaceHolderText' )
          , font  = ci.api().fonts().find( 'id', def.font_item_id ) || ci.api().fonts( 0 )
          , color = ci.api().defaultColor( 'text' )

        // apply text flow
        f.textFlow( map.xmlFlow.replace( '{{text}}', text ) )

        // apply styling
        f.textFlow( function( flow ) {
          flow.find( 'p, [fontFamily]' )
            .attr({ 'fontFamily': font.name() })
          flow.find( 'p, [textAlign]' )
            .attr({ 'textAlign':  def.text_align || 'left' })
          flow.find( 'p, [fontSize]' )
            .attr({ 'fontSize':  def.font_size || 16 })
          flow.find( 'span, [color]' )
            .attr({ 'color': color.hex(), 'my_color': color.attr( 'id' ) })

          return flow
        })

      } else if ( type != 'image' ) {
        // apply fill color
        f.fill( ci.api().defaultColor( 'fill' ) )
        f.stroke( ci.api().defaultColor( 'stroke' ) )

        // apply stroke width
        f.strokeWidth( type == 'line' ? 3 : 0 )
      }

      // insert frame before first help layer frame
      if ( tops.any() )
        f.attr( 'index', tops.first().index() )
      
    }, 150 )
    
    // create frame
    return obj.javaScriptDescriptor
  }

  // Select frame
  WhiteChili.Frame.prototype.select = function() {
    // select page
    this.page().select()

    // select frame
    ci.execute( this.path, 'Select' )

    return this
  }

  // Get index
  WhiteChili.Frame.prototype.index = function( index ) {
    // act as a getter
    if ( typeof index !== 'number' )
      return this.attr( 'index' )

    // continue as a setter
    return this.attr( 'index', index )
  }

  // Get frame Page
  WhiteChili.Frame.prototype.page = function() {
    return new WhiteChili.Page( this.attr( 'page' ) )
  }

  // Frame fill
  WhiteChili.Frame.prototype.fill = function( color ) {
    // act as a setter
    if ( color )
      return this.attr({ fillColor: color, hasFill: color.has() })

    // act as getter
    return this.attr( 'fillColor' )
  }

  // Stroke color
  WhiteChili.Frame.prototype.stroke = function( color ) {
    // act as a setter
    if ( color ) {
      // ensure border width
      if ( color.has() && this.attrIs( 'borderWidth', 0 ) )
        this.attr( 'borderWidth', 1 )

      // set color and broder apprearance
      return this.attr({
        borderColor: color
      , hasBorder:   color.has()
      })
    }
    
    // act as getter
    return this.attr( 'borderColor' )
  }
  
  // Destroy frame
  WhiteChili.Frame.prototype.destroy = function() {
    // remove frame from any sets
    for ( var i = ( this.sets || [] ).length - 1; i >= 0; i-- )
      this.sets[i].remove( this )

    // remove frame from chili
    ci.execute( this.path, 'Delete' )

    // clear references to prevent memory leaks
    delete this.sets
    delete this.instance
  }

  //////////////////////////////
  ////////// helpers ///////////
  //////////////////////////////

  // Ensure presence of all geometry values
  function ensureGeometry( geo ) {
    // get default values
    var def = ci.config.default_value || {}

    // ensure a geometry object
    geo = geo || {}

    // merge values with absolute fallback
    geo.x      = geo.x      != null ? geo.x      : def.frame_x      != null ? def.frame_x      : 30
    geo.y      = geo.y      != null ? geo.y      : def.frame_y      != null ? def.frame_y      : 30
    geo.width  = geo.width  != null ? geo.width  : def.frame_width  != null ? def.frame_width  : 30
    geo.height = geo.height != null ? geo.height : def.frame_height != null ? def.frame_height : 30

    return geo
  }


})( WhiteChili.ci, WhiteChili.ui, WhiteChili.map, WhiteChili.utils )
















;( function( utils ) {

  // FrameConstraints class
  WhiteChili.FrameConstraints = function( path ) {
    // reference chili object
    this.path = this.build( path )
  }

  // Inherit from WhiteChili.Resource
  WhiteChili.FrameConstraints.prototype = new WhiteChili.Resource

  // Add colors trait
  utils.extend( WhiteChili.FrameConstraints, WhiteChili.Traits.FrameConstraints )
  
})( WhiteChili.utils )
;( function( ci, utils ) {

  // FrameContentConstraints class
  WhiteChili.FrameContentConstraints = function( path ) {
    // reference chili object
    this.path = this.build( path )
  }

  // Inherit from WhiteChili.Resource
  WhiteChili.FrameContentConstraints.prototype = new WhiteChili.Resource

  // Add colors trait
  utils.extend( WhiteChili.FrameContentConstraints, WhiteChili.Traits.FrameContentConstraints )
  
})( WhiteChili.ci, WhiteChili.utils )
;( function( ci, utils ) {

  // History class
  WhiteChili.History = function() {
    this.path = 'document.undoManager'
  }

  // Inherit from WhiteChili.Resource
  WhiteChili.History.prototype = new WhiteChili.Resource

  // Undo last action
  WhiteChili.History.prototype.undo = function() {
    ci.execute( 'document.undoManager', 'Undo', 1 )
  }

  // Redo last action
  WhiteChili.History.prototype.redo = function() {
    ci.execute( 'document.undoManager', 'Redo', 1 )
  }

  // Get current undo and redo status
  WhiteChili.History.prototype.status = function() {
    return {
      undos: parseStack( this.attr( 'undoStack' ) )
    , redos: parseStack( this.attr( 'redoStack' ) )
    }
  }

  // Unified stack parser for Flash and HTML5 version
  function parseStack( stack ) {
    if ( typeof stack === 'string' ) {
      // try to find a comma separated array
      if ( stack.match( /\[/ ) )
        return stack.split( ',' ).length
      
      // try to find an integer
      if ( stack = stack.match( /\d+/ ) )
        return stack[0] * 1
    }
    
    return 0
  }

})( WhiteChili.ci, WhiteChili.utils )
;( function( regex ) {

  // Number class
  WhiteChili.Number = function( value, unit ) {
    // initialize defaults
    this.value = 0
    this.unit  = unit || ''

    // parse value
    if ( typeof value === 'number' ) {
      // ensure a valid numeric value
      this.value = isNaN( value ) ?
        0 :
      ! isFinite( value ) ?
        ( value < 0 ? Number.MIN_VALUE : Number.MAX_VALUE ) :
        value

    } else if ( typeof value === 'string' ) {
      unit = value.match( regex.number )

      if ( unit ) {
        // make value numeric
        this.value = +unit[1]
      
        // normalize
        if ( unit[2] == '%' )
          this.value /= 100
      
        // store unit
        this.unit = unit[2]
      }

    } else {
      if ( value instanceof WhiteChili.Number ) {
        this.value = value.valueOf()
        this.unit  = value.unit
      }
    }

  }

  // Add number
  WhiteChili.Number.prototype.plus = function( number ) {
    return new WhiteChili.Number( this + new WhiteChili.Number( number ), this.unit )
  }
  // Subtract number
  WhiteChili.Number.prototype.minus = function( number ) {
    return this.plus( -new WhiteChili.Number( number ) )
  }
  // Multiply number
  WhiteChili.Number.prototype.times = function( number ) {
    return new WhiteChili.Number( this * new WhiteChili.Number( number ), this.unit )
  }
  // Divide number
  WhiteChili.Number.prototype.divide = function( number ) {
    return new WhiteChili.Number( this / new WhiteChili.Number( number ), this.unit )
  }
  // Convert to different unit
  WhiteChili.Number.prototype.to = function( unit ) {
    var number = new WhiteChili.Number( this )
    
    if ( typeof unit === 'string' )
      number.unit = unit

    return number
  }

  // Convert object to cp_object string
  WhiteChili.Number.prototype.toString = function() {
    return (
      this.unit == '%' ?
        ~~( this.value * 1e8 ) / 1e6:
        this.value
    ) + this.unit
  }

  // Convert to primitive
  WhiteChili.Number.prototype.valueOf = function() {
    return this.value
  }

})( WhiteChili.regex )





;( function( ci, regex ) {

  // Page class
  WhiteChili.Page = function( path ) {
    // reference chili object
    this.path = this.build( path )

    // register resources
    this.hasMany( 'frame' )
  }

  // Inherit from WhiteChili.Resource
  WhiteChili.Page.prototype = new WhiteChili.Resource
  
  // Select frame
  WhiteChili.Page.prototype.select = function() {
    ci.doc().select( this.attr( 'name' ) )

    return this
  }

  // Destroy page
  WhiteChili.Page.prototype.destroy = function() {
    // remove page from chili
    ci.execute( this.path, 'Delete' )

    // clear references to prevent memory leaks
    delete this.instance
  }

  // Clear frames
  WhiteChili.Page.prototype.clear = function() {
    // remove all frames
    this.frames().destroy()

    return this
  }

  // Get page thumbnail
  WhiteChili.Page.prototype.imageUrl = function( subject, ext ) {
    // get parameters
    var id   = ci.doc().attr( 'id' )
      , env  = ci.config.chili_env
      , name = this.attr( 'name' )

    // ensure extention
    ext = ext ? '.' + ext : ''
      
    // build url
    return '/' + env + '/templates/' + id + '/' + subject + '/' + name + ext
  }

  // Get page thumbnail
  WhiteChili.Page.prototype.thumbnailUrl = function() {
    return this.imageUrl( 'thumbnail', 'json' )
  }

  // Get page download
  WhiteChili.Page.prototype.downloadUrl = function() {
    return this.imageUrl( 'download' )
  }
  
})( WhiteChili.ci, WhiteChili.regex )
;( function() {

  // ParagraphStyle class
  WhiteChili.ParagraphStyle = function( path ) {
    // reference chili object
    this.path = this.build( path )

    this.hasOne( 'format', { root: this.path } )
        .hasOne( 'color' )
  }

  // Inherit from WhiteChili.Resource
  WhiteChili.ParagraphStyle.prototype = new WhiteChili.Resource
  
})()
;( function() {

  // Path class
  WhiteChili.Path = function( path ) {
    // reference chili object
    this.path = this.build( path )

    // add points a resources
    this.hasMany( 'point' )
  }

  // Inherit from WhiteChili.Resource
  WhiteChili.Path.prototype = new WhiteChili.Resource
  
})()
;( function( utils ) {

  // Preflight result class
  WhiteChili.PreflightResult = function( path ) {
    // reference chili object
    this.path = this.build( path )
  }

  // Inherit from WhiteChili.Resource
  WhiteChili.PreflightResult.prototype = new WhiteChili.Resource

  // Add document trait
  utils.extend( WhiteChili.PreflightResult, WhiteChili.Traits.PreflightResult )
  
})( WhiteChili.utils )
;( function() {

  // Table class
  WhiteChili.Table = function( path ) {
    // reference chili object
    this.path = path

    // add points a resources
    this.hasMany( 'cell', { root: path + '.allCells' })
  }

  // Inherit from WhiteChili.Resource
  WhiteChili.Table.prototype = new WhiteChili.Resource
  
})()
;( function( ci ) {

  // Text class
  WhiteChili.Text = function() {
    // reference chili object
    this.path = 'document.selectedText'

    // register resources
    this.hasOne( 'text_format' )
  }

  // Inherit from WhiteChili.Resource
  WhiteChili.Text.prototype = new WhiteChili.Resource
  
  // Collect colors from current selection
  WhiteChili.Text.prototype.colors = function() {
    // prepare color set
    var colors = new WhiteChili.ColorSet()

    // make sure some text is selected
    if ( this.attr( 'length' ) > 0 )
      colors.add( this.textFormat().attr( 'color' ) )
    else
      colors.add( this.textFormat().paragraphStyle().attr( 'color' ) )

    return colors
  }

  // Get current text size
  WhiteChili.Text.prototype.size = function() {
    if ( this.attr( 'length' ) > 0 )
      return this.textFormat().attr( 'fontSize' )
    else
      return this.textFormat().paragraphStyle().attr( 'fontSize' )
  }

  // Get current text align
  WhiteChili.Text.prototype.align = function() {
    if ( this.attr( 'length' ) > 0 )
      return this.textFormat().attr( 'textAlign' )
    else
      return this.textFormat().paragraphStyle().attr( 'textAlign' )
  }

  // Get current font family
  WhiteChili.Text.prototype.font = function() {
    return this.textFormat().font()
  }
  
})( WhiteChili.ci )
;( function( ci ) {

  // TextFormat class
  WhiteChili.TextFormat = function() {
    // reference chili object
    this.path = 'document.selectedText.textFormat'

    // register resources
    this.hasOne( 'character_style' )
        .hasOne( 'paragraph_style' )
        .hasOne( 'font' )
  }

  // Inherit from WhiteChili.Resource
  WhiteChili.TextFormat.prototype = new WhiteChili.Resource
  
})( WhiteChili.ci )
;( function( ci ) {

  // WorkSpace class
  WhiteChili.WorkSpace = function( path ) {
    // reference chili object
    this.path = this.build( path )

    // register resources
    this.hasMany( 'frame' )
  }

  // Inherit from WhiteChili.Resource
  WhiteChili.WorkSpace.prototype = new WhiteChili.Resource
  
})( WhiteChili.ci )
;( function( ci, map, utils, regex ) {

  // Cursor class
  WhiteChili.Models.Api = function() {
    this.selector = 'document'

    // build selectors
    var table_selector = "document > pages > item > frames > item[type=table]"
      , image_selector = "document > pages > item > frames > item[type=image]"
      , view_selector  = "document > pages > item > frames > item[layerName*='layer.mask.preview.']"
      , edit_selector  = "document > pages > item > frames > item[layerName*='layer.mask.edit.']"

    // build relationships
    this.hasOne( 'view_preferences' )
        .hasOne( 'preflight_preferences' )
        .hasOne( 'frame_constraints' )
        .hasOne( 'frame_content_constraints' )
        .hasMany( 'table',           { order: 'index', model: 'frame', selector: table_selector })
        .hasMany( 'image',           { order: 'index', model: 'frame', selector: image_selector })
        .hasMany( 'mask_view_frame', { order: 'index', model: 'frame', selector: view_selector  })
        .hasMany( 'mask_edit_frame', { order: 'index', model: 'frame', selector: edit_selector  })
        .hasMany( 'font' )
        .hasMany( 'page' )
        .hasMany( 'layer' )
        .hasMany( 'color' )
        .hasMany( 'variable' )
        .hasMany( 'paragraph_style' )
        .hasMany( 'preflight_result' )
  }

  // Inherit from WhiteChili.Resource
  WhiteChili.Models.Api.prototype = new WhiteChili.Model

  // Add document trait
  utils.extend( WhiteChili.Models.Api, WhiteChili.Traits.Document )

  // Find all help layers (optionally only visible)
  WhiteChili.Models.Api.prototype.helpLayers = function( visible ) { // visible:boolean
    // prepare set
    var set = new WhiteChili.Set
      , layer, is_help_layer

    // collect
    this.layers().each( function() {
      // get the corresponding chili object
      layer = ci.query( 'document.layers[' + this.attr( 'id' ) + ']' )

      // find out if it's a help layer
      is_help_layer = layer.isHelpLayer == 'true' || regex.help_layer.test( layer.name )

      if ( is_help_layer && ( visible !== true || layer.visible == 'true' ) )
        set.add( this )
    })

    return set
  }

  // Get all table frames
  WhiteChili.Models.Api.prototype.tables = function( i ) {
    var tables = new WhiteChili.Set

    this.frames().where( 'type', 'table' ).each( function() {
      tables.add( new WhiteChili.Table( this.path ) )
    })

    return typeof i == 'number' ? tables.get( i ) : tables
  }

  // Get the cursor type
  WhiteChili.Models.Api.prototype.cursor = function( type ) {
    // act as a getter if typs isn't given
    if ( ! type )
      return ci.status.cursor
    
    // make sure not to execute when unnecessary
    if ( type != ci.status.cursor )
      ci.execute( 'document', 'SetCursor', type )
  }

  // Get selected chili frames
  WhiteChili.Models.Api.prototype.selected = function( i ) { // i:number|null
    var set = new WhiteChili.Collections.Frame
      , frame

    // try to acces frame on selected text
    if ( ci.query( 'document.selectedText' ) ) {
      frame = ci.query( 'document.selectedText.frame' )
      
      return set.add( this.selector + ' > pages > item > frames > item[id=' + frame.id + ']' )
      
    } else if ( typeof i === 'number' ) {
      // get frame at index
      if ( frame = ci.execute( 'document.selectedFrames', 'GetItemAt', i ) )
        return new WhiteChili.Models.Frame( this.selector + ' > pages > item > frames > item[id=' + frame.id + ']' )

      return frame
    }

    if ( ! ci.cache.mixed.selected ) {
      // get the total selected frames
      var length = ci.query( 'document.selectedFrames' ).length

      for ( i = 0; i < +length; i++ ) {
        // get frame
        frame = ci.execute( 'document.selectedFrames', 'GetItemAt', i )

        // add frame
        set.add( new WhiteChili.Models.Frame( this.selector + ' > pages > item > frames > item[id=' + frame.id + ']' ) )
      }

      // store selected
      ci.cache.mixed.selected = set
    }
    
    return ci.cache.mixed.selected
  }

  // Get selected text as an object
  WhiteChili.Models.Api.prototype.selectedText = function() {
    return new WhiteChili.Models.Text
  }

  // Test if document currently has selected text
  WhiteChili.Models.Api.prototype.hasSelectedText = function() {
    return !! ci.query( 'document.selectedText' )
  }

  // Get currently selected page
  WhiteChili.Models.Api.prototype.selectedPage = function() {
    return this.pages( ci.status.page )
  }
  
})( WhiteChili.ci, WhiteChili.map, WhiteChili.utils, WhiteChili.regex )
;( function( utils ) {

  // Color class
  WhiteChili.Models.Color = function( selector ) {
    this.selector = selector
  }

  // Inherit from WhiteChili.Model
  WhiteChili.Models.Color.prototype = new WhiteChili.Model

  // Add colors trait
  utils.extend( WhiteChili.Models.Color, WhiteChili.Traits.Color )
  
})( WhiteChili.utils )
;( function( ci ) {

  // Flow class
  WhiteChili.Models.Flow = function( flow ) {
    // parse flow to XML DOM
    this.dom = $( $.parseXML( flow ) )
  }

  // Query flow DOM
  WhiteChili.Models.Flow.prototype.find = function( selector ) {
    return this.dom.find( selector )
  }

  // Collect attribute values from DOM nodes
  WhiteChili.Models.Flow.prototype.collect = function( name ) {
    var values = []

    // collect attribute values
    this.find( '[' + name + ']' ).each( function() {
      values.push( $( this ).attr( name ) )
    })
    
    return values
  }

  // Import string to given textarea
  WhiteChili.Models.Flow.prototype.importTo = function( frame ) {
    frame.textFlow( this )

    return this
  }

  // Convert flow to string
  WhiteChili.Models.Flow.prototype.toString = function() {
    return $( '<div>' ).append( this.dom.find( 'TextFlow' ).get(0) ).html()
  }
  
})( WhiteChili.ci )
;( function( utils ) {

  // Font class
  WhiteChili.Models.Font = function( selector ) {
    this.selector = selector
  }

  // Inherit from WhiteChili.Model
  WhiteChili.Models.Font.prototype = new WhiteChili.Model

  // Build font name as used as font family attribute
  WhiteChili.Models.Font.prototype.name = function() {
    return [ this.attr( 'family' ), this.attr( 'style' ) ].join( '_' )
  }
  
})( WhiteChili.utils )
;( function( ci, utils ) {

  // Cursor class
  WhiteChili.Models.Frame = function( selector ) {
    this.selector = selector

    this.hasOne( 'frame_content_constraints', { selector: selector })
        .belongsTo( 'page' )
  }

  // Inherit from WhiteChili.Resource
  WhiteChili.Models.Frame.prototype = new WhiteChili.Model

  // Add document trait
  utils.extend( WhiteChili.Models.Frame, WhiteChili.Traits.Frame )

  // Get frame constraints
  WhiteChili.Models.Frame.prototype.frameConstraints = function() {
    return new WhiteChili.FrameConstraints( this.path() + '.frameConstraints' )
  }

  // Get layer
  WhiteChili.Models.Frame.prototype.layer = function() {
    return new WhiteChili.Models.Layer( 'document > layers > item[id=' + this.attr( 'layer' ) + ']' )
  }

  // Get frame geometry
  WhiteChili.Models.Frame.prototype.geo = function() {
    return {
      x:       this.attr( 'x' )
    , y:       this.attr( 'y' )
    , width:   this.attr( 'width' )
    , height:  this.attr( 'height' )
    , fitMode: this.attr( 'fitMode' )
    , layer:   this.layer()
    }
  }

  // Frame fill
  WhiteChili.Models.Frame.prototype.fill = function( color ) {
    // act as a setter
    if ( color )
      return this.attr({ fillColor: color, hasFill: color.has() })

    // act as getter
    return this.attrColor( 'fill' )
  }

  // Stroke color
  WhiteChili.Models.Frame.prototype.stroke = function( color ) {
    // act as a setter
    if ( color ) {
      // ensure border width
      if ( color.has() && this.attrIs( 'borderWidth', 0 ) )
        this.attr( 'borderWidth', 1 )

      // set color and broder apprearance
      return this.attr({
        borderColor: color
      , hasBorder:   color.has()
      })
    }
    
    // act as getter
    return this.attrColor( 'border' )
  }

  // Select frame
  WhiteChili.Models.Frame.prototype.select = function() {
    // select frame
    ci.execute( this.path(), 'Select' )

    return this
  }

  // Destroy frame
  WhiteChili.Models.Frame.prototype.destroy = function() {
    ci.execute( this.path(), 'Delete' )
  }

  // Dynamically build Chili object path
  WhiteChili.Models.Frame.prototype.path = function() {
    return 'document.allFrames[' + ( this.attr( 'id' ) || utils.uuid( this.selector ) ) + ']'
  }

})( WhiteChili.ci, WhiteChili.utils )
;( function( utils ) {

  // Frame constraints class
  WhiteChili.Models.FrameConstraints = function( selector ) {
    this.selector = selector
  }

  // Inherit from WhiteChili.Model
  WhiteChili.Models.FrameConstraints.prototype = new WhiteChili.Model

  // Add colors trait
  utils.extend( WhiteChili.Models.FrameConstraints, WhiteChili.Traits.FrameConstraints )
  
})( WhiteChili.utils )
;( function( utils ) {

  // FrameContentConstraints class
  WhiteChili.Models.FrameContentConstraints = function( selector ) {
    this.selector = selector
  }

  // Inherit from WhiteChili.Model
  WhiteChili.Models.FrameContentConstraints.prototype = new WhiteChili.Model

  // Add colors trait
  utils.extend( WhiteChili.Models.FrameContentConstraints, WhiteChili.Traits.FrameContentConstraints )
  
})( WhiteChili.utils )
;( function( utils ) {

  // Layer class
  WhiteChili.Models.Layer = function( selector ) {
    this.selector = selector
  }

  // Inherit from WhiteChili.Model
  WhiteChili.Models.Layer.prototype = new WhiteChili.Model
  
})( WhiteChili.utils )
;( function( ci, utils ) {

  // Page class
  WhiteChili.Models.Page = function( selector ) {
    this.selector = selector

    // build specific frame selectors
    var temp_selector = selector + " > frames > item[tag*='white_chili.background_template']"
      , help_selector = selector + " > frames > item[layerName*='layer.help.']"
      , mask_selector = selector + " > frames > item[layerName*='layer.mask.']"
      , top_selector  = help_selector + ', ' + mask_selector
      , bg_selector   = selector + " > frames > item[layerName='layer.background']"

    // relationships
    this.hasMany( 'frame',           { order: 'index' })
        .hasMany( 'temp_frame',      { order: 'index', model: 'frame', selector: temp_selector })
        .hasMany( 'help_frame',      { order: 'index', model: 'frame', selector: help_selector })
        .hasMany( 'mask_frame',      { order: 'index', model: 'frame', selector: mask_selector })
        .hasMany( 'top_frame',       { order: 'index', model: 'frame', selector: top_selector  })
        .hasMany( 'bg_frame',        { order: 'index', model: 'frame', selector: bg_selector   })
        .hasMany( 'i_frame',         { model: 'frame' })
  }

  // Inherit from WhiteChili.Resource
  WhiteChili.Models.Page.prototype = new WhiteChili.Model

  // Get background template geometry
  WhiteChili.Models.Page.prototype.backgroundTemplate = function() {
    var t = ci.cache.mixed['backgroundTemplate.' + this.attr( 'id' )]

    return { x: t.x, y: t.y, width: t.width, height: t.height }
  }

  // Set page orientation
  WhiteChili.Models.Page.prototype.orientation = function( value ) {
    var frame = this.bgFrames().first()
    
    if ( frame ) frame.attr( utils.portland( this.backgroundTemplate(), value ) )
  }
  
})( WhiteChili.ci, WhiteChili.utils )
;( function( utils ) {

  // PreflightResult class
  WhiteChili.Models.PreflightResult = function( selector ) {
    this.selector = selector
  }

  // Inherit from WhiteChili.Model
  WhiteChili.Models.PreflightResult.prototype = new WhiteChili.Model

  // Add colors trait
  utils.extend( WhiteChili.Models.PreflightResult, WhiteChili.Traits.PreflightResult )
  
})( WhiteChili.utils )
;( function( ci ) {

  // Text class
  WhiteChili.Models.Text = function() {
    this.object = ci.query( this.path())
  }

  // Inherit from WhiteChili.Object
  WhiteChili.Models.Text.prototype = new WhiteChili.Object

  // Get text format as an object
  WhiteChili.Models.Text.prototype.textFormat = function() {
    return new WhiteChili.Models.TextFormat
  }

  // Collect colors from current selection
  WhiteChili.Models.Text.prototype.colors = function() {
    // prepare color set
    var colors = new WhiteChili.Collections.Color()

    // make sure some text is selected
    if ( this.attr( 'length' ) > 0 ) {
      colors.add( this.textFormat().attr( 'color' ))
    } else {
      colors.add( this.textFormat().paragraphStyle().attr( 'color' ))
    }

    return colors
  }

  // Get current text size
  WhiteChili.Models.Text.prototype.size = function() {
    if ( this.attr( 'length' ) > 0 )
      return this.textFormat().attr( 'fontSize' )
    else
      return this.textFormat().paragraphStyle().attr( 'fontSize' )
  }

  // Get current text align
  WhiteChili.Models.Text.prototype.align = function() {
    var align

    if ( this.attr( 'length' ) > 0 ) {
      align = this.textFormat().attr( 'textAlign' )
    } else {
      align = this.textFormat().paragraphStyle().attr( 'textAlign' )
    }

    return align == '[error]' ? 'center' : align
  }

  // Get current font family
  WhiteChili.Models.Text.prototype.font = function() {
    return this.textFormat().font()
  }

  // Get path
  WhiteChili.Models.Text.prototype.path = function() {
    return 'document.selectedText'
  }

})( WhiteChili.ci )

;( function( ci ) {

  // TextFormat class
  WhiteChili.Models.TextFormat = function( path ) {
    this.object = ci.query( this.path() )
  }

  // Inherit from WhiteChili.Object
  WhiteChili.Models.TextFormat.prototype = new WhiteChili.Object

  // Get font as an object
  WhiteChili.Models.TextFormat.prototype.font = function() {
    return new WhiteChili.Object( this.path() + '.font' )
  }

  // Get paragraph style as an object
  WhiteChili.Models.TextFormat.prototype.paragraphStyle = function() {
    return new WhiteChili.Object( this.path() + '.paragraphStyle' )
  }
  
  // Get path
  WhiteChili.Models.TextFormat.prototype.path = function() {
    return 'document.selectedText.textFormat'
  }

})( WhiteChili.ci )
;( function() {
  // define some convenience methods
  var convenience = [
    'hex'
  , 'rgb'
  ]

  // Set for colors
  WhiteChili.ColorSet = function() {
    // register member class
    this.member_class = WhiteChili.Color

    // initialize store
    this.members = []
  }

  // Inherit from WhiteChili.Set
  WhiteChili.ColorSet.prototype = new WhiteChili.Set

  // Define convenience methods
  convenience.forEach( function( method ) {
    WhiteChili.ColorSet.prototype[method] = function() {
      return this.collect( function() {
        return this[method]()
      })
    }
  })

})()
;( function( utils ) {
  // define some convenience methods
  var fillable = [ 'fill', 'stroke' ]
    , redirect = [ 'front', 'back', 'forward', 'backward', 'destroy', 'strokeWidth', 'strokeEdge' ]

  // Set for frames
  WhiteChili.FrameSet = function() {
    // register member class
    this.member_class = WhiteChili.Frame

    // initialize store
    this.members = []
  }

  // Inherit from WhiteChili.Set
  WhiteChili.FrameSet.prototype = new WhiteChili.Set

  // Select all frames in set
  WhiteChili.FrameSet.prototype.select = function() {
    return this.each( function() {
      this.select()
    })
  }

  // Get all colors for a given attribute/purpose
  WhiteChili.FrameSet.prototype.colors = function( purpose ) {
    // call to convenience methods
    if ( fillable.indexOf( purpose ) > -1 )
      return this[purpose]()

    // collect colors from every text frame
    return this.textColors()
  }

  // Get text colors from every text frame
  WhiteChili.FrameSet.prototype.textColors = function() {
    var set = new WhiteChili.ColorSet
      , colors

    // fetch all text colors from every text frame
    this.each( function() {
      if ( colors = this.textColors() )
        set.add( colors )
    })

    return set
  }

  // Get text sizes from every text frame
  WhiteChili.FrameSet.prototype.textSizes = function() {
    var list = []
      , sizes

    // fetch all text sizes from every text frame
    this.each( function() {
      if ( sizes = this.textSizes() )
        list = list.concat( sizes )
    })

    return utils.unique( list )
  }

  // Get fonts from every text frame
  WhiteChili.FrameSet.prototype.fonts = function() {
    var list = []
      , fonts

    // fetch all fonts from every text frame
    this.each( function() {
      if ( fonts = this.fonts() )
        list = list.concat( fonts )
    })

    return utils.unique( list )
  }

  // Define convenience methods for fillable
  fillable.forEach( function( method ) {
    WhiteChili.FrameSet.prototype[method] = function( value ) {
      // act as a setter
      if ( value )
        return this.each( function() { this[method]( value ) })
      
      // act as a getter
      value = new WhiteChili.ColorSet()

      this.each( function() {
        value.add( this[method]() )
      })

      return value
    }
  })

  // Define plain convenience methods
  redirect.forEach( function( method ) {
    WhiteChili.FrameSet.prototype[method] = function() {
      var values = arguments
      
      return this.each( function() {
        this[method].apply( this, values || [] )
      })
    }
  })

})( WhiteChili.utils )
;( function() {
  // define some convenience methods
  var convenience = [
    'hex'
  , 'rgb'
  ]

  // Collection for colors
  WhiteChili.Collections.Color = function() {
    // register member class
    this.member_class = WhiteChili.Models.Color

    // initialize store
    this.members = []
  }

  // Inherit from WhiteChili.Set
  WhiteChili.Collections.Color.prototype = new WhiteChili.Set

  // Define convenience methods
  convenience.forEach( function( method ) {
    WhiteChili.Collections.Color.prototype[method] = function() {
      return this.collect( function() {
        return this[method]()
      })
    }
  })

})()
;( function( ci, utils ) {
  // define some convenience methods
  var fillable = [ 'fill', 'stroke' ]
    , redirect = [ 'front', 'back', 'forward', 'backward', 'strokeWidth', 'strokeEdge' ]
  
  // Collection for colors
  WhiteChili.Collections.Frame = function() {
    // register member class
    this.member_class = WhiteChili.Models.Frame

    // initialize store
    this.members = []
  }

  // Inherit from WhiteChili.Set
  WhiteChili.Collections.Frame.prototype = new WhiteChili.Set

  // Get all colors for a given attribute/purpose
  WhiteChili.Collections.Frame.prototype.colors = function( purpose ) {
    // call to convenience methods
    if ( fillable.indexOf( purpose ) > -1 )
      return this[purpose]()

    // collect colors from every text frame
    return this.textColors()
  }

  // Get text colors from every text frame
  WhiteChili.Collections.Frame.prototype.textColors = function() {
    var set = new WhiteChili.Collections.Color
      , colors

    // fetch all text colors from every text frame
    this.each( function() {
      if ( colors = this.textColors() )
        set.add( colors )
    })

    return set
  }

  // Get text sizes from every text frame
  WhiteChili.Collections.Frame.prototype.textSizes = function() {
    var list = []
      , sizes

    // fetch all text sizes from every text frame
    this.each( function() {
      if ( sizes = this.textSizes() )
        list = list.concat( sizes )
    })

    return utils.unique( list )
  }

  // Get fonts from every text frame
  WhiteChili.Collections.Frame.prototype.fonts = function() {
    var list = []
      , fonts

    // fetch all fonts from every text frame
    this.each( function() {
      if ( fonts = this.fonts() )
        list = list.concat( fonts )
    })

    return utils.unique( list )
  }

  // Destroy all frames in the set
  WhiteChili.Collections.Frame.prototype.destroy = function() {
    // collect paths for destruction
    var paths = this.collect( function() {
      return this.path()
    })

    // destroy paths
    paths.forEach( function( path ) {
      ci.execute( path, 'Delete' )
    })
  }

  // Define convenience methods for fillable
  fillable.forEach( function( method ) {
    WhiteChili.Collections.Frame.prototype[method] = function( value ) {
      // act as a setter
      if ( value )
        return this.each( function() { this[method]( value ) })
      
      // act as a getter
      value = new WhiteChili.Collections.Color()

      this.each( function() {
        value.add( this[method]() )
      })

      return value
    }
  })

  // Define plain convenience methods
  redirect.forEach( function( method ) {
    WhiteChili.Collections.Frame.prototype[method] = function() {
      var values = arguments
      
      return this.each( function() {
        this[method].apply( this, values || [] )
      })
    }
  })

})( WhiteChili.ci, WhiteChili.utils )
//});
;( function( ci, load, build, i18n ) {
  // get editor frame
  var doc    = document.getElementById( 'editor' )
    , frame  = window.frames['editor']

  // register start time
  ci.status.start  = ( new Date ).getTime()

  // fetch config
  ci.config = white_chili_config()

  // add proxy for callback handler
  window.OnEditorEvent = WhiteChili.handler

  // show splash screen
  build.splash()

  // initialize app on load
  doc.onload = function() {
    // update splash loader
    WhiteChili.ui.loaderGrowth( 30, i18n.t( 'loader.splash.sort_fonts' ) )

    // only proceed if it's a chili iframe
    if ( this.contentWindow.GetEditor ) {
      // enable keymaster
      keymasterify( window )
      keymasterify( this.contentWindow )

      // hook into native chili interface for activation
      this.contentWindow.GetEditor( function() {
        // create new app instance
        new WhiteChili( frame.editorObject )

        // update loader bar
        WhiteChili.ui.loaderGrowth( 30, i18n.t( 'loader.splash.mix_colors' ) )

        // calculate tame taken since start
        ci.log( 'Activated after ' + ( ( new Date ).getTime() - ci.status.start ) + 'ms' )

        // reference app instance on window if not in production
        if ( ci.config.env == 'development' )
          window.ci = ci

        // create performer
        window.perform = function( action, value ) {
          ci.controller.perform( action, value )
        }
      })
    }
  }

  // try to catch errors on the iframe
  doc.onerror = function() {
    ci.log( 'Failed to load the Chili frame!!!', 'error' )
    reloadEditor()
  }

  // start loading the editor
  doc.src = doc.getAttribute( 'data-src' )

  // initialize loading status
  WhiteChili.ui.loaderGrowth( 5, i18n.t( 'loader.splash.wake_up' ) )

  // reload if the app has not been loaded in 30 seconds
  setTimeout( function() {
    if ( ci.status.loader < 85 )
      location.href = location.href
  }, 60000 )

  //////////////////////////////
  ////////// helpers ///////////
  //////////////////////////////

  // Perform a hard reload
  function reloadEditor() {
    window.location.href = window.location.href
  }

})( WhiteChili.ci, WhiteChili.load, WhiteChili.build, WhiteChili.i18n )
