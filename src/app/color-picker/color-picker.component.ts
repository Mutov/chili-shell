import { Component, OnInit } from '@angular/core';
import {ColorService} from '../services/color.service';

@Component({
  selector: 'app-color-picker',
  templateUrl: './color-picker.component.html',
  styleUrls: ['./color-picker.component.scss']
})
export class ColorPickerComponent implements OnInit {
  color: any;

  constructor(private colorService: ColorService) { }

  ngOnInit() {
    if (this.colorService.currentColor) {
      this.color = this.colorService.currentColor.name;
    }
  }

  colorPickerChange() {
    this.colorService.addColor(this.color);
  }

}
