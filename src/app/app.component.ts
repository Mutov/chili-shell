import {
  Component,
  SecurityContext,
  ViewChild,
  ElementRef,
  OnInit,
  NgZone
} from "@angular/core";
import { BrowserModule, DomSanitizer } from "@angular/platform-browser";
import { ChiliShellService } from "./chili-shell/services/chili-shell.service";
import { ColorService } from "./services/color.service";
import { FrameHandleService } from "./services/frame-handle.service";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent implements OnInit {
  //getting html element iFrame

  //<------- Init bundle START ------->
  // editor is manipulation object to interact with iFrame
  public editor;
  public eManipulation;
  public editorStatus;
  public mainFrameID;
  //<------- Init bundle END ------->

  //<------- Frames manipulation START ------->
  public selectedFrame;
  public lastSelectedFrame;
  public selectedFrameAvailableManipulations;
  //<------- Frames manipulation END ------->

  //<------- Until vars START ------->
  public previewMode = true;
  public zoomVal;
  public shapePopupStatus = false;
  public textSizePopupStatus = false;
  public colorPickerPopupStatus = false;
  public dimentions = {
    width: "0mm",
    height: "0mm"
  };
  public layerList = [];
  public testImageUrl =
    "https://images.ctfassets.net/o59xlnp87tr5/nywabPmH5Y6W4geG8IYuk/0a59905671f8d637350df8e7ec9e7fb9/backgrounds-min.jpg?w=360&h=240&fit=fill";
  public imgXMLstring = null;
  public xmlString = `<item id="2" name="Peter Griffin" remoteURL="https://www.w3schools.com/w3css/img_lights.jpg" thumb="http://192.168.0.52/EA_NEW/Serve.aspx?img=Peter-Griffin-transparant.png&amp;amp;type=thumb" highResPdfURL="https://www.w3schools.com/w3css/img_lights.jpg" keepExternal="false" accessibleFromClient="false">'
                        <fileInfo width="100" height="100" resolution="72" fileSize="280 kB" />
                      </item>`;

  public frameEvents = [
    "FrameDeleted",
    "FrameAdded",
    "FrameTagChanged",
    "FrameIndexChanged"
  ];
  //<------- Until vars END ------->

  constructor(
    public colorService: ColorService,
    public chiliShellService: ChiliShellService,
    private ngZone: NgZone,
    public frame: FrameHandleService,
  ) {
    this.chiliShellService.shellReady.subscribe(ready => {
      console.log("Chili ready", ready);
      if (!!ready) {
        this.initIframeManipulations();
        // this.subscribeToColorChange();
      }
    });
  }

  onSelectFile($event) {
    if ($event.target.files && $event.target.files[0]) {
      const name = $event.target.files[0].name;
      const size = $event.target.files[0].size;

      window.URL = window.URL;

      const image = new Image();

      image.onload = (event) => {
        const width = image.width;
        const height = image.height;
        const url = reader.result;

        this.xmlString =
          `<item id="2" 
            name="${name}" 
            remoteURL="${url}" 
            thumb="http://192.168.0.52/EA_NEW/Serve.aspx?img=Peter-Griffin-transparant.png&amp;amp;type=thumb" 
            highResPdfURL="${url}" 
            keepExternal="true" 
            accessibleFromClient="true">'
            <fileInfo width="${width}" height="${height}" resolution="72" fileSize="${size}kB" />
          </item>`;
      };

      image.src =  window.URL.createObjectURL($event.target.files[0]);

      const reader = new FileReader();
      reader.readAsDataURL($event.target.files[0]); // read file as data url
    }
  }

  initIframeManipulations() {
    // this.editor = this.chiliShellService.shell.editor;
    this.ngZone.run(() => {
      this.editor = this.chiliShellService.getEditor();
      this.eManipulation = this.editor.GetObject("document");
      this.dimentions.width = this.editor.GetObject("document").width;
      this.dimentions.height = this.editor.GetObject("document").height;
      this.editor.ExecuteFunction("document.editor", "Fit", "page");

      if (!!this.editor && !!this.eManipulation) {
        this.editorStatus = true;
        this.getZoom();

        this.chiliShellService
          .selectEvent$("TextSelectionChanged")
          .subscribe(evt => this.handleTextSelectionChanged(evt));

        this.frameEvents.forEach(evName => {
          this.chiliShellService
            .selectEvent$(evName)
            .subscribe(evt => this.handleLayers(evt));
        });

        this.frame.selectedFrame$.subscribe((event)=>{
          console.log('selectedFrameChanged in APP', event);
        });

        this.previewMode = false;

        // this.eventList.forEach(eventName => {
        //   this.chiliShellService
        //     .selectEvent$(eventName)
        //     .subscribe(evt => this.handleAllEventsTest(evt));
        // });
      }
    });
  }

  testBlockAlign(direction) {
    this.editor.SetProperty('document.selectedFrame', 'verticalAlign', direction);
  }

  handleAllEventsTest(event) {
    // console.log('-------------------------------------------------------');
    console.log(event.type, event);
  }

  handleTextSelectionChanged(event) {
    if (event.targetID.length == 0) {
      this.changeCursor("pointer");
    }
  }

  handleLayers(event) {
    console.log(event.type, event);
    this.getAllFrames();
    if (this.layerList.length) {
      this.layerList.sort((a, b) => {
        return b - a;
      });
    }
  }

  handleSelectedFrameChanged(event) {
    console.log("handleSelectedFrameChanged", event);

  }

  getZoom() {
    setTimeout(() => {
      this.zoomVal = this.editor.GetObject("document").zoom;
    }, 2000);
  }

  // Frame adding section START

  addFrame(type, meta) {
    if (type === "image") {
      // this.chiliShellService.addFrame(type, meta);
    } else {
      this.chiliShellService.addFrame(type, null);
    }

    this.closeShapePopup();
  }

  // addShapeFrame popup manipulations START
  swithcShapePopup() {
    if (this.shapePopupStatus) {
      this.shapePopupStatus = false;
    } else {
      this.shapePopupStatus = true;
    }
  }

  closeShapePopup() {
    this.shapePopupStatus = false;
  }
  // addShapeFrame popup manipulations END
  // Frame adding section END

  // Common editor manipulations START

  getSelectedFrame() {
    if (this.chiliShellService.shell.getSelectedFrame()) {
      console.log(
        "selectedFrame",
        this.chiliShellService.shell.getSelectedFrame()
      );

      this.editor.SetProperty("document.selectedFrame", "width", "120mm");

      // console.log(
      //   "Frame index",
      //   this.editor.GetObject("document.selectedFrame.layer")
      // );
      return this.chiliShellService.shell.getSelectedFrame();
    } else {
      console.log("frame is not selected");
      return null;
    }
  }

  getSelectedFrameType() {
    if (!!this.editor && !!this.editor.GetObject("document.selectedFrame")) {
      console.log(
        "Frame type",
        this.editor.GetObject("document.selectedFrame").type
      );
      return this.editor.GetObject("document.selectedFrame").type;
    } else {
      console.log("frame is not selected");
      return null;
    }
  }

  getSelectedText() {
    let selectedText = this.editor.GetObject(
      "document.selectedText.textFormat"
    );
    console.log("selectedText", selectedText);
  }

  // subscribeToColorChange() {
  //   this.colorService.currentColorId$.subscribe(id => {
  //     console.log("id", id);
  //     if (id) {
  //       this.changeTextColor();
  //     }
  //   });
  // }
  //
  // changeTextColor() {
  //   this.colorService.setCurrentColor();
  // }

  selectedTextAlign(type) {
    let status = false;

    try {
      if (!!this.editor.GetObject("document.selectedText")) {
        this.editor.SetProperty(
          "document.selectedText.textFormat",
          "textAlign",
          type
        );
        status = true;
      }
    } catch (err) {
      console.log("error");
      //TODO add to logger handler
      console.log("error in selected text align");
    } finally {
      if (!status) {
        console.log("error in selected text align");
      }
    }
  }

  allTextInFrameAlign(type) {
    if (!!this.getSelectedFrame()) {
      this.editor.ExecuteFunction(
        "document.selectedFrame.textFlow.composer.selection",
        "SelectAll"
      );
      this.editor.ExecuteFunction("document", "SetCursor", "Text");
      this.editor.SetProperty(
        "document.selectedText.textFormat",
        "textAlign",
        type
      );
      this.editor.ExecuteFunction("document", "SetCursor", "pointer");
    }

    if (!!this.editor.GetObject("document.selectedText")) {
      this.editor.ExecuteFunction("document", "SetCursor", "pointer");
      this.editor.ExecuteFunction(
        "document.selectedFrame.textFlow.composer.selection",
        "SelectAll"
      );
      this.editor.ExecuteFunction("document", "SetCursor", "Text");
      this.editor.SetProperty(
        "document.selectedText.textFormat",
        "textAlign",
        type
      );
      this.editor.ExecuteFunction("document", "SetCursor", "pointer");
    }
  }

  changeTextSize(size) {
    console.log('changeTextSize');
    let status = false;

    console.log("size", size);
    try {
      if (!!this.editor.GetObject("document.selectedText")) {
        this.editor.SetProperty(
          "document.selectedText.textFormat",
          "fontSize",
          size
        );
        status = true;
      }

      if (!status && !!this.getSelectedFrame()) {
        this.editor.ExecuteFunction(
          "document.selectedFrame.textFlow.composer.selection",
          "SelectAll"
        );
        this.editor.ExecuteFunction("document", "SetCursor", "Text");
        this.editor.SetProperty(
          "document.selectedText.textFormat",
          "fontSize",
          size
        );
        this.editor.ExecuteFunction("document", "SetCursor", "pointer");
        status = true;
      }
    } catch (err) {
      console.log("error");
      //TODO add to logger handler
      console.log("error in changeTextSize");
    } finally {
      if (!status) {
        console.log("error in changeTextSize");
      }
      // this.closeTextSizePopup();
    }
  }

  changeTextColorPopup() {
    if (this.colorPickerPopupStatus) {
      this.colorPickerPopupStatus = false;
    } else {
      this.colorPickerPopupStatus = true;
    }
  }

  closeTextColorPopup() {
    this.colorPickerPopupStatus = false;
  }

  rotateFrame(direction) {
    let currentDegree = this.editor.GetObject(
      "document.selectedFrame.rotation"
    );

    if (direction === "left") {
      this.editor.SetProperty(
        "document.selectedFrame",
        "rotation",
        currentDegree - 15
      );
    } else {
      this.editor.SetProperty(
        "document.selectedFrame",
        "rotation",
        currentDegree + 15
      );
    }
  }

  changeFrameOpacity(direction) {
    let currentOpacity = this.editor.GetObject(
      "document.selectedFrame.opacity"
    );
    if (direction === "up") {
      this.editor.SetProperty(
        "document.selectedFrame",
        "opacity",
        currentOpacity + 10
      );
    } else {
      this.editor.SetProperty(
        "document.selectedFrame",
        "opacity",
        currentOpacity - 10
      );
    }
  }

  getPages() {
    console.log("document", this.editor.GetObject("document"));
    console.log("pages", this.editor.GetObject("document.pages"));
    console.log("SelectedPage", this.editor.GetObject("document.selectedPage"));
    // console.log('SelectedPage.frames', this.editor.GetObject('document.selectedPage.frames'));
    // console.log('SelectedPage.alternateLayoutProperties', this.editor.GetObject('document.selectedPage.alternateLayoutProperties'));
    // console.log('control', this.editor.GetObject("document.editor.pagePanel"));
    // this.editor.SetProperty("document.editor.pagePanel", "backgroundColor", "R: 153 G: 51 B: 255 A: 1");
    // this.editor.SetProperty("document.editor.pagePanel", "height", "800");
  }

  selectPage(pgNumber) {
    console.log(
      `page ${pgNumber}`,
      this.editor.GetObject("document.pages[" + pgNumber + "]")
    );
    this.editor.ExecuteFunction("document.pages[" + pgNumber + "]", "Select");
  }

  changeCursor(type) {
    this.editor.ExecuteFunction("document", "SetCursor", type);
    //types are: "hand", "pointer", "text" etc
  }

  changeTextType(type) {
    let status = false;

    console.log(
      "changeTextType",
      this.editor.GetObject("document.selectedText.textFormat")
    );
    if (type === "underline") {
      //check if its underline, change to false
      let underlined = this.editor.GetObject("document.selectedText.textFormat")
        .underLine;
      if (underlined === "false") {
        console.log("set true");
        this.editor.SetProperty(
          "document.selectedText.textFormat",
          "underLine",
          true
        );
      } else {
        console.log("set false");
        this.editor.SetProperty(
          "document.selectedText.textFormat",
          "underLine",
          false
        );
      }
    } else if (type === "bold") {
      let bold = this.editor.GetObject("document.selectedText.textFormat")
        .textOverprint;
      if (bold === "false") {
        console.log("set true");
        this.editor.SetProperty(
          "document.selectedText.textFormat",
          "textOverprint",
          true
        );
      } else {
        console.log("set false");
        this.editor.SetProperty(
          "document.selectedText.textFormat",
          "textOverprint",
          false
        );
      }
    }
    // try {
    //   if (!!this.editor.GetObject("document.selectedText")) {
    //     this.editor.SetProperty(
    //       "document.selectedText.textFormat",
    //       "textAlign",
    //       type
    //     );
    //     status = true;
    //   }
    // } catch (err) {
    //   console.log('error');
    //   //TODO add to logger handler
    //   console.log('error in selected text align');
    // } finally {
    //   if (!status) {
    //     console.log('error in selected text align');
    //   }
    // }
  }

  generateColor(colorName) {
    return (
      "cp_object:" +
      this.editor.GetObject(
        "document.colors" + { colorName } + ".javaScriptDescriptor"
      )
    );
  }

  changeSelectedTextFont(fontNameOrId) {
    // this.editor.SetProperty(
    //   "document.selectedText.textFormat",
    //   "color",
    //   this.editor.GetObject('document.colors["red"].javaScriptDescriptor'));
    console.log(
      "textFormat",
      this.editor.GetObject("document.selectedText.textFormat")
    );
    console.log("frames", this.editor.GetObject("document.selectedFrame"));

    // this.editor.SetProperty('document.selectedText.textFormat', 'characterStyle', 'b69d18ee-76c8-46bb-853a-3b2305e38b05');
    // this.editor.GetObject('document.fonts');
    // b69d18ee-76c8-46bb-853a-3b2305e38b05
  }

  ClearFrame() {
    let frameForClear = this.editor.GetObject("document.selectedFrame");
    if (frameForClear != null && frameForClear != undefined) {
      if (frameForClear.type == "image") {
        this.editor.ExecuteFunction("document.selectedFrame", "ClearContent");
      }
    }
  }

  /////TEST
  getAllFrames() {
    console.log("getAllFrames");
    this.ngZone.run(() => {
      this.layerList = [];
      let numFrames = this.editor.GetObject("document.selectedPage.frames")
        .length;

      console.log("numFrames", numFrames);

      for (let j = 0; j < numFrames; j++) {
        let layerTemplate = {
          name: null,
          id: null,
          type: null,
          index: null,
          sorce: {}
        };

        let frame = this.editor.GetObject(
          "document.selectedPage.frames[" + j + "]"
        );

        layerTemplate.name = frame.name;
        layerTemplate.id = frame.id;
        layerTemplate.type = frame.type;
        layerTemplate.sorce = frame;
        layerTemplate.index = frame.index;

        this.layerList.push(layerTemplate);
      }
    });
  }

  testChangeFrameIndex() {
    // let selectedFrameId = this.editor.GetObject('document.selectedFrame.id');

    // this.editor.SetProperty("document.selectedPage.frames[0]", "index", "2");

    console.log("selectedPage", this.editor.GetObject("document.selectedPage"));

    console.log(
      "frameLayer",
      this.editor.GetObject("document.selectedFrame.index")
    );

    // console.log('document.pages[0].frames[0]', this.editor.GetObject('document.pages[0].frames[0]'));
    console.log("FIRST", this.editor.GetObject("document.selectedFrame.index"));
    console.log(
      "SECOND",
      this.editor.GetObject("document.selectedFrame.layer.list")
    );
    // let frame = this.editor.GetObject('document.pages[0].frames[0]');
    // frame.index +=5;
    // console.log('SECOND', this.editor.GetObject('document.selectedFrame.index'));
  }

  increaseSelectedFrameIndex() {
    // this.editor.SetProperty("document.selectedFrame", "index", this.editor.GetObject("document.selectedFrame.index") + 1);
    this.editor.ExecuteFunction("document.selectedFrame", "MoveUp");
  }
  decreaseSelectedFrameIndex() {
    // this.editor.SetProperty("document.selectedFrame", "index", this.editor.GetObject("document.selectedFrame.index") - 1);
    this.editor.ExecuteFunction("document.selectedFrame", "MoveDown");
  }

  SetImageContent(assetDefinitionXML) {
    let numFrames = this.editor.GetObject("document.pages[0].frames").length;
    for (let j = 0; j < numFrames; j++) {
      let type = this.editor.GetObject(
        "document.pages[0].frames[" + j + "].type"
      );
      console.log("111111111", type);
      if (type.name == "image") {
        this.editor.ExecuteFunction(
          "document.pages[0].frames[" + j + "]",
          "LoadContentFromExternalServerXmlString",
          assetDefinitionXML
        );
      }
    }
  }

  setExternalAssetImage() {
    console.log("this.xmlString", this.xmlString);
    this.SetImageContent(this.xmlString);
  }

  addImageFrame() {
    this.editor.ExecuteFunction(
      "document.selectedPage.frames",
      "Add",
      "image",
      "10mm",
      "50mm",
      "100mm",
      "70mm"
    );
  }

  // Image test Url
  // https://images.ctfassets.net/o59xlnp87tr5/nywabPmH5Y6W4geG8IYuk/0a59905671f8d637350df8e7ec9e7fb9/backgrounds-min.jpg?w=360&h=240&fit=fill

  /////TEST

  deleteFrame() {
    this.editor.ExecuteFunction("document.selectedFrame", "Delete");
  }

  copyFrame() {
    this.editor.ExecuteFunction("document.selectedFrames", "Copy");
  }

  pasteFrame() {
    this.editor.ExecuteFunction("document", "PasteFrames");
  }

  undo() {
    this.editor.ExecuteFunction("document.undoManager", "Undo", 1);
  }

  redo() {
    this.editor.ExecuteFunction("document.undoManager", "Redo", 1);
  }

  zoomHandle(direction) {
    if (direction == "up") {
      this.zoomVal = (parseInt(this.zoomVal) + 1).toString();
      this.chiliShellService.shell.editor.SetProperty(
        "document",
        "zoom",
        this.zoomVal
      );
    } else if (direction == "down") {
      if (this.zoomVal <= 1) {
        console.log("error");
      } else {
        this.zoomVal = (parseInt(this.zoomVal) - 1).toString();
        this.chiliShellService.shell.editor.SetProperty(
          "document",
          "zoom",
          this.zoomVal
        );
      }
    } else if (direction === "100%") {
      this.zoomVal = "100";
      this.chiliShellService.shell.editor.SetProperty(
        "document",
        "zoom",
        this.zoomVal
      );
    }
  }

  fitPage() {
    if (!!this.editor) {
      this.editor.ExecuteFunction("document.editor", "Fit", "page");
      this.zoomVal = this.editor.GetObject("document").zoom;
    }
  }

  getPageInfo() {
    let info = this.editor.GetObject("document");
    console.log("info", info);

    let width = info.width;
    let height = info.height;

    console.log("width", width, "height", height);
  }

  // Common editor manipulations END



  changeBorderColor(color) {
    const frame = "document.selectedFrame";
    let propName = "borderColor";
    if(!!color) {
      this.editor.SetProperty(frame, propName, color);
    }
  }

  alignAllText() {
    // this.editor.ExecuteFunction('document.selectedFrame');
    if (!!this.editor.GetObject("document.selectedFrame")) {
      console.group("Frame Details");
      console.log("frame", this.editor.GetObject("document.selectedFrame"));
      // console.log('baseLineOptions', this.editor.GetObject("document.selectedFrame.baseLineOptions"));
      console.log(
        "borderColor",
        this.editor.GetObject("document.selectedFrame.borderColor")
      );
      console.log(
        "borderCornerRadius",
        this.editor.GetObject("document.selectedFrame.borderCornerRadius")
      );
      console.log(
        "borderWidth",
        this.editor.GetObject("document.selectedFrame.borderWidth")
      );
      // console.log('controller', this.editor.GetObject("document.selectedFrame.controller"));
      console.log(
        "hasBorder",
        this.editor.GetObject("document.selectedFrame.hasBorder")
      );
      // console.log('layer', this.editor.GetObject("document.selectedFrame.layer"));
      console.log("name", this.editor.GetObject("document.selectedFrame.name"));
      console.log(
        "opacity",
        this.editor.GetObject("document.selectedFrame.opacity")
      );
      console.log(
        "rotation",
        this.editor.GetObject("document.selectedFrame.rotation")
      );
      console.log(
        "textStrokeColor",
        this.editor.GetObject("document.selectedFrame.textStrokeColor")
      );
      console.log(
        "translatedName",
        this.editor.GetObject("document.selectedFrame.translatedName")
      );
      console.log(
        "verticalAlign",
        this.editor.GetObject("document.selectedFrame.verticalAlign")
      );
      console.log(
        "width",
        this.editor.GetObject("document.selectedFrame.width")
      );
      console.log(
        "height",
        this.editor.GetObject("document.selectedFrame.height")
      );
      console.log("x", this.editor.GetObject("document.selectedFrame.x"));
      console.log("y", this.editor.GetObject("document.selectedFrame.y"));
      // console.log('alternateLayoutProperties', this.editor.GetObject("document.selectedFrame.alternateLayoutProperties"));
      console.log(
        "fillColor",
        this.editor.GetObject("document.selectedFrame.fillColor")
      );
      console.log(
        "frameConstraints",
        this.editor.GetObject("document.selectedFrame.frameConstraints")
      );
      console.log(
        "frameContentConstraints",
        this.editor.GetObject("document.selectedFrame.frameContentConstraints")
      );
      // console.log('privateData', this.editor.GetObject('document.selectedFrame.privateData'));
      // console.log('javaScriptDescriptor', this.editor.GetObject("document.selectedFrame.javaScriptDescriptor"));
      // index
      console.log(
        "index",
        this.editor.GetObject("document.selectedFrame.index")
      );
      console.groupEnd();
    }
  }

  previewModeSwitch() {
    this.previewMode ? (this.previewMode = false) : (this.previewMode = true);

    if (
      this.editor.GetObject("document.viewPreferences.viewMode.name") ==
      "preview"
    ) {
      console.log("set Edit");
      this.editor.SetProperty("document.viewPreferences", "viewMode", "edit");
    } else {
      console.log("set Preview");
      this.editor.SetProperty(
        "document.viewPreferences",
        "viewMode",
        "preview"
      );
    }
    console.log(
      "111",
      this.editor.GetObject("document.selectedPage.privateData")
    );
    // this.editor.SetProperty('document.selectedPage', 'visible', 'false');

    console.log("VP", this.editor.GetObject("document.viewPreferences"));
    console.log(
      "VP.annotationSettings",
      this.editor.GetObject("document.viewPreferences.annotationSettings")
    );
    this.editor.ExecuteFunction("document.editor", "Fit", "page");
  }

  //editor.ExecuteFunction("document","CreateTempPDF_FromXml",pdfSettingsXML,"_blank")

  ngOnInit() {
    //doInit
  }

  public eventList = [
    "AdTypeOverlayViewerVisibilityChanged",
    "AdTypeOverlayVisibilityChanged",
    "AlternateLayoutAdded",
    "AlternateLayoutDefinitionChanged",
    "AlternateLayoutDeleted",
    "AlternateLayoutIndexChanged",
    "AlternateLayoutNameChanged",
    "AlternateLayoutSelectionChanged",
    "AnnotationAdded",
    "AnnotationDefinitionChanged",
    "AnnotationDeleted",
    "AnnotationMoved",
    "AutoTextCursorChanged",
    "BarcodeFrameVariableChanged",
    "BaseLineGridOptionsChanged",
    "BaseLineGridVisibilityChanged",
    "BleedVisibilityChanged",
    "BookMarkAdded",
    "BookMarkDefinitionChanged",
    "BookMarkDeleted",
    "BookMarkIndexChanged",
    "BookMarkLinkSettingsChanged",
    "BookMarkNameChanged",
    "BookViewNavigationChanged",
    "BookViewPreferencesChanged",
    "BorderVisibilityChanged",
    "ChainVisibilityChanged",
    "CharacterStyleAdded",
    "CharacterStyleColorChanged",
    "CharacterStyleDefinitionChanged",
    "CharacterStyleDeleted",
    "CharacterStyleIndexChanged",
    "CharacterStyleNameChanged",
    "CharacterStyleUnderLineChanged",
    "ColorAdded",
    "ColorDefinitionChanged",
    "ColorDeleted",
    "ColorIndexChanged",
    "ColorNameChanged",
    "CopyFrameChanged",
    "CursorChanged",
    "CustomButtonAdded",
    "CustomButtonDefinitionChanged",
    "CustomButtonDeleted",
    "CustomButtonIndexChanged",
    "DataSourceChanged",
    "DataSourceRecordChanged",
    "DisplayQualityChanged",
    "DocumentActionChanged",
    "DocumentAdSettingsChanged",
    "DocumentAnnotationSettingsChanged",
    "DocumentBleedChanged",
    "DocumentClosing",
    "DocumentConstraintsChanged",
    "DocumentDefaultDirectoriesChanged",
    "DocumentDimensionsChanged",
    "DocumentDirtyStateChanged",
    "DocumentEventActionAdded",
    "DocumentEventActionDefinitionChanged",
    "DocumentEventActionDeleted",
    "DocumentEventActionIndexChanged",
    "DocumentEventActionNameChanged",
    "DocumentFlapChanged",
    "DocumentFullyLoaded",
    "DocumentLinkSettingsChanged",
    "DocumentLoaded",
    "DocumentLoading",
    "DocumentSaved",
    "DocumentSaveRequested",
    "DocumentSerialized",
    "DocumentSlugChanged",
    "DocumentSpineChanged",
    "FontAdded",
    "FontDeleted",
    "FontIndexChanged",
    "FontLoaded",
    "FontNameChanged",
    "FrameAdded",
    "FrameAdNotesChanged",
    "FrameAdSettingsChanged",
    "FrameAlternateLayoutSelectionChanged",
    "FrameAnchorChanged",
    "FrameBarcodeSettingsChanged",
    "FrameBorderChanged",
    "FrameChainChanged",
    "FrameConstraintsChanged",
    "FrameDeleted",
    "FrameDisplayQualityChanged",
    "FrameDropShadowChanged",
    "FrameFillChanged",
    "FrameHandlePreferencesChanged",
    "FrameIndexChanged",
    "FrameLayerChanged",
    "FrameLinkSettingsChanged",
    "FrameMoved",
    "FrameMoveFinished",
    "FrameMoveInProgress",
    "FrameMoveRegionChanged",
    "FrameOpacityChanged",
    "FrameRotated",
    "FrameTagChanged",
    "FrameTextVariablesChanged",
    "FrameWrapChanged",
    "GridSettingsChanged",
    "HelpLayerVisibilityChanged",
    "HiddenCharacterVisibilityChanged",
    "IconSetDownloaded",
    "IconSetDownloadStarted",
    "IconSetFullDownloaded",
    "Idle",
    "ImageFrameVariableDefinitionChanged",
    "InlineFrameAdded",
    "InlineFrameDeleted",
    "InlinePanelVisibilityChanged",
    "InlineToolBarVisibilityChanged",
    "LanguageCacheRequiresClearing",
    "LanguageDownloaded",
    "LanguageDownloadStarted",
    "LayerAdded",
    "LayerAlternateLayoutSelectionChanged",
    "LayerContentTypeChanged",
    "LayerDeleted",
    "LayerIndexChanged",
    "LayerMoveRegionChanged",
    "LayerNameChanged",
    "LayerOpacityChanged",
    "LayerPrintingChanged",
    "LayerTextWrapChanged",
    "LayerVisibilityChanged",
    "LayerVisibleVariableChanged",
    "LinkSettingsChanged",
    "LinkSettingsVariableChanged",
    "MarginVisibilityChanged",
    "MeasurementUnitsChanged",
    "MoveRegionVisibilityChanged",
    "ObjectReplaceRequested",
    "PageAdded",
    "PageAlternateLayoutSelectionChanged",
    "PageBackgroundColorChanged",
    "PageBookMarkSettingsChanged",
    "PageCanvasScrollChanged",
    "PageControlsLoaded",
    "PageDeleted",
    "PageIncludeInLayoutChanged",
    "PageIncludeInOutputChanged",
    "PageIndexChanged",
    "PageLabelVisibilityChanged",
    "PageMarginsChanged",
    "PageModeChanged",
    "PageRulerVisibilityChanged",
    "PageSectionChanged",
    "PageVisibilityModeChanged",
    "PageVisibleVariableChanged",
    "PaginationEditorVisibilityChanged",
    "PanelAdded",
    "PanelControlAdded",
    "PanelControlDeleted",
    "PanelControlIndexChanged",
    "PanelControlTitleChanged",
    "PanelControlViewPreferencesChanged",
    "PanelControlVisibilityChanged",
    "PanelDeleted",
    "PanelIndexChanged",
    "PanelTitleChanged",
    "PanelViewPreferencesChanged",
    "ParagraphStyleAdded",
    "ParagraphStyleBackgroundDefinitionChanged",
    "ParagraphStyleColorChanged",
    "ParagraphStyleDefinitionChanged",
    "ParagraphStyleDeleted",
    "ParagraphStyleDiagonalRuleDefinitionChanged",
    "ParagraphStyleIndexChanged",
    "ParagraphStyleNameChanged",
    "ParagraphStyleRuleDefinitionChanged",
    "PathAdded",
    "PathChanged",
    "PathCursorChanged",
    "PathDeleted",
    "PathPointAdded",
    "PathPointDeleted",
    "PathPointMoved",
    "PDFSettingsChanged",
    "PluginAdded",
    "PluginDeleted",
    "PluginIndexChanged",
    "PluginNameChanged",
    "PreflightPreferencesChanged",
    "PreflightResultAdded",
    "PreflightResultDeleted",
    "PreflightResultDescriptionChanged",
    "RotateViewChanged",
    "SearchCompleted",
    "SearchResultAdded",
    "SearchResultDeleted",
    "SearchResultsSettingsChanged",
    "SearchResultsStatusChanged",
    "SearchStarted",
    "SectionBreakVisibilityChanged",
    "SectionVisibleVariableChanged",
    "SelectedAlternateLayoutChanged",
    "SelectedAnnotationChanged",
    "SelectedBookMarkChanged",
    "SelectedCharacterStyleChanged",
    "SelectedColorChanged",
    "SelectedDataSourceChanged",
    "SelectedDocumentEventActionChanged",
    "SelectedFontChanged",
    "SelectedFrameChanged",
    "SelectedImportWarningChanged",
    "SelectedLayerChanged",
    "SelectedPageChanged",
    "SelectedPageMasterChanged",
    "SelectedPanelChanged",
    "SelectedParagraphStyleChanged",
    "SelectedParagraphStyleListSettingsChanged",
    "SelectedPathChanged",
    "SelectedPathPointChanged",
    "SelectedPluginChanged",
    "SelectedSearchResultChanged",
    "SelectedTableCellChanged",
    "SelectedTableColumnChanged",
    "SelectedTableRowChanged",
    "SelectedVariableChanged",
    "SelectionContentChanged",
    "SlugVisibilityChanged",
    "SnapSettingsChanged",
    "SnippetVariablesChanged",
    "SourceViewChanged",
    "SpellCheckDictionariesLoaded",
    "SpellCheckHunSpellFilesLoaded",
    "SpellCheckPreferencesChanged",
    "StoryFrameChanged",
    "StoryViewPreferencesChanged",
    "TableBorderAndFillChanged",
    "TableCellBorderAndFillChanged",
    "TableCellDefinitionChanged",
    "TableColumnAdded",
    "TableColumnBorderAndFillChanged",
    "TableColumnDefinitionChanged",
    "TableColumnDeleted",
    "TableColumnIndexChanged",
    "TableFrameBorderAndFillChanged",
    "TableRowAdded",
    "TableRowBorderAndFillChanged",
    "TableRowDefinitionChanged",
    "TableRowDeleted",
    "TableRowIndexChanged",
    "TabRulerControlVisibilityChanged",
    "TabRulerMarkerSelectionChanged",
    "TabRulerVisibilityChanged",
    "TextColumnGuideVisibilityChanged",
    "TextFormatChanged",
    "TextFrameCopyFittingAppliedPercentageChanged",
    "TextFrameCustomBaseLineGridChanged",
    "TextFrameFlowComposed",
    "TextFrameLinksUpdated",
    "TextFramePathSettingChanged",
    "TextFrameReadOnlyFlowComposed",
    "TextFrameStrokeChanged",
    "TextSelectionChanged",
    "ToolBarItemAdded",
    "ToolBarItemDeleted",
    "ToolBarItemIndexChanged",
    "ToolBarItemViewPreferencesChanged",
    "UndoStackChanged",
    "VariableAdded",
    "VariableDefinitionChanged",
    "VariableDeleted",
    "VariableIndexChanged",
    "VariableNameChanged",
    "VariableRowAdded",
    "VariableRowDeleted",
    "VariableRowIndexChanged",
    "VariableTextDirectionChanged",
    "VariableValueChanged",
    "VariableVisibilityChanged",
    "ViewModeChanged",
    "WarningVisibilityChanged",
    "WorkSpaceChanged",
    "WorkSpaceColorChanged",
    "WorkSpaceInfoChanged",
    "WrapMarginVisibilityChanged",
    "ZoomAfterChange",
    "ZoomBeforeChange",
    "ZoomChanged",
    "ZoomSettingsChanged"
  ];
}
