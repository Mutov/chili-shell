import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TextFrameToolsComponent } from './text-frame-tools.component';

describe('TextFrameToolsComponent', () => {
  let component: TextFrameToolsComponent;
  let fixture: ComponentFixture<TextFrameToolsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TextFrameToolsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TextFrameToolsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
