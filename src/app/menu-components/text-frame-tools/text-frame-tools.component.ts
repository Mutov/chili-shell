import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-text-frame-tools',
  templateUrl: './text-frame-tools.component.html',
  styleUrls: ['./text-frame-tools.component.scss']
})
export class TextFrameToolsComponent implements OnInit {

  public textSizePopupStatus = false;
  public textSizeList = [8, 10, 12, 14, 18, 20, 24, 30, 36, 50];

  @Output()
  public changeTextSizeClick = new EventEmitter();
  @Output()
  public changeSelectedTextFontClick = new EventEmitter();
  @Output()
  public changeTextTypeClick = new EventEmitter();
  @Output()
  public selectedTextAlignClick = new EventEmitter();
  @Output()
  public allTextInFrameAlignClick = new EventEmitter();

  constructor() { }

  switchTextSizePopup() {
    if (this.textSizePopupStatus) {
      this.textSizePopupStatus = false;
    } else {
      this.textSizePopupStatus = true;
    }
  }

  closeTextSizePopup() {
    this.textSizePopupStatus = false;
  }

  ngOnInit() {
  }
}
