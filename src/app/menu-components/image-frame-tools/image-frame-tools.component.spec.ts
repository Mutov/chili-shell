import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImageFrameToolsComponent } from './image-frame-tools.component';

describe('ImageFrameToolsComponent', () => {
  let component: ImageFrameToolsComponent;
  let fixture: ComponentFixture<ImageFrameToolsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImageFrameToolsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImageFrameToolsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
