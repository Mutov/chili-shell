import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShapeFrameToolsComponent } from './shape-frame-tools.component';

describe('ShapeFrameToolsComponent', () => {
  let component: ShapeFrameToolsComponent;
  let fixture: ComponentFixture<ShapeFrameToolsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShapeFrameToolsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShapeFrameToolsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
