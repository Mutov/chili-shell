import {Injectable, ɵEMPTY_ARRAY} from '@angular/core';
import {ChiliShellService} from '../chili-shell/services/chili-shell.service';
import {BehaviorSubject} from 'rxjs';
import * as _ from 'lodash';
import {FrameHandleService} from './frame-handle.service';

@Injectable({
  providedIn: 'root'
})
export class FontService {
  private editor: any;
  public fonts: Array<any> = [];
  public currentRegularFont: any;
  public currentFont: any;
  public currentFontId$: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  public regularFonts: Array<any> = [];

  constructor(
    private chiliShellService: ChiliShellService,
    private frameHandleService: FrameHandleService) {
    this.chiliShellService.shellReady.subscribe(ready => {
      if (ready) {
        this.editor = this.chiliShellService.getEditor();

      const passedFonts = [
          {id: 'f3100f92-4121-4e6a-b49e-f960de52edaf', name: 'Minion Pro Regular'},
          {id: 'fd0cc729-3e8b-40f3-b4c1-292eb4f10f62', name: 'Roboto Regular'}
        ];

        this.addFonts(passedFonts);
        // this.filterRegularFonts();
      }
    });

    this.currentFontId$.subscribe(id => {
      if (id) {
        this.setFont();
      }
    });
  }

  public addFont(font: any): void {
    const createdFont = this.editor.ExecuteFunction('document.fonts', 'Add');

    // this.editor.SetProperty(`document.fonts[${ createdFont.id }]`, 'family', font.family);
    // this.editor.SetProperty(`document.fonts[${ createdFont.id }]`, 'style', font.style);
    this.editor.SetProperty(`document.fonts[${ createdFont.id }]`, 'name', font.name);
    // this.editor.SetProperty(`document.fonts[${ createdFont.id }]`, 'id', font.id.toUpperCase());

    font._id = createdFont.id;

    this.fonts.push(font);
  }

  private addFonts(fonts) {
    fonts.forEach((font) => {
      this.addFont(font);
    });
  }

  public setCurrentFont(font): void {
    if (font.style === 'regular') {
      this.currentRegularFont = font;
    }

    this.currentFont = font;
    this.currentFontId$.next(this.currentFont.id);
  }

  private setFont(): void {
    const font = this.getCurrentFont();
    const target = this.frameHandleService.getCurrentFramePath();

    this.editor.SetProperty(target, 'font', font);
  }

  private getCurrentFont() {
    const font = this.editor.GetObject(`document.fonts[${ this.currentFont._id }]`);

    return font.javaScriptDescriptor;
  }

  public getRegularFonts() {
    // return this.regularFonts;
    return this.fonts;
  }

  private filterRegularFonts() {
    _.forEach(this.fonts, (font) => {
      if (font.style === 'regular') {
        this.regularFonts.push(font);
      }
    }) ;
  }

  public setFontByStyle(styleType, target) {
    const font = _.find(this.fonts, (_font) => {
      if (_font.style !== styleType) {
        return;
      }

      return this.currentRegularFont.family === _font.family;
    });

    this.setCurrentFont(font);
  }
}
