import { TestBed } from '@angular/core/testing';

import { FrameHandleService } from './frame-handle.service';

describe('FrameHandleService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FrameHandleService = TestBed.get(FrameHandleService);
    expect(service).toBeTruthy();
  });
});
