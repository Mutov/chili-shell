import { Injectable } from '@angular/core';
import {ChiliShellService} from '../chili-shell/services/chili-shell.service';
import {BehaviorSubject} from 'rxjs';
import {FrameHandleService} from './frame-handle.service';

@Injectable({
  providedIn: 'root'
})
export class ColorService {
  private editor: any;
  public currentColor: any = null;
  public currentColorId$: BehaviorSubject<any> = new BehaviorSubject<any>(null);

  constructor(
    private chiliShellService: ChiliShellService,
    private frameHandleService: FrameHandleService) {
    this.chiliShellService.shellReady.subscribe(ready => {
      if (ready) {
        this.editor = this.chiliShellService.getEditor();
      }
    });

    this.currentColorId$.subscribe(id => {
      if (id) {
        this.setCurrentColor();
      }
    });
  }

  private parseRGB(rgb: string) {
    return rgb.replace(/[^\d,]/g, '').split(',');
  }

  public addColor(color: string): void {
    const rgbParts = ['r', 'g', 'b'];
    const rgb = this.parseRGB(color);

    const newColor = this.editor.ExecuteFunction('document.colors', 'Add');

    this.editor.SetProperty(`document.colors[${ newColor.id }]`, 'name', color);
    this.editor.SetProperty(`document.colors[${ newColor.id }]`, 'type', 'RGB');
    rgbParts.forEach((part, index) => {
      this.editor.SetProperty(`document.colors[${ newColor.id }]`, part, rgb[index]);
    });

    this.currentColor = {
      id: newColor.id,
      name:  color
    };

    this.currentColorId$.next(this.currentColor.id);
  }

  public setCurrentColor() {
    const color = this.getCurrentColor();
    const target = this.frameHandleService.getCurrentFramePath();

    if (target) {
      this.editor.SetProperty(target, 'color', color);
    }
  }

  private getCurrentColor() {
    const color = this.editor.GetObject(`document.colors[${ this.currentColor.id }]`);

    return color.javaScriptDescriptor;
  }
}
