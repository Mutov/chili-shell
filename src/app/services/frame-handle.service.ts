import { Injectable } from "@angular/core";
import { ChiliShellService } from "../chili-shell/services/chili-shell.service";
import { BehaviorSubject } from "rxjs";
import { map } from "rxjs/operators";
export interface SelectedFrame {
  id: string;
  type: string;
  meta: object;
  updatePath: string;
  rawFrame: object;
}

@Injectable({
  providedIn: "root"
})
export class FrameHandleService {
  private editor: any;

  public selectedFrame$ = new BehaviorSubject<SelectedFrame>(null);

  private selectedFrameData: any;

  constructor(private chiliShellService: ChiliShellService) {
    this.chiliShellService.shellReady.subscribe(ready => {
      if (ready) {
        this.editor = this.chiliShellService.getEditor();

        this.chiliShellService
          .selectEvent$("SelectedFrameChanged")
          .pipe(
            map(evt => {
               this.selectedFrameData = {
                id: "",
                type: "",
                updatePath: "",
                meta: {
                  hasBorder: "",
                  borderCornerRadius: "",
                  borderWidth: "",
                  borderColor: "",
                  hasFill: "",
                  fillColor: "",
                  height: "",
                  width: "",
                  name: "",
                  opacity: "",
                  rotation: ""
                },
                rawFrame: {}
              };

              // TextSelectionChanged subscribe on it
              if (this.getSelectedText() != null && this.getSelectedText() != undefined) {
                let frameOfSelectedText = this.getFrameTroughSelectedText();
                //REMINDER default type is text
                this.selectedFrameData.id = frameOfSelectedText.id;
                this.selectedFrameData.type = 'text';
                this.selectedFrameData.updatePath = 'document.selectedText.textFormat';
                this.selectedFrameData.meta.hasBorder = frameOfSelectedText.hasBorder;
                this.selectedFrameData.meta.borderCornerRadius = frameOfSelectedText.borderCornerRadius;
                this.selectedFrameData.meta.borderWidth = frameOfSelectedText.borderWidth;
                this.selectedFrameData.meta.borderColor = frameOfSelectedText.borderColor;
                this.selectedFrameData.meta.hasFill = frameOfSelectedText.hasFill;
                this.selectedFrameData.meta.fillColor = frameOfSelectedText.fillColor;
                this.selectedFrameData.meta.height = frameOfSelectedText.height;
                this.selectedFrameData.meta.width = frameOfSelectedText.width;
                this.selectedFrameData.meta.name = frameOfSelectedText.name;
                this.selectedFrameData.meta.opacity = frameOfSelectedText.opacity;
                this.selectedFrameData.meta.rotation = frameOfSelectedText.rotation;
                this.selectedFrameData.rawFrame = this.getFrameTroughSelectedText();
                console.log('selectedFrameData',  this.selectedFrameData);
                return this.selectedFrameData;
              } else if (!!evt.targetID && !!this.getSelectedFrame()) {
                let frame = this.getSelectedFrame();
                this.selectedFrameData.id = frame.id;
                this.selectedFrameData.type = this.getSelectedFrameType();
                this.selectedFrameData.updatePath = 'document.selectedFrame';
                this.selectedFrameData.meta.hasBorder = frame.hasBorder;
                this.selectedFrameData.meta.borderCornerRadius = frame.borderCornerRadius;
                this.selectedFrameData.meta.borderWidth = frame.borderWidth;
                this.selectedFrameData.meta.borderColor = frame.borderColor;
                this.selectedFrameData.meta.hasFill = frame.hasFill;
                this.selectedFrameData.meta.fillColor = frame.fillColor;
                this.selectedFrameData.meta.height = frame.height;
                this.selectedFrameData.meta.width = frame.width;
                this.selectedFrameData.meta.name = frame.name;
                this.selectedFrameData.meta.opacity = frame.opacity;
                this.selectedFrameData.meta.rotation = frame.rotation;
                this.selectedFrameData.rawFrame = this.getSelectedFrame();
                console.log('selectedFrameData',  this.selectedFrameData);
                return this.selectedFrameData;
              } else {
                return null;
              }
            })
          )
          .subscribe(this.selectedFrame$);

          this.chiliShellService
          .selectEvent$("TextSelectionChanged").pipe(
            map(evt => {
              if (!evt.targetID) {
                return null;
              }
            })
          ).subscribe(this.selectedFrame$);

      }
    });
  }

  public getCurrentFrame() {
    return this.selectedFrame$.value;
  }

  public getCurrentFramePath() {
    return this.selectedFrameData.updatePath;
  }

  public frameChangeEvent(event: any) {
    console.log("frameChangeEvent", "do smth");
  }

  private getFrameTemplate() {}

  public getEditor() {
    return this.editor.GetObject("document.selectedFrame.type");
  }

  private getSelectedText() {
    return this.editor.GetObject("document.selectedText");
  }

  private getFrameTroughSelectedText() {
    return this.editor.GetObject("document.selectedText.frame");
  }

  private getSelectedFrame() {
    return this.editor.GetObject("document.selectedFrame");
  }

  private getSelectedFrameType() {
    return this.editor.GetObject("document.selectedFrame.type").name;
  }

  private getFrameByID(frameID) {

  }
}
