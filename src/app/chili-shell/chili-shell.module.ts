import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChiliShellComponent } from './components/chili-shell/chili-shell.component';
import { ChiliShellService } from './services/chili-shell.service';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [ChiliShellComponent],
  exports: [ChiliShellComponent],
  providers: [ChiliShellService],
})
export class ChiliShellModule { }
