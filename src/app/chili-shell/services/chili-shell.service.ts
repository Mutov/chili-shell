import { Injectable, NgZone } from '@angular/core';
import { ChiliShellComponent } from '../components/chili-shell/chili-shell.component';
import { ReplaySubject, BehaviorSubject, Subject } from 'rxjs';
import { filter } from 'rxjs/operators';

export interface EditorEvent {
  type: string;
  targetID: string;
  data: any;
}

@Injectable({
  providedIn: 'root'
})
export class ChiliShellService {

  public shell: ChiliShellComponent;
  public shellReady = new BehaviorSubject<boolean>(false);
  private _events = new Subject<EditorEvent>();

  constructor(private _zone: NgZone) {}

  events$() {
    return this._events.asObservable();
  }

  startEventListening() {
    this._zone.run(() => {
      window["OnEditorEvent"] = this.OnEditorEvent.bind(this);
    });
  }

  OnEditorEvent(type, targetID, data) {
    this._zone.run(() => this._events.next({ type, targetID, data }));
  }

  selectEvent$(eventName: string) {
    this.shell.editor.AddListener(eventName);
    return this.events$().pipe(
      filter(evt => evt.type === eventName),
    );
  }

  getEditor() {
    return this.shell.editor;
  }

  addFrame(frameType, metaData) {
    this.prepareAddFrame (frameType, metaData);
  }

  prepareAddFrame(frameType, metaData) {
    let frameForAdd;
    let img = metaData;

    if (frameType === 'text') {
      frameForAdd = 'text';
    } else if (frameType === 'image') {
      frameForAdd = 'image';
    } else if (frameType === 'rectangle') {
      frameForAdd = 'rectangle';
    } else if (frameType === 'circle') {
      frameForAdd = 'circle';
    } else if (frameType === 'line') {
      frameForAdd = 'line';
    } else {
      console.log('wrong frame type');
      return;
    }

    this.executeAddFrame(frameForAdd, metaData);
    if(frameType === 'text') {
      this.shell.editor.ExecuteFunction(
            "document.selectedFrame",
            "ImportTextFlow",
            '<TextFlow><p fontFamily="Arial"><span>Enter your text here!</span></p></TextFlow>',
            true
          );
    }
  }

  executeAddFrame(type, metaData) {
    if(!!metaData) {
      //todo add image
      this.shell.editor.ExecuteFunction(
        "document.selectedPage.frames",
        "Add",
        type,
        "10mm",
        "50mm",
        "100mm",
        "70mm"
      );
      return;
    }

    this.shell.editor.ExecuteFunction(
      "document.selectedPage.frames",
      "Add",
      type,
      "10mm",
      "50mm",
      "100mm",
      "70mm"
    );
  }
  
}
