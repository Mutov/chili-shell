import { TestBed } from '@angular/core/testing';

import { ChiliShellService } from './chili-shell.service';

describe('ChiliShellService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ChiliShellService = TestBed.get(ChiliShellService);
    expect(service).toBeTruthy();
  });
});
