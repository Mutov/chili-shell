import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  NgZone
} from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";
import { ChiliShellService } from "../../services/chili-shell.service";

@Component({
  selector: "app-chili-shell",
  templateUrl: "./chili-shell.component.html",
  styleUrls: ["./chili-shell.component.scss"]
})
export class ChiliShellComponent implements OnInit {
  @ViewChild("chili")
  //getting html element iFrame
  public chili: ElementRef;

  //iFrame core object
  public frameWindow;
  public editor;
  public editorManipulation;
  //url for iFrame src
  public iframeUrl;

  constructor(
    sanitizer: DomSanitizer,
    private chiliShellService: ChiliShellService,
    private ngZone: NgZone
  ) {
    this.iframeUrl = sanitizer.bypassSecurityTrustResourceUrl(
      "http://jackprint.loc/CHILI/3motion_DEV/editor_html.aspx?doc=4e819ad7-f179-4cd1-a6f9-34657b6c5a3e&apiKey=02INtr2_hmo_4YlllCVwBpmByBz9bv4gX1NGQezY_3SZ2oBR+dkg2F_Jw4YKa2Gl9ulAnHVm8cQT99M_S90M3A=="
      );
      this.chiliShellService.shell = this;
    this.chiliShellService.startEventListening();
    //this.chiliShellService.events$().subscribe(this.HandleEvent.bind(this));
  }

  iFrameLoaded() {
    try {
      this.frameWindow = this.chili.nativeElement.contentWindow;
      this.frameWindow.GetEditor(this.EditorLoaded.bind(this));
    } catch (error) {
      console.log("error", error);
      //handle error
      return false;
    }
  }

  //Create editor object from loaded iFrame
  EditorLoaded(jsInterface) {
    this.editor = this.frameWindow.editorObject;
    this.editor.AddListener("CursorChanged");
    this.editor.AddListener("FrameAdded");
    this.chiliShellService
      .selectEvent$("DocumentFullyLoaded")
      .subscribe(evt => {
        this.editorManipulation = this.editor.GetObject("document");

          if (!this.editorReadyForConsole()) {
            var rfcInterval = setInterval(
              function() {
                var ready = this.editorReadyForConsole();
                if (ready) {
                  clearInterval(rfcInterval);
                }
              }.bind(this),
              200
            );
          }
      });
  }

  editorReadyForConsole() {
    var statusMessage = this.editor.GetObject("document.readyForConsole");
    if (statusMessage.length > 0) {
      console.info("The editor is preparing your document");
      return false;
    } else {
      console.info("Finished preparing the document");
      this.chiliShellService.shellReady.next(true);
      return true;
    }
  }

  renewEditor() {
    let obj = this.frameWindow.editorObject.GetObject("document");
    return obj;
  }

  getSelectedFrame() {
    return this.frameWindow.editorObject.GetObject("document.selectedFrame");
  }

  getEditorManipulator() {
    if (this.editor) {
      return this.editor.GetObject("document");
    }
    return null;
  }

  public eventList = [
    "AdTypeOverlayViewerVisibilityChanged",
    "AdTypeOverlayVisibilityChanged",
    "AlternateLayoutAdded",
    "AlternateLayoutDefinitionChanged",
    "AlternateLayoutDeleted",
    "AlternateLayoutIndexChanged",
    "AlternateLayoutNameChanged",
    "AlternateLayoutSelectionChanged",
    "AnnotationAdded",
    "AnnotationDefinitionChanged",
    "AnnotationDeleted",
    "AnnotationMoved",
    "AutoTextCursorChanged",
    "BarcodeFrameVariableChanged",
    "BaseLineGridOptionsChanged",
    "BaseLineGridVisibilityChanged",
    "BleedVisibilityChanged",
    "BookMarkAdded",
    "BookMarkDefinitionChanged",
    "BookMarkDeleted",
    "BookMarkIndexChanged",
    "BookMarkLinkSettingsChanged",
    "BookMarkNameChanged",
    "BookViewNavigationChanged",
    "BookViewPreferencesChanged",
    "BorderVisibilityChanged",
    "ChainVisibilityChanged",
    "CharacterStyleAdded",
    "CharacterStyleColorChanged",
    "CharacterStyleDefinitionChanged",
    "CharacterStyleDeleted",
    "CharacterStyleIndexChanged",
    "CharacterStyleNameChanged",
    "CharacterStyleUnderLineChanged",
    "ColorAdded",
    "ColorDefinitionChanged",
    "ColorDeleted",
    "ColorIndexChanged",
    "ColorNameChanged",
    "CopyFrameChanged",
    "CursorChanged",
    "CustomButtonAdded",
    "CustomButtonDefinitionChanged",
    "CustomButtonDeleted",
    "CustomButtonIndexChanged",
    "DataSourceChanged",
    "DataSourceRecordChanged",
    "DisplayQualityChanged",
    "DocumentActionChanged",
    "DocumentAdSettingsChanged",
    "DocumentAnnotationSettingsChanged",
    "DocumentBleedChanged",
    "DocumentClosing",
    "DocumentConstraintsChanged",
    "DocumentDefaultDirectoriesChanged",
    "DocumentDimensionsChanged",
    "DocumentDirtyStateChanged",
    "DocumentEventActionAdded",
    "DocumentEventActionDefinitionChanged",
    "DocumentEventActionDeleted",
    "DocumentEventActionIndexChanged",
    "DocumentEventActionNameChanged",
    "DocumentFlapChanged",
    "DocumentFullyLoaded",
    "DocumentLinkSettingsChanged",
    "DocumentLoaded",
    "DocumentLoading",
    "DocumentSaved",
    "DocumentSaveRequested",
    "DocumentSerialized",
    "DocumentSlugChanged",
    "DocumentSpineChanged",
    "FontAdded",
    "FontDeleted",
    "FontIndexChanged",
    "FontLoaded",
    "FontNameChanged",
    "FrameAdded",
    "FrameAdNotesChanged",
    "FrameAdSettingsChanged",
    "FrameAlternateLayoutSelectionChanged",
    "FrameAnchorChanged",
    "FrameBarcodeSettingsChanged",
    "FrameBorderChanged",
    "FrameChainChanged",
    "FrameConstraintsChanged",
    "FrameDeleted",
    "FrameDisplayQualityChanged",
    "FrameDropShadowChanged",
    "FrameFillChanged",
    "FrameHandlePreferencesChanged",
    "FrameIndexChanged",
    "FrameLayerChanged",
    "FrameLinkSettingsChanged",
    "FrameMoved",
    "FrameMoveFinished",
    "FrameMoveInProgress",
    "FrameMoveRegionChanged",
    "FrameOpacityChanged",
    "FrameRotated",
    "FrameTagChanged",
    "FrameTextVariablesChanged",
    "FrameWrapChanged",
    "GridSettingsChanged",
    "HelpLayerVisibilityChanged",
    "HiddenCharacterVisibilityChanged",
    "IconSetDownloaded",
    "IconSetDownloadStarted",
    "IconSetFullDownloaded",
    "Idle",
    "ImageFrameVariableDefinitionChanged",
    "InlineFrameAdded",
    "InlineFrameDeleted",
    "InlinePanelVisibilityChanged",
    "InlineToolBarVisibilityChanged",
    "LanguageCacheRequiresClearing",
    "LanguageDownloaded",
    "LanguageDownloadStarted",
    "LayerAdded",
    "LayerAlternateLayoutSelectionChanged",
    "LayerContentTypeChanged",
    "LayerDeleted",
    "LayerIndexChanged",
    "LayerMoveRegionChanged",
    "LayerNameChanged",
    "LayerOpacityChanged",
    "LayerPrintingChanged",
    "LayerTextWrapChanged",
    "LayerVisibilityChanged",
    "LayerVisibleVariableChanged",
    "LinkSettingsChanged",
    "LinkSettingsVariableChanged",
    "MarginVisibilityChanged",
    "MeasurementUnitsChanged",
    "MoveRegionVisibilityChanged",
    "ObjectReplaceRequested",
    "PageAdded",
    "PageAlternateLayoutSelectionChanged",
    "PageBackgroundColorChanged",
    "PageBookMarkSettingsChanged",
    "PageCanvasScrollChanged",
    "PageControlsLoaded",
    "PageDeleted",
    "PageIncludeInLayoutChanged",
    "PageIncludeInOutputChanged",
    "PageIndexChanged",
    "PageLabelVisibilityChanged",
    "PageMarginsChanged",
    "PageModeChanged",
    "PageRulerVisibilityChanged",
    "PageSectionChanged",
    "PageVisibilityModeChanged",
    "PageVisibleVariableChanged",
    "PaginationEditorVisibilityChanged",
    "PanelAdded",
    "PanelControlAdded",
    "PanelControlDeleted",
    "PanelControlIndexChanged",
    "PanelControlTitleChanged",
    "PanelControlViewPreferencesChanged",
    "PanelControlVisibilityChanged",
    "PanelDeleted",
    "PanelIndexChanged",
    "PanelTitleChanged",
    "PanelViewPreferencesChanged",
    "ParagraphStyleAdded",
    "ParagraphStyleBackgroundDefinitionChanged",
    "ParagraphStyleColorChanged",
    "ParagraphStyleDefinitionChanged",
    "ParagraphStyleDeleted",
    "ParagraphStyleDiagonalRuleDefinitionChanged",
    "ParagraphStyleIndexChanged",
    "ParagraphStyleNameChanged",
    "ParagraphStyleRuleDefinitionChanged",
    "PathAdded",
    "PathChanged",
    "PathCursorChanged",
    "PathDeleted",
    "PathPointAdded",
    "PathPointDeleted",
    "PathPointMoved",
    "PDFSettingsChanged",
    "PluginAdded",
    "PluginDeleted",
    "PluginIndexChanged",
    "PluginNameChanged",
    "PreflightPreferencesChanged",
    "PreflightResultAdded",
    "PreflightResultDeleted",
    "PreflightResultDescriptionChanged",
    "RotateViewChanged",
    "SearchCompleted",
    "SearchResultAdded",
    "SearchResultDeleted",
    "SearchResultsSettingsChanged",
    "SearchResultsStatusChanged",
    "SearchStarted",
    "SectionBreakVisibilityChanged",
    "SectionVisibleVariableChanged",
    "SelectedAlternateLayoutChanged",
    "SelectedAnnotationChanged",
    "SelectedBookMarkChanged",
    "SelectedCharacterStyleChanged",
    "SelectedColorChanged",
    "SelectedDataSourceChanged",
    "SelectedDocumentEventActionChanged",
    "SelectedFontChanged",
    "SelectedFrameChanged",
    "SelectedImportWarningChanged",
    "SelectedLayerChanged",
    "SelectedPageChanged",
    "SelectedPageMasterChanged",
    "SelectedPanelChanged",
    "SelectedParagraphStyleChanged",
    "SelectedParagraphStyleListSettingsChanged",
    "SelectedPathChanged",
    "SelectedPathPointChanged",
    "SelectedPluginChanged",
    "SelectedSearchResultChanged",
    "SelectedTableCellChanged",
    "SelectedTableColumnChanged",
    "SelectedTableRowChanged",
    "SelectedVariableChanged",
    "SelectionContentChanged",
    "SlugVisibilityChanged",
    "SnapSettingsChanged",
    "SnippetVariablesChanged",
    "SourceViewChanged",
    "SpellCheckDictionariesLoaded",
    "SpellCheckHunSpellFilesLoaded",
    "SpellCheckPreferencesChanged",
    "StoryFrameChanged",
    "StoryViewPreferencesChanged",
    "TableBorderAndFillChanged",
    "TableCellBorderAndFillChanged",
    "TableCellDefinitionChanged",
    "TableColumnAdded",
    "TableColumnBorderAndFillChanged",
    "TableColumnDefinitionChanged",
    "TableColumnDeleted",
    "TableColumnIndexChanged",
    "TableFrameBorderAndFillChanged",
    "TableRowAdded",
    "TableRowBorderAndFillChanged",
    "TableRowDefinitionChanged",
    "TableRowDeleted",
    "TableRowIndexChanged",
    "TabRulerControlVisibilityChanged",
    "TabRulerMarkerSelectionChanged",
    "TabRulerVisibilityChanged",
    "TextColumnGuideVisibilityChanged",
    "TextFormatChanged",
    "TextFrameCopyFittingAppliedPercentageChanged",
    "TextFrameCustomBaseLineGridChanged",
    "TextFrameFlowComposed",
    "TextFrameLinksUpdated",
    "TextFramePathSettingChanged",
    "TextFrameReadOnlyFlowComposed",
    "TextFrameStrokeChanged",
    "TextSelectionChanged",
    "ToolBarItemAdded",
    "ToolBarItemDeleted",
    "ToolBarItemIndexChanged",
    "ToolBarItemViewPreferencesChanged",
    "UndoStackChanged",
    "VariableAdded",
    "VariableDefinitionChanged",
    "VariableDeleted",
    "VariableIndexChanged",
    "VariableNameChanged",
    "VariableRowAdded",
    "VariableRowDeleted",
    "VariableRowIndexChanged",
    "VariableTextDirectionChanged",
    "VariableValueChanged",
    "VariableVisibilityChanged",
    "ViewModeChanged",
    "WarningVisibilityChanged",
    "WorkSpaceChanged",
    "WorkSpaceColorChanged",
    "WorkSpaceInfoChanged",
    "WrapMarginVisibilityChanged",
    "ZoomAfterChange",
    "ZoomBeforeChange",
    "ZoomChanged",
    "ZoomSettingsChanged"
  ];

  ngOnInit() {}
}
