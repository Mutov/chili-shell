import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChiliShellComponent } from './chili-shell.component';

describe('ChiliShellComponent', () => {
  let component: ChiliShellComponent;
  let fixture: ComponentFixture<ChiliShellComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChiliShellComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChiliShellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
