import { Component, OnInit } from '@angular/core';
import {ChiliShellService} from '../chili-shell/services/chili-shell.service';
import {FontService} from '../services/font.service';

@Component({
  selector: 'app-fonts-dropdown',
  templateUrl: './fonts-dropdown.component.html',
  styleUrls: ['./fonts-dropdown.component.scss']
})
export class FontsDropdownComponent implements OnInit {
  public options: any[] = [];
  public selectedOption: any;

  constructor(
    public chiliShellService: ChiliShellService,
    public fontService: FontService,
  ) {
    this.chiliShellService.shellReady.subscribe(ready => {
      if (ready) {
        this.options = this.fontService.getRegularFonts();
      }
    });
  }

  ngOnInit() {
    if (this.fontService.currentFont) {
      this.selectedOption = this.fontService.currentFont.name;
    } else {
      this.selectedOption = 'Select Font';
    }
  }

  public handleOptionSelect(option): void {
    this.selectedOption = option.name;
    this.fontService.setCurrentFont(option);
  }
}
