import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FontsDropdownComponent } from './fonts-dropdown.component';

describe('FontsDropdownComponent', () => {
  let component: FontsDropdownComponent;
  let fixture: ComponentFixture<FontsDropdownComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FontsDropdownComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FontsDropdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
