import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

import { FormsModule } from '@angular/forms';
import { ChiliShellModule } from './chili-shell/chili-shell.module';
import { ColorPickerModule } from 'ngx-color-picker';
import { ColorPickerComponent } from './color-picker/color-picker.component';
import { FontsDropdownComponent } from './fonts-dropdown/fonts-dropdown.component';
import { DropdownComponent } from './dropdown/dropdown.component';
import { TextFrameToolsComponent } from './menu-components/text-frame-tools/text-frame-tools.component';
import { ImageFrameToolsComponent } from './menu-components/image-frame-tools/image-frame-tools.component';
import { ShapeFrameToolsComponent } from './menu-components/shape-frame-tools/shape-frame-tools.component';

@NgModule({
  declarations: [
    AppComponent,
    ColorPickerComponent,
    FontsDropdownComponent,
    DropdownComponent,
    TextFrameToolsComponent,
    ImageFrameToolsComponent,
    ShapeFrameToolsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ChiliShellModule,
    ColorPickerModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
