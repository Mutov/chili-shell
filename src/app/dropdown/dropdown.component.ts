import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.scss']
})
export class DropdownComponent implements OnInit {
  @Input() options: any[] = [];
  @Input() selectedOption: any;
  @Output() optionSelected = new EventEmitter<any>();
  public dropdownIsOpen: boolean = false;

  constructor() { }

  ngOnInit() {
  }

  toggleDropdown() {
    this.dropdownIsOpen = !this.dropdownIsOpen;
  }

  public onDropdownOptionSelect(option): void {
    this.dropdownIsOpen = false;
    this.optionSelected.emit(option);
  }

}
