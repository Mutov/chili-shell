var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from "@angular/core";
import { ChiliShellService } from "./chili-shell/services/chili-shell.service";
var AppComponent = /** @class */ (function () {
    //<------- Frames manipulation END ------->
    function AppComponent(chiliShellService) {
        var _this = this;
        this.chiliShellService = chiliShellService;
        this.shapePopupStatus = false;
        this.dimentions = {
            width: '0mm',
            height: '0mm'
        };
        // this.chiliShellService.shellReady.subscribe(ready => console.log('Chili ready', ready));
        this.chiliShellService.shellReady.subscribe(function (ready) {
            console.log("Chili ready", ready);
            if (!!ready) {
                _this.initIframeManipulations();
            }
        });
    }
    AppComponent.prototype.initIframeManipulations = function () {
        // this.editor = this.chiliShellService.shell.editor;
        this.editor = this.chiliShellService.getEditor();
        this.dimentions.width = this.editor.GetObject("document").width;
        this.dimentions.height = this.editor.GetObject("document").height;
        this.eManipulation = this.editor.GetObject("document");
        this.editor.ExecuteFunction("document.editor", "Fit", "page");
        this.zoomVal = this.editor.GetObject("document").zoom;
        console.log('zoom 1');
        if (!!this.editor && !!this.eManipulation) {
            this.editorStatus = true;
            // this.editor.SetProperty("document", "zoom", "100");
            this.zoomVal = this.editor.GetObject("document").zoom;
            // console.log('this.editor.GetObject("document").zoom;', this.editor.GetObject("document").zoom);
            console.log('zoom 2');
            this.getZoom();
        }
    };
    AppComponent.prototype.getZoom = function () {
        console.log('callback started');
        setTimeout(function () {
            this.zoomVal = this.editor.GetObject("document").zoom;
        }.bind(this), 2000);
    };
    // Frame adding section START
    AppComponent.prototype.addFrame = function (type, meta) {
        this.chiliShellService.addFrame(type, meta);
        this.closeShapePopup();
    };
    // addShapeFrame popup manipulations START
    AppComponent.prototype.swithcShapePopup = function () {
        if (this.shapePopupStatus) {
            this.shapePopupStatus = false;
        }
        else {
            this.shapePopupStatus = true;
        }
    };
    AppComponent.prototype.closeShapePopup = function () {
        this.shapePopupStatus = false;
    };
    // addShapeFrame popup manipulations END
    // Frame adding section END
    // Common editor manipulations START
    AppComponent.prototype.getSelectedFrame = function () {
        if (this.chiliShellService.shell.getSelectedFrame()) {
            console.log("selectedFrame", this.chiliShellService.shell.getSelectedFrame());
            return this.chiliShellService.shell.getSelectedFrame();
        }
        else {
            console.log("frame is not selected");
            return null;
        }
    };
    AppComponent.prototype.getSelectedFrameType = function () {
        if (!!this.editor && !!this.editor.GetObject("document.selectedFrame")) {
            console.log("Frame type", this.editor.GetObject("document.selectedFrame").type);
            return this.editor.GetObject("document.selectedFrame").type;
        }
        else {
            console.log("frame is not selected");
            return null;
        }
    };
    AppComponent.prototype.getSelectedText = function () {
        var selectedText = this.editor.GetObject("document.selectedText");
        console.log("selectedText", selectedText);
    };
    AppComponent.prototype.changeSelectedText = function () {
        var frame = "document.selectedText";
        var propName = "fontSize";
        var propValue = 24;
        this.editor.SetProperty(frame, propName, propValue);
    };
    AppComponent.prototype.selectedTextAlign = function (type) {
        var status = false;
        try {
            if (!!this.editor.GetObject("document.selectedText")) {
                this.editor.SetProperty("document.selectedText.textFormat", "textAlign", type);
                status = true;
            }
        }
        catch (err) {
            console.log('error');
            //TODO add to logger handler
            console.log('error in selected text align');
        }
        finally {
            if (!status) {
                console.log('error in selected text align');
            }
        }
    };
    AppComponent.prototype.allTextInFrameAlign = function (type) {
        if (!!this.getSelectedFrame()) {
            this.editor.ExecuteFunction("document.selectedFrame.textFlow.composer.selection", "SelectAll");
            this.editor.ExecuteFunction("document", "SetCursor", "Text");
            this.editor.SetProperty("document.selectedText.textFormat", "textAlign", type);
            this.editor.ExecuteFunction("document", "SetCursor", "pointer");
        }
        if (!!this.editor.GetObject("document.selectedText")) {
            this.editor.ExecuteFunction("document", "SetCursor", "pointer");
            this.editor.ExecuteFunction("document.selectedFrame.textFlow.composer.selection", "SelectAll");
            this.editor.ExecuteFunction("document", "SetCursor", "Text");
            this.editor.SetProperty("document.selectedText.textFormat", "textAlign", type);
            this.editor.ExecuteFunction("document", "SetCursor", "pointer");
        }
    };
    AppComponent.prototype.changeTextSize = function (size) {
        var status = false;
        try {
            if (!!this.editor.GetObject("document.selectedText")) {
                this.editor.SetProperty("document.selectedText.textFormat", "fontSize", size);
                status = true;
            }
            if (!status && !!this.getSelectedFrame()) {
                this.editor.ExecuteFunction("document.selectedFrame.textFlow.composer.selection", "SelectAll");
                this.editor.ExecuteFunction("document", "SetCursor", "Text");
                this.editor.SetProperty("document.selectedText.textFormat", "fontSize", size);
                this.editor.ExecuteFunction("document", "SetCursor", "pointer");
                status = true;
            }
        }
        catch (err) {
            console.log('error');
            //TODO add to logger handler
            console.log('error in changeTextSize');
        }
        finally {
            if (!status) {
                console.log('error in changeTextSize');
            }
        }
    };
    AppComponent.prototype.changeCursor = function (type) {
        this.editor.ExecuteFunction("document", "SetCursor", type);
        //types are: "hand", "pointer", "text" etc
    };
    AppComponent.prototype.changeTextType = function (type) {
        var status = false;
        console.log('changeTextType', this.editor.GetObject("document.selectedText.textFormat"));
        if (type === 'underline') {
            //check if its underline, change to false
            var underlined = this.editor.GetObject('document.selectedText.textFormat').underLine;
            if (underlined === 'false') {
                console.log('set true');
                this.editor.SetProperty("document.selectedText.textFormat", "underLine", true);
            }
            else {
                console.log('set false');
                this.editor.SetProperty("document.selectedText.textFormat", "underLine", false);
            }
        }
        else if (type === "bold") {
            var bold = this.editor.GetObject('document.selectedText.textFormat').textOverprint;
            if (bold === 'false') {
                console.log('set true');
                this.editor.SetProperty("document.selectedText.textFormat", "textOverprint", true);
            }
            else {
                console.log('set false');
                this.editor.SetProperty("document.selectedText.textFormat", "textOverprint", false);
            }
        }
        // try {
        //   if (!!this.editor.GetObject("document.selectedText")) {
        //     this.editor.SetProperty(
        //       "document.selectedText.textFormat",
        //       "textAlign",
        //       type
        //     );
        //     status = true;
        //   }
        // } catch (err) {
        //   console.log('error');
        //   //TODO add to logger handler
        //   console.log('error in selected text align');
        // } finally {
        //   if (!status) {
        //     console.log('error in selected text align');
        //   }
        // }
    };
    AppComponent.prototype.changeTextColor = function (color) {
        // let setColor = "cp_object:" + this.editor.GetObject('document.colors[nameORID].javaScriptDescriptor');
        console.log('COLORS: ', this.editor.GetObject('document.colors["red"].javaScriptDescriptor'));
        this.editor.SetProperty("document.selectedText.textFormat", "color", this.editor.GetObject('document.colors["red"].javaScriptDescriptor'));
        // editorObject.SetProperty('document.selectedText.textFormat', 'color', "cp_object:" + editorObject.GetObject('document.colors[red].javaScriptDescriptor'));
    };
    AppComponent.prototype.generateColor = function (colorName) {
        return "cp_object:" + this.editor.GetObject('document.colors[colorName].javaScriptDescriptor');
    };
    AppComponent.prototype.changeSelectedTextFont = function (fontName) {
        this.editor.SetProperty("document.selectedText.textFormat", "color", this.editor.GetObject('document.colors["red"].javaScriptDescriptor'));
    };
    AppComponent.prototype.ClearFrame = function () {
        var frameForClear = this.editor.GetObject("document.selectedFrame");
        if (frameForClear != null && frameForClear != undefined) {
            if (frameForClear.type == "image") {
                this.editor.ExecuteFunction("document.selectedFrame", "ClearContent");
            }
        }
    };
    AppComponent.prototype.deleteFrame = function () {
        this.editor.ExecuteFunction("document.selectedFrame", "Delete");
    };
    AppComponent.prototype.copyFrame = function () {
        this.editor.ExecuteFunction("document.selectedFrames", "Copy");
    };
    AppComponent.prototype.pasteFrame = function () {
        this.editor.ExecuteFunction("document", "PasteFrames");
    };
    AppComponent.prototype.undo = function () {
        this.editor.ExecuteFunction("document.undoManager", "Undo", 1);
    };
    AppComponent.prototype.redo = function () {
        this.editor.ExecuteFunction("document.undoManager", "Redo", 1);
    };
    AppComponent.prototype.zoomHandle = function (direction) {
        if (direction == "up") {
            this.zoomVal = (parseInt(this.zoomVal) + 1).toString();
            this.chiliShellService.shell.editor.SetProperty("document", "zoom", this.zoomVal);
        }
        else if (direction == "down") {
            if (this.zoomVal <= 1) {
                console.log("error");
            }
            else {
                this.zoomVal = (parseInt(this.zoomVal) - 1).toString();
                this.chiliShellService.shell.editor.SetProperty("document", "zoom", this.zoomVal);
            }
        }
        else if (direction === "100%") {
            this.zoomVal = "100";
            this.chiliShellService.shell.editor.SetProperty("document", "zoom", this.zoomVal);
        }
    };
    AppComponent.prototype.fitPage = function () {
        if (!!this.editor) {
            this.editor.ExecuteFunction("document.editor", "Fit", "page");
            this.zoomVal = this.editor.GetObject("document").zoom;
        }
    };
    AppComponent.prototype.getPageInfo = function () {
        var info = this.editor.GetObject("document");
        console.log("info", info);
        var width = info.width;
        var height = info.height;
        console.log("width", width, "height", height);
    };
    // Common editor manipulations END
    AppComponent.prototype.changeBorderWidth = function (width) {
        var frame = "document.selectedFrame";
        var propName = "borderWidth";
        var propValue = "20";
        this.editor.SetProperty(frame, propName, width);
    };
    AppComponent.prototype.changeBorderColor = function () {
        var frame = "document.selectedFrame";
        var propName = "borderColor";
        var propValue = "#36a026";
        this.editor.SetProperty(frame, propName, propValue);
    };
    AppComponent.prototype.alignAllText = function () {
        // this.editor.ExecuteFunction('document.selectedFrame');
        if (!!this.editor.GetObject("document.selectedFrame")) {
            console.group("Frame Details");
            console.log("frame", this.editor.GetObject("document.selectedFrame"));
            // console.log('baseLineOptions', this.editor.GetObject("document.selectedFrame.baseLineOptions"));
            console.log("borderColor", this.editor.GetObject("document.selectedFrame.borderColor"));
            console.log("borderCornerRadius", this.editor.GetObject("document.selectedFrame.borderCornerRadius"));
            console.log("borderWidth", this.editor.GetObject("document.selectedFrame.borderWidth"));
            // console.log('controller', this.editor.GetObject("document.selectedFrame.controller"));
            console.log("hasBorder", this.editor.GetObject("document.selectedFrame.hasBorder"));
            // console.log('layer', this.editor.GetObject("document.selectedFrame.layer"));
            console.log("name", this.editor.GetObject("document.selectedFrame.name"));
            console.log("opacity", this.editor.GetObject("document.selectedFrame.opacity"));
            console.log("rotation", this.editor.GetObject("document.selectedFrame.rotation"));
            console.log("textStrokeColor", this.editor.GetObject("document.selectedFrame.textStrokeColor"));
            console.log("translatedName", this.editor.GetObject("document.selectedFrame.translatedName"));
            console.log("verticalAlign", this.editor.GetObject("document.selectedFrame.verticalAlign"));
            console.log("width", this.editor.GetObject("document.selectedFrame.width"));
            console.log("height", this.editor.GetObject("document.selectedFrame.height"));
            console.log("x", this.editor.GetObject("document.selectedFrame.x"));
            console.log("y", this.editor.GetObject("document.selectedFrame.y"));
            // console.log('alternateLayoutProperties', this.editor.GetObject("document.selectedFrame.alternateLayoutProperties"));
            console.log("fillColor", this.editor.GetObject("document.selectedFrame.fillColor"));
            console.log("frameConstraints", this.editor.GetObject("document.selectedFrame.frameConstraints"));
            console.log("frameContentConstraints", this.editor.GetObject("document.selectedFrame.frameContentConstraints"));
            // console.log('privateData', this.editor.GetObject('document.selectedFrame.privateData'));
            // console.log('javaScriptDescriptor', this.editor.GetObject("document.selectedFrame.javaScriptDescriptor"));
            // index
            console.log("index", this.editor.GetObject("document.selectedFrame.index"));
            console.groupEnd();
        }
    };
    //editor.ExecuteFunction("document","CreateTempPDF_FromXml",pdfSettingsXML,"_blank")
    AppComponent.prototype.ngOnInit = function () {
        //doInit
    };
    AppComponent = __decorate([
        Component({
            selector: "app-root",
            templateUrl: "./app.component.html",
            styleUrls: ["./app.component.scss"]
        }),
        __metadata("design:paramtypes", [ChiliShellService])
    ], AppComponent);
    return AppComponent;
}());
export { AppComponent };
//# sourceMappingURL=app.component.js.map