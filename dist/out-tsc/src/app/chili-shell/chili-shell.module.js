var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChiliShellComponent } from './components/chili-shell/chili-shell.component';
import { ChiliShellService } from './services/chili-shell.service';
var ChiliShellModule = /** @class */ (function () {
    function ChiliShellModule() {
    }
    ChiliShellModule = __decorate([
        NgModule({
            imports: [
                CommonModule
            ],
            declarations: [ChiliShellComponent],
            exports: [ChiliShellComponent],
            providers: [ChiliShellService],
        })
    ], ChiliShellModule);
    return ChiliShellModule;
}());
export { ChiliShellModule };
//# sourceMappingURL=chili-shell.module.js.map