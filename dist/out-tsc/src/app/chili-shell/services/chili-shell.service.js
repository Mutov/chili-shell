var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable, NgZone } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
var ChiliShellService = /** @class */ (function () {
    function ChiliShellService(_zone) {
        this._zone = _zone;
        this.shellReady = new BehaviorSubject(false);
        this._events = new Subject();
    }
    ChiliShellService.prototype.events$ = function () {
        return this._events.asObservable();
    };
    ChiliShellService.prototype.startEventListening = function () {
        var _this = this;
        this._zone.run(function () {
            window["OnEditorEvent"] = _this.OnEditorEvent.bind(_this);
        });
    };
    ChiliShellService.prototype.OnEditorEvent = function (type, targetID, data) {
        this._events.next({ type: type, targetID: targetID, data: data });
    };
    ChiliShellService.prototype.getEditor = function () {
        return this.shell.editor;
    };
    ChiliShellService.prototype.addFrame = function (frameType, metaData) {
        this.prepareAddFrame(frameType, metaData);
    };
    ChiliShellService.prototype.prepareAddFrame = function (frameType, metaData) {
        var frameForAdd;
        var img = metaData;
        if (frameType === 'text') {
            frameForAdd = 'text';
        }
        else if (frameType === 'image') {
            frameForAdd = 'image';
        }
        else if (frameType === 'rectangle') {
            frameForAdd = 'rectangle';
        }
        else if (frameType === 'circle') {
            frameForAdd = 'circle';
        }
        else if (frameType === 'line') {
            frameForAdd = 'line';
        }
        else {
            console.log('wrong frame type');
            return;
        }
        this.executeAddFrame(frameForAdd, metaData);
        if (frameType === 'text') {
            this.shell.editor.ExecuteFunction("document.selectedFrame", "ImportTextFlow", '<TextFlow><p fontFamily="Arial"><span>Enter your text here!</span></p></TextFlow>', true);
        }
    };
    ChiliShellService.prototype.executeAddFrame = function (type, metaData) {
        if (!!metaData) {
            //todo add image
            this.shell.editor.ExecuteFunction("document.pages[1].frames", "Add", type, "10mm", "50mm", "100mm", "70mm");
            return;
        }
        this.shell.editor.ExecuteFunction("document.pages[1].frames", "Add", type, "10mm", "50mm", "100mm", "70mm");
    };
    ChiliShellService = __decorate([
        Injectable({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [NgZone])
    ], ChiliShellService);
    return ChiliShellService;
}());
export { ChiliShellService };
//# sourceMappingURL=chili-shell.service.js.map