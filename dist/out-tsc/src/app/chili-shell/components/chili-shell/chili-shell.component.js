var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewChild, ElementRef, NgZone } from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";
import { ChiliShellService } from "../../services/chili-shell.service";
var ChiliShellComponent = /** @class */ (function () {
    function ChiliShellComponent(sanitizer, chiliShellService, ngZone) {
        this.chiliShellService = chiliShellService;
        this.ngZone = ngZone;
        this.eventList = [
            'AdTypeOverlayViewerVisibilityChanged',
            'AdTypeOverlayVisibilityChanged',
            'AlternateLayoutAdded',
            'AlternateLayoutDefinitionChanged',
            'AlternateLayoutDeleted',
            'AlternateLayoutIndexChanged',
            'AlternateLayoutNameChanged',
            'AlternateLayoutSelectionChanged',
            'AnnotationAdded',
            'AnnotationDefinitionChanged',
            'AnnotationDeleted',
            'AnnotationMoved',
            'AutoTextCursorChanged',
            'BarcodeFrameVariableChanged',
            'BaseLineGridOptionsChanged',
            'BaseLineGridVisibilityChanged',
            'BleedVisibilityChanged',
            'BookMarkAdded',
            'BookMarkDefinitionChanged',
            'BookMarkDeleted',
            'BookMarkIndexChanged',
            'BookMarkLinkSettingsChanged',
            'BookMarkNameChanged',
            'BookViewNavigationChanged',
            'BookViewPreferencesChanged',
            'BorderVisibilityChanged',
            'ChainVisibilityChanged',
            'CharacterStyleAdded',
            'CharacterStyleColorChanged',
            'CharacterStyleDefinitionChanged',
            'CharacterStyleDeleted',
            'CharacterStyleIndexChanged',
            'CharacterStyleNameChanged',
            'CharacterStyleUnderLineChanged',
            'ColorAdded',
            'ColorDefinitionChanged',
            'ColorDeleted',
            'ColorIndexChanged',
            'ColorNameChanged',
            'CopyFrameChanged',
            'CursorChanged',
            'CustomButtonAdded',
            'CustomButtonDefinitionChanged',
            'CustomButtonDeleted',
            'CustomButtonIndexChanged',
            'DataSourceChanged',
            'DataSourceRecordChanged',
            'DisplayQualityChanged',
            'DocumentActionChanged',
            'DocumentAdSettingsChanged',
            'DocumentAnnotationSettingsChanged',
            'DocumentBleedChanged',
            'DocumentClosing',
            'DocumentConstraintsChanged',
            'DocumentDefaultDirectoriesChanged',
            'DocumentDimensionsChanged',
            'DocumentDirtyStateChanged',
            'DocumentEventActionAdded',
            'DocumentEventActionDefinitionChanged',
            'DocumentEventActionDeleted',
            'DocumentEventActionIndexChanged',
            'DocumentEventActionNameChanged',
            'DocumentFlapChanged',
            'DocumentFullyLoaded',
            'DocumentLinkSettingsChanged',
            'DocumentLoaded',
            'DocumentLoading',
            'DocumentSaved',
            'DocumentSaveRequested',
            'DocumentSerialized',
            'DocumentSlugChanged',
            'DocumentSpineChanged',
            'FontAdded',
            'FontDeleted',
            'FontIndexChanged',
            'FontLoaded',
            'FontNameChanged',
            'FrameAdded',
            'FrameAdNotesChanged',
            'FrameAdSettingsChanged',
            'FrameAlternateLayoutSelectionChanged',
            'FrameAnchorChanged',
            'FrameBarcodeSettingsChanged',
            'FrameBorderChanged',
            'FrameChainChanged',
            'FrameConstraintsChanged',
            'FrameDeleted',
            'FrameDisplayQualityChanged',
            'FrameDropShadowChanged',
            'FrameFillChanged',
            'FrameHandlePreferencesChanged',
            'FrameIndexChanged',
            'FrameLayerChanged',
            'FrameLinkSettingsChanged',
            'FrameMoved',
            'FrameMoveFinished',
            'FrameMoveInProgress',
            'FrameMoveRegionChanged',
            'FrameOpacityChanged',
            'FrameRotated',
            'FrameTagChanged',
            'FrameTextVariablesChanged',
            'FrameWrapChanged',
            'GridSettingsChanged',
            'HelpLayerVisibilityChanged',
            'HiddenCharacterVisibilityChanged',
            'IconSetDownloaded',
            'IconSetDownloadStarted',
            'IconSetFullDownloaded',
            'Idle',
            'ImageFrameVariableDefinitionChanged',
            'InlineFrameAdded',
            'InlineFrameDeleted',
            'InlinePanelVisibilityChanged',
            'InlineToolBarVisibilityChanged',
            'LanguageCacheRequiresClearing',
            'LanguageDownloaded',
            'LanguageDownloadStarted',
            'LayerAdded',
            'LayerAlternateLayoutSelectionChanged',
            'LayerContentTypeChanged',
            'LayerDeleted',
            'LayerIndexChanged',
            'LayerMoveRegionChanged',
            'LayerNameChanged',
            'LayerOpacityChanged',
            'LayerPrintingChanged',
            'LayerTextWrapChanged',
            'LayerVisibilityChanged',
            'LayerVisibleVariableChanged',
            'LinkSettingsChanged',
            'LinkSettingsVariableChanged',
            'MarginVisibilityChanged',
            'MeasurementUnitsChanged',
            'MoveRegionVisibilityChanged',
            'ObjectReplaceRequested',
            'PageAdded',
            'PageAlternateLayoutSelectionChanged',
            'PageBackgroundColorChanged',
            'PageBookMarkSettingsChanged',
            'PageCanvasScrollChanged',
            'PageControlsLoaded',
            'PageDeleted',
            'PageIncludeInLayoutChanged',
            'PageIncludeInOutputChanged',
            'PageIndexChanged',
            'PageLabelVisibilityChanged',
            'PageMarginsChanged',
            'PageModeChanged',
            'PageRulerVisibilityChanged',
            'PageSectionChanged',
            'PageVisibilityModeChanged',
            'PageVisibleVariableChanged',
            'PaginationEditorVisibilityChanged',
            'PanelAdded',
            'PanelControlAdded',
            'PanelControlDeleted',
            'PanelControlIndexChanged',
            'PanelControlTitleChanged',
            'PanelControlViewPreferencesChanged',
            'PanelControlVisibilityChanged',
            'PanelDeleted',
            'PanelIndexChanged',
            'PanelTitleChanged',
            'PanelViewPreferencesChanged',
            'ParagraphStyleAdded',
            'ParagraphStyleBackgroundDefinitionChanged',
            'ParagraphStyleColorChanged',
            'ParagraphStyleDefinitionChanged',
            'ParagraphStyleDeleted',
            'ParagraphStyleDiagonalRuleDefinitionChanged',
            'ParagraphStyleIndexChanged',
            'ParagraphStyleNameChanged',
            'ParagraphStyleRuleDefinitionChanged',
            'PathAdded',
            'PathChanged',
            'PathCursorChanged',
            'PathDeleted',
            'PathPointAdded',
            'PathPointDeleted',
            'PathPointMoved',
            'PDFSettingsChanged',
            'PluginAdded',
            'PluginDeleted',
            'PluginIndexChanged',
            'PluginNameChanged',
            'PreflightPreferencesChanged',
            'PreflightResultAdded',
            'PreflightResultDeleted',
            'PreflightResultDescriptionChanged',
            'RotateViewChanged',
            'SearchCompleted',
            'SearchResultAdded',
            'SearchResultDeleted',
            'SearchResultsSettingsChanged',
            'SearchResultsStatusChanged',
            'SearchStarted',
            'SectionBreakVisibilityChanged',
            'SectionVisibleVariableChanged',
            'SelectedAlternateLayoutChanged',
            'SelectedAnnotationChanged',
            'SelectedBookMarkChanged',
            'SelectedCharacterStyleChanged',
            'SelectedColorChanged',
            'SelectedDataSourceChanged',
            'SelectedDocumentEventActionChanged',
            'SelectedFontChanged',
            'SelectedFrameChanged',
            'SelectedImportWarningChanged',
            'SelectedLayerChanged',
            'SelectedPageChanged',
            'SelectedPageMasterChanged',
            'SelectedPanelChanged',
            'SelectedParagraphStyleChanged',
            'SelectedParagraphStyleListSettingsChanged',
            'SelectedPathChanged',
            'SelectedPathPointChanged',
            'SelectedPluginChanged',
            'SelectedSearchResultChanged',
            'SelectedTableCellChanged',
            'SelectedTableColumnChanged',
            'SelectedTableRowChanged',
            'SelectedVariableChanged',
            'SelectionContentChanged',
            'SlugVisibilityChanged',
            'SnapSettingsChanged',
            'SnippetVariablesChanged',
            'SourceViewChanged',
            'SpellCheckDictionariesLoaded',
            'SpellCheckHunSpellFilesLoaded',
            'SpellCheckPreferencesChanged',
            'StoryFrameChanged',
            'StoryViewPreferencesChanged',
            'TableBorderAndFillChanged',
            'TableCellBorderAndFillChanged',
            'TableCellDefinitionChanged',
            'TableColumnAdded',
            'TableColumnBorderAndFillChanged',
            'TableColumnDefinitionChanged',
            'TableColumnDeleted',
            'TableColumnIndexChanged',
            'TableFrameBorderAndFillChanged',
            'TableRowAdded',
            'TableRowBorderAndFillChanged',
            'TableRowDefinitionChanged',
            'TableRowDeleted',
            'TableRowIndexChanged',
            'TabRulerControlVisibilityChanged',
            'TabRulerMarkerSelectionChanged',
            'TabRulerVisibilityChanged',
            'TextColumnGuideVisibilityChanged',
            'TextFormatChanged',
            'TextFrameCopyFittingAppliedPercentageChanged',
            'TextFrameCustomBaseLineGridChanged',
            'TextFrameFlowComposed',
            'TextFrameLinksUpdated',
            'TextFramePathSettingChanged',
            'TextFrameReadOnlyFlowComposed',
            'TextFrameStrokeChanged',
            'TextSelectionChanged',
            'ToolBarItemAdded',
            'ToolBarItemDeleted',
            'ToolBarItemIndexChanged',
            'ToolBarItemViewPreferencesChanged',
            'UndoStackChanged',
            'VariableAdded',
            'VariableDefinitionChanged',
            'VariableDeleted',
            'VariableIndexChanged',
            'VariableNameChanged',
            'VariableRowAdded',
            'VariableRowDeleted',
            'VariableRowIndexChanged',
            'VariableTextDirectionChanged',
            'VariableValueChanged',
            'VariableVisibilityChanged',
            'ViewModeChanged',
            'WarningVisibilityChanged',
            'WorkSpaceChanged',
            'WorkSpaceColorChanged',
            'WorkSpaceInfoChanged',
            'WrapMarginVisibilityChanged',
            'ZoomAfterChange',
            'ZoomBeforeChange',
            'ZoomChanged',
            'ZoomSettingsChanged'
        ];
        this.iframeUrl = sanitizer.bypassSecurityTrustResourceUrl("http://jackprint.loc/CHILI/3motion_DEV/editor_html.aspx?doc=8c86f0fb-8899-4576-a14c-15cb2236081b&apiKey=02INtr2_hmrfJUWcALtFlFt6+FBEecdfDFHGxpE91S8c+bIFy5aBYL6VRek86oAm5EZoYyIZEEDXOennH3j9Tw==");
        this.chiliShellService.shell = this;
        this.chiliShellService.startEventListening();
        this.chiliShellService.events$().subscribe(this.HandleEvent.bind(this));
    }
    ChiliShellComponent.prototype.iFrameLoaded = function () {
        try {
            this.frameWindow = this.chili.nativeElement.contentWindow;
            this.frameWindow.GetEditor(this.EditorLoaded.bind(this));
        }
        catch (error) {
            console.log('error', error);
            //handle error
            return false;
        }
    };
    //Create editor object from loaded iFrame
    ChiliShellComponent.prototype.EditorLoaded = function (jsInterface) {
        var _this = this;
        this.editor = this.frameWindow.editorObject;
        this.editor.AddListener("CursorChanged");
        this.editor.AddListener("SelectedFrameChanged");
        this.editor.AddListener("FrameAdded");
        this.eventList.forEach(function (element) {
            _this.editor.AddListener(element);
        });
    };
    ChiliShellComponent.prototype.HandleEvent = function (event) {
        var _this = this;
        this.ngZone.run(function () {
            switch (event.type) {
                case "DocumentFullyLoaded":
                    //TODO
                    _this.editorManipulation = _this.editor.GetObject("document");
                    _this.chiliShellService.shellReady.next(true);
                    break;
                case "SelectedFrameChanged":
                    //TODO
                    break;
                case "CursorChanged":
                    //TODO
                    break;
                case "EditorLoaded":
                    //TODO
                    break;
                case "FrameAdded":
                    //TODO
                    break;
                case "SelectedFrameChanged":
                    //TODO
                    console.log('SelectedFrameChanged');
                    break;
                default:
                    // console.log("unregistred event", event);
                    break;
            }
        });
    };
    ChiliShellComponent.prototype.renewEditor = function () {
        var obj = this.frameWindow.editorObject.GetObject("document");
        return obj;
    };
    ChiliShellComponent.prototype.getSelectedFrame = function () {
        var frame = this.frameWindow.editorObject.GetObject("document.selectedFrame");
        if (!!frame) {
            console.log("frame", frame);
        }
        else {
            console.log("null frame", this.frameWindow.editorObject.GetObject("document"));
        }
        return frame;
    };
    ChiliShellComponent.prototype.getEditorManipulator = function () {
        if (this.editor) {
            return this.editor.GetObject("document");
        }
        return null;
    };
    ChiliShellComponent.prototype.ngOnInit = function () { };
    __decorate([
        ViewChild("chili")
        //getting html element iFrame
        ,
        __metadata("design:type", ElementRef)
    ], ChiliShellComponent.prototype, "chili", void 0);
    ChiliShellComponent = __decorate([
        Component({
            selector: "app-chili-shell",
            templateUrl: "./chili-shell.component.html",
            styleUrls: ["./chili-shell.component.scss"]
        }),
        __metadata("design:paramtypes", [DomSanitizer,
            ChiliShellService,
            NgZone])
    ], ChiliShellComponent);
    return ChiliShellComponent;
}());
export { ChiliShellComponent };
//# sourceMappingURL=chili-shell.component.js.map